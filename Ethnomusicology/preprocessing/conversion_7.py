from cmath import nan
import csv
from dict2xml import dict2xml
import requests


class Dictlist(dict):
	# to allow duplicate records
	def __setitem__(self, key, value):
		try:
			self[key]
		except KeyError:
			super(Dictlist, self).__setitem__(key, [])
		self[key].append(value)

def main(
	input_path='../data/input_csv/1_Catalogue_7_Artifacts.csv',
	output_path='../data/preprocessed_xml/Catalogue_7_Artifacts.xml',
	geonames_path='../data/input_csv/GeonamesCodes.csv',
	places_path='../data/preprocessed_xml/places.xml',
	features_path='../data/preprocessed_xml/feature_codes.txt',
	sample=False):

	# Change this line to false to run on full data
	sample = False

	if sample:
		input_path = '../data/input_csv/1_catalogue_7_artifacts_sampleRows.csv'
		output_path = '../data/preprocessed_xml/sample_Catalogue_7_Artifacts.xml'

	file = open(input_path, encoding='utf-8')
	csv_reader = csv.reader(file)

	N = 3       # change this value to edit number of lines to be skipped in csv
	for i in range(N):
		next(csv_reader)

	# get header row and convert to lowercase
	rows = list(csv_reader)
	headers = [header.lower() for header in rows[0]]
	# Edit column names

	headers  = ['','artifact_id','mkey / m_id','id_number','category_name','category_id',
				'item_name','item_id','item_name','item_id','title_name','title_id','culture_name',
				'culture_id','culture_name','culture_id','maker_name','maker_id','maker_group_name',
				'maker_group_id','place_name','place_id','place_name','place_id','place_name','place_id',
				'place_name','place_id','date_made','date_made_begin','date_made_end',
				'materials','material_name','material_id','material_name','material_id','material_name',
				'material_id','material_name','material_id','material_name','material_id','material_name',
				'material_id','material_name','material_id','measurements','length','width','height',
				'description','credit_line','credit_line individual','credit_line - reconciled', 'credit_line group','credit_line group - reconciled', 'item_count','parent_key','broader_text','value','id','note',
				'instrument','additional notes','label','registered collection']

	# delete header column
	rows.pop(0)

	# specify columns to skip
	skip_col = ['mkey / m_id','category_name','culture_name','material_name','item_name','registered collection','place_name','place_id']

	#define xml
	xml = ''
	sub_xml = ''
	place_xml = ''
	sub_items = Dictlist()
	broader_text = {}
	artifacts = []

	'''The for loop iterates through the rows in the sheet, whereas the while loop iterates through each cell in the row'''
	for row in rows:

		# dict to hold all the details of each catalogue artifact
		artifact = {}

		# dict to hold each detail present in a catalogue artifact

		items = Dictlist() # this definition allows duplicate values to stored in the dict

		# define subcategory dicts
		maker_grp = {}
		maker = {}
		prod = Dictlist()
		part = {}
		place = {}

		i = 1
		while i < len(row):

			# check if cell value is null
			if row[i]:

				'''The following statements check if the current column name is part of a nested category, if yes - it then copys
				the cell's value into the nested category dict we defined above'''


				#                                      **** ADD nested categories here ****

				# Add production event URIs
				if headers[i] == 'id_number':
					prod['production_lincs'] = 'http://temp.lincsproject.ca/production_uri/' + str(row[i].strip())

				# maker
				if headers[i] == 'maker_id' or headers[i] == 'maker_name':
					# add cell value with column name into dict
					maker[headers[i]] = row[i].strip()

				# maker_group
				elif headers[i] == 'maker_group_id' or headers[i] == 'maker_group_name':
					# add cell value with column name into dict
					maker_grp[headers[i]] = row[i].strip()

				# place
				elif headers[i] == 'place_name' or headers[i] == 'place_id' or headers[i] == 'date_made' or headers[i] == 'date_made_begin' or headers[i] == 'date_made_end':

					if headers[i] == 'place_name' or headers[i] == 'place_id':
						if headers[i] == 'place_name':
							place['place_name'] = row[i].strip()
						elif headers[i] == 'place_id':
							place['place_id'] = row[i] = 'https://sws.geonames.org/{}'.format(row[i].strip().split('/')[3])
							prod['place'] = place
							place = {}
					elif headers[i] == 'date_made' or headers[i] == 'date_made_begin' or headers[i] == 'date_made_end':
						prod[headers[i]] = row[i].strip()

				# whole_part
				elif headers[i] == 'value' or headers[i] == 'id':
					part[headers[i]] = row[i].strip()

				# add https:// to artifact id's
				elif headers[i] == 'artifact_id':
					items[str(headers[i])] = 'https://' + str(row[i].strip())

				else:
					items[str(headers[i])] = str(row[i].strip())
			i += 1

			'''The following statements check if the current column name is part of a nested category, if yes - it then copys
			the cell's value into the nested category dict we defined above'''
		# xml for sections
		#                                      **** ADD subcategories here ****

		# subcategory template example
		#if 'column name1' in items.keys() and 'column name2' in items.keys():
		#    sub_items['column name1'] = {'column name1':items['column name1'][0],'column name2':items['column name2'][0]}

		# category
		if 'category_name' in items.keys() and 'category_id' in items.keys():
			sub_items['category'] = {'category_name':items['category_name'][0].strip(),'category_id':items['category_id'][0]}

		# material
		if 'material_name' in items.keys() and 'material_id' in items.keys():
			for i in range(len(items['material_id'])):
				sub_items['material'] = {'material_name':items['material_name'][i].strip(),'material_id':items['material_id'][i]}

		# item
		if 'item_name' in items.keys() and 'item_id' in items.keys():
			for i in range(len(items['item_id'])):
				temp = {'item_name':items['item_name'][i].strip(),'item_id':items['item_id'][i]}
				try:
					temp['category_id'] = items['category_id'][i]
				except:
					pass
				sub_items['item'] = temp

		# title
		if 'title_name' in items.keys() and 'title_id' in items.keys():
			sub_items['title'] = {'title_name':items['title_name'][0].strip(),'title_id':items['title_id'][0]}

		# culture
		if 'culture_name' in items.keys() and 'culture_id' in items.keys():
			for i in range(len(items['culture_id'])):
				sub_items['culture'] = {'culture_name':items['culture_name'][i].strip(),'culture_id':items['culture_id'][i]}

		# maker
		if 'maker_name' in items.keys() and 'maker_id' in items.keys():
			sub_items['maker_individual'] = {'maker_name':items['maker_name'][0].strip(),'maker_id':items['maker_id'][0]}

		# maker_group
		if 'maker_group_name' in items.keys() and 'maker_group_id' in items.keys():
			sub_items['maker_group'] = {'maker_name':items['maker_group_name'][0].strip(),'maker_id':items['maker_group_id'][0]}

		# add nested categories to artifact dict
		if maker:
			items['maker'] = maker
		if maker_grp:
			items['maker_group'] = maker_grp
		if prod:
			items['production'] = prod
		if part:
			items['whole_part'] = part

		# skip columns
		for col in skip_col:
			items.pop(col, None)

		# add id_number and URI for broader text lookup
		broader_text[items['id_number'][0]] = items['artifact_id'][0]

		# add items to artifact dict
		artifact['catalogue_artifact'] = items

		#define artifact xml
		artifacts.append(artifact)
	# add broader text id
	all_artifacts = Dictlist()
	for artifact in artifacts:
		if 'broader_text' in artifact['catalogue_artifact'].keys():
			key = artifact['catalogue_artifact']['broader_text'][0]
			if key in broader_text.keys():
				artifact['catalogue_artifact']['broader_text_id'] = broader_text[key]
		# convert to xml
		all_artifacts['catalogue_artifact'] = artifact['catalogue_artifact']

	xml += dict2xml({'artifacts':{'catalogue_artifact':all_artifacts['catalogue_artifact']}})

	# write to file
	with open(output_path, 'w', encoding='utf-8') as f:
		f.write("<?xml version='1.0' encoding='UTF-8'?>")
		f.write("<catalogue_all>")
		f.write(xml)

	# dedeuplicate sub categories
	final_dict = {}
	for key in sub_items.keys():
		result_list = []
		item_list = sub_items[key]
		for i in range(len(item_list)):
			if item_list[i] not in item_list[i + 1:]:
				result_list.append(item_list[i])
		final_dict[key] = result_list

	# enclose subcategories in tags
	sub_xml+= (dict2xml({'categories':{'category':final_dict['category']}})) + '\n'
	sub_xml+= (dict2xml({'cultures':{'culture':final_dict['culture']}})) + '\n'
	sub_xml+= (dict2xml({'items':{'item':final_dict['item']}})) + '\n'
	sub_xml+= (dict2xml({'materials':{'material':final_dict['material']}})) + '\n'
	try:
		sub_xml+= (dict2xml({'maker':{'maker_individual':final_dict['maker_individual'],'maker_group':final_dict['maker_group']}})) + '\n'
	except:
		pass
	sub_xml+= (dict2xml({'titles':{'title':final_dict['title']}}))

	with open(output_path, 'a', encoding='utf-8') as f:
		f.write('\n')
		f.write(sub_xml)
		f.write("</catalogue_all>")

	# create place xml
	places = []
	feature_codes = []
	for artifact in artifacts:
		if 'production' in artifact['catalogue_artifact'].keys():
			for item in artifact['catalogue_artifact']['production']:
				if 'place' in item.keys():
					places.append(item['place'])

	for locations in places: # location is a list of dict
		#print(locations)
		for i in range(len(locations)):
			locations[i]['place_column_source'] = 'place_made'+str(i+1)
			if i !=0:
				locations[i]['is_part_of'] = locations[i-1]['place_id']
			if i < len(locations)-1:
				locations[i]['has_part'] = locations[i+1]['place_id']

			# fetch geonames info
			link = locations[i]['place_id']+'/about.rdf'
			req = requests.get(link)

			# name
			start = req.text.find("<gn:name>") + len("<gn:name>")
			end = req.text.find("</gn:name>")
			locations[i]['place_column_geonames'] = req.text[start:end].strip()

			# feature code and class

			text = req.content.decode('utf-8')
			fc = ""
			for line in text.split('\n'):
				if 'featureCode' in line:
					fc = line.strip('<gn:featureCode rdf:resource="https://www.geonames.org/ontology#').strip('"/>')

			locations[i]['feature_code'] = fc
			feature_codes.append(fc)
			locations[i]['feature_class'] = fc.split('.')[0]

			# feature code name
			file = open(geonames_path, encoding='utf-8')
			csv_reader = csv.reader(file)
			names = list(csv_reader)
			for name in names:
				if fc in name:
					locations[i]['feature_code_name'] = name[1]

			# feature class
			place_xml += dict2xml({'place':locations[i]}) +'\n'

			with open(places_path, 'w', encoding='utf-8') as f:
				f.write(place_xml)

			# output feature code list
			feature_codes = list(dict.fromkeys(feature_codes))
			with open(features_path, 'w', encoding='utf-8') as f:
				for code in feature_codes:
					f.write(code + '\n')

if __name__ == "__main__":
	main()
