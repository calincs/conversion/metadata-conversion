# Ethnomusicology - Technical Documentation

## Conversion_2.py

This python script extracts all of the data from the 2_Publications tab of this [spreadsheet](https://docs.google.com/spreadsheets/d/1iGkG7FJmmYjIRWUopup8Dvu8mvRzQwoxAi51het54ks/edit#gid=515617503)

## Conversion_3.py

This python script extracts all of the data from the 3_People_Artifacts tab of this [spreadsheet](https://docs.google.com/spreadsheets/d/1iGkG7FJmmYjIRWUopup8Dvu8mvRzQwoxAi51het54ks/edit#gid=515617503)

## Conversion_4.py

This python script extracts all of the data from the 4_People tab of this [spreadsheet](https://docs.google.com/spreadsheets/d/1iGkG7FJmmYjIRWUopup8Dvu8mvRzQwoxAi51het54ks/edit#gid=515617503)

## Conversion_6.py

This python script extracts all of the data from the 1_Catalogue_6_Recordings tab of this [spreadsheet](https://docs.google.com/spreadsheets/d/1iGkG7FJmmYjIRWUopup8Dvu8mvRzQwoxAi51het54ks/edit#gid=515617503)

## Conversion_7.py

This python script extracts all of the data from the 1_Catalogue_7_Artifacts tab of this [spreadsheet](https://docs.google.com/spreadsheets/d/1iGkG7FJmmYjIRWUopup8Dvu8mvRzQwoxAi51het54ks/edit#gid=515617503). This script includes queries to geonames to get place types.


## Use the following steps to modify the scripts and extract data from a different tab:

*   Download the tab in CSV format and save in the 'input_csv' folder in the repo.
*   Change variable N to skip first N lines from the csv sheet.
*   The column names are stored in the 'headers' list. They can be changed as needed.
*   Columns to be skipped in the extraction can be added to the 'skip_col' list.
*   Any additional categories can be defined followng the marked examples.
*   Similarly, any additional sub category can be defined too.
*   Refer to comments in each script for additional details.

# Logic:
Iterates through each row and stores the values from each cell in a dict which represents a root element. These dicts are added to a larger dict and then converted to xml using the dict2xml library.