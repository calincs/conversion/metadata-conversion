from cmath import nan
import csv
from dict2xml import dict2xml

class Dictlist(dict):
    # to allow duplicate records
    def __setitem__(self, key, value):
        try:
            self[key]
        except KeyError:
            super(Dictlist, self).__setitem__(key, [])
        self[key].append(value)

def main(input_path='../data/input_csv/2_Publications.csv', output_path='../data/preprocessed_xml/Publications.xml'):
    # reading csv file to pandas dataframe
    file = open(input_path, encoding='utf-8')
    csv_reader = csv.reader(file)

    N = 4       # change this value to edit number of lines to be skipped in csv
    for i in range(N):
        next(csv_reader)

    # get header row and convert to lowercase
    rows = list(csv_reader)
    headers = [header.lower() for header in rows[0]]

    # edit column names
    # headers = ['','pkey','authlinkkey','artifact_id','mkey','link_id','type','relationship','relationship_id','note','note_id','option1','option1_id','record_type','record_type_id']

    # delete header column 
    rows.pop(0)

    # specify columns to skip
    skip_col = ['pkey','cce_p_pages_tracks']

    #define xml
    xml = ''
    all_publications = Dictlist()
    '''The for loop iterates through the rows in the sheet, whereas the while loop iterates through each cell in the row'''
    for row in rows:

        # dict to hold all the details of each publication
        publication = {}

        # dict to hold each detail present in a publication

        items = Dictlist() # this definition allows duplicate values to stored in the dict

        i = 1
        while i < len(row):

            # check if cell value is null
            if row[i]:
                # add https:// to publication id's
                if headers[i] == 'search.museums.ualberta.ca link':
                    items[str(headers[i])] = 'https://'+str(row[i].strip())
                else:
                    items[str(headers[i])] = str(row[i])
            i +=1

        # skip columns
        for col in skip_col:
            items.pop(col,None)

        # add items to publication dict
        all_publications['publication'] = items

    #define xml
    xml += dict2xml({'publications':{'publication':all_publications['publication']}})

    # write to file
    with open(output_path, 'w', encoding='utf-8') as f:
        f.write("<?xml version='1.0' encoding='UTF-8'?>")
        f.write('\n'+xml)

if __name__ == "__main__":
    main()