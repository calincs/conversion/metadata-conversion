from cmath import nan
import csv
from re import sub
from dict2xml import dict2xml
from requests import head

class Dictlist(dict):
	# to allow duplicate records
	def __setitem__(self, key, value):
		try:
			self[key]
		except KeyError:
			super(Dictlist, self).__setitem__(key, [])
		self[key].append(value)

def main(input_path='../data/input_csv/1_Catalogue_6_Recordings.csv', output_path='../data/preprocessed_xml/Catalogue_6_Recordings.xml'):
	# reading csv file to pandas dataframe
	file = open(input_path, encoding='utf-8')
	csv_reader = csv.reader(file)

	N = 3       # change this value to edit number of lines to be skipped in csv
	for i in range(N):
		next(csv_reader)

	# get header row and convert to lowercase
	rows = list(csv_reader)
	headers = [header.lower() for header in rows[0]]

	# Add/Edit columns
	headers = ['','artifact_id','mkey / m_id','id_number','category1','category1_id','category2',
				'category2_id','all_items','item_name','item_id','item_name','item_id','item_name','item_id',
				'item_name','item_id','item_name','item_id','item_name','item_id','item_name','item_id',
				'item_name','item_id','title_name','maker_all','maker_name','maker_id','maker_name','maker_id','maker_name','maker_id',
				'maker_name','maker_id','maker_name','maker_id','maker_name','maker_id','maker_name','maker_id','maker_name','maker_id','maker_name','maker_id','maker_name','maker_id',
				'maker_name','maker_id','maker_name','maker_id','maker_name','maker_id','maker_name','maker_id',
				'maker_group_name','maker_group_id','maker_group_name','maker_group_id','maker_group_name','maker_group_id','maker_group_name','maker_group_id','maker_group_name','maker_group_id','maker_group_name','maker_group_id',
				'maker_unknown_name','maker_unknown_id','maker_unknown_name','maker_unknown_id','maker_unknown_name','maker_unknown_id','maker_unknown_name','maker_unknown_id','maker_unknown_name','maker_unknown_id','maker_unknown_name','maker_unknown_id',
				'places','place_made','place_id','place_made','place_id','place_made','place_id','place_made','place_id','place_made','place_id',
				'place_made','place_id','place_made','place_id','place_made','place_id','all_cultures','culture_name','culture_id','culture_name','culture_id',
				'culture_name','culture_id','culture_name','culture_id','culture_name','culture_id','culture_name','culture_id','culture_name','culture_id','date_made',
				'date_made_begin','date_made_end','materials','measurements (index/track no.)','description','credit_line','credit_line individual','credit_line - reconciled','credit_line group','credit_line group - reconciled','item_count','parent_key','broader_text',
				'whole_part','whole_part - reconciled','note','instrument','instrument_id','instrument','instrument_id','instrument','instrument_id',
				'instrument','instrument_id','instrument','instrument_id','instrument','instrument_id','instrument','instrument_id','instrument','instrument_id',
				'instrument','instrument_id','instrument','instrument_id','instrument','instrument_id','instrument','instrument_id','instrument','instrument_id',
				'instrument','instrument_id','instrument','instrument_id','instrument','instrument_id','instrument','instrument_id','instrument','instrument_id',
				'instrument','instrument_id','instrument','instrument_id','additional notes','label','label - reconciled','registered collection']

	# delete header column
	rows.pop(0)

	# specify columns to skip
	skip_col = ['mkey / m_id','all_items','places','all_cultures','maker_all']

	# define xml
	xml = ''
	broader_text = {}
	records = []

	'''The for loop iterates through the rows in the sheet, whereas the while loop iterates through each cell in the row'''

	for row in rows:

		# dict to hold all the details of each catalogue recording
		record = {}

		# dict to hold each detail present in a catalogue recording

		items = Dictlist()  # this definition allows duplicate values to stored in the dict

		# define nested category dicts for every nested category you choose to make
		maker = Dictlist()
		maker_group = Dictlist()
		maker_unknown = Dictlist()
		item = Dictlist()
		culture = Dictlist()
		instrument = Dictlist()
		prod = Dictlist()
		maker_dict = {}
		maker_group_dict = {}
		maker_unknown_dict = {}
		place = {}
		item_dict = {}
		culture_dict = {}
		instrument_dict = {}

		i = 1
		while i < len(row):

			# check if cell value is null
			if row[i]:

				'''The following statements check if the current column name is part of a nested category, if yes - it then copys
				the cell's value into the nested category dict we defined above'''

				#                                      **** ADD nested categories here ****

				# Add production event URIs
				if headers[i] == 'id_number':
					prod['production_lincs'] = 'http://temp.lincsproject.ca/production_uri/' + str(row[i].strip()).split('-', 1)[0]

				#maker
				if headers[i] == 'maker_name' or headers[i] == 'maker_id':
					if headers[i] == 'maker_name':
						maker_dict['maker_name'] = row[i].strip()
					elif headers[i] == 'maker_id':
						maker_dict['maker_id'] = row[i].strip()
						maker['maker'] = maker_dict
						maker_dict = {}

				#maker group
				elif headers[i] == 'maker_group_name' or headers[i] == 'maker_group_id':
					if headers[i] == 'maker_group_name':
						maker_group_dict['maker_group_name'] = row[i].strip()
					elif headers[i] == 'maker_group_id':
						maker_group_dict['maker_group_id'] = row[i].strip()
						maker_group['maker_group'] = maker_group_dict
						maker_group_dict = {}

				#maker unknown
				elif headers[i] == 'maker_unknown_name' or headers[i] == 'maker_unknown_id':
					if headers[i] == 'maker_unknown_name':
						maker_unknown_dict['maker_unknown_name'] = row[i].strip()
					elif headers[i] == 'maker_unknown_id':
						maker_unknown_dict['maker_unknown_id'] = row[i].strip()
						maker_unknown['maker_unknown'] = maker_unknown_dict
						maker_unknown_dict = {}

				# item
				elif headers[i] == 'item_name' or headers[i] == 'item_id':
					# add cell value with column name into dict
					if headers[i] == 'item_name':
						item_dict['item_name'] = row[i].strip()
					elif headers[i] == 'item_id':
						item_dict['item_id'] = row[i].strip()
						item['item'] = item_dict
						item_dict = {}

				# culture
				elif headers[i] == 'culture_name' or headers[i] == 'culture_id':
					# add cell value with column name into dict
					if headers[i] == 'culture_name':
						culture_dict['culture_name'] = row[i].strip()
					elif headers[i] == 'culture_id':
						culture_dict['culture_id'] = row[i].strip()
						culture['culture'] = culture_dict
						culture_dict = {}

				# instrument
				elif headers[i] == 'instrument' or headers[i] == 'instrument_id':
					# add cell value with column name into dict
					if headers[i] == 'instrument':
						instrument_dict['instrument'] = row[i].strip()
					elif headers[i] == 'instrument_id':
						instrument_dict['instrument_id'] = row[i].strip()
						instrument['instrument'] = instrument_dict
						instrument_dict = {}

				# place
				elif headers[i] == 'place_made' or headers[i] == 'place_id' or headers[i] == 'date_made' or headers[i] == 'date_made_begin' or headers[i] == 'date_made_end':

					if headers[i] == 'place_made' or headers[i] == 'place_id':
						if headers[i] == 'place_made':
							place['place_made'] = row[i].strip()
						elif headers[i] == 'place_id':
							place['place_id'] = row[i]
							prod['place'] = place
							place = {}
					elif headers[i] == 'date_made' or headers[i] == 'date_made_begin' or headers[i] == 'date_made_end':
						prod[headers[i]] = row[i].strip()

				# add https:// to artifact id's
				elif headers[i] == 'artifact_id':
					items[str(headers[i])] = 'https://' + str(row[i].strip())

				else:
					items[str(headers[i])] = str(row[i].strip())

			i += 1

		# add nested categories to record dict
		if maker:
			items['makers'] = maker
		if maker_group:
			items['makers_group'] = maker_group
		if maker_unknown:
			items['makers_unknown'] = maker_unknown
		if prod:
			items['production'] = prod
		if item:
			items['items'] = item
		if culture:
			items['cultures'] = culture
		if instrument:
			items['instruments'] = instrument
		# skip columns
		for col in skip_col:
			items.pop(col, None)

		# add id_number and URI for broader text lookup
		if items['id_number'][0] not in broader_text.keys():
			broader_text[items['id_number'][0]] = items['artifact_id'][0]

		# add items to record dict
		record['catalogue_recording'] = items
		records.append(record)

	all_recordings = Dictlist()
	for record in records:

		# add broader text id
		if 'broader_text' in record['catalogue_recording'].keys():
			key = record['catalogue_recording']['broader_text'][0]
			if key in broader_text.keys():
				record['catalogue_recording']['broader_text_id'] = broader_text[key]
		# define recording xml
		all_recordings['catalogue_recording'] = record['catalogue_recording']
		#xml += dict2xml(record) + '\n'

	xml += dict2xml({'catalogue_recordings':{'catalogue_recording':all_recordings['catalogue_recording']}})

	# write to file
	with open(output_path, 'w', encoding='utf-8') as f:
		f.write("<?xml version='1.0' encoding='UTF-8'?>")
		f.write('\n' + xml)


if __name__ == "__main__":
	main()
