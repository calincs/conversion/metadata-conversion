from cmath import nan
import csv
from typing import Dict
from dict2xml import dict2xml

class Dictlist(dict):
    # to allow duplicate records
    def __setitem__(self, key, value):
        try:
            self[key]
        except KeyError:
            super(Dictlist, self).__setitem__(key, [])
        self[key].append(value)

def main(input_path='../data/input_csv/4_People.csv', output_path='../data/preprocessed_xml/People_4.xml', sample=False):
    # Change this line to false to run on full data
    sample = False

    if sample:
        input_path = '../data/input_csv/4_People_sampleRows.csv'
        output_path = '../data/preprocessed_xml/sample_4_People.xml'

    file = open(input_path, encoding='utf-8')
    csv_reader = csv.reader(file)


    N = 3       # change this value to edit number of lines to be skipped in csv

    for i in range(N):
        next(csv_reader)
                                                                    
    # get header row and convert to lowercase
    rows = list(csv_reader)
    headers = [header.lower() for header in rows[0]]

    # edit column names
    
    headers = ['', 'link_id', 'person_id', 'individual', 'preferred_name', 'title_name', 'firstmid_name',
      'lastsuff_name', 'gender', 'brief_bio','brief_bio(date)', 'birth_date', 'birth_date_begin', 'birth_date_end', 'birth_place',
      'birth_place1', 'birth_place1_id', 'birth_place2', 'birth_place2_id',
      'death_date', 'death_date_begin', 'death_date_end', 'death_place', 'death_place1',
      'death_place1_id', 'death_place2', 'death_place2_id', 'nationality',
      'nationality - reconciled', 'occupations', 'occupation1', 'occupation1_id1','occupation1_id2','occupation1_id3',
      'occupation2', 'occupation2_id', 'occupation3', 'occupation3_id','occupation4', 'occupation4_id',
      'description', 'appraiser', 'authorizer', 'borrower', 'collector', 'conservator', 'copyright_holder',
      'examiner', 'identifier', 'insurer', 'lender', 'maker', 'mover', 'owner', 'publisher', 'related',
      'reproducer', 'shipper', 'source', 'sponsor', 'staff', 'surveyor', 'venue', 'other', 'musical styles',
      'musical_style1','musical style1_id','musical_style2','musical style2_id','musical_style3','musical style3_id', 'instrument', 'instrument1', 'instrument1_id',
      'instrument2', 'instrument2_id', 'instrument3', 'instrument3_id',
      'instrument4', 'instrument4_id', 'instrument5', 'instrument5_id']

    # delete header column 
    rows.pop(0)

    # specify columns to skip
    skip_col = ['musical styles','occupations','instrument','birth_place','death_place','appraiser','authorizer','borrower','collector','conservator','copyright_holder','examiner',
    'identifier','insurer','lender','maker','mover','owner','publisher','related','reproducer','shipper',
    'source','sponsor','staff','surveyor','venue','other','brief_bio(date)']

    #define xml
    xml = ''
    all_people = Dictlist()
    '''The for loop iterates through the rows in the sheet, whereas the while loop iterates through each cell in the row'''
    for row in rows:

        # dict to hold all the details of each people artifact
        artifact = {}

        # dict to hold each detail present in a people artifact
        
        items = Dictlist() # this definition allows duplicate values to stored in the dict

        i = 1
        while i < len(row):
            
            # check if cell value is null
            if row[i]:
                items[str(headers[i])] = str(row[i])
            i +=1

        #print(len(row))
        # skip columns
        for col in skip_col:
            items.pop(col,None)

        # add items to artifact dict
        all_people['person'] = items

    #define xml
    xml += dict2xml({'peoples':{'people':all_people['person']}})

    # write to file
    with open(output_path,'w',encoding='utf-8') as f:
        f.write("<?xml version='1.0' encoding='UTF-8'?>")
        f.write('\n'+xml)

if __name__ == "__main__":
    main()