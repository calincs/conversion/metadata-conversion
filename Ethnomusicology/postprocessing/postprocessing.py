import requests
import re
import zipfile
import io
import os

from rdflib import Graph


def replacement(input_file_path, post_process_url, replacements_file, regex, output_file_path):
	# calling post processing api to replace wikidata links

	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	# remove trailing viaf slashes
	original_matches = re.findall(r'(<http:\/\/viaf\.org\/viaf\/[0-9]+?/)', content)
	for orig_match in original_matches:
		content = content.replace(orig_match, orig_match[:-1])

	# remove trailing geonames slashes
	original_matches = re.findall(r'(<https:\/\/sws\.geonames\/.org\/[0-9]+?\/)', content)
	for orig_match in original_matches:
		content = content.replace(orig_match, orig_match[:-1])

	# remove .html from loc
	original_matches = re.findall(r'(<http://id\.loc\.gov[a-zA-Z0-9\/]+?\.html>)', content)
	for orig_match in original_matches:
		content = content.replace(orig_match, orig_match.replace(".html", ""))

	# replace spaces in temp lincs URIs with underscores
	# the temp URIs had several invalid characters
	print("normalizing temp lincs")
	original_matches = re.findall(r'<http:\/\/temp\.lincsproject\.ca\/(.+?>)', content)
	for orig_match in original_matches:
		rep_match = orig_match.strip()
		rep_match = rep_match.replace(" ", "_")
		rep_match = rep_match.replace('"', "_")
		rep_match = rep_match.replace('[', "_")
		rep_match = rep_match.replace(']', "_")
		rep_match = rep_match.replace("'", "_")
		rep_match = rep_match.replace("`", "_")
		rep_match = rep_match.replace(",", "_")
		rep_match = rep_match.replace("(", "_")
		rep_match = rep_match.replace(")", "_")
		rep_match = rep_match.replace(";", "_")
		content = content.replace(orig_match, rep_match)

	# there shouldn't be any uuid prefixes but in case there are, replace so file is valid
	content = content.replace("uuid:", "http://uuid.ca/")

	with open(output_file_path, 'w') as f:
		f.write(content)


def replacement_simple(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	with open(output_rdf_path, 'w') as f:
		f.write(content)


def external_labels(input_path, output_path, post_process_url, authority):
	if authority == "wikidata":
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs&lang_code=fr'
	else:
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs'
	payload = {}
	headers = {}
	files = [('file', ('test_labels.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	try:
		z = zipfile.ZipFile(io.BytesIO(response.content))
		z.extractall(output_path)
	except Exception as err:
		print(f"Output failed for external labels with {authority}\n", response.text)
		print(err)


def coordinates(input_path, output_path, post_process_url):
	url = post_process_url + f'/coordinates/geonames?labels=True'
	payload = {}
	headers = {}
	files = [('file', ('test_coordinates.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	with open(output_path, "w") as f_out:
		f_out.write(response.text)


def condense_ttl_graph(rootdir, output_file):

	# get a list of all .ttl files in the rootdir. This includes within nested folders
	filelist = []
	for subdir, dirs, files in os.walk(rootdir):
		for file in files:
			filepath = subdir + os.sep + file

			if filepath.endswith(".ttl"):
				filelist.append(filepath)

	# creates graph and outputs the new graph to replace the original file
	g = Graph()

	for ttl_path in filelist:
		print(ttl_path)
		g.parse(ttl_path)

	g.serialize(destination=output_file)


def replace_inverse_props(input_path, output_path, post_process_url):
	url = post_process_url + '/inverses/'
	files = {'file': ("inverses.ttl", open(input_path, 'rb'))}
	response = requests.request("POST", url, files=files)
	if "200" in str(response.status_code):
		with open(output_path, 'w') as f:
			f.write(response.text)
	else:
		print(f"ERROR: did not replace inverse properties in {input_path}\n{response}")


def validation(input_path_list, output_path, post_process_url, analyze="True"):
	files = []
	for file in input_path_list:
		filename = file.split("/")[-1]
		files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	payload = {
		'encoding': 'utf-8',
		'rdf_format': 'turtle',
		'analyze': analyze}
	headers = {}
	response = requests.post(post_process_url + '/validate/', headers=headers, files=files, data=payload)

	with open(output_path + "rdf_validation.json", "w") as out_file:
		out_file.write(response.text)


def main(
	post_process_url="https://postprocess.lincsproject.ca",
	input_path="../data/X3ML/",
	output_path="../data/postprocessing_output/",
	error_output_path="output/error_files/",
	input_files=[
		"Catalogue_7_Artifacts.ttl",
		"People_4.ttl",
		"People_Artifacts.ttl",
		"Catalogue_6_Recordings.ttl",
		"Publications.ttl",
		"Catalogue_6_Recordings_3m_fix.ttl"],
	combined_output_path='../../Datasets/ethnomusicology.ttl',
	replacement_mapping="entity_replacements_noRegex.tsv",
	mint_mapping="lincs_minting_replacements_noRegex.tsv",
	replace_basic=False,
	replace_minted=False,
	get_external_labels=False,
	get_coordinates=False):

	# This usually only needs to happen once so you can skip this step for future processing
	# if the entities did not change
	# also need to fix an error in artifacts getty label file (centimeters)


	#input_files=["People_Artifacts.ttl"]

	for file in input_files:
		print("Processing file: ", file)
		input_file_path = input_path + file
		output_file_path = output_path + file

		# use the file with the replaced output for other functions
		if replace_basic:
			replacements_file = replacement_mapping
			replacement(input_file_path, post_process_url, replacements_file, "False", output_file_path)

		# add minted URIs
		if replace_minted:
			replacements_file = mint_mapping
			replacement_simple(post_process_url, output_file_path, replacements_file, "False", output_file_path)

		print("replacing inverse properties")
		replace_inverse_props(output_file_path, output_file_path, post_process_url)

		print(f"validating {file}")
		validation([output_file_path], error_output_path + file[:-4] + "_", post_process_url, analyze="False")

		# These should run on the output from replacements
		# TODO Need to do LOC labels in batches
		if get_external_labels:
			for authority in ["wikidata", "viaf", "getty", "lincs"]:
				external_labels(output_file_path, output_path + file[:-4], post_process_url, authority)

		if get_coordinates:
			if "recording" in file:
				coordinates(output_file_path, output_path + file[:-4] + "_geonames_coordinates.ttl", post_process_url)

	print("combining files")
	condense_ttl_graph(output_path, combined_output_path)

	print("validation")
	validation([combined_output_path], error_output_path, post_process_url)


if __name__ == "__main__":
	main()
