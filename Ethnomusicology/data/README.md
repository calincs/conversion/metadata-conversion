## input_csv

This folder contains csv files extracted from the data spreadsheets in Google Drive. See the preprocessing scripts for details.


## preprocessed_xml

This folder contains the output of the preprocessing scripts. These files are meant to be input for X3ML.

## X3ML

This folder contains the mapping files needed to run each X3ML conversion for this project and the output from running X3ML.

## postprocessing_output

This folder contains all final .ttl files of converted data for this project. They are combined and then saved in the main Datasets directory.