# 3M export data/backup for Ethnomusicology

Note that the .xml files of input data and the output .ttl files inside the folders named `Mappingxxx` are not up to date. But they are representative of the format expected for input and output.

## Note on original_output_ttl

This folder contains the original full output ttl files for all the mappings. The ttl files in the main folder are edited versions of those originals. They are also the most up-to-date version of the data. See [issue 154](https://gitlab.com/calincs/conversion/metadata-conversion/-/issues/154) for more details.

## 3M Mappings

All xml files mentioned in this section can be found in [Ethnomusicology/data/preprocessed_xml/](https://gitlab.com/calincs/conversion/metadata-conversion/-/tree/master/Ethnomusicology/data/preprocessed_xml).

- Mapping189
	+ This is the most recent mapping for the `Ethnomusicology_Catalogue_7_Artifacts` data as of July 25, 2022. 
	+ For sample input, use `sample_Catalogue_7_Artifacts.xml` which is a sampling of 13 entities from Catalogue_7_Artifacts.xml.
	+ For full input, use `Catalogue_7_Artifacts.xml`.
- Mapping182
	+ This is the most recent mapping for the `Ethnomusicology_4_People` data as of July 18, 2022. 
	+ For sample input, use `sample_4_People.xml` which is a sampling of 6 entities from 4_People.xml. (Note: Sample input and output is out of date)
	+ For full input, use `People_4.xml`.
- Mapping188
	+ This is the most recent mapping for the `Ethnomusicology_Catalogue_6_Recordings` as of August 3, 2022. 
	+ For full input, use `Catalogue_6_Recordings.xml`.
	+ NOTE: DATE_MADE_BEGIN and DATE_MADE_END are missing from the mapping as they require data cleaning/entry work by researchers.
	+ NOTE: No sample input available as sample input is out of date.
- Mapping177
	+ This is the most recent mapping for the `Ethnomusicology_3_People_Artifacts` data as of July 19, 2022.
	+ For full input, use `People_Artifacts.xml`.
- Mapping186
	+ This is the most recent mapping for the `Ethnomusicology_2_Publications` data as of July 21, 2022.
	+ For full input, use `Publications.xml`	
- Ethnomusicology_Recordings_Fix_v1
	+ This mapping is to be used in conjunction with Mapping188. The output of the two mappings can be combined to make the full recordings conversion.
	+ This mapping fixed the F31 > P125 relationship in `Ethnomusicology_Catalogue_6_Recordings`. This was done on October 20, 2022.
	+ For full input, use `Catalogue_6_Recordings.xml`.


## 3M Outputs

### artifacts
- artifacts_3m_output_sample.ttl
	+ Most recent output file for Ethnomusicology_Catalogue_7_Artifacts data, using Mapping189 with sample_Catalogue_7_Artifacts.xml as input.
- artifacts_3m_output_full.ttl
	+ Most recent output file for Ethnomusicology_Catalogue_7_Artifacts data, using Mapping189 with Catalogue_7_Artifacts.xml as input.

### people
- people_3m_output_sample.ttl
	+ Most recent output file for Ethnomusicology_4_People data, using Mapping182 with sample_4_People.xml as input
- people_3m_output_full.ttl
	+ Most recent output file for Ethnomusicology_4_People data, using Mapping182 with People_4.xml as input

### people-artifacts
- people_artifacts_3m_output_sample.ttl
	+ Most recent output file for Ethnomusicology_3_People_Artifacts data, using Mapping177 with sample file as input
- people_artifacts_3m_output_full.ttl
	+ Most recent output file for Ethnomusicology_3_People_Artifacts data, using Mapping177 with People_Artifacts.xml as input

### recordings
- recordings_3m_output_sample.ttl
	+ Most recent sample output file for Ethnomusicology_Catalogue_6_Recordings data, using Mapping188 with sample_Catalogue_6_Recordings.xml as input
- recordings_3m_output_full.ttl
	+ Most recent output file for Ethnomusicology_Catalogue_6_Recordings data, using Mapping188 with Catalogue_6_Recordings.xml as input
- recordings_fix.ttl
	+ Most recent output file for mapping fix (add P125 to F31s) of Ethnomusicology_Catalogue_6_Recordings data
	
### publications
- publications_3m_output_full.ttl
	+ Most recent output file for Ethnomusicology_2_Publications data, using Mapping 186 with Publications.xml as input


## 3M Projects

- Mapping189 (Ethnomusicology_Catalogue_7_Artifacts_v3) - most up to date mapping for Artifacts
- Mapping173 (Ethnomusicology_Catalogue_7_Artifacts_v2)
- Mapping172 (Ethnomusicology_Catalogue_7_Artifacts_v1)
- Ethnomusicology_Recordings_Fix_v1 - A fix to Mapping 188. Both should be run to get the full output.
- Mapping188 (Ethnomusicology_1_Catalogue_6_Recordings_v2) - most up to date mapping for Recordings. To be used with Ethnomusicology_Recordings_Fix_v1.
- Mapping176 (Ethnomusicology_1_Catalogue_6_Recordings_v1) 
- Mapping177 (Ethnomusicology_3_People_Artifacts) - most up to date mapping for People_Artifacts 
- Mapping186 (Ethnomusicology_2_Publications_v2) - most up to date mapping for Publications 
- Mapping178 (Ethnomusicology_2_Publications_v1)
- Mapping182 (Ethnomusicology_4_People_v4) - most up to date mapping for People
- Mapping180 (Ethnomusicology_4_People_v3)
- Mapping179 (Ethnomusicology_4_People_v2)
- Mapping175 (Ethnomusicology_4_People_v1)