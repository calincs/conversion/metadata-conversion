from preprocessing import conversion_2, conversion_3, conversion_4, conversion_6, conversion_7
from postprocessing.postprocessing import main as postprocessing

import requests
import zipfile
import os
import io
import shutil


def preprocessing(publications=True, people_artifacts=True, people=True, artifacts=True, recordings=True):

	# Publications
	if publications:
		conversion_2.main(input_path="data/input_csv/2_Publications.csv", output_path="data/preprocessed_xml/Publications.xml")
		print("publications preprocessed")

	# People Artifacts
	if people_artifacts:
		conversion_3.main(input_path='data/input_csv/3_People_Artifacts.csv', output_path='data/preprocessed_xml/People_Artifacts.xml')
		print("people_artifacts preprocessed")

	# People
	if people:
		conversion_4.main(input_path='data/input_csv/4_People.csv', output_path='data/preprocessed_xml/People_4.xml', sample=False)
		print("people preprocessed")

	# Artifacts
	if artifacts:
		conversion_7.main(
			input_path='data/input_csv/1_Catalogue_7_Artifacts.csv',
			output_path='data/preprocessed_xml/Catalogue_7_Artifacts.xml',
			geonames_path='data/input_csv/GeonamesCodes.csv',
			places_path='data/preprocessed_xml/places.xml',
			features_path='data/preprocessed_xml/feature_codes.txt',
			sample=False)
		print("artifacts preprocessed")

	# Recordings
	if recordings:
		conversion_6.main(input_path='data/input_csv/1_Catalogue_6_Recordings.csv', output_path='data/preprocessed_xml/Catalogue_6_Recordings.xml')
		print("recordings preprocessed")


def process_3m(input_path, map_file, policy_file, output_folder, post_process_url, encoding="utf-8", uuid_test_size="2", output_format="text/turtle"):
	"""
	This function sends your data to X3ML.
	"""

	# Checking if input_file is a file or folder
	is_file = os.path.isfile(input_path)
	is_directory = os.path.isdir(input_path)

	if is_directory:
		files = []
		for filename in os.listdir(input_path):
			if filename.endswith(".xml"):
				files.append(os.path.join(input_path, filename))
	elif is_file:
		files = [input_path]
	else:
		print("Error: Please input a valid input file name or path")
		return

	# send each input file to X3ML engine
	for input_file in files:
		print(f"Converting {input_file}")
		output_filename = input_file.split("/")[-1].replace(".xml", ".ttl")

		url = post_process_url + f"/conversion/x3ml/custom?output_file={output_filename}&format_output_file={output_format}&uuid_test_size={uuid_test_size}"
		payload = {}
		files = [
			('input_file', ('input.xml', open(input_file, 'rb'), 'text/xml')),
			('map_file', ('Mapping.x3ml', open(map_file, 'rb'), 'application/octet-stream')),
			('policy_file', ('generator.xml', open(policy_file, 'rb'), 'text/xml'))]
		headers = {}
		response = requests.request("POST", url, headers=headers, data=payload, files=files)
		response.encoding = encoding

		if str(response.status_code).startswith("2"):
			print(f"3m complete conversion for:  {input_file}")
			try:
				z = zipfile.ZipFile(io.BytesIO(response.content))
				z.extractall(output_folder)
			except Exception as err:
				print(f"Failed to save X3ML conversion output\n", response.text)
				print(err)
		else:
			print(f"Error: Your file {input_file} could not be converted.")
			print(response.status_code)
			print(response.text)


def send_to_3m(post_process_url, x3ml_path, input_xml_path, publications=True, people_artifacts=True, people=True, artifacts=True, recordings=True):

	# Publications
	if publications:
		process_3m(
			input_xml_path + "Publications.xml",
			x3ml_path + "Mapping186/Mapping186.x3ml",
			x3ml_path + "Mapping186/gen_policy-music_rec___20-06-2022145601___1229___28-06-2022143603___3958___29-06-2022201857___4565___11-07-2022180710___4898___13-07-2022204446___3111___14-07-2022153316___1763___15-07-2022183906___7472___15-07-2022184142___12587.xml",
			x3ml_path,
			post_process_url)

	# People Artifacts
	if people_artifacts:
		process_3m(
			input_xml_path + "People_Artifacts.xml",
			x3ml_path + "Mapping177/Mapping177.x3ml",
			x3ml_path + "Mapping177/gen_policy-music_rec___20-06-2022145601___1229___28-06-2022143603___3958___29-06-2022151220___4628.xml",
			x3ml_path,
			post_process_url)

	# People
	if people:
		process_3m(
			input_xml_path + "People_4.xml",
			x3ml_path + "Mapping182/Mapping182.x3ml",
			x3ml_path + "Mapping182/gen_policy-music_ppl___11-07-2022145133___5166___11-07-2022203023___744.xml",
			x3ml_path,
			post_process_url)

	# Artifacts
	if artifacts:
		process_3m(
			input_xml_path + "Catalogue_7_Artifacts.xml",
			x3ml_path + "Mapping189/Mapping189.x3ml",
			x3ml_path + "Mapping189/gen_policy-music___15-06-2022154630___9864.xml",
			x3ml_path,
			post_process_url)

	# Recordings
	if recordings:
		# make a copy of recordings input for the full mapping and the fix mapping
		shutil.copyfile(input_xml_path + "Catalogue_6_Recordings.xml", input_xml_path + "Catalogue_6_Recordings_3m_fix.xml")
		process_3m(
			input_xml_path + "Catalogue_6_Recordings.xml",
			x3ml_path + "Mapping188/Mapping188.x3ml",
			x3ml_path + "Mapping188/gen_policy-music_rec___20-06-2022145601___1229___28-06-2022143603___3958___29-06-2022201857___4565___11-07-2022180710___4898.xml",
			x3ml_path,
			post_process_url)

		process_3m(
			input_xml_path + "Catalogue_6_Recordings_3m_fix.xml",
			x3ml_path + "Ethnomusicology_Recordings_Fix_v1/3m-x3ml-output.x3ml",
			x3ml_path + "Ethnomusicology_Recordings_Fix_v1/generator-policy.xml",
			x3ml_path,
			post_process_url)


def main():
	preprocess = False
	x3ml = False
	postprocess = True

	post_process_url = "https://postprocess.lincsproject.ca"
	local_docker_post_process_url = "http://0.0.0.0:80"
	x3ml_path = "data/X3ML/"
	input_xml_path = "data/preprocessed_xml/"

	#post_process_url = local_docker_post_process_url

	# Note that artifacts preprocessing queries geonames so if you need to run it many times, it should be updated to use a cached version of places.xml after the first run.
	if preprocess:
		preprocessing(
			publications=False,
			people_artifacts=True,
			people=False,
			artifacts=False,
			recordings=False)

	if x3ml:
		send_to_3m(
			post_process_url,
			x3ml_path,
			input_xml_path,
			publications=False,
			people_artifacts=True,
			people=False,
			artifacts=False,
			recordings=False)

	# TODO replacements are slow in recordings so could do the temp uri cleanup in a previous step and cache the result
	if postprocess:
		postprocessing(
			post_process_url=post_process_url,
			input_path=x3ml_path,
			output_path="data/postprocessing_output/",
			error_output_path="postprocessing/output/error_files/",
			input_files=[
				"Catalogue_7_Artifacts.ttl",
				"People_4.ttl",
				"People_Artifacts.ttl",
				"Catalogue_6_Recordings.ttl",
				"Publications.ttl",
				"Catalogue_6_Recordings_3m_fix.ttl"],
			combined_output_path='../Datasets/ethnomusicology.ttl',
			replacement_mapping="postprocessing/entity_replacements_noRegex.tsv",
			mint_mapping="postprocessing/lincs_minting_replacements_noRegex.tsv")


main()
