import os
import csv
import re
from pymods.reader import MODSReader


def write_csv(output_dir, data):
	with open(output_dir, "w") as file:
		csvWriter = csv.writer(file, delimiter=',')
		csvWriter.writerows(data)


# replace empty lists and None with empty string
# removes duplicates from lists
def empty(input_list):
	if input_list is None:
		return ""
	if input_list == [None]:
		return ""
	if len(input_list) == 0:
		return ""
	return list(dict.fromkeys(input_list))


def parse(input_dir):
	csv_rows = [[
		"filename",
		"titles[0]",
		"titles[1]",
		"titles[2]",
		"titles[3]",
		"person_name1",
		"person_name2",
		"person_name3",
		"person_name_rest",
		"person_dates",
		"birthdate",
		"deathyear"
		"name.role.text",
		"work_date",
		"copyrightDate",
		"dateIssued",
		"dateCreated",
		"edition",
		"issuance",
		"publisher",
		"publication_place",
		"genre",
		"language",
		"id_doi",
		"id_isbn",
		"id_isni",
		"id_uri",
		"id_lccn",
		"id_issn",
		"id_local"]]
	for filename in os.listdir(input_dir):
		if not filename.startswith('.'):

			mods = MODSReader(input_dir + filename)
			for record in mods:

				dates = record.dates  # text, type
				copyrightDate = ""
				dateIssued = ""
				dateCreated = ""
				if dates is not None:
					for date in dates:
						if dateIssued == "" and copyrightDate == "" and dateCreated == "":
							if 'copyrightDate' in date.type and date.text is not None:
								copyrightDate = date.text
							elif 'dateIssued' in date.type and date.text is not None:
								dateIssued = date.text
							elif 'dateCreated' in date.type and date.text is not None:
								dateCreated = date.text
							else:
								pass

				work_date = ""
				for d in [copyrightDate, dateIssued, dateCreated]:
					if d is not None and d != "":
						work_date = d

				#TODO add a date that is whichever isnt' blank
				titles = []
				titles_raw = record.titles  # list
				for t in titles_raw:
					split_t = t.split("|")
					for i in split_t:
						if i is not None and i != "":
							titles.append(i)


				identifiers = record.identifiers  # text, type
				id_doi, id_isbn, id_isni, id_uri, id_lccn, id_issn, id_local = [], [], [], [], [], [], []
				if identifiers is not None:
					for identifier in identifiers:
						if identifier.text is not None:
							if identifier.type == 'doi':
								id_doi.append(identifier.text)
							elif identifier.type == "isbn":
								id_isbn.append(identifier.text)
							elif identifier.type == "isni":
								id_isni.append(identifier.text)
							elif identifier.type == "uri":
								id_uri.append(identifier.text)
							elif identifier.type == "lccn":
								id_lccn.append(identifier.text)
							elif identifier.type == "issn":
								id_issn.append(identifier.text)
							elif identifier.type == "local":
								id_local.append(identifier.text)
							else:
								print(filename, ": Missed Identifer Type: ", identifier.type)


				edition = record.edition  # list

				issuance = record.issuance   # list

				publisher = record.publisher  # list

				publication_place = []
				raw_publication_place = record.publication_place  # text, type
				for p in raw_publication_place:
					if p is not None:
						if p.text is not None and p.text != "":
							publication_place.append(p.text)


				language = record.language # text, code, uri
				languages = []
				for l in language:
					if l is not None:
						if l.text is not None and l.text != "":
							languages.append(l.text)

				genre = []
				raw_genre = record.genre  # text, uri, authority, authorityURI
				for g in raw_genre:
					if g is not None:
						if g.text != "" and g.text is not None:
							genre.append(g.text)

				while len(titles) < 4:
					titles.append("")


				def hasNumbers(inputString):
					return bool(re.search(r'\d', inputString))

				names = record.get_pers_names
				person_dates = ""
				person_name1 = ""
				person_name2 = ""
				person_name3 = ""
				person_name_rest = ""
				for name in names:
					split_names = name.text.split(" | ")
					if split_names is not None:
						if hasNumbers(split_names[-1]):
							person_dates = split_names[-1]
							split_names = split_names[:-1]
						if len(split_names) > 0:
							person_name1 = split_names[0]
						if len(split_names) > 1:
							person_name2 = split_names[1]
						if len(split_names) > 2:
							person_name3 = split_names[2]
						if len(split_names) > 3:
							person_name_rest = split_names[3]

				birth_date = ""
				death_date = ""
				# both birth and death dates
				matches = re.findall(r'(^.*[0-9]{1,4}.*)-(.*[0-9]{1,4}.*$)', person_dates)
				if matches:
					birth_date = matches[0][0]
					death_date = matches[0][1]
				else:
					# death date only
					matches = re.findall(r'^.*-\s*([0-9]{1,4}.*$)', person_dates)
					if matches:
						death_date = matches[0]
					else:
						# birth date only
						matches = re.findall(r'(^.*[0-9]{1,4})\s*-.*$', person_dates)
						if matches:
							birth_date = matches[0]
						else:
							# single date
							matches = re.findall(r'[0-9]{1,4}', person_dates)
							if matches:
								birth_date = matches[0]
					if person_dates is not None and person_dates != "":
						print(person_dates)
						print(birth_date)
						print(death_date)
						print("________________")

					csv_rows.append([
						filename,
						titles[0],
						titles[1],
						titles[2],
						titles[3],
						person_name1,
						person_name2,
						person_name3,
						person_name_rest,
						person_dates,
						birth_date,
						death_date,
						name.role.text,
						work_date,
						copyrightDate,
						dateIssued,
						dateCreated,
						empty(edition),
						empty(issuance),
						empty(publisher),
						empty(publication_place),
						empty(genre),
						empty(languages),
						empty(id_doi),
						empty(id_isbn),
						empty(id_isni),
						empty(id_uri),
						empty(id_lccn),
						empty(id_issn),
						empty(id_local)])
	return csv_rows


def main():

	# Update with your own file path
	input_dir = "../Original_Datasets/CWRC/BIBLIFO_MODS/"
	output_dir = "output/BIBLIFO_MODS_extract.csv"

	extracted_data = parse(input_dir)
	write_csv(output_dir, extracted_data)


	input_dir = "../Original_Datasets/CWRC/TPatT_MODS/"
	output_dir = "output/TPatT_MODS_extract.csv"

	extracted_data = parse(input_dir)
	write_csv(output_dir, extracted_data)

	input_dir = "../Original_Datasets/CWRC/CEWW_MODS/"
	output_dir = "output/CEWW_MODS_extract.csv"

	extracted_data = parse(input_dir)
	write_csv(output_dir, extracted_data)


main()
