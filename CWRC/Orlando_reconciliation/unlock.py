import requests
import re
import sys
import os
import json


"""

Run the following as setup:
	pip3 install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib oauth2client
	pip3 install gspread
	pip3 install PyDrive

Requires a google sheets service account as described here:
https://www.analyticsvidhya.com/blog/2020/07/read-and-update-google-spreadsheets-with-python/

Replace the filenames of the form 'orlando-reconciliation-4d563aff7010.json' with files containing valid keys


Must have a cookies.txt file in the current directory for the cwrc commands to work
Run ./cookies_private.sh in terminal if no cookies.txt file exists.
The cookies_private.sh file must contain valid cwrc login credentials.
Use cookies_template.sh to add your credentials to and rename.


To run this code
	python3 unlock.py <batchnumber>

"""



def get_cwrc_cookies():
	with open("cookies.txt") as f:
		content = f.read()
		cookie = re.findall('(SSESS.*)', content)
		return cookie[0].replace("\t", "=").strip("\n")


def update_cwrc_datastream(pid, cookie, updated_file, datastream, filename):
	# Once ready to place items back into the repository, update object datastream in repository
	# curl -b token.txt -X POST -F "method=PUT" -F "file=@${SOURCE_FILE}" https://${SERVER_NAME}/islandora/rest/v1/object/${PID}/datastream/${DSID}

	url = 'https://cwrc.ca/islandora/rest/v1/object/' + pid + '/datastream/' + datastream
	payload = {'method': 'PUT'}
	files = [('file',(filename,open(updated_file,'rb'),'text/xml'))]
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {'Cookie': c}
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	print("Updating Datastream:  ", response.status_code)
	if str(response.status_code).startswith("2"):
		return True
	else:
		#print("datastream response: ", response.status_code)
		#print("datastream response: ", response.text)
		return False


def update_cwrc_workflow(pid, cookie):
	# Add workflow information describing the change details here : ask a CWRC admin for the workflow parameters to add via the activity parameter
	# curl -b token.txt -G -X GET "https://${SERVER_NAME}/islandora_workflow_rest/v1/add_workflow" -d PID=${PID} -d activity='{"category":"metadata_contribution","stamp":"orlando:ENH","status":"c","note":"entity"}'

	url = "https://cwrc.ca/islandora_workflow_rest/v1/add_workflow?PID=" + pid + "&activity={\"category\":\"metadata_contribution\",\"stamp\":\"orlando:ENH\",\"status\":\"c\",\"note\":\"Entity reconciliation via OpenRefine and manual vetting.\"}"
	payload = f'PID={pid}&activity=%7B%22category%22%3A%22metadata_contribution%22%2C%22stamp%22%3A%22orlando%3AENH%22%2C%22status%22%3A%22c%22%2C%22note%22%3A%22Entity%20reconciliation%20via%20OpenRefine%20and%20manual%20vetting.%22%7D'
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Cookie': c
		}
	response = requests.request("GET", url, headers=headers, data=payload)
	print("Updating Workflow Stamp:  ", response.status_code)
	#print("Updating Workflow response:  ", response.text)
	if str(response.status_code).startswith("2"):
		return True
	else:
		print(response.text)
		return False


def check_cwrc_workflow(pid, cookie):
	# Add workflow information describing the change details here
	# curl -b token.txt -G -X GET "https://${SERVER_NAME}/islandora_workflow_rest/v1/get_full_workflow" -d PID=${PID}

	url = "https://cwrc.ca/islandora_workflow_rest/v1/get_full_workflow?PID=" + pid
	payload = f'PID={pid}'

	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {
		'Content-Type': 'application/x-www-form-urlencoded',
		'Cookie': c
		}
	response = requests.request("GET", url, headers=headers, data=payload)
	print("CHECK WORKFLOW:  ", response.status_code, response.text)
	if str(response.status_code).startswith("2"):
		return True
	else:
		return False


def check_cwrc_lock(pid, cookie):
	# returns false if there is no lock set on the object
	url = 'https://cwrc.ca/islandora/rest/v1/object/' + pid + '/lock'
	payload = {}
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {'Cookie': c}
	response = requests.request("GET", url, headers=headers, data=payload)
	#print("CHECK LOCK:  ", response.status_code, response.text)
	if response.status_code == 500:
		print("Unable to check lock on the following PID")
		print(url)

	return json.loads(response.content)["locked"]


def set_cwrc_lock(pid, cookie):
	# aquire lock on objects that are unlocked:
	# curl -b token.txt -X POST https://${SERVER_NAME}/islandora/rest/v1/object/${PID}/lock
	url = 'https://cwrc.ca/islandora/rest/v1/object/' + pid + '/lock'
	payload = {}
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {'Cookie': c}
	response = requests.request("POST", url, headers=headers, data=payload)
	return response.status_code


def remove_cwrc_lock(pid, cookie):
	# Remove lock (if lock exists):
	# CWRC datastream will not release the lock on an update event as is the case with other datastreams will
	# curl -b token.txt -X DELETE https://${SERVER_NAME}/islandora/rest/v1/object/${PID}/lock

	url = 'https://cwrc.ca/islandora/rest/v1/object/' + pid + '/lock'
	payload = {}
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {'Cookie': c}
	response = requests.request("DELETE", url, headers=headers, data=payload)
	print("Removing cwrc lock:  ", response.status_code)
	print("lock response: ", response.content)
	if str(response.status_code).startswith("2"):
		return True
	else:
		print(response.text)
		return False


def main():
	# set these to true if you want to skip that step
	# if you skip one of these steps, then it will assume it was previously successful for the whole batch
	skip_datastream = False
	skip_workflow = False
	skip_unlock = True  # don't need to unlock unless the other two setting are true

	batch_id = str(sys.argv[1])
	if batch_id.startswith("p"):
		datastream = "PERSON"
	else:
		datastream = "ORGANIZATION"

	augmented_dir = "data/augmented/" + batch_id + "/"

	# for now only processsing and unlocking updated files.
	# But we need to add process to eventually unlock the uncertain matches
	# get the PIDs from the folder of augmented files
	cwrc_cookie = get_cwrc_cookies()
	for filename in os.listdir(augmented_dir):
		if filename.endswith(".xml"):
			updated_file = os.path.join(augmented_dir, filename)
			cwrc_pid = filename[:-4]

			"""
			Update datastream with updated file
			If successful, update worklow stamp
			If successful, unlock file
			Some error reporting at each step
			Manual checking and correction will need to be done if there is an error at any step
			"""
			print("\n\n", cwrc_pid)
			if skip_datastream is False:
				datastream_status = update_cwrc_datastream(cwrc_pid, cwrc_cookie, updated_file, datastream, filename)
			else:
				datastream_status = True

			if datastream_status is False:
				print("ERROR: datastream could not be updated: ", cwrc_pid)
			else:
				if skip_workflow is False:
					workflow_status = update_cwrc_workflow(cwrc_pid, cwrc_cookie)
				else:
					workflow_status = True

				## for confirmation that the update workflow was successful, uncomment the next line
				#check_cwrc_workflow(cwrc_pid, cwrc_cookie)

				if workflow_status is False:
					print("ERROR: workflow could not be updated: ", cwrc_pid)

				## updating the datastream and workflow both unlock the file. No need to do it manually.
				else:
					if skip_unlock is False:
						remove_cwrc_lock(cwrc_pid, cwrc_cookie)

					lock_status = check_cwrc_lock(cwrc_pid, cwrc_cookie)
					if lock_status is False:
						pass
						print("File is unlocked")
					else:
						print("ERROR: File is locked: ", cwrc_pid)


if __name__ == "__main__":
	main()
