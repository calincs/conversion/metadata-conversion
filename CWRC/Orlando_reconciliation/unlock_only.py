import gspread
from oauth2client.service_account import ServiceAccountCredentials
import requests
import re
import sys
import json


"""
This program will unlock all entities in a batch.
It takes the batch id as input.
It then finds all cwrc PIDs in that batch from the google sheet.
It then unlocks all of those PIDs.
The use case for this program is if there was an error during the lock script and you want to restart
or after you've updated the high certainty matches on CWRC, you can unlock all of the rest of the objects too.


Run the following as setup:
	pip3 install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib oauth2client
	pip3 install gspread
	pip3 install PyDrive

Requires a google sheets service account as described here:
https://www.analyticsvidhya.com/blog/2020/07/read-and-update-google-spreadsheets-with-python/

replace the filenames of the form 'orlando-reconciliation-4d563aff7010.json' with files containing valid keys


Must have a cookies.txt file in the current directory for the cwrc commands to work
Run ./cookies_private.sh in terminal if no cookies.txt file exists.
The cookies_private.sh file must contain valid cwrc login credentials.
Use cookies_template.sh to add your credentials to and rename.


To run this code
	python3 unlock_only.py <batchnumber>


"""


def get_sheets_client(credentials):
	# use creds to create a client to interact with the Google Drive API
	sheets_scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
	sheets_creds = ServiceAccountCredentials.from_json_keyfile_name(credentials, sheets_scope)
	sheets_client = gspread.authorize(sheets_creds)
	return sheets_client


def get_ids_from_sheets(batch_id, sheets_client):
	sheet = sheets_client.open("Orlando_Entities_Tracking")
	if batch_id.startswith("p"):
		tab = sheet.worksheet("People")
	else:
		tab = sheet.worksheet("Organizations")
	records = tab.get_all_records()
	ids = []
	for r in records:
		if r["Batch"].lower() == batch_id.lower():
			ids.append([r["CWRC Entity ID"], r["row_num"]])
	return ids


def get_cwrc_cookies():
	with open("cookies.txt") as f:
		content = f.read()
		cookie = re.findall('(SSESS.*)', content)
		return cookie[0].replace("\t", "=").strip("\n")


def check_cwrc_lock(pid, cookie):
	# returns false if there is no lock set on the object
	url = 'https://cwrc.ca/islandora/rest/v1/object/' + pid + '/lock'
	payload = {}
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {'Cookie': c}
	response = requests.request("GET", url, headers=headers, data=payload)
	if response.status_code == 500:
		print("Unable to check lock on the following PID")
		print(url)

	return json.loads(response.content)["locked"]


def remove_cwrc_lock(pid, cookie):
	# Remove lock (if lock exists):
	# CWRC datastream will not release the lock on an update event as is the case with other datastreams will
	# curl -b token.txt -X DELETE https://${SERVER_NAME}/islandora/rest/v1/object/${PID}/lock
	url = 'https://cwrc.ca/islandora/rest/v1/object/' + pid + '/lock'
	payload = {}
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {'Cookie': c}
	response = requests.request("DELETE", url, headers=headers, data=payload)
	# print("Removing cwrc lock:  ", response.status_code)
	# print("lock response: ", response.content)
	if str(response.status_code).startswith("2"):
		return True
	else:
		#print(str(pid), "  :Could not remove lock:  ", response.text)
		return False


def main():
	# authenticate to access google sheets
	sheets_client = get_sheets_client('orlando-reconciliation-4d563aff7010.json')

	batch_id = str(sys.argv[1])

	# get pid list from google sheets
	print("Gathering PIDs from Google Sheets")
	batch_pids = get_ids_from_sheets(batch_id, sheets_client)

	cwrc_cookie = get_cwrc_cookies()

	print("Unlocking entities")
	for pid in batch_pids:
		remove_cwrc_lock(pid[0], cwrc_cookie)

main()
