from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from oauth2client.service_account import ServiceAccountCredentials
from apiclient.http import MediaFileUpload

"""
https://developers.google.com/drive/api/v3/

pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib


ids for folders:
	data (1SG-gUgvA-zlo-_Lc29-Yog5ZsxsWNX67)
	Orlando_Entities_Tracking (1NW0xbP7H7Ds2ZHNXF-8Wz7bg1C2xVKi2jJFibaBlk2U)
	reconciled (1beZbfVIG-_-TyR5Dw7nK-QZnF2q2Udx9)
	ready_to_reconcile (1leye5p0qCezLatIZgDRF59LvRaX6PJF9)


"""

SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly', 'https://www.googleapis.com/auth/drive']


def get_list_files(service):
	# Call the Drive v3 API and find a list of files the user has access to
	results = service.files().list(
		pageSize=10, fields="nextPageToken, files(id, name)").execute()
	items = results.get('files', [])

	if not items:
		print('No files found.')
	else:
		print('Files:')
		for item in items:
			print(u'{0} ({1})'.format(item['name'], item['id']))


def upload_csv(batch_id):
	# uploads the csv ready for reconciling

	# authenticate
	creds = None
	token_file = "orlando-reconciliation-734c823328bc.json"
	if os.path.exists(token_file):
		creds = ServiceAccountCredentials.from_json_keyfile_name(token_file, SCOPES)
	drive_service = build('drive', 'v3', credentials=creds)

	# upload data
	folder_id = '1leye5p0qCezLatIZgDRF59LvRaX6PJF9'
	file_metadata = {
		'name': batch_id + ".csv",
		'parents': [folder_id]
	}
	media = MediaFileUpload("data/csv/" + batch_id + ".csv",
							mimetype='text/csv',
							resumable=True)
	file = drive_service.files().create(body=file_metadata,
										media_body=media,
										fields='id').execute()
	print('File ID: %s -- uploaded to drive' % file.get('id'))


def upload_uncertain(batch_id):
	# uploads tsv files of uncertain matches

	# authenticate
	creds = None
	token_file = "orlando-reconciliation-734c823328bc.json"
	if os.path.exists(token_file):
		creds = ServiceAccountCredentials.from_json_keyfile_name(token_file, SCOPES)
	drive_service = build('drive', 'v3', credentials=creds)

	# upload data
	folder_id = '1KCyztu9lYuJjoNcBfcASF1InFLMVHNj7'
	file_metadata = {
		'name': batch_id + ".tsv",
		'parents': [folder_id]
	}
	media = MediaFileUpload("data/uncertain_matches/" + batch_id + ".tsv",
							mimetype='text/tsv',
							resumable=True)
	file = drive_service.files().create(body=file_metadata,
										media_body=media,
										fields='id').execute()
	print('File ID: %s -- uploaded to drive' % file.get('id'))