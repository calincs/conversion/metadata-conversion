import sys
import os
from bs4 import BeautifulSoup as bs
import re
import pandas as pd
import csv
import numpy as np


def get_cwrc_bio_ids():
	bio_id_dict = {}
	with open("bio_ids.txt", "r") as ids_f:
		for line in ids_f:
			line_split = line.split("\t")
			bio_id_dict[line_split[1].strip("\n")] = line_split[0]
	return bio_id_dict


def date_to_year(date):
	matches = re.findall("[0-9]{4}", str(date))
	if len(matches) == 0:
		return date
	elif len(matches) > 0:
		return matches[0]


def extract_data(file, name_type_list):
	# Read each line in the file, readlines() returns a list of lines
	content = file.readlines()

	# Combine the lines in the list into a string
	content = "".join(content)
	soup = bs(content, "lxml")


	identity = soup.find("identity")
	description = soup.find("description").find("existdates")
	dates = description.find_all("datesingle")

	## Dates
	death = np.nan
	birth = np.nan
	for date in dates:
		if date.find("datetype").text.lower() == "birth":
			birth = date.find("standarddate").text
		elif date.find("datetype").text.lower() == "death":
			death = date.find("standarddate").text


	## Names

	# Prefered form
	preferred_form = identity.find("preferredform")
	name_parts = preferred_form.find_all("namepart")
	given = np.nan
	family = np.nan
	full = np.nan
	for name_part in name_parts:
		# try statement because sometimes the full name is not labelled
		try:
			if name_part["parttype"].lower() == "given":
				given = name_part.text
			elif name_part["parttype"].lower() == "family":
				family = name_part.text
		except KeyError:
			full = name_part.text


	# Variants
	variant_names = {}
	variants = identity.find("variantforms").find_all("variant")
	for variant in variants:
		variant_names[variant.find("varianttype").text] = variant.find("namepart").text

	for v in name_type_list:
		try:
			temp = variant_names[v]
		except KeyError:
			variant_names[v] = np.nan

	return birth, death, given, family, full, variant_names


def write_csv(batch):
	# get all cwrc biography ids and their corresponding cwrc ids
	bio_id_dict = get_cwrc_bio_ids()

	variant_name_types = [
		"orlandoStandardName",
		"surname",
		"abusiveName",
		"additionalName",
		"authorialName",
		"birthname",
		"crypticName",
		"familiarName",
		"forename",
		"generationalName",
		"honorificName",
		"indexedName",
		"literaryName",
		"localName",
		"marriedName",
		"nameLink",
		"nickname",
		"preferredName",
		"pseudonym",
		"ReligousName",
		"RoleName",
		"RomanceName"]

	out_csv_path = "data/csv/" + batch + ".csv"
	directory = "data/original/" + batch + "/"
	headings = [
		"cwrc_url",
		"bio_url",
		"name",
		"name_date"] + variant_name_types + ["birthdate", "deathdate"]
	df = pd.DataFrame(columns=headings)
	row_num = 1

	for filename in os.listdir(directory):
		if filename.endswith(".xml"):
			file_pid = filename[:-4]
			with open(os.path.join(directory, filename), "r", encoding="utf-8") as f:
				birth, death, given, family, full, variant_names = extract_data(f, variant_name_types)
			birthyear = date_to_year(birth)
			deathyear = date_to_year(death)

			cwrc_url = "https://cwrc.ca/islandora/object/" + file_pid

			try:
				bio_url = "https://cwrc.ca/islandora/object/" + bio_id_dict[file_pid]
			except KeyError:
				bio_url = np.nan


			name = np.nan
			if str(full) != "nan":
				name = full
			else:
				name = given + " " + family

			standardName = variant_names["orlandoStandardName"] + ", "
			if str(birthyear) != "nan" and str(deathyear) == "nan":
				standardName = standardName + ", " + str(birthyear) + "-"
			if str(birthyear) == "nan" and str(deathyear) != "nan":
				standardName = standardName + ", " + "-" + str(deathyear)
			if str(birthyear) != "nan" and str(deathyear) != "nan":
				standardName = standardName + ", " + str(birthyear) + "-" + str(deathyear)
			else:
				standardName = variant_names["orlandoStandardName"]

			values = [
				cwrc_url,
				bio_url,
				name,
				standardName,
				variant_names["orlandoStandardName"],
				variant_names["surname"],
				variant_names["abusiveName"],
				variant_names["additionalName"],
				variant_names["authorialName"],
				variant_names["birthname"],
				variant_names["crypticName"],
				variant_names["familiarName"],
				variant_names["forename"],
				variant_names["generationalName"],
				variant_names["honorificName"],
				variant_names["indexedName"],
				variant_names["literaryName"],
				variant_names["localName"],
				variant_names["marriedName"],
				variant_names["nameLink"],
				variant_names["nickname"],
				variant_names["preferredName"],
				variant_names["pseudonym"],
				variant_names["ReligousName"],
				variant_names["RoleName"],
				variant_names["RomanceName"],
				birth,
				death]

			df.loc[row_num] = values
			row_num = row_num + 1

	# drop empty columns since not all name types will occur in each batch
	df.dropna(how='all', axis=1, inplace=True)
	df.insert(2, "certainty", "")
	sorted_df = df.sort_values(by=["name"], ascending=False)

	# write to file
	sorted_df.to_csv(out_csv_path, index=False)
