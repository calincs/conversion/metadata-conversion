# Orlando Reconciliation Instructions

Project Google Drive folder: https://drive.google.com/drive/folders/1_hOCk9ebrcdH4UAGSrJ-is6bIA7_2irX

All of the IDs for person and organization entities in the Orlando data are listed in the People and Organization tabs in the [Orlando_Entities_Tracking sheet](https://docs.google.com/spreadsheets/d/1NW0xbP7H7Ds2ZHNXF-8Wz7bg1C2xVKi2jJFibaBlk2U/edit#gid=1342282808). This sheet will be used to track the reconciliation progress for those entities and enables a semi-automated process of extracting records from CWRC.ca and reuploading the reconciled entity records.

All of the code that connects to the cwrc.ca API requires you to first run Run ./cookies_private.sh in terminal if no cookies.txt file exists. Where you should start with cookies_template.sh, replace the username and password placeholders with valid cwrc.ca credentials, and rename the file to cookies_private.sh. You also need to have credentials and keys for the orlando-reconciliation google account to be able to run the code that interacts with the files in google drive. Contact Natalie to get access to those files (or the LINCS google drive).

Start by going to the People or Organization tabs and adding a value to the Batch column for a selection of records. Then run lock.py with the batch id as the command line argument. For example, 
	
	python3 lock.py p_001

 This will automatically get the list of PIDs in that batch, check if they are unlocked, remove any already locked records from that batch (will update the sheet automatically), lock the rest of the records, download the person or organization xml files for those records, extract the relevant data from the xml records as a csv file, and upload the csv file to the data/ready_to_reconcile google drive folder.

Once the updated csv that includes the reconciled values is available in the data/reconciled drive folder, run:
	
	python3 update_xml.py p_001

This will create an updated xml file locally in data/augmented/{batch_id}/ for each original xml file that had matches in the csv with a certainty of 3 or 4. Note that the possible names for columns in the csv file can be named "wikidata", "viaf", "getty", "certainty", "wikidata_certainty", "getty_certainty", "viaf_certainty". Any other columns will be ignored in this step. You should either use a general certainty column or the authority specific certainty columns. This program will also save files of uncertain matches in /data/uncertain_matches/ and upload them to google drive automatically. Finally, it will updated the data/confirmed_ids spreadsheet on google drive with the confirmed matches for each record id.

Now that you have the updated xml files on your machine and have confirmed they are accurate, run the unlock.py program with the batch id as the command line argument. 

	python3 unlock.py p_001

This upload the updated xml files for that batch to cwrc.ca as an update to the person or organization datastream and unlock the record on cwrc.ca.

Note that unlock.py does not unlock the files that did not have high certainty matches. So the final step should be to run

	python3 unlock_only.py p_001

to release all other locks for this batch. 


## Workflow Steps
1. Add a batch id for some selection of records in the tracking spreadsheet. The batch id should go in the Batch column of the People tab.
2. ./cookies_private.sh
3. lock.py
	- checks the locks on the records in that batch
	- removes already locked records from the batch (updates the tracking sheet batch column)
	- locks the rest of the records
	- downloads those newly locked records
	- extracts relevant information into a spreadsheet for that batch
	- uploads that batch spreadsheet to the ready to reconcile drive folder
4. Assign to a student to reconcile that batch
5. Student adds the reconciled spreadsheet to the reconciled folder
6. Open that spreadsheet in google sheets and double check the work. Add any changes to this google sheet. Move the document to the data/final folder. The sheet should be named the batch id and the tab should be named the batch id. columns names should be named as described above.
7. update_xml.py
	- reads from the google sheet to get the matches and certainty scores for a given batch
	- adds those values (above 3 and 4) to the original xml files stored locally (on the computer of the  person who ran lock.py for that batch).
	- Saves a tsv file of uncertain matches locally and then uploads it to the google folder data/uncertain_matches/. Those can be reviewed later on.
	- adds all certain matches to the data/confirmed_ids google sheet.
		- The main tracking document reads from this file
		- If you mess up and accidentally add an inaccurate batch to this file, just delete them from the confirmed_ids file and rerun update_xml.py
8. Manually inspect some of the augmented xml files that were generated locally
9. unlock.py
	- uploads new xml files to cwrc
	- updates cwrc workflow info
	- releases the lock on the record in cwrc
10. unlock_only.py


