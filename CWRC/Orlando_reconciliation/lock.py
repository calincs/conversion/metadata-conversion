import gspread
from oauth2client.service_account import ServiceAccountCredentials
import requests
import re
import sys
from pathlib import Path
import time
import json
import os
from pydrive.auth import GoogleAuth
from xml_to_csv import write_csv
from upload_to_drive import upload_csv

"""
README

Run the following as setup:
	pip3 install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib oauth2client
	pip3 install gspread
	pip3 install PyDrive

Requires a google sheets service account as described here:
https://www.analyticsvidhya.com/blog/2020/07/read-and-update-google-spreadsheets-with-python/

replace the filenames of the form 'orlando-reconciliation-4d563aff7010.json' with files containing valid keys


Must have a cookies.txt file in the current directory for the cwrc commands to work
Run ./cookies_private.sh in terminal if no cookies.txt file exists.
The cookies_private.sh file must contain valid cwrc login credentials.
Use cookies_template.sh to add your credentials to and rename.


To run this code
	python3 lock.py <batchnumber>


If this script does not succeed and only some of the PIDs were downloaded and locked,
then you should run unlock_only.py for this batch and then restart the lock.py script.
If you immediately rerun lock.py on a partially locked batch then it will remove the
already locked objects from the batch.
"""


def get_sheets_client(credentials):
	# use creds to create a client to interact with the Google Drive API
	sheets_scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
	sheets_creds = ServiceAccountCredentials.from_json_keyfile_name(credentials, sheets_scope)
	sheets_client = gspread.authorize(sheets_creds)
	return sheets_client


def get_ids_from_sheets(batch_id, sheets_client):
	sheet = sheets_client.open("Orlando_Entities_Tracking")
	if batch_id.startswith("p"):
		tab = sheet.worksheet("People")
	else:
		tab = sheet.worksheet("Organizations")
	records = tab.get_all_records()
	ids = []
	for r in records:
		if r["Batch"].lower() == batch_id.lower():
			ids.append([r["CWRC Entity ID"], r["row_num"]])
	return ids


def get_cwrc_cookies():
	with open("cookies.txt") as f:
		content = f.read()
		cookie = re.findall('(SSESS.*)', content)
		return cookie[0].replace("\t", "=").strip("\n")


def get_cwrc_file(pid, format, cookie):
	url = 'https://cwrc.ca/islandora/rest/v1/object/' + pid + '/datastream/' + format
	payload = {}
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {'Cookie': c}
	response = requests.request("GET", url, headers=headers, data=payload)
	response.encoding = "utf-8"
	if response.status_code not in [200, 201]:
		print("CWRC download status: ", response.status_code, ".  For file: ", pid)
	return response.text


def check_cwrc_lock(pid, cookie):
	# returns false if there is no lock set on the object
	url = 'https://cwrc.ca/islandora/rest/v1/object/' + pid + '/lock'
	payload = {}
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {'Cookie': c}
	response = requests.request("GET", url, headers=headers, data=payload)
	if response.status_code == 500:
		print("Unable to check lock on the following PID")
		print(url)

	return json.loads(response.content)["locked"]


def set_cwrc_lock(pid, cookie):
	url = 'https://cwrc.ca/islandora/rest/v1/object/' + pid + '/lock'
	payload = {}
	c = 'has_js=1; collapsiblock=%7B%20%20%22block-islandora-blocks-metadata%22%3A%201%20%7D; ' + cookie
	headers = {'Cookie': c}
	response = requests.request("POST", url, headers=headers, data=payload)
	return response.status_code


def save_batch_xml(batch_pids, path, batch_id, cwrc_cookie):
	for pid in batch_pids:
		pid = pid[0]
		if batch_id.startswith("p"):
			data = get_cwrc_file(pid, "PERSON", cwrc_cookie)
		else:
			data = get_cwrc_file(pid, "ORGANIZATION", cwrc_cookie)
		Path(path + batch_id).mkdir(parents=True, exist_ok=True)
		with open(path + batch_id + "/" + pid + ".xml", "w", encoding="utf-8") as f:
			f.write(data)
		time.sleep(3)


def remove_from_batch(pids, batch_id, sheets_client):
	# Must update the number here if you add more columns to the People or Organization tabs
	# this function will update the tracking sheet to remove the value in the batch column for a given PID

	for p in pids:
		row_num = p[1]
		sheet = sheets_client.open("Orlando_Entities_Tracking")
		if batch_id.startswith("p"):
			tab = sheet.worksheet("People")
			batch_column_num = 11
		else:
			tab = sheet.worksheet("Organizations")
			batch_column_num = 10

		tab.update_cell(row_num, batch_column_num, "already_locked")


def main():
	# set csv_only to True if you want to create a csv from already locked and downloaded xml files
	csv_only = False

	# authenticate to access google sheets
	sheets_client = get_sheets_client('orlando-reconciliation-4d563aff7010.json')

	batch_id = str(sys.argv[1])

	if csv_only is False:
		# get pid list from google sheets
		print("Gathering PIDs from Google Sheets")
		batch_pids = get_ids_from_sheets(batch_id, sheets_client)

		# cwrc cookies
		#os.popen('sh cookies_private.sh')
		cwrc_cookie = get_cwrc_cookies()

		# check if lock exists on objects: curl -b token.txt -X GET https://${SERVER_NAME}/islandora/rest/v1/object/${PID}/lock
		unlocked_pids = []
		not_ready_pids = []
		print("Checking lock status on each PID in the batch")
		for pid in batch_pids:
			locked = check_cwrc_lock(pid[0], cwrc_cookie)
			if locked:
				not_ready_pids.append(pid)
			else:
				unlocked_pids.append(pid)

		print("\nPIDS that are unlocked and will be locked: ", unlocked_pids, "\n\n")
		print("\nPIDS already locked. Will be removed from this batch: ", not_ready_pids, "\n\n")

		# aquire lock on objects that are unlocked:
		# curl -b token.txt -X POST https://${SERVER_NAME}/islandora/rest/v1/object/${PID}/lock
		ready_pids = []
		for pid in unlocked_pids:
			response = set_cwrc_lock(pid[0], cwrc_cookie)
			if response == 201:
				print(pid[0], " has been locked")
				ready_pids.append(pid)
			elif response == 403:
				print(pid[0], " could not be locked")
				not_ready_pids.append(pid)
			else:
				print("Error: ", response)
				print(pid[0])

		# if one is locked remove it from the current batch on google sheets
		# all pids in not_ready_pids list
		remove_from_batch(not_ready_pids, batch_id, sheets_client)

		# export data from newly locked pids from cwrc.ca
		# save xml data in folder for batch (labelled original)
		save_batch_xml(unlocked_pids, "data/original/", batch_id, cwrc_cookie)

	# extract csv data from xml
	write_csv(batch_id)

	# upload csv to google drive
	upload_csv(batch_id)

	# TODO automatically update Batches tab of tracking sheet to say ready to reconcile when ready


if __name__ == "__main__":
	main()
