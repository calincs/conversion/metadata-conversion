from pathlib import Path
import os
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup as bs
import pandas as pd
import sys
from oauth2client.service_account import ServiceAccountCredentials
import requests
import json
import gspread
import numpy as np
from datetime import datetime
from upload_to_drive import upload_uncertain
import time


"""
to run:
	python3 update_xml.py <batch_id>

Must have a cookies.txt file in the current directory for the cwrc commands to work
Run ./cookies_private.sh in terminal if no cookies.txt file exists.
The cookies_private.sh file must contain valid cwrc login credentials.
Use cookies_template.sh to add your credentials to and rename.


Xpath for where sameAs relationships should be input
Organization:
	/cwrc/entity/organization/identity/sameAs[@cert]
Person:
	/cwrc/entity/person/identity/sameAs[@cert]
Place:
	/cwrc/entity/place/identity/sameAs[@cert]
@cert values:
	<value a:en="definite" a:fr="" default="yes">4</value>
	<value a:en="reasonably certain" a:fr="">3</value>
	<value a:en="probable" a:fr="">2</value>
	<value a:en="speculative" a:fr="">1</value>


#https://github.com/cwrc/tech_documentation#downloading-content-pseudocode-given-a-collection-pid-authenticate-and-download-a-specified-datastream-from-all-items-in-the-collection


This program expects there to be a google sheet labelled only with the batch_id in the data/final folder in drive. That data folder needs to be shared with the sheets client used.
The spreadsheet must have some of the following column names: viaf, wikidata, getty, viaf_certainty, wikidata_certainty, getty_certainty, certainty. and those column names are case sensitive

This program will read the spreadsheet to get all of the identifiers and certainty values. It will add any identifiers with certainty of 3 or 4 to the original xml file and save it in the
local data/augmented folder. Those won't be added to cwrc.ca unless you run the unlock.py program. These steps were separated to allow for inspections before uploading.
This program will also add all uncertain matches (certainty less than 3) to the drive folder data/uncertain_matches/.
It will add all certain matches to the data/comfirmed_matches google sheet so that we can have a master list. That spreadsheet is pulled into the Orlando_Entity_Tracking sheet as well.

"""


def get_sheets_client(credentials):
	# use creds to create a client to interact with the Google Drive API
	sheets_scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
	sheets_creds = ServiceAccountCredentials.from_json_keyfile_name(credentials, sheets_scope)
	sheets_client = gspread.authorize(sheets_creds)
	return sheets_client


def get_ids_from_sheets(batch_id, sheets_client):
	# returns json of spreadsheet
	sheet = sheets_client.open(batch_id)
	tab = sheet.worksheet(batch_id)
	records = tab.get_all_records()
	return records


def get_cwrc_cookies():
	with open("cookies.txt") as f:
		content = f.read()
		cookie = re.findall('(SSESS.*)', content)
		return cookie[0].replace("\t", "=").strip("\n")


def add_url(id_value):
	if id_value.startswith("Q"):
		url = 'http://www.wikidata.org/entity/' + str(id_value.strip())
	elif id_value.startswith("ulan"):
		url = 'http://vocab.getty.edu/page/' + str(id_value.strip())
	else:
		url = 'http://viaf.org/viaf/' + str(id_value.strip())
	return url


def cert_to_string(num):
	# if empty, return empty string
	# if number with decimal, return string of integer
	if str(num) == "" or num is None:
		return ""
	if "(" in str(num):
		return str(num)
	try:
		out_num = str(int(float(num)))
	except:
		out_num = str(num)
	return out_num.strip()


def choose_certainty(cwrc_pid, general_cert, specific_cert):
	# this function is to help the case where there are fields like wikidata_certainty and certainty in the dataset
	# if there is only authority specific ones or a general column then it will set it to the non-empty value
	# if cwrc pid is empty for that value and that authority, then it will return an empty string as the certainty
	if cwrc_pid == "":
		return ""
	if str(specific_cert) is not None:
		if str(specific_cert) != "":
			return cert_to_string(specific_cert)
	return cert_to_string(general_cert)


def update_master_sheet(sheets_client, row):
	error_count = 0
	sheet = sheets_client.open("confirmed_ids")
	tab = sheet.get_worksheet(0)
	while(error_count < 5):
		try:
			tab.append_row(row)
			time.sleep(1)
			return
		except:
			error_count = error_count + 1
			print("ERROR while appending row to google sheet master list. Sleeping for 30 seconds before trying again.")
			time.sleep(30)

	print("ERROR: over 5 attempts at appending row to google sheet. Did not complete.")
	return


def main():
	batch = sys.argv[1]
	og_xml_dir = "data/original/" + batch + "/"
	uncertain_dir = "data/uncertain_matches/"

	## get the google sheet for this batch from google drive
	## read data from it
	sheets_client = get_sheets_client('orlando-reconciliation-4d563aff7010.json')
	batch_data = get_ids_from_sheets(batch, sheets_client)

	# get all same as relationships for this batch
	#data = pd.read_csv(csv_path, dtype=str)
	data = pd.DataFrame.from_dict(batch_data)
	df = pd.DataFrame(data, columns=['cwrc_url', "viaf", "wikidata", "getty", "viaf_certainty", "wikidata_certainty", "getty_certainty", "certainty"])

	# clean data to have correct URL formats
	df = df.replace(to_replace='', value=np.nan, regex=True)
	df = df.replace(to_replace='https://cwrc.ca/islandora/object/', value='', regex=True)
	#df = df.replace(to_replace=' ', value='', regex=True)
	df = df.replace(to_replace=np.nan, value='', regex=True)

	# create dictionaires of relevant fields indexed by cwrc id
	certainty_dict = df.set_index('cwrc_url')['certainty'].to_dict()
	viaf_dict = df.set_index('cwrc_url')['viaf'].to_dict()
	wikidata_dict = df.set_index('cwrc_url')['wikidata'].to_dict()
	getty_dict = df.set_index('cwrc_url')['getty'].to_dict()
	v_certainty_dict = df.set_index('cwrc_url')['viaf_certainty'].to_dict()
	w_certainty_dict = df.set_index('cwrc_url')['wikidata_certainty'].to_dict()
	g_certainty_dict = df.set_index('cwrc_url')['getty_certainty'].to_dict()

	# create output directory for this batch
	augmented_path_dir = "data/augmented/" + batch
	Path(augmented_path_dir).mkdir(parents=True, exist_ok=True)

	# output any uncertain matches for later processing
	with open(uncertain_dir + batch + ".tsv", "w") as uncert_out:
		uncert_out.write("\t".join(["cwrc_url", "cwrc_id", "viaf", "viaf_certainty", "getty", "getty_certainty", "wikidata", "wikidata_certainty"]))
		uncert_out.write("\n")

		# for each xml file in the batch open the original xml file
		for filename in os.listdir(og_xml_dir):
			if filename.endswith(".xml"):

				filepath = os.path.join(og_xml_dir, filename)

				cwrc_pid = filename[:-4]
				viaf = [cert_to_string(viaf_dict[cwrc_pid]), choose_certainty(viaf_dict[cwrc_pid], certainty_dict[cwrc_pid], v_certainty_dict[cwrc_pid])]
				wikidata = [wikidata_dict[cwrc_pid], choose_certainty(wikidata_dict[cwrc_pid], certainty_dict[cwrc_pid], w_certainty_dict[cwrc_pid])]
				getty = [getty_dict[cwrc_pid], choose_certainty(getty_dict[cwrc_pid], certainty_dict[cwrc_pid], g_certainty_dict[cwrc_pid])]

				max_certainty = cert_to_string(max(
					str(certainty_dict[cwrc_pid]),
					str(v_certainty_dict[cwrc_pid]),
					str(w_certainty_dict[cwrc_pid]),
					str(g_certainty_dict[cwrc_pid])))

				all_same_as = []
				for authority in [viaf, wikidata, getty]:
					if authority[0]:
						# check if there are multiple identifiers concatenated together in a cell
						if ";" in str(authority[0]):
							id_split = str(authority[0]).split(";")
							for i in id_split:
								all_same_as.append([i, authority[1]])
						else:
							all_same_as.append(authority)


				# only make an updated xml file if there is atleast one high confidence URL
				if max_certainty == "3" or max_certainty == "4":
					# get the XML headers from the original xml file
					content = []
					with open(filepath, "r", encoding="utf-8") as file:
						content = file.readlines()
						content = "".join(content)
						bs_content = bs(content, "lxml")
						header = ""
						for i in bs_content:
							if str(i).startswith("xml"):
								h = "<?" + str(i) + ">"
								header = header + h
						header = header.replace("?><?", "?>\n<?")

					# open original xml again and this time get xml tree
					with open(filepath, "r", encoding="utf-8") as file:
						# add sameAs tag to xml
						tree = ET.parse(file)
						root = tree.getroot()
						identity = root[0][1]

						current_date = datetime.today().strftime('%Y-%m-%d')
						record_info = root[0][0]
						record_change_date = record_info.find(".//recordChangeDate")
						if record_change_date is None:
							print("\nERROR: no record change date present in file: ", filename, "\n")
						else:
							record_change_date.text = str(current_date)

						for same_as in all_same_as:
							same_as_id = add_url(str(same_as[0]))

							# remove decimals from certainty values
							certainty = cert_to_string(same_as[1])

							# if the certainty is less than a 3 then we won't add it
							if certainty == "3" or certainty == "4":
								sameAs = ET.SubElement(identity, 'sameAs')
								sameAs.text = same_as_id.strip()
								sameAs.set('cert', str(certainty))

					# write new xml to file
					xml = (bytes(header + "\n", encoding='utf-8') + ET.tostring(root, encoding='utf-8'))
					xml = xml.decode('utf-8')

					with open(augmented_path_dir + "/" + filename, "w", encoding="utf-8") as output:
						# This could replace with more robust xml fomatter (indentation was wrong once inserted node)
						# But it works with one or more sameAs relationships fine
						output.write(xml.replace("<sameAs", "   <sameAs").replace("</sameAs>", "</sameAs>\n      "))

					update_master_sheet(sheets_client, [batch, "https://cwrc.ca/islandora/object/"+ cwrc_pid, cwrc_pid, str(viaf[0]), viaf[1], getty[0], getty[1], wikidata[0], wikidata[1]])


				# save all low certainty entities in new file
				# save the row if there is no certainty value of 3 or 4 in the row
				else:
					uncert_out.write("\t".join(["https://cwrc.ca/islandora/object/" + cwrc_pid, cwrc_pid, str(viaf[0]), viaf[1], getty[0], getty[1], wikidata[0], wikidata[1]]))
					uncert_out.write("\n")

	upload_uncertain(batch)


if __name__ == "__main__":
	main()
