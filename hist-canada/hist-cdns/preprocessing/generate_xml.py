import pandas as pd
import re
import xml.etree.ElementTree as ET
from collections import defaultdict

"""
mapping file: https://app.diagrams.net/#G1SmNXSmCagZZcgibaab1sfCCRgs7-GODQ#%7B%22pageId%22%3A%22ButgsyDtvoxgVzK5l8OJ%22%7D


APRIL 17 TODO LIST

- fonds using this sheet: https://docs.google.com/spreadsheets/d/17bAdCBeDqDZmi8CptoI9XU8WcJOTxulCnLq6-QR-NjI/edit#gid=0
- links using this sheet: https://docs.google.com/spreadsheets/d/17bAdCBeDqDZmi8CptoI9XU8WcJOTxulCnLq6-QR-NjI/edit#gid=0

- occupations extracted
	- merge into main data: https://docs.google.com/spreadsheets/d/1vectyDib9VGvKCkLzkmJ_f_4pleRbmK4TVm5uLQVPjs/edit#gid=1714113131
	- occupation_extraction tab: https://docs.google.com/spreadsheets/d/1-hCk4LCeJuhdBWKS1_t5a718SsWyTWz58BxACCLkt7M/edit#gid=963027742
- occupations categories
	- make sure we aren't using indigenous terms in red: https://docs.google.com/spreadsheets/d/1ghfzoWUNvC2eMuaSD3KSpQkQ4_VXXdVd74g6u2gqnBA/edit#gid=0
	- https://docs.google.com/spreadsheets/d/1wEealpU5apsNW1lgrdqyBrPXE7vWjiLi0PScG6wAWsA/edit#gid=202732466
	- occupation_categories_translation tab: https://docs.google.com/spreadsheets/d/1-hCk4LCeJuhdBWKS1_t5a718SsWyTWz58BxACCLkt7M/edit#gid=563427271
	- occupations_from_categories tab: https://docs.google.com/spreadsheets/d/1-hCk4LCeJuhdBWKS1_t5a718SsWyTWz58BxACCLkt7M/edit#gid=1916307173

- date uncertainty data
	- https://docs.google.com/spreadsheets/d/12WV_HIvhmS55fY-qqc-jlLyqS1Tt4kGcZG2yR4WvXQw/edit#gid=760440796
	- and turn the uncertainty columns for birth and death dates into xml fields and update x3ml mapping

- x3ml mapping for date and place uncertainty
- x3ml mapping to use newly cleaned full name instead of name parts or title


- make suggestions for how RS setup should change for dcb and ia data
- update gitlab ci for this data
- update gitlab readme for this data
- rename the bucket in S3 (cnds vs cdns)
"""




"""
master_list tab
	- for each row

	<dcb_article>
		<page_link dcb_id="macdonald_john_alexander_12" lang_name="English" lang_id="http://www.lexvo.org/id/iso639-3/eng">http://www.biographi.ca/en/bio/macdonald_john_alexander_12E.html</page_link>
		<page_link dcb_id="macdonald_john_alexander_12" lang_name="French" lang_id="http://www.lexvo.org/id/iso639-3/fra">http://www.biographi.ca/fr/bio/macdonald_john_alexander_12F.html</page_link>
		<page_id>macdonald_john_alexander_12</page_id>
		<page_title>MACDONALD, Sir JOHN ALEXANDER</page_title>
		<gender label="man">http://id.lincsproject.ca/identity/man</gender>
	</dcb_article>


biography_metadata tab
	- ignoring for now




name tab

dcb_id	Permalink	biography_title	full_name	last_name	first_name	First Name Blank?	First Name Capitalized?	dit_name1	dit_name2	dit_name3	dit_name4	other_name1	other_name2	other_name3	other_name4	other_name5	other_name6	other_name7	other_name8	other_name9	other_name10	other_name11	other_name12	other_name13


	<label>
		- one for full name
		- one for first_name + last name if not the same as full_name
	<name>
		- one for first name plus last name and combine for full name
		- one for each dit name with the fist_name and then ditname as last name
		- one for each other_name and have it as full_name


		<label lang="en">John A. Macdonald</label>
		<label lang="en">Sir JOHN ALEXANDER MACDONALD</label>
		<name>
			<full_name>Sir JOHN ALEXANDER MACDONALD</full_name>
			<first_name>Sir JOHN ALEXANDER</first_name>
			<last_name>MACDONALD</last_name>
		</name>

# todo check that no spouses end up with two birth or death dates if they were also dcb people

# if a spouse is not named then i didn't include a marriage event

TODO:
- these bios are about groups not people
	-charite_1
	-downing_john_downing_william_1
- split this into batches. the output file is too big

"""


def split_xml(tree, output_path, num_files):
	root = tree.getroot()

	# Calculate the number of elements in each file
	total_elements = len(root)
	elements_per_file = total_elements // num_files
	remainder = total_elements % num_files

	# Iterate through the root elements and split them into separate files
	start_index = 0
	for i in range(num_files):
		end_index = start_index + elements_per_file
		if i < remainder:
			end_index += 1  # Distribute the remainder evenly across the files

		# Create a new XML tree with the selected elements
		new_root = ET.Element(root.tag)
		new_root.extend(root[start_index:end_index])
		new_tree = ET.ElementTree(new_root)
		ET.indent(tree, space="\t", level=0)

		# Define the output filename
		output_file = output_path.replace(".xml", f"_{i}.xml")

		# Write the XML tree to a new file
		#with open(output_file, "wb") as f:
			#new_tree.write(f)
		new_tree.write(output_file, encoding="utf-8", xml_declaration=False, method="xml", short_empty_elements=True)

		# Update the start index for the next iteration
		start_index = end_index


def remove_empty_elements(elem):
	# Recursively iterate over children
	for child in elem.findall('*'):
		# If child has no text content and no children, it's empty
		if not child.text and not child.findall('*'):
			# Remove the empty element
			elem.remove(child)
		else:
			# Continue recursively for non-empty children
			remove_empty_elements(child)


def get_names_labels_xml(row):
	page_title = row["full_name_case_clean"].strip()

	# full name always has a value and last name does
	full_name = row["full_name_case_clean"].strip()
	first_name = row["first_name_case_clean"]
	last_name = row["last_name_case_clean"].strip()
	if isinstance(first_name, str):
		full_name2 = first_name + " " + last_name
	else:
		full_name2 = last_name
	full_name2 = full_name2.strip()

	# full name and combined first and last go into <labels>
	label_xml = f'<label lang="en">{full_name}</label>'
	if full_name2 != full_name:
		label_xml = label_xml + f'\t\t<label lang="en">{full_name2}</label>'

	# <names> xml
	names_xml = ""
	if isinstance(first_name, str):
		names_xml = names_xml + f"""
		<name>
			<full_name>{full_name2}</full_name>
			<first_name>{first_name}</first_name>
			<last_name>{last_name}</last_name>
		</name>\n"""
	else:
		names_xml = names_xml + f"""
		<name>
			<full_name>{full_name2}</full_name>
			<last_name>{last_name}</last_name>
		</name>\n"""

	# other_name columns goes into names but not labels
	for i in range(1, 14):
		other_name_col = f"other_name{i}"
		other_name = row[other_name_col]
		if isinstance(other_name, str):
			names_xml = names_xml + f"\t\t<name>\n\t\t\t<full_name>{other_name.strip()}</full_name>\n\t\t</name>\n"

	# dit names go into names as the last names
	for i in range(1, 5):
		dit_name_col = f"dit_name{i}"
		dit_name = row[dit_name_col]
		if isinstance(dit_name, str):
			if isinstance(first_name, str):
				names_xml = names_xml + f"""
		<name>
			<full_name>{first_name + ' ' + dit_name}</full_name>
			<first_name>{first_name}</first_name>
			<last_name>{dit_name}</last_name>
		</name>\n"""
			else:
				names_xml = names_xml + f"""
		<name>
			<full_name>{dit_name}</full_name>
			<last_name>{dit_name}</last_name>
		</name>\n"""

	return page_title, full_name, first_name, last_name, names_xml, label_xml


def get_gender_xml(row):
	gender = row.strip()
	gender_xml = ""
	if gender == "male":
		gender_xml = '<gender label="man">http://id.lincsproject.ca/identity/man</gender>'
	elif gender == "female":
		gender_xml = '<gender label="woman">http://id.lincsproject.ca/identity/woman</gender>'
	return gender_xml


def get_marriage_xml(rows, dcb_id, full_name):
	marriage_xml = ""
	for index, row in rows.iterrows():
		# TODO 	machine readable date <date>1867-02-16</date>
		if isinstance(row['spouse_name'], str):
			spouse_id = row['spouse_uri'].replace("http://temp.lincsproject.ca/dcb/", "").replace("http://www.wikidata.org/entity/", "")

			# put the names in alphabetical order for the label
			spouses_ordered = sorted([full_name, row['spouse_name']])

			# change d-m-y to y-m-d
			# change m-y to y-m
			marriage_range_start = ""
			marriage_range_end = ""
			if isinstance(row['marriage_range_start'], str):
				marriage_range_start = convert_date(row['marriage_range_start'])
			if isinstance(row['marriage_range_end'], str):
				marriage_range_end = convert_date(row['marriage_range_end'])

			marriage_xml = marriage_xml + f"""
		<spouse>
			<marriage_label>Marriage between {spouses_ordered[0]} and {spouses_ordered[1]}</marriage_label>
			<marriage_id>http://temp.lincsproject.ca/marriage/{dcb_id}/{spouse_id}</marriage_id>
			<dcb_name>{row['spouse_name']}</dcb_name>
			<wikidata_name>{row['spouse_wikidata_name']}</wikidata_name>
			<uri>{row['spouse_uri']}</uri>
			<date_text>{row['marriage_date_text']}</date_text>
			<date_start>{marriage_range_start}</date_start>
			<date_end>{marriage_range_end}</date_end>
			<birth_date>{row['spouse_birth_date']}</birth_date>
			<death_date>{row['spouse_death_date']}</death_date>
		</spouse>""".replace(">nan<", "><").replace("<date_text></date_text>", "").replace("<wikidata_name></wikidata_name>", "").replace("<birth_date></birth_date>", "").replace("<death_date></death_date>", "").replace("<date_start></date_start>", "").replace("<date_end></date_end>", "")
	return marriage_xml


def get_parent_xml(row):
	parent_xml = ""
	if row.empty:
		pass
	else:
		father_name = row["father_name"]
		father_uri = row["father_uri"]
		mother_name = row["mother_name"]
		mother_uri = row["mother_uri"]

		if isinstance(father_name, str):
			parent_xml = parent_xml + f"""
		<father>
			<dcb_name>{father_name}</dcb_name>
			<uri>{father_uri}</uri>
		</father>
		"""

		if isinstance(mother_name, str):
			parent_xml = parent_xml + f"""
		<mother>
			<dcb_name>{mother_name}</dcb_name>
			<uri>{mother_uri}</uri>
		</mother>
		"""
	return parent_xml


def get_birth_xml(row, place_row, place_cat_row):
	date_xml = ""
	if row.empty:
		pass
	else:
		date_xml = f"""
				<note_text>{row["Baptized Excerpt"]}</note_text>
				<date>
					<text>{row["Birth Date"]}</text>
					<start>{row["Birth Date_Exact_Start"]}</start>
					<end>{row["Birth Date_Exact_End"]}</end>
					<certainty>{row["Birth Date_Uncertainty"]}</certainty>
				</date>""".replace(">nan<", "><")

	place_xml = ""
	if place_cat_row.empty:
		pass
	else:
		for place in [place_cat_row["regionBirth1_recon"], place_cat_row["regionBirth2_recon"], place_cat_row["regionBirth3_recon"]]:
			if isinstance(place, str):
				place_xml = f"""
			<place><uri>{place}</uri></place>"""

	if place_row.empty:
		pass
	else:
		for specific_uri_list in [[place_row["Final_URI_specific_1"], place_row["Final Specific Place Name 1"]], [place_row["Final_URI_specific_2"], place_row["Final Specific Place Name 2"]]]:
			if isinstance(specific_uri_list[0], str):

				if isinstance(place_row["Final_URI_general"], str):
					place_xml = place_xml + f"""
					<nested_place>
						<broader_place>
							<uri>{place_row["Final_URI_general"]}</uri>
						</broader_place>
						<specific_place>
							<english_text>{specific_uri_list[1]}</english_text>
							<uri>{specific_uri_list[0]}</uri>
							<certainty>{place_row["Uncertainty"]}</certainty>
						</specific_place>
					</nested_place>""".replace(">nan<", "><")

				# if there is no general URI then we just output each specific place
				else:
					place_xml = place_xml + f"""
					<place>
						<english_text>{specific_uri_list[1]}</english_text>
						<uri>{specific_uri_list[0]}</uri>
						<certainty>{place_row["Uncertainty"]}</certainty>
					</place>""".replace(">nan<", "><")

	return f"""
			<birth>
			{date_xml}
			{place_xml}
			</birth>
			"""


def get_death_xml(row, place_row):
	date_xml = ""
	if row.empty:
		pass
	else:
		date_xml = f"""
				<note_text>{row["Burial Excerpt"]}</note_text>
				<date>
					<text>{row["Death_Date_Text"]}</text>
					<start>{row["Death_Date_Exact_Start"]}</start>
					<end>{row["Death_Date_Exact_End"]}</end>
					<certainty>{row["Death_Date_Uncertainty"]}</certainty>
				</date>""".replace(">nan<", "><")

	place_xml = ""
	if place_row.empty:
		pass
	else:
		for specific_uri_list in [[place_row["Final_URI_specific_1"], place_row["Final Specific Place Name 1"]], [place_row["Final_URI_specific_2"], place_row["Final Specific Place Name 2"]]]:
			if isinstance(specific_uri_list[0], str):

				if isinstance(place_row["Final_URI_general"], str):
					place_xml = place_xml + f"""
					<nested_place>
						<broader_place>
							<uri>{place_row["Final_URI_general"]}</uri>
						</broader_place>
						<specific_place>
							<english_text>{specific_uri_list[1]}</english_text>
							<uri>{specific_uri_list[0]}</uri>
							<certainty>{place_row["Uncertainty"]}</certainty>
						</specific_place>
					</nested_place>""".replace(">nan<", "><")

				# if there is no general URI then we just output each specific place
				else:
					place_xml = place_xml + f"""
					<place>
						<english_text>{specific_uri_list[1]}</english_text>
						<uri>{specific_uri_list[0]}</uri>
						<certainty>{place_row["Uncertainty"]}</certainty>
					</place>""".replace(">nan<", "><")

	return f"""
			<death>
			{date_xml}
			{place_xml}
			</death>
			"""


def get_fond_xml(row):
	fond_xml = ""
	if row.empty:
		pass
	else:
		if isinstance(row["Canadiana Main Link"], str):
			fond_xml = fond_xml + f"<link><uri>{row['Canadiana Main Link']}</uri></link>"

		if isinstance(row["Peels Prairie Provinces Link"], str):
			fond_xml = fond_xml + f"<link><uri>{row['Peels Prairie Provinces Link']}</uri></link>"

		if isinstance(row["LAC Fond"], str):
			fond_xml = fond_xml + f"<fond><uri>{row['LAC Fond']}</uri><title>Library and Archives Canada - {row['LAC Fond Title']}</title></fond>"

	return fond_xml.replace(">nan<", "><")


def get_occupation_xml(row1, row2):
	"""
	if the row contains "SKIP" then don't use the info

	occupation{i}
	occupation{i}_uri
	occupation{i}_name
	occupation{i}_general_approx_match
	occupation{i}_specific_approx_match
	"""

	occupation_xml = ""
	for row in [row1, row2]:
		if row.empty:
			pass
		else:
			try:
				for i in range(1, 16):
					if isinstance(row[f"occupation{i}_uri"], str):
						if row[f"occupation{i}_uri"] != "SKIP":
							general = row[f"occupation{i}_general_approx_match"]
							specific = row[f"occupation{i}_specific_approx_match"]
							if isinstance(general, str):
								if isinstance(specific, float):
									specific = general
									general = ""

							occupation_xml = occupation_xml + f"""
					<occupation>
						<label lang="en">{row[f"occupation{i}_name"]}</label>
						<uri>{row[f"occupation{i}_uri"]}</uri>
						<specific_approx>{specific}</specific_approx>
						<general_approx>{general}</general_approx>
					</occupation>""".replace(">nan<", "><")
			except KeyError as err:
				# one of the sheets has fewer columns
				pass

	return occupation_xml


def convert_date(date_str):

	if isinstance(date_str, str):
		# Pattern to match d-m-y format
		dmy_pattern = r'(\d{1,2})-(\d{1,2})-(\d{2,4})'
		# Pattern to match m-y format
		my_pattern = r'(\d{1,2})-(\d{2,4})'

		# Check if the string matches the d-m-y pattern
		match = re.match(dmy_pattern, date_str)
		if match:
			d, m, y = match.groups()
			if len(y) == 2:
				y = '20' + y if int(y) <= 21 else '19' + y
			return f'{y}-{m.zfill(2)}-{d.zfill(2)}'

		# Check if the string matches the m-y pattern
		match = re.match(my_pattern, date_str)
		if match:
			m, y = match.groups()
			if len(y) == 2:
				y = '20' + y if int(y) <= 21 else '19' + y
			return f'{y}-{m.zfill(2)}'

	# If the string doesn't match any pattern, return it unchanged
	return str(date_str)


def convert_row(dcb_id, df_master_tab_row, df_names_tab_row, marriage_rows, df_parents_tab_row, df_birth_tab_row, df_birth_place_row, df_birth_place_cats_row, df_death_tab_row, df_death_place_row, df_fond_row, df_occupation_cats_row, df_occupation_row):
	# i is the row index for the current IA data row we are converting into XML
	# generate the full xml

	subject_uri = df_master_tab_row["uri"].strip()
	page_title, full_name, first_name, last_name, names_xml, label_xml = get_names_labels_xml(df_names_tab_row)

	xml = f"""
	<dcb_article>
		<page_link dcb_id="{dcb_id}" lang_name="English" lang_id="http://www.lexvo.org/id/iso639-3/eng">http://www.biographi.ca/en/bio/{dcb_id}E.html</page_link>
		<page_link dcb_id="{dcb_id}" lang_name="French" lang_id="http://www.lexvo.org/id/iso639-3/fra">http://www.biographi.ca/fr/bio/{dcb_id}F.html</page_link>
		<page_id>{dcb_id}</page_id>
		<page_title>{page_title}</page_title>
		<subject_uri>{subject_uri}</subject_uri>
		{get_gender_xml(df_master_tab_row["gender"])}
		{label_xml}{names_xml}
		{get_marriage_xml(marriage_rows, dcb_id, full_name)}
		{get_parent_xml(df_parents_tab_row)}
		{get_birth_xml(df_birth_tab_row, df_birth_place_row, df_birth_place_cats_row)}
		{get_death_xml(df_death_tab_row, df_death_place_row)}
		{get_fond_xml(df_fond_row)}
		{get_occupation_xml(df_occupation_cats_row, df_occupation_row)}
	</dcb_article>"""

	return xml


def convert_reference_row(df, i):
	source_bio = df["source_bio"][i]
	linked_bio = df["linked_bio"][i]
	linked_bio_url = df["linked_bio_url"][i]

	xml = f"""
	<reference>
		<english_source_uri>http://www.biographi.ca/en/bio/{source_bio}E.html</english_source_uri>
		<french_source_uri>http://www.biographi.ca/fr/bio/{source_bio}F.html</french_source_uri>
		<target_dcb_id>{linked_bio}</target_dcb_id>
		<target_uri>{linked_bio_url}</target_uri>
	</reference>"""

	return xml


def convert_place_row(df, i):
	place_xml = ""

	# level 1. define each place
	# Level 1
	# Level 1 - French
	# Level 1 - URI
	place_xml = f"""
	<place>
		<english_text>{df["Level 1"][i]}</english_text>
		<french_text>{df["Level 1 - French"][i]}</french_text>
		<uri>{df["Level 1 - URI"][i]}</uri>
	</place>"""

	# level 2. define each place and list broader place URI as level 1
	# Level 2
	# Level 2 - French
	# Level 2 - URI
	if isinstance(df["Level 2"][i], str):
		place_xml = place_xml + f"""
		<place>
			<english_text>{df["Level 2"][i]}</english_text>
			<french_text>{df["Level 2 - French"][i]}</french_text>
			<uri>{df["Level 2 - URI"][i]}</uri>
			<broader_uri>{df["Level 1 - URI"][i]}</broader_uri>
		</place>"""

	# level 3. define each place and list broader place URI as level 2 URI
	# Level 3
	# Level 3 - French
	# Level 3 - URI
	if isinstance(df["Level 3"][i], str):
		place_xml = place_xml + f"""
		<place>
			<english_text>{df["Level 3"][i]}</english_text>
			<french_text>{df["Level 3 - French"][i]}</french_text>
			<uri>{df["Level 3 - URI"][i]}</uri>
			<broader_uri>{df["Level 2 - URI"][i]}</broader_uri>
		</place>"""

	# level 4. define each place and list broader place URI as level 3 URI
	# Level 4
	# Level 4 - French
	# Level 4 - URI
	if isinstance(df["Level 4"][i], str):
		place_xml = place_xml + f"""
		<place>
			<english_text>{df["Level 4"][i]}</english_text>
			<french_text>{df["Level 4 - French"][i]}</french_text>
			<uri>{df["Level 4 - URI"][i]}</uri>
			<broader_uri>{df["Level 3 - URI"][i]}</broader_uri>
		</place>"""

	return place_xml


def main():

	sample = False

	tab_master_path = "../data/1_source_tsv/DCB_extraction - master_list.tsv"
	tab_names_path = "../data/1_source_tsv/DCB_extraction - names.tsv"
	tab_bio_links_path = "../data/1_source_tsv/DCB_extraction - bio_links.tsv"
	tab_marriage_path = "../data/1_source_tsv/DCB_extraction - marriage_extraction.tsv"
	tab_parent_path = "../data/1_source_tsv/DCB_extraction - parent_extraction.tsv"
	tab_birth_path = "../data/1_source_tsv/DCB_extraction - birth_extraction.tsv"
	tab_death_path = "../data/1_source_tsv/DCB_extraction - death_extraction.tsv"
	tab_birth_place_path = "../data/1_source_tsv/DCB_extraction - birth_place_reconciled.tsv"
	tab_death_place_path = "../data/1_source_tsv/DCB_extraction - death_place_reconciled.tsv"
	tab_birth_cats_path = "../data/1_source_tsv/DCB_extraction - region_birth.tsv"
	tab_place_path = "../data/1_source_tsv/DCB_extraction - region_category_recon.tsv"
	tab_fond_path = "../data/1_source_tsv/DCB_extraction - fonds.tsv"
	tab_occupation_cats_path = "../data/1_source_tsv/DCB_extraction - occupations_from_categories.tsv"
	tab_occupation_path = "../data/1_source_tsv/DCB_extraction - occupation_extraction.tsv"
	output_path = "../data/2_source_xml/hist_cdns.xml"
	reference_output_path = "../data/2_source_xml/reference.xml"
	place_output_path = "../data/2_source_xml/place.xml"

	# load one dataframe for each spreadsheet tab
	# set the dcb_id as the index for each dataframe

	# master tab
	df_master_tab = pd.read_csv(tab_master_path, sep="\t")

	# get a list of all dcb_ids
	dcb_ids = df_master_tab['dcb_id'].tolist()
	df_master_tab.set_index("dcb_id", inplace=True)

	# names tab
	df_named_tab = pd.read_csv(tab_names_path, sep="\t")
	df_named_tab.set_index("dcb_id", inplace=True)

	# marriage tab
	df_marriage_tab = pd.read_csv(tab_marriage_path, sep="\t")

	# parents tab
	df_parents_tab = pd.read_csv(tab_parent_path, sep="\t")
	df_parents_tab.set_index("dcb_id", inplace=True)

	# occupation category tab
	df_occupation_cats_tab = pd.read_csv(tab_occupation_cats_path, sep="\t")
	df_occupation_cats_tab.set_index("dcb_id", inplace=True)

	# occupation tab
	df_occupation_tab = pd.read_csv(tab_occupation_path, sep="\t")
	df_occupation_tab.set_index("dcb_id", inplace=True)

	# fond tab
	df_fond_tab = pd.read_csv(tab_fond_path, sep="\t")
	df_fond_tab.set_index("dcb_id", inplace=True)

	# birth tab and birth place category tab and birth place recon tab
	df_birth_tab = pd.read_csv(tab_birth_path, sep="\t")
	df_birth_tab.set_index("dcb_id", inplace=True)
	df_birth_cats_tab = pd.read_csv(tab_birth_cats_path, sep="\t")
	df_birth_cats_tab.set_index("dcb_id", inplace=True)
	df_birth_place_tab = pd.read_csv(tab_birth_place_path, sep="\t")
	df_birth_place_tab.set_index("dcb_id", inplace=True)

	# death tab and death place recon tab
	df_death_tab = pd.read_csv(tab_death_path, sep="\t")
	df_death_tab.set_index("dcb_id", inplace=True)
	df_death_place_tab = pd.read_csv(tab_death_place_path, sep="\t")
	df_death_place_tab.set_index("dcb_id", inplace=True)

	xml_list = []
	count = 0
	for dcb_id in dcb_ids:
		try:
			marriage_rows = df_marriage_tab[df_marriage_tab['dcb_id'] == dcb_id]
		except KeyError:
			marriage_rows = pd.DataFrame([])

		try:
			parent_row = df_parents_tab.loc[dcb_id]
		except KeyError:
			parent_row = pd.DataFrame([])

		try:
			fond_row = df_fond_tab.loc[dcb_id]
		except KeyError:
			fond_row = pd.DataFrame([])

		try:
			occupation_cats_row = df_occupation_cats_tab.loc[dcb_id]
		except KeyError:
			occupation_cats_row = pd.DataFrame([])

		try:
			occupation_row = df_occupation_tab.loc[dcb_id]
		except KeyError:
			occupation_row = pd.DataFrame([])

		try:
			birth_row = df_birth_tab.loc[dcb_id]
		except KeyError:
			birth_row = pd.DataFrame([])

		try:
			birth_place_row = df_birth_place_tab.loc[dcb_id]
		except KeyError:
			birth_place_row = pd.DataFrame([])

		try:
			birth_place_cats_row = df_birth_cats_tab.loc[dcb_id]
		except KeyError:
			birth_place_cats_row = pd.DataFrame([])

		try:
			death_row = df_death_tab.loc[dcb_id]
		except KeyError:
			death_row = pd.DataFrame([])

		try:
			death_place_row = df_death_place_tab.loc[dcb_id]
		except KeyError:
			death_place_row = pd.DataFrame([])

		if sample:
			if count < 100:
				xml_list.append(convert_row(dcb_id, df_master_tab.loc[dcb_id], df_named_tab.loc[dcb_id], marriage_rows, parent_row, birth_row, birth_place_row, birth_place_cats_row, death_row, death_place_row, fond_row, occupation_cats_row, occupation_row))
		else:
			xml_list.append(convert_row(dcb_id, df_master_tab.loc[dcb_id], df_named_tab.loc[dcb_id], marriage_rows, parent_row, birth_row, birth_place_row, birth_place_cats_row, death_row, death_place_row, fond_row, occupation_cats_row, occupation_row))

		count += 1

	# bio_links tab (<reference> elements are saved to a separate XML file
	reference_list = []
	df_bio_links_tab = pd.read_csv(tab_bio_links_path, sep="\t")
	for row_index in range(df_bio_links_tab["source_bio"].size):
		reference_list.append(convert_reference_row(df_bio_links_tab, row_index))

	# full place hierarchy saved to a separate XML file
	place_list = []
	df_place_tab = pd.read_csv(tab_place_path, sep="\t")
	for row_index in range(df_place_tab["Combined Location Name"].size):
		place_list.append(convert_place_row(df_place_tab, row_index))

	# main person xml
	person_root = ET.fromstring("<xml>" + "".join(xml_list).replace("&", "%26").replace("\t", "").replace("\n", "") + "</xml>")
	remove_empty_elements(person_root)
	person_tree = ET.ElementTree(person_root)
	ET.indent(person_tree, space="\t", level=0)
	split_xml(person_tree, output_path, 10)

	# reference xml is output into multiple files
	reference_root = ET.fromstring("<xml>" + "".join(reference_list).replace("&", "%26").replace("\t", "").replace("\n", "") + "</xml>")
	reference_tree = ET.ElementTree(reference_root)
	ET.indent(reference_tree, space="\t", level=0)
	split_xml(reference_tree, reference_output_path, 10)

	# place xml
	place_root = ET.fromstring("<xml>" + "".join(place_list).replace("&", "%26").replace("\t", "").replace("\n", "") + "</xml>")
	place_tree = ET.ElementTree(place_root)
	ET.indent(place_tree, space="\t", level=0)
	place_tree.write(place_output_path, encoding="utf-8", xml_declaration=False, method="xml", short_empty_elements=True)


main()
