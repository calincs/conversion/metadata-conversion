from rdflib import Graph, URIRef
from rdflib.namespace import RDF
import re
import requests
import os
import json
import zipfile
import io
import time


def process_3m(input_path, map_file, policy_file, output_folder, post_process_url, encoding, uuid_test_size, output_format):
	"""
	This function sends your data to X3ML.
	"""

	# Checking if input_file is a file or folder
	is_file = os.path.isfile(input_path)
	is_directory = os.path.isdir(input_path)

	if is_directory:
		files = []
		for filename in os.listdir(input_path):
			if filename.endswith(".xml"):
				files.append(os.path.join(input_path, filename))
	elif is_file:
		files = [input_path]
	else:
		print("Error: Please input a valid input file name or path")
		return

	# send each input file to X3ML engine
	for input_file in files:
		print(f"Converting {input_file}")
		output_filename = input_file.split("/")[-1].replace(".xml", ".ttl")

		url = post_process_url + f"/conversion/x3ml/custom?output_file={output_filename}&format_output_file={output_format}&uuid_test_size={uuid_test_size}"
		payload = {}
		files = [
			('input_file', ('input.xml', open(input_file, 'rb'), 'text/xml')),
			('map_file', ('Mapping.x3ml', open(map_file, 'rb'), 'application/octet-stream')),
			('policy_file', ('generator.xml', open(policy_file, 'rb'), 'text/xml'))]
		headers = {}
		response = requests.request("POST", url, headers=headers, data=payload, files=files)
		response.encoding = encoding

		if str(response.status_code).startswith("2"):
			try:
				z = zipfile.ZipFile(io.BytesIO(response.content))
				z.extractall(output_folder)
			except Exception as err:
				print(f"Failed to save X3ML conversion output\n", response.text)
				print(err)
		else:
			print(f"Error: Your file {input_file} could not be converted.")
			print(response.status_code)
			print(response.text)


def combine_rdf(post_process_url: str, files: list, output_rdf_path: str):
	# Takes a list of .ttl file names as input and combined them into a single .ttl file
	# Saves combined .ttl file
	payload = {}
	headers = {}

	request_files = []
	for file in files:
		filename = file.split("/")[-1]
		request_files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	response = requests.request("POST", post_process_url + "/combine", headers=headers, data=payload, files=request_files)

	with open(output_rdf_path, "w") as out_file:
		out_file.write(response.text)


def replacement_simple(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	with open(output_rdf_path, 'w') as f:
		f.write(content)


def replacement(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	# replace spaces in temp lincs URIs with underscores
	# the temp URIs had several invalid characters
	original_matches = re.findall(r'<http:\/\/temp\.lincsproject\.ca\/(.+?>)', content)
	for orig_match in list(set(original_matches)):
		rep_match = orig_match.strip()
		rep_match = rep_match.replace(" ", "_")
		rep_match = rep_match.replace('"', "_")
		rep_match = rep_match.replace('[', "_")
		rep_match = rep_match.replace(']', "_")
		rep_match = rep_match.replace("'", "_")
		rep_match = rep_match.replace("`", "_")
		rep_match = rep_match.replace(",", "_")
		rep_match = rep_match.replace("(", "_")
		rep_match = rep_match.replace(")", "_")
		rep_match = rep_match.lower()
		content = content.replace(orig_match, rep_match)

	with open(output_rdf_path, 'w') as f:
		f.write(content)


def external_labels(post_process_url, input_path, output_path, authority):
	if authority == "wikidata":
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs&lang_code=fr'
	else:
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs'
	payload = {}
	headers = {}
	files = [('file', ('test_labels.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	try:
		z = zipfile.ZipFile(io.BytesIO(response.content))
		z.extractall(output_path)
	except Exception as err:
		print(f"Output failed for external labels with {authority}\n", response.text)
		print(err)


def wikidata_coordinates(input_path, output_path, post_process_url):
	url = post_process_url + f'/coordinates/wikidata?skip_predicate=owl:sameAs'
	payload = {}
	headers = {}
	files = [('file', ('test_coordinates.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)

	with open(output_path, 'w') as f:
		f.write(response.text)


def handle_redirects(post_process_url, input_file_path, output_path):
	# see if there is a redirects.json file to use as replacements
	try:
		with open(f"{output_path}redirects.json") as redirects:
			redirects_dict = json.loads(redirects.read())
			redirect_list = redirects_dict.items()
			with open("redirects.tsv", "w") as out_f:
				count = 0
				for r in redirect_list:
					if count < len(redirect_list) - 1:
						out_f.write(r[0] + "\t" + r[1] + "\n")
						if "wikidata" in r[0]:
							out_f.write(r[0].replace("wikidata:", "<http://www.wikidata.org/entity/") + ">\t" + r[1] + "\n")
					else:
						out_f.write(r[0] + "\t" + r[1])
						if "wikidata" in r[0]:
							out_f.write("\n" + r[0].replace("wikidata:", "<http://www.wikidata.org/entity/") + ">\t" + r[1])
					count += 1
			replacement_simple(post_process_url, input_file_path, "redirects.tsv", False, input_file_path)
	except Exception as err:
		print("Did not replace redirects. Error: ", err)


def validation(input_path_list, output_path, post_process_url, analyze="True"):
	files = []
	for file in input_path_list:
		filename = file.split("/")[-1]
		files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	payload = {
		'encoding': 'utf-8',
		'rdf_format': 'turtle',
		'analyze': analyze}
	headers = {}
	response = requests.post(post_process_url + '/validate/', headers=headers, files=files, data=payload)

	with open(output_path + "rdf_validation.json", "w") as out_file:
		out_file.write(response.text)


def get_replacements(mapping_file):
	replacements_list = []
	with open(mapping_file, "r") as mappings:
		for line in mappings:
			line_split = line.strip("\n").split("\t")
			replacements_list.append(line_split)
	return replacements_list


def condense_ttl_graph(input_ttl_dir, output_ttl_path):
	# get a list of all .ttl files in the rootdir. This includes within nested folders
	filelist = []
	for subdir, dirs, files in os.walk(input_ttl_dir):
		for file in files:
			filepath = subdir + os.sep + file

			if filepath.endswith(".ttl"):
				filelist.append(filepath)

	# sort the filelist so that they always get added in the same order
	filelist.sort()

	# creates graph and outputs the new graph to replace the original file
	g_people = Graph()
	g_refs = Graph()

	for ttl_path in filelist:
		print(ttl_path)
		if "reference" in ttl_path:
			g_refs.parse(ttl_path)
		else:
			g_people.parse(ttl_path)

	g_refs.serialize(destination=output_ttl_path.replace(".ttl", "_references.ttl"))
	g_people.serialize(destination=output_ttl_path)


def list_wikidata_people(input_ttl_dir, output_file):
	# get a list of all .ttl files in the rootdir. This includes within nested folders
	filelist = []
	for subdir, dirs, files in os.walk(input_ttl_dir):
		for file in files:
			filepath = subdir + os.sep + file

			if filepath.endswith(".ttl"):
				filelist.append(filepath)

	# creates graph and outputs the new graph to replace the original file
	g = Graph()

	for ttl_path in filelist:
		g.parse(ttl_path)

	wikidata_list = []
	# Iterate through the triples
	for subject, predicate, obj in g:
		# Check if the subject is of type E21_Person and belongs to the Wikidata namespace
		if (str(predicate) == "http://www.w3.org/1999/02/22-rdf-syntax-ns#type" and str(obj) == "http://www.cidoc-crm.org/cidoc-crm/E21_Person" and str(subject).startswith("http://www.wikidata.org/entity/")):
			wikidata_list.append(str(subject))

	wikidata_list = list(set(wikidata_list))
	with open(output_file, 'w') as file:
		for item in wikidata_list:
			file.write(str(item) + '\n')
	return wikidata_list


def list_geonames_places(input_ttl_dir, output_file):
	# get a list of all .ttl files in the rootdir. This includes within nested folders
	filelist = []
	for subdir, dirs, files in os.walk(input_ttl_dir):
		for file in files:
			filepath = subdir + os.sep + file

			if filepath.endswith(".ttl"):
				filelist.append(filepath)

	# creates graph and outputs the new graph to replace the original file
	g = Graph()

	for ttl_path in filelist:
		g.parse(ttl_path)

	geonames_list = []
	# Iterate through the triples
	for subject, predicate, obj in g:
		# Check if the subject is of type E21_Person and belongs to the Wikidata namespace
		if str(subject).startswith("https://sws.geonames.org/"):
			geonames_list.append(str(subject))

	geonames_list = sorted(list(set(geonames_list)))
	with open(output_file, 'w') as file:
		for item in geonames_list:
			file.write(str(item) + '\n')
	return geonames_list


def query_wikidata_batch(wd_uris, output_file, output_ttl, batch_size=50):

	with open(output_file, 'w', encoding='utf-8') as tsv_file:
		with open(output_ttl, 'w', encoding='utf-8') as ttl_file:
			ttl_file.write("@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n@prefix wikidata: <http://www.wikidata.org/entity/> .\n@prefix viaf:  <http://viaf.org/viaf/> .\n\n")

			for i in range(0, len(wd_uris), batch_size):
				batch_uris = wd_uris[i:i + batch_size]
				wd_query = """
				SELECT ?item ?viaf WHERE {
					VALUES ?item { %s }
					?item wdt:P214 ?viaf.
				}
				""" % ' '.join(['wd:' + uri.split('/')[-1] for uri in batch_uris])

				response = requests.get('https://query.wikidata.org/sparql', params={'query': wd_query, 'format': 'json'})
				data = response.json()

				for item in data['results']['bindings']:
					wd_uri = item['item']['value']
					wd_uri = wd_uri.replace("http://www.wikidata.org/entity/", "")
					viaf = item['viaf']['value']
					tsv_file.write(f"wikidata:{wd_uri} \tviaf:{viaf} \n")
					ttl_file.write(f"viaf:{viaf} owl:sameAs wikidata:{wd_uri} .\n")
				time.sleep(15)


def split_ttl_file(post_process_url, input_file, output_dir):
	url = f"{post_process_url}/split?file_size=10000"
	payload = {}
	files = [('file', (input_file, open(input_file, 'rb'), 'application/octet-stream'))]
	headers = {}
	response = requests.request("POST", url, headers=headers, data=payload, files=files)

	if str(response.status_code).startswith("2"):
		try:
			z = zipfile.ZipFile(io.BytesIO(response.content))
			z.extractall(output_dir)
		except Exception as err:
			print(f"Failed to save X3ML conversion output\n", response.text)
			print(err)
	else:
		print(f"Error: Your file {input_file} could not be converted.")
		print(response.status_code)
		print(response.text)


def main():

	data_x3ml_input_dir = "../data/2_source_xml/"
	data_x3ml_output_dir = "../data/3_x3ml_output/"

	data_postprocessing_output_dir = "../data/4_postprocessing_output/"

	x3ml_mapping_dir = "../x3ml/mapping/"
	x3ml_mapping_file = x3ml_mapping_dir + "3m-x3ml-output.x3ml"
	x3ml_generator_file = x3ml_mapping_dir + "generator-policy.xml"

	postprocessing_error_dir = "errors/"

	data_final_path = "../../../Datasets/hist-cdns/"

	data_file_list = ["place", "manual_images"]
	for i in range(0, 10):
		data_file_list.append(f"reference_{i}")
		data_file_list.append(f"hist_cdns_{i}")

	#post_process_url = "http://0.0.0.0:80"
	post_process_url = "https://postprocess.lincsproject.ca"

	x3ml_step = True
	x3ml_skip_reference = True  # don't need to reprocess reference files as often
	clean_step = True
	wikidata_people_step = False
	geonames_list_step = False
	wikidata_coordinates_step = False  # Done separately from this code for geonames since there were so many. To run again if more are added
	labels_step = False  # TODO LINCS labels, note this is done for geonames in the coords step
	redirects_step = False
	final_save_step = True
	validation_step = True

	# Run each XML file through X3ML
	for data_file in data_file_list:
		print()
		print("_" * 100)
		print(f"Processing {data_file}")

		if x3ml_step:
			if x3ml_skip_reference:
				if "reference" in data_file:
					pass
				else:
					print("x3ml step")
					process_3m(
						data_x3ml_input_dir + data_file + ".xml",
						x3ml_mapping_file,
						x3ml_generator_file,
						data_x3ml_output_dir,
						post_process_url,
						"utf-8",
						"2",
						"text/turtle")
			else:
				print("x3ml step")
				process_3m(
					data_x3ml_input_dir + data_file + ".xml",
					x3ml_mapping_file,
					x3ml_generator_file,
					data_x3ml_output_dir,
					post_process_url,
					"utf-8",
					"2",
					"text/turtle")

		if clean_step:
			print("clean step")
			replacement(post_process_url, f"{data_x3ml_output_dir}{data_file}.ttl", "replacements.tsv", False, f"{data_postprocessing_output_dir}{data_file}.ttl")
			replacement(post_process_url, f"{data_postprocessing_output_dir}{data_file}.ttl", "wikidata_to_viaf.tsv", False, f"{data_postprocessing_output_dir}{data_file}.ttl")

			print("file validation")
			validation([f"{data_postprocessing_output_dir}{data_file}.ttl"], postprocessing_error_dir + data_file + "_", post_process_url, analyze="False")

		# These should run on the output from replacements
		# The output in redirects.json were manually added to entity_replacements_noRegex.tsv and then the cleaning step was re-run
		# That step needs to be done again if the entities change
		if labels_step:
			if "reference" not in data_file:  # we don't need to get labels for the reference files since all the URIs are in the main files too
				for authority in ["wikidata", "viaf"]:
					print("label step: ", authority)
					external_labels(post_process_url, f"{data_postprocessing_output_dir}{data_file}.ttl", f"{data_postprocessing_output_dir}{data_file}/", authority)

		# if redirects_step:
		# 	print("handle redirects")
		# 	handle_redirects(post_process_url, final_rdf_paths[0], postprocess_output_path)

		if wikidata_coordinates_step:
			if "reference" not in data_file:
				wikidata_coordinates(f"{data_postprocessing_output_dir}{data_file}.ttl", f"{data_postprocessing_output_dir}{data_file}/wikidata_coordinates.ttl", post_process_url)

	if wikidata_people_step:
		wikidata_uris = list_wikidata_people(data_postprocessing_output_dir, "wikidata_people.txt")

		# Query Wikidata in batches to get VIAF URIs
		query_wikidata_batch(wikidata_uris, "wikidata_to_viaf.tsv", f"{data_postprocessing_output_dir}wikidata_viaf_sameas.ttl", 400)

	# use the get_geonames_coords.py to make the file TODO integrate that here too though it will be an LDE API endpoint soon
	if geonames_list_step:
		list_geonames_places(data_postprocessing_output_dir, "geonames_places.txt")

	if final_save_step:
		# print("Condensing all ttl and labels into one graph and saving in main dataset folder")
		condense_ttl_graph(data_postprocessing_output_dir, f"{data_final_path}hist-cdns.ttl")

		print("Splitting file that is too large for triplestore loading")
		split_ttl_file(post_process_url, f"{data_final_path}hist-cdns.ttl", data_final_path)
		# rename split files
		for file_name in os.listdir(data_final_path):
			if "split_file" in file_name:
				os.rename(data_final_path + file_name, data_final_path + file_name.replace("split_file", "hist-cdns"))

		print("Deleting file that has been split into parts")
		if os.path.exists(f"{data_final_path}hist-cdns.ttl"):
			os.remove(f"{data_final_path}hist-cdns.ttl")

	if validation_step:
		print("RDF Validation")
		file_list = []
		files_in_folder = os.listdir(data_final_path)
		for file_name in files_in_folder:
			if file_name.endswith(".ttl"):
				file_list.append(data_final_path + file_name)
		validation(file_list, postprocessing_error_dir, post_process_url, analyze="False") 		# TODO turn analyze back on when needed but it's too much data to process quickly


main()
