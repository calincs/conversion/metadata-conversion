import requests
from rdflib import Graph, Literal, RDFS, URIRef, RDF


def get_coordinates_from_id(id):
	# get the first response with coordinates
	url = f"https://geonames.lincsproject.ca/geocode?id={id}"
	response = requests.get(url)

	label = None
	lat = None
	lon = None

	if response.status_code == 200:
		data = response.json()
		if data.get("success"):
			for info in data["data"]:
				if "properties" in info:
					if "name" in info["properties"]:
						label = info["properties"]["name"]
				if "geometry" in info:
					if "coordinates" in info["geometry"]:
						[lon, lat] = info["geometry"]["coordinates"]
						return lon, lat, label
	return lon, lat, label


def main():
	g = Graph()
	g.bind('geonames', 'https://sws.geonames.org/')
	g.bind('crm', 'http://www.cidoc-crm.org/cidoc-crm/')

	# Read list of IDs from a text file
	with open("geonames_places.txt", "r") as file:
		id_list = file.read().replace("https://sws.geonames.org/", "").splitlines()
	id_list = list(set(id_list))

	# Send requests for each ID and get coordinates
	for geo_id in id_list:
		lon, lat, label = get_coordinates_from_id(geo_id.replace("https://sws.geonames.org/", ""))

		s = URIRef(f'https://sws.geonames.org/{geo_id}')

		# Add label to graph
		if label:
			o = Literal(label, lang='en')
			g.add((s, RDFS.label, o))
			# Add place definition to graph
			g.add((s, RDF.type, URIRef("http://www.cidoc-crm.org/cidoc-crm/E53_Place")))
		else:
			print("No label found for : ", geo_id)

		if lat and lon:
			try:
				# Add place definition to graph
				g.add((s, RDF.type, URIRef("http://www.cidoc-crm.org/cidoc-crm/E53_Place")))

				# Add coordinates to graph
				coords = Literal(f"POINT({lon} {lat})")
				g.add((s, URIRef("http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by"), coords))
			except Exception as err:
				print("Failed to add coordinates: ", err)
		else:
			print("No coordinates found for : ", geo_id)

	with open("../data/4_postprocessing_output/geonames_coordinates.ttl", "w", encoding="utf-8") as f:
		f.write(g.serialize(format='turtle'))


if __name__ == "__main__":
	main()
