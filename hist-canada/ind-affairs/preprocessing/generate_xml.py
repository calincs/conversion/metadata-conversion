import pandas as pd

# this code takes a tsv download of the cleaned and reconciled spreadsheet at https://docs.google.com/spreadsheets/d/11wiDaoEhAD-l2Oc_xf3jE_A_TanE3NKnsTnUKr-X_tg
# it outputs an XML file that can be used as input for the X3ML mapping for this project


def label_xml(first, last, full):
	# full name always has a value
	labels = f"<label>{full}</label>"

	# if either first or last name exist and first last != full name
	if isinstance(first, str) and isinstance(last, str):
		if f"{first} {last}" != full:
			labels = labels + """
		<label>%s %s</label>""" % (first, last)
	return labels


def first_name_xml(first):
	first_name = ""
	if isinstance(first, str):
		first_name = f"<first_name>{first}</first_name>"
	return first_name


def last_name_xml(last):
	last_name = ""
	if isinstance(last, str):
		last_name = f"<last_name>{last}</last_name>"
	return last_name


def dcb_links_xml(dcb_id):
	dcb_links = ""
	if isinstance(dcb_id, str):
		dcb_links = """
		<page_link dcb_id="%s" lang_name="English" lang_id="http://www.lexvo.org/id/iso639-3/eng">http://www.biographi.ca/en/bio/%sE.html</page_link>
		<page_link dcb_id="%s" lang_name="French" lang_id="http://www.lexvo.org/id/iso639-3/fra">http://www.biographi.ca/fr/bio/%sF.html</page_link>""" % (dcb_id, dcb_id, dcb_id, dcb_id)
	return dcb_links


def potential_matches_xml(wikidata, dcb):
	potential_match = ""
	if isinstance(wikidata, str):
		potential_match = """
		<potential_match>
			<wikidata>%s</wikidata>
			<page_link dcb_id="%s" lang_name="English" lang_id="http://www.lexvo.org/id/iso639-3/eng">http://www.biographi.ca/en/bio/%sE.html</page_link>
			<page_link dcb_id="%s" lang_name="French" lang_id="http://www.lexvo.org/id/iso639-3/fra">http://www.biographi.ca/fr/bio/%sF.html</page_link>
		</potential_match>""" % (wikidata, dcb, dcb, dcb, dcb)
	return potential_match


def mentioned_dcb_xml(dcb_ids):
	# this field can have many values separated by a semi colon
	dcb_links = ""

	if isinstance(dcb_ids, str):
		dcb_id_list = dcb_ids.split(";")
		for dcb_id in dcb_id_list:
			dcb_links = dcb_links + """
		<dcb_mention>
			<page_link dcb_id="%s" lang_name="English" lang_id="http://www.lexvo.org/id/iso639-3/eng">http://www.biographi.ca/en/bio/%sE.html</page_link>
			<page_link dcb_id="%s" lang_name="French" lang_id="http://www.lexvo.org/id/iso639-3/fra">http://www.biographi.ca/fr/bio/%sF.html</page_link>
		</dcb_mention>""" % (dcb_id, dcb_id, dcb_id, dcb_id)
	return dcb_links


def father_xml(uri, name):
	father = ""
	if isinstance(uri, str):
		father = """
		<father>
			<uri>%s</uri>
			<dcb_name>%s</dcb_name>
		</father>""" % (uri, name)
	return father


def mother_xml(uri, name):
	mother = ""
	if isinstance(uri, str):
		mother = """
		<mother>
			<uri>%s</uri>
			<dcb_name>%s</dcb_name>
		</mother>""" % (uri, name)
	return mother


def date_xml(val):
	# date is never blank
	date = """
		<date>%s</date>
		<year>%s</year>""" % (str(val), str(val))
	return date


def location_xml(broad_name, broad_uri, specific_uri, specific_name, lat, lon):
	location = ""
	coords_element = ""
	if isinstance(specific_uri, str):

		if isinstance(lat, float) and isinstance(lon, float) and str(lat) != "nan" and str(lon) != "nan":
			if specific_uri.startswith("http://temp.lincsproject.ca/"):
				coords_element = f"""
					<lat>{lat}</lat>
					<lon>{lon}</lon>"""

		if isinstance(broad_uri, str):
			broad_uri_list = broad_uri.split(";")
			broad_element = ""
			for broad_instance in broad_uri_list:
				broad_element = broad_element + f"<broader_place><uri>{broad_instance}</uri></broader_place>"

			location = f"""
		<place>
			{broad_element}
			<specific_place>
				<uri>{specific_uri}</uri>
				<name>{specific_name}</name>
				{coords_element}
			</specific_place>
		</place>"""
		else:
			location = f"""
		<place>
			<specific_place>
				<uri>{specific_uri}</uri>
				<name>{specific_name}</name>
				{coords_element}
			</specific_place>
		</place>"""

	return location


def agency_xml(uri, name):
	agency = ""
	if isinstance(uri, str):
		agency = """
		<agency>
			<mention>https://data2.archives.ca/pdf/pdf002/10-157-133712_E_Guide_open.pdf</mention>
			<mention>https://publications.gc.ca/collections/collection_2018/aanc-inac/R32-413-1960-eng.pdf</mention>
			<uri>%s</uri>
			<name>%s</name>
		</agency>""" % (uri, name)
	return agency


def dfa_xml(val):
	dfa = ""
	if isinstance(val, str) or isinstance(val, float):
		dfa = """
		<dfa>%s</dfa>""" % (val)
	return dfa


def lac_xml(val):
	# lac is never blank
	# TODO probably need to URL encode value
	lac = """
		<lac>%s</lac>""" % (str(val))
	return lac.replace("&", "%26")


def occupation_xml(df, i):
	occupation = ""
	if df["is_occupation"][i] == "Yes":
		occupation = """
		<occupation>
			<department>http://temp.lincsproject.ca/department_of_indian_affairs</department>
			<label lang="en">%s</label>""" % (df["occupation_original_text"][i])

		if isinstance(df["occupation1"][i], str):
			occupation = occupation + """
			<type>
				<name lang="en">%s</name>
				<uri>%s</uri>
				<specific_approx>%s</specific_approx>
				<general_approx>%s</general_approx>
			</type>""" % (df["occupation1"][i], df["occupation1_specific_exact_match"][i], df["occupation1_specific_approx_match"][i], df["occupation1_general_approx_match"][i])

		if isinstance(df["occupation2"][i], str):
			occupation = occupation + """
			<type>
				<name lang="en">%s</name>
				<uri>%s</uri>
				<specific_approx>%s</specific_approx>
				<general_approx>%s</general_approx>
			</type>""" % (df["occupation2"][i], df["occupation1_specific_exact_match"][i], df["occupation1_specific_approx_match"][i], df["occupation1_general_approx_match"][i])

		if isinstance(df["agency_uri"][i], str):
			occupation = occupation + """
			<agency_uri>%s</agency_uri>""" % (df["agency_uri"][i])

		occupation = occupation +"""
		</occupation>"""

		occupation = occupation.replace('<specific_approx>nan</specific_approx>', "").replace('<general_approx>nan</general_approx>', "")

	return occupation


def convert_row(df, i):
	# i is the row index for the current IA data row we are converting into XML

	# only include fields when they are not blank
	first_name = first_name_xml(df["person_first_name"][i])
	last_name = last_name_xml(df["person_last_name"][i])
	dcb_links = dcb_links_xml(df["person_dcb_id"][i])
	potential_match = potential_matches_xml(df["person_potential_wikidata_uri"][i], df["person_potential_dcb_id"][i])
	mentioned_in_dcb = mentioned_dcb_xml(df["mentioned_in_dcb_id"][i])
	labels = label_xml(df["person_first_name"][i], df["person_last_name"][i], df["person_name"][i])
	father = father_xml(df["father_uri"][i], df["father_name"][i])
	mother = mother_xml(df["mother_uri"][i], df["mother_name"][i])
	occupation = occupation_xml(df, i)
	date = date_xml(df["date"][i])
	lac = lac_xml(df["lac_url_new"][i])
	#dfa = dfa_xml(df["dfa"][i])
	agency = agency_xml(df["agency_uri"][i], df["agency"][i])

	# we use coordinates only for exact locations that have lincs uris. only location 1 has any
	location1 = location_xml(df["loc_mer"][i], df["loc_mer_general_uri"][i], df["loc_mer1_exact_uri"][i], df["loc_mer1"][i], df["lat"][i], df["lon"][i])
	location2 = location_xml(df["loc_mer"][i], df["loc_mer_general_uri"][i], df["loc_mer2_exact_uri"][i], df["loc_mer2"][i], df["lat"][i], df["lon"][i])

	# generate the full xml
	xml = """
	<dcb_article>
		<subject_uri>%s</subject_uri>
		<subject_id>%s</subject_id>
		%s
		<name>
			<full_name>%s</full_name>
			%s
			%s
		</name>
		<entry_id>%s</entry_id>%s%s%s%s%s%s%s%s%s%s%s
	</dcb_article>""" % (
							df["person_uri"][i],
							df["person_id"][i],
							labels,
							df["person_name"][i],
							first_name,
							last_name,
							df["entry_id"][i],
							dcb_links,
							potential_match,
							mentioned_in_dcb,
							father,
							mother,
							occupation,
							date,
							lac,
							agency,
							location1,
							location2)

	return xml


def main():

	sample = False

	input_path = "../data/1_source_tsv/Department of Indian Affairs data (Ben Hoy) - indianagentsFULL2.tsv"
	output_path = "../data/2_source_xml/Department_of_Indian_Affairs_data_(Ben Hoy).xml"

	xml_list = []

	df = pd.read_csv(input_path, sep="\t")

	if sample:
		for row_index in range(20):
			xml_list.append(convert_row(df, row_index))

	else:
		for row_index in range(df["person_id"].size):
			xml_list.append(convert_row(df, row_index))

	with open(output_path, "w") as f_out:
		f_out.write('<?xml version="1.0" encoding="UTF-8"?>\n<DIA>')
		for xml in xml_list:
			f_out.write(xml.replace("&", "%26"))
		f_out.write("\n</DIA>")


main()
