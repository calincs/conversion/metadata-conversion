

with open("DCB.tsv", "r") as dcb_in:
	lines = dcb_in.readlines()
	dcb = {}
	for line in lines:
		split = line.strip().split("\t")
		split.extend([''] * (14 - len(split)))

		first_name = split[2].lower().replace(".", " ").replace("  ", " ").strip()
		last_name = split[3].lower().replace(".", " ").replace("  ", " ").strip()
		wiki_name = split[6].lower().replace(".", " ").replace("  ", " ")
		name_list = list(set([wiki_name, first_name + " " + last_name]))
		name_list = [i for i in name_list if i]
		dcb[split[0]] = {
		"first_name": first_name,
		"last_name": last_name,
		"names": name_list,
		"birth_year": split[10].strip(),
		"death_year": split[13].strip()}


with open("IA.tsv", "r") as ia_in:
	lines = ia_in.readlines()
	ia = {}
	for line in lines:
		split = line.strip().split("\t")
		split.extend([''] * (6 - len(split)))

		alt_names_temp = split[4]
		alt_names = []
		if split[4].endswith(" | "):
			alt_names_temp = split[4][:-3]
		alt_names_temp = alt_names_temp.split(" | ")
		for name in alt_names_temp:
			alt_names.append(name.lower().replace(".", " ").replace("  ", " ").strip())

		first_name = split[1].lower().replace(".", " ").replace("  ", " ").strip()
		last_name = split[2].lower().replace(".", " ").replace("  ", " ").strip()
		full_name = split[3].lower().replace(".", " ").replace("  ", " ").strip()

		name_list = list(set(alt_names + [full_name, first_name + " " + last_name]))
		name_list = [i for i in name_list if i]
		ia[split[0]] = {
		"first_name": first_name,
		"last_name": last_name,
		"names": name_list,
		"first_date": split[5],
		"last_date": split[6]}


candidate_matches = {}
for ia_id in ia:

	#print("_" * 60)
	candidate_matches[ia_id] = []
	for dcb_id in dcb:

		exact = False

		for ia_name in ia[ia_id]["names"]:
			if ia_name in dcb[dcb_id]["names"]:
				exact = True

			if dcb[dcb_id]["first_name"] != "" and len(dcb[dcb_id]["first_name"]) <= 3 and dcb[dcb_id]["last_name"] in ia_name and ia[ia_id]["first_name"].startswith(dcb[dcb_id]["first_name"][0]):
				exact = True

		if len(dcb[dcb_id]["first_name"]) > 1 and len(dcb[dcb_id]["last_name"]) > 1:
			if dcb[dcb_id]["first_name"] in ia_name and dcb[dcb_id]["last_name"] in ia_name:
				exact = True

		for dcb_name in dcb[dcb_id]["names"]:
			if dcb_name in ia[ia_id]["names"]:
				exact = True

			if len(ia[ia_id]["first_name"]) > 1 and len(ia[ia_id]["last_name"]) > 1:
				if ia[ia_id]["first_name"] in dcb_name and ia[ia_id]["last_name"] in dcb_name:
					exact = True

			if ia[ia_id]["first_name"] != "" and len(ia[ia_id]["first_name"]) <= 3 and ia[ia_id]["last_name"] in dcb_name and dcb[dcb_id]["first_name"].startswith(ia[ia_id]["first_name"][0]):
				exact = True

		if dcb[dcb_id]["first_name"] != "" and len(dcb[dcb_id]["first_name"]) <= 3 and dcb[dcb_id]["last_name"] in ia[ia_id]["names"] and ia[ia_id]["first_name"].startswith(dcb[dcb_id]["first_name"][0]):
			exact = True

		if exact:
			if dcb[dcb_id]["birth_year"] != "":
				# if born after first start date, not it
				if int(dcb[dcb_id]["birth_year"]) > int(ia[ia_id]["first_date"]):
					exact = False

				# if the person was less than 15
				elif int(ia[ia_id]["first_date"]) - int(dcb[dcb_id]["birth_year"]) < 15:
					exact = False

			if dcb[dcb_id]["death_year"] != "":
				# if dead before last start date not it
				if int(dcb[dcb_id]["death_year"]) < (int(ia[ia_id]["last_date"]) - 3):
					# print(dcb[dcb_id]["death_year"])
					# print(ia[ia_id]["last_date"])
					# print("_" * 30)
					exact = False

		if exact:
			# print(ia[ia_id]["names"])
			# print(dcb[dcb_id]["names"])
			candidate_matches[ia_id].append(dcb_id)

i = 0
for row in candidate_matches:
	if candidate_matches[row] != []:
		i += 1
		if len(list(set(candidate_matches[row]))) < 7:
			print(row, "\t", "\t".join(list(set(candidate_matches[row]))))

print(i)




"""

{'bio_id': 'BLENKINSOP, GEORGE', 'first_name': 'george', 'last_name': 'blenkinsop', 'names': ['george blenkinsop'], 'birth_year': '1822', 'death_year': '1904'}
{'first_name': 'g', 'last_name': 'blenkinsop', 'names': ['g blenkinsop', 'g belnkinsop'], 'first_date': '1881', 'last_date': '1885'}
"""