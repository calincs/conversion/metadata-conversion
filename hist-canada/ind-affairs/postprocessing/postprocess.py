import re
import requests
import os
import json
import zipfile
import io


def process_3m(input_path, map_file, policy_file, output_folder, post_process_url, encoding, uuid_test_size, output_format):
	"""
	This function sends your data to X3ML.
	"""

	# Checking if input_file is a file or folder
	is_file = os.path.isfile(input_path)
	is_directory = os.path.isdir(input_path)

	if is_directory:
		files = []
		for filename in os.listdir(input_path):
			if filename.endswith(".xml"):
				files.append(os.path.join(input_path, filename))
	elif is_file:
		files = [input_path]
	else:
		print("Error: Please input a valid input file name or path")
		return

	# send each input file to X3ML engine
	for input_file in files:
		print(f"Converting {input_file}")
		output_filename = input_file.split("/")[-1].replace(".xml", ".ttl")

		url = post_process_url + f"/conversion/x3ml/custom?output_file={output_filename}&format_output_file={output_format}&uuid_test_size={uuid_test_size}"
		payload = {}
		files = [
			('input_file', ('input.xml', open(input_file, 'rb'), 'text/xml')),
			('map_file', ('Mapping.x3ml', open(map_file, 'rb'), 'application/octet-stream')),
			('policy_file', ('generator.xml', open(policy_file, 'rb'), 'text/xml'))]
		headers = {}
		response = requests.request("POST", url, headers=headers, data=payload, files=files)
		response.encoding = encoding

		if str(response.status_code).startswith("2"):
			try:
				z = zipfile.ZipFile(io.BytesIO(response.content))
				z.extractall(output_folder)
			except Exception as err:
				print(f"Failed to save X3ML conversion output\n", response.text)
				print(err)
		else:
			print(f"Error: Your file {input_file} could not be converted.")
			print(response.status_code)
			print(response.text)


def combine_rdf(post_process_url: str, files: list, output_rdf_path: str):
	# Takes a list of .ttl file names as input and combined them into a single .ttl file
	# Saves combined .ttl file
	payload = {}
	headers = {}

	request_files = []
	for file in files:
		filename = file.split("/")[-1]
		request_files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	response = requests.request("POST", post_process_url + "/combine", headers=headers, data=payload, files=request_files)

	with open(output_rdf_path, "w") as out_file:
		out_file.write(response.text)


def replacement_simple(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	with open(output_rdf_path, 'w') as f:
		f.write(content)


def replacement(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	# replace spaces in temp lincs URIs with underscores
	# the temp URIs had several invalid characters
	original_matches = re.findall(r'<http:\/\/temp\.lincsproject\.ca\/(.+?>)', content)
	for orig_match in list(set(original_matches)):
		rep_match = orig_match.strip()
		rep_match = rep_match.replace(" ", "_")
		rep_match = rep_match.replace('"', "_")
		rep_match = rep_match.replace('[', "_")
		rep_match = rep_match.replace(']', "_")
		rep_match = rep_match.replace("'", "_")
		rep_match = rep_match.replace("`", "_")
		rep_match = rep_match.replace(",", "_")
		rep_match = rep_match.replace("(", "_")
		rep_match = rep_match.replace(")", "_")
		content = content.replace(orig_match, rep_match)

	with open(output_rdf_path, 'w') as f:
		f.write(content)


def external_labels(post_process_url, input_path, output_path, authority):
	if authority == "wikidata":
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs&lang_code=fr'
	else:
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs'
	payload = {}
	headers = {}
	files = [('file', ('test_labels.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	try:
		z = zipfile.ZipFile(io.BytesIO(response.content))
		z.extractall(output_path)
	except Exception as err:
		print(f"Output failed for external labels with {authority}\n", response.text)
		print(err)


def coordinates(input_path, output_path, post_process_url):
	url = post_process_url + f'/coordinates/wikidata?skip_predicate=owl:sameAs'
	payload = {}
	headers = {}
	files = [('file', ('test_coordinates.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)

	try:
		z = zipfile.ZipFile(io.BytesIO(response.content))
		z.extractall(output_path)
	except Exception as err:
		print(f"Output failed for external labels with wikidata\n", response.text)
		print(err)


def handle_redirects(post_process_url, input_file_path, output_path):
	# see if there is a redirects.json file to use as replacements
	try:
		with open(f"{output_path}redirects.json") as redirects:
			redirects_dict = json.loads(redirects.read())
			redirect_list = redirects_dict.items()
			with open("redirects.tsv", "w") as out_f:
				count = 0
				for r in redirect_list:
					if count < len(redirect_list) - 1:
						out_f.write(r[0] + "\t" + r[1] + "\n")
						if "wikidata" in r[0]:
							out_f.write(r[0].replace("wikidata:", "<http://www.wikidata.org/entity/") + ">\t" + r[1] + "\n")
					else:
						out_f.write(r[0] + "\t" + r[1])
						if "wikidata" in r[0]:
							out_f.write("\n" + r[0].replace("wikidata:", "<http://www.wikidata.org/entity/") + ">\t" + r[1])
					count += 1
			replacement_simple(post_process_url, input_file_path, "redirects.tsv", False, input_file_path)
	except Exception as err:
		print("Did not replace redirects. Error: ", err)


def validation(input_path_list, output_path, post_process_url, analyze="True"):
	files = []
	for file in input_path_list:
		filename = file.split("/")[-1]
		files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	payload = {
		'encoding': 'utf-8',
		'rdf_format': 'turtle',
		'analyze': analyze}
	headers = {}
	response = requests.post(post_process_url + '/validate/', headers=headers, files=files, data=payload)

	with open(output_path + "rdf_validation.json", "w") as out_file:
		out_file.write(response.text)


def get_replacements(mapping_file):
	replacements_list = []
	with open(mapping_file, "r") as mappings:
		for line in mappings:
			line_split = line.strip("\n").split("\t")
			replacements_list.append(line_split)
	return replacements_list


# def replace_inverse_props(input_path, output_path, post_process_url):
# 	url = post_process_url + '/inverses/'
# 	files = {'file': ("inverses.ttl", open(input_path, 'rb'))}
# 	response = requests.request("POST", url, files=files)
# 	if "200" in str(response.status_code):
# 		with open(output_path, 'w') as f:
# 			f.write(response.text)
# 	else:
# 		print(f"ERROR: did not replace inverse properties in {input_path}\n{response}")


# def fix_group_person_double_classing(ttl_path):
# 	# finds entities that are double classed as E21_Person and E74_Group
# 	# removes the E21 class
# 	# outputs the new graph to replace the original file
# 	g = Graph()
# 	g.parse(ttl_path)

# 	query = """
# 		SELECT DISTINCT ?s
# 		WHERE {
# 			?s a <http://www.cidoc-crm.org/cidoc-crm/E21_Person> .
# 			?s a <http://www.cidoc-crm.org/cidoc-crm/E74_Group> .
# 		}"""

# 	qres = g.query(query)
# 	for row in qres:
# 		print(f"Removing {row.s} a crm:E21_Person")
# 		g = g.remove((row.s, RDF.type, URIRef("http://www.cidoc-crm.org/cidoc-crm/E21_Person")))

# 	g.serialize(destination=ttl_path)


def main():

	x3ml_path = "../x3ml/mapping/"
	mapping_file_path = x3ml_path + "3m-x3ml-output.x3ml"
	generator_file_path = x3ml_path + "generator-policy.xml"
	input_xml_file = "../data/2_source_xml/Department_of_Indian_Affairs_data_(Ben Hoy).xml"
	x3ml_output_folder_path = "../data/3_x3ml_output/"

	postprocess_output_path = "../data/4_postprocessing_output/"
	postprocessing_errors_path = "errors/"
	final_output_path = "../../../Datasets/"

	final_rdf_paths = [f"{postprocess_output_path}ind-affairs.ttl", f"{final_output_path}ind-affairs.ttl"]

	post_process_url = "http://0.0.0.0:80"
	#post_process_url = "https://postprocess.lincsproject.ca"

	x3ml_step = True
	clean_step = True
	coordinates_step = False  # Done separately from this code since there were so many. To run again if more are added
	labels_step = False  # TODO LINCS, done for geonames in the coords step
	redirects_step = False
	final_save_step = True
	validation_step = True

	# Run each XML file through X3ML
	if x3ml_step:
		print("x3ml step")
		process_3m(
			input_xml_file,
			mapping_file_path,
			generator_file_path,
			x3ml_output_folder_path,
			post_process_url,
			"utf-8",
			"2",
			"text/turtle")

	if clean_step:
		print("clean step")
		replacement(post_process_url, f"{x3ml_output_folder_path}Department_of_Indian_Affairs_data_(Ben Hoy).ttl", "replacements.tsv", False, f"{postprocess_output_path}ind-affairs.ttl")

	# These should run on the output from replacements
	# The output in redirects.json were manually added to entity_replacements_noRegex.tsv and then the cleaning step was re-run
	# That step needs to be done again if the entities change
	if labels_step:
		for authority in ["wikidata"]:
			print("label step: ", authority)
			external_labels(post_process_url, final_rdf_paths[0], postprocess_output_path, authority)

	if redirects_step:
		print("handle redirects")
		handle_redirects(post_process_url, final_rdf_paths[0], postprocess_output_path)

	if coordinates_step:
		coordinates(final_rdf_paths[0], postprocess_output_path, post_process_url)

	# Combine combined_xm.ttl with the external label files
	# Save that new combined file in Datasets/
	# Then rest of validation on that file
	if final_save_step:
		print("Final combine step")
		combine_file_list = []
		for filename in os.listdir(postprocess_output_path):
			if filename.endswith(".ttl"):
				combine_file_list.append(os.path.join(postprocess_output_path, filename))
		combine_rdf(post_process_url, combine_file_list, final_rdf_paths[1])

		# print("Adding minted LINCS URIs")
		# replacement_simple(post_process_url, final_rdf_paths[1], "lincs_minted_replacements.tsv", False, final_rdf_paths[1])

		# there shouldn't be inverse props in this one since there are none in the x3ml file
		# print("replace inverse properties")
		# replace_inverse_props(final_rdf_paths[1], final_rdf_paths[1], post_process_url)

	if validation_step:
		print("RDF Validation")
		# TODO turn analyze back on when needed
		validation([final_rdf_paths[1]], postprocessing_errors_path, post_process_url, analyze="False")


	# #TODO
	# #kg_lookup(input_file_path)
	# #mint entities


main()
