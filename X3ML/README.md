# Directory Notes

## 3M_walkthrough

Files for 3M documentation on LINCS Portal

### Uploaded
* 3m_walkthrough_source_data.xml
  * Source data used for 3M walkthrough.

### To Do
* finished_example.x3ml
  * Back up of the example mapping

## 3M_setup

Source of setup files. Currently loaded in 3M. NOTE: Target schema files are stored here: https://gitlab.com/calincs/conversion/metadata-conversion/-/tree/master/Ontologies

* lincs_gen_policy_v1.xml
  * Generator policy for LINCS created April 2023

## X3ML_python_script

You can use the `run_X3ML.py` script to send one or more files to be converted using the X3ML engine. You will need to setup the mappings using the 3M interface. Once those are working, you can export them and use them here. This is ideal when you need to process large files that make the interface freeze or if you want to process multiple files in a row.

### Python Requirements
To run this script, you will need to install Python3.

You will also need to run the following in terminal if this is your first time using these packages:

	python3 -m pip install requests

Note: This script has not been tested on Windows

### Linked Data Enhancement API
You also need access to the Linked Data Enhancement API You can either run the LINCS Linked Data Enhancement API locally using Docker (see instructions here: https://gitlab.com/calincs/conversion/post-processing-service/-/wikis/Running-the-API-Locally) or you can use the LINCS hosted version. We cannot guarantee that the LINCS version is always available.


### Running the Script
Now go to the `main()` function in `run_X3ML.py` and follow the instructions to set up the script to convert your own data.

Once you have changed all the setting needed in `main()`, run the following command in terminal from the same folder where `run_X3ML.py` is saved:

	python3 run_X3ML.py
