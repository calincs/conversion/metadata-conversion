import requests
import os


def process_3m(input_path, map_file, policy_file, output_folder, post_process_url, encoding, uuid_test_size, output_format):
	"""
	This function sends your data to X3ML.
	You shouldn't need to change anything here.
	All of changes should be in main() below.
	"""

	# Checking if input_file is a file or folder
	is_file = os.path.isfile(input_path)
	is_directory = os.path.isdir(input_path)

	if is_directory:
		files = []
		for filename in os.listdir(input_path):
			if filename.endswith(".xml"):
				files.append(os.path.join(input_path, filename))
	elif is_file:
		files = [input_path]
	else:
		print("Error: Please input a valid input file name or path")
		return

	# send each input file to X3ML engine
	for input_file in files:
		url = post_process_url + f"/conversion/x3ml/custom?output_file=output.ttl&format_output_file={output_format}&uuid_test_size={uuid_test_size}"
		payload = {}
		files = [
			('input_file', ('input.xml', open(input_file, 'rb'), 'text/xml')),
			('map_file', ('Mapping.x3ml', open(map_file, 'rb'), 'application/octet-stream')),
			('policy_file', ('generator.xml', open(policy_file, 'rb'), 'text/xml'))]
		headers = {}
		response = requests.request("POST", url, headers=headers, data=payload, files=files)
		response.encoding = encoding

		if str(response.status_code).startswith("2"):
			output_path = output_folder + input_file.split("/")[-1][:-4] + ".ttl"
			with open(output_path, "w", encoding=encoding) as out_file:
				out_file.write(response.text)
		else:
			print(f"Error: Your file {input_file} could not be converted.")
			print(response.status_code)
			print(response.text)


def main():

	"""
	You can either run the LINCS post-processing service locally using Docker
	(see instructions here: https://gitlab.com/calincs/conversion/post-processing-service/-/wikis/Running-the-API-Locally)

	or you can use the LINCS hosted version.
	But we cannot guarantee that the LINCS version is always available.

	Uncomment the line below to choose which service you will use. You may need to change the URL for the local version.
	"""

	post_process_url = "http://0.0.0.0:80"
	#post_process_url = "http://127.0.0.1:8000"
	#post_process_url = "https://postprocess.stage.lincsproject.ca"


	"""
	In the next two lines, you need to change the paths to indicate where the 3M mapping files are saved on your computer.
	The easiest way is to put those files in the same folder as this run_X3ML.py file and then you only need to input the file names.
	"""

	mapping_file_path = "sample_mapping.x3ml"
	generator_file_path = "sample_generator_policy.xml"

	"""
	Now you need to indicate where the input xml file is saved.
	You can either input a file path or a folder path.
	If you put a folder path, then it will convert every .xml file in that folder.
	"""

	input_file_or_folder = "sample_input_folder/"

	"""Indicate what folder the output should be saved to"""
	output_folder_path = "sample_output_folder/"


	"""If needed, you can set the encoding for the input file and the uuid size for 3m output"""
	encoding = "utf-8"
	uuid_test_size = "2"
	output_format = "text/turtle"


	process_3m(
		input_file_or_folder,
		mapping_file_path,
		generator_file_path,
		output_folder_path,
		post_process_url,
		encoding,
		uuid_test_size,
		output_format)


if __name__ == "__main__":
	main()
