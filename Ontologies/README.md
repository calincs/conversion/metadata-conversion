# Ontologies

This folder contains ontology files for use in 3M, ResearchSpace, and the user-facing triplestore.

### For use in 3M

Note: The following are already listed as default target schemas in 3M and ready for use there

- CIDOC_CRM_v7.1.1.rdfs
  - Current official version of CIDOC CRM, located at https://cidoc-crm.org/rdfs/7.1.1/CIDOC_CRM_v7.1.1.rdf
- CIDOC_CRM_v7.1.1_PC.rdfs
  - Current official PC extension for CIDOC CRM, located at https://cidoc-crm.org/rdfs/7.1.1/CIDOC_CRM_v7.1.1_PC.rdfs
- CRMdig_v3.2.1.rdfs
  - Current official CRMdig extension for CIDOC CRM, located at https://cidoc-crm.org/crmdig/sites/default/files/CRMdig_v3.2.1.rdfs
- CRMtex_v1.rdfs
  - Current official CRMtex extension for CIDOC CRM, located at https://github.com/Akillus/CRMtex/blob/master/CRMtex_v1.1.rdfs (note: link not from offical CRM site so might be subject to change)
- FRBR2.4-draft.rdfs
  - FRBROO harmonized with CRM, located at https://www.cidoc-crm.org/frbroo/sites/default/files/FRBR2.4-draft.rdfs


### 3M allignments

- crm_owl_alignment.rdfs, <http://www.cidoc-crm.org/cidoc-crm>
  - Alignment between CRM and OWL
- crm_rso_alignment.rdfs, <http://www.cidoc-crm.org/cidoc-crm>
  - ResearchSpace ontology extension of CRM encoded in RDFS. This is only the portion of the RS ontology relevant to the LINCS datasets that have been converted so far.
- crm_skos_alignment.rdfs, <http://www.cidoc-crm.org/cidoc-crm>
  - Alignment betwen SKOS and CRM
- crm_oa_alignment.rdfs, <http://www.cidoc-crm.org/cidoc-crm>
  - Alignment between CRM and the Web Annotation Data Model
- rdfs_xsd_alignment.rdfs, <http://www.w3.org/2000/01/rdf-schema>
  - Alignment between rdfs:Literal and xsd:date, xsd:time, and xsd:dateTime formats

### ResearchSpace versions

- crm_based_on_RS_July_2021_with_pc14_p190.ttl, <http://www.cidoc-crm.org/cidoc-crm>
- FRBR2.4-edited.ttl, <http://iflastandards.info/ns/fr/frbr/frbroo>

### Triplestore uploads

In addititon to ResearchSpace and alignment files:

- owl.ttl, <https://www.w3.org/2002/07/owl>
- skos.rdf, <http://www.w3.org/2004/02/skos/core>
- rdf.ttl, <http://www.w3.org/1999/02/22-rdf-syntax-ns>
- rdfs.ttl, <http://www.w3.org/2000/01/rdf-schema>
