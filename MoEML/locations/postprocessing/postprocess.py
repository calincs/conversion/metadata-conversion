import json
import re
import requests
from urllib.parse import unquote
import shutil

"""
If there are any GeometryCollection values for P168 then they need to be broken into multiple P168 in the source data before running this code.

So far there is only one instance and it's for http://mapoflondon.uvic.ca/ABCH1:
	crm:P168_place_is_defined_by "\"geometry\": {\"type\":\"GeometryCollection\",\"geometries\":[{\"type\":\"LineString\",\"coordinates\":[[-0.088363,51.51127],[-0.088175,51.51158],[-0.088009,51.511784],[-0.08781,51.511991]]},{\"type\":\"LineString\",\"coordinates\":[[-0.087767,51.512135],[-0.087622,51.512191],[-0.087499,51.512338],[-0.087354,51.512518],[-0.087252,51.512629]]}]}" ;

should become:

	crm:P168_place_is_defined_by "\"geometry\": { \"type\": \"LineString\", \"coordinates\": [ [-0.088363,51.51127],[-0.088175,51.51158],[-0.088009,51.511784],[-0.08781,51.511991] ] }" ;
	crm:P168_place_is_defined_by "\"geometry\": { \"type\": \"LineString\", \"coordinates\": [ [-0.087767,51.512135],[-0.087622,51.512191],[-0.087499,51.512338],[-0.087354,51.512518],[-0.087252,51.512629] ] }" ;


These are the patterns for the GeoJSON conversions, with letters representing the numbers in the actual coordinates

***
Point
[A, B]
"POINT(A B)"

***
MultiPoint
[[A, B], [C, D]]
"MULTIPOINT(A B,C D)"

***
LineString
[[A, B],[C, D], [E, F]]
"LINESTRING(A B,C D,E F)"

***
Polygon
[[[A, B], [C, D], [E, F]]]
"POLYGON((A B, C D, E F))"

***
MultiPolygon
[[[[A, B],[C, D]]],[[[E, F]]]]
"MULTIPOLYGON(((A B, C D),(E F)))"

***
MultiLineString
[[[A, B], [C, D]], [[E, F]]]
"MULTILINESTRING((A B, C D), (E F))"

"""


def replacement(input_file_path, output_file_path, post_process_url, replacements_file, regex):
	# calling post processing api to replace wikidata links

	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	# separate coordinates onto their own lines
	content = content.replace(";	crm:", ";\ncrm:")

	to_replace = '''<https://mapoflondon.uvic.ca/STBR1.htm> a crm:E33_E41_Linguistic_Appellation ;
	rdfs:label "Name for St. Bride" ;
	crm:P2_has_type aat:300404670 ;
	crm:P72_has_language <http://temp.lincsproject.ca/english> ;
	crm:P190_has_symbolic_content "https://mapoflondon.uvic.ca/STBR1.htm" .'''
	new_replace = ""
	content = content.replace(to_replace, new_replace)

	to_replace = '''<https://mapoflondon.uvic.ca/ABCH1.htm> a crm:E33_E41_Linguistic_Appellation ;
	rdfs:label "Name for Abchurch Lane" ;
	crm:P2_has_type aat:300404670 ;
	crm:P72_has_language <http://temp.lincsproject.ca/english> ;
	crm:P190_has_symbolic_content "https://mapoflondon.uvic.ca/ABCH1.htm" .'''
	new_replace = ""
	content = content.replace(to_replace, new_replace)

	with open(output_file_path, 'w') as f:
		f.write("@prefix eml:   <http://id.lincsproject.ca/eml/> .\n")
		f.write("@prefix aat:   <http://vocab.getty.edu/aat/> .\n")
		f.write("@prefix wikidata:   <http://www.wikidata.org/entity/> .\n")
		f.write("@prefix temp_lincs_temp:   <http://temp.lincsproject.ca/> .\n")
		f.write("@prefix moeml:   <https://mapoflondon.uvic.ca/> .\n")
		f.write(content)
		f.write("\naat:300404012 a crm:E55_Type .\n")
		f.write('aat:300404012 rdfs:label  "unique identifiers"@en .\n')


def replace_coordinates(input_ttl_path):
	with open(input_ttl_path, "r") as in_f:
		file_contents_text = in_f.read()

	with open(input_ttl_path, "r") as in_f:
		file_contents_lines = in_f.readlines()

		geojson_list = []

		for line in file_contents_lines:
			## skipped any lines below that include GeometryCollection and added the converted data directly to the ttl at the end of this function
			if "GeometryCollection" not in line:
				if "crm:P168_place_is_defined_by" in line:
					# remove characters on both sides of the geojson dict
					geojson_list.append([line, '"' + line[line.index("{"):line.rindex(";")].replace("@en", "").strip()])
			else:
				# remove the geometry collection line from the document
				file_contents_text = file_contents_text.replace(line, "")

		# add the parsed geometry collection back in.
		# TODO automate this. Right now this needs to be manually added for any new geometry collections
		file_contents_text = file_contents_text + '\n<https://mapoflondon.uvic.ca/ABCH1> crm:P168_place_is_defined_by	"LINESTRING(-0.088363 51.51127,-0.088175 51.51158,-0.088009 51.511784,-0.08781 51.511991)"@en ;\ncrm:P168_place_is_defined_by	"LINESTRING(-0.087767 51.512135,-0.087622 51.512191,-0.087499 51.512338,-0.087354 51.512518,-0.087252 51.512629)"@en .\n'

	#temp_index = 0  # this was used if outputting temporary coordinate file for validation
	for geometry in geojson_list:
		geo_dict = json.loads(json.loads(geometry[1]))
		geo_type = geo_dict["type"]
		geo_coords = geo_dict['coordinates']

		if geo_type == "Point":
			new_coords = f'crm:P168_place_is_defined_by	"{geo_type.upper()}({geo_coords[0]} {geo_coords[1]})"@en ;'

		if geo_type in ["MultiPoint", "LineString"]:
			coord_string = ""
			for coords in geo_coords:
				coord_string = coord_string + f"{coords[0]} {coords[1]},"
			new_coords = f'crm:P168_place_is_defined_by	"{geo_type.upper()}({coord_string[:-1]})"@en ;'

		if geo_type in ["Polygon"]:
			coord_string = ""
			for coords in geo_coords[0]:
				coord_string = coord_string + f"{coords[0]} {coords[1]},"
			new_coords = f'crm:P168_place_is_defined_by	"{geo_type.upper()}(({coord_string[:-1]}))"@en ;'

		if geo_type in ["MultiPolygon"]:
			final_string = ""
			for level2 in geo_coords:
				coord_string = ""
				for coords in level2:
					sub_coord_string = ""
					for sub in coords:
						sub_coord_string = sub_coord_string + f"{sub[0]} {sub[1]},"
					coord_string = coord_string + "(" + sub_coord_string + ")"
				final_string = final_string + coord_string + ","
			new_coords = f'crm:P168_place_is_defined_by	"{geo_type.upper()}(({final_string.replace(",)", ")")[:-1]}))"@en ;'

		if geo_type in ["MultiLineString"]:
			final_string = ""
			for level2 in geo_coords:
				coord_string = ""
				for coords in level2:
					coord_string = coord_string + f"{coords[0]} {coords[1]},"
				coord_string = "(" + coord_string + ")"
				final_string = final_string + coord_string + ","
			new_coords = f'crm:P168_place_is_defined_by	"{geo_type.upper()}({final_string.replace(",)", ")")[:-1]})"@en ;'

		file_contents_text = file_contents_text.replace(geometry[0], new_coords)

		# These next two lines are to output just the coordinates as valid ttl when using the coordinate test file as input.
		#temp_index += 1
		#file_contents_text = file_contents_text.replace(geometry[0], f"<http://temp.lincsproject.ca/{temp_index}>	" + new_coords[:-1] + ".")

	return file_contents_text


def add_temp_URIs(ttl_file_contents):
	# for every <> element, if it does not contain http then add temp prefix to the start
	# fix url encoding for all uris

	replacements = {}

	matches = re.findall(r"<.*?>", ttl_file_contents)
	for match in matches:
		if "http" not in match.lower():
			replacements[match] = "<http://temp.lincsproject.ca/" + match[1:]
		else:
			decoded = unquote(match)
			if decoded != match:
				replacements[match] = decoded

	with open("url_decode_replacements.tsv", "w") as f_urls:
		for r in replacements:
			f_urls.write(f"{r}\t{replacements[r]}\n")

	for r in replacements:
		ttl_file_contents = ttl_file_contents.replace(r, replacements[r].replace(" ", "_"))

	return ttl_file_contents


def replace_inverse_props(input_path, output_path, post_process_url):
	url = post_process_url + '/inverses/'
	files = {'file': ("inverses.ttl", open(input_path, 'rb'))}
	response = requests.request("POST", url, files=files)
	if "200" in str(response.status_code):
		with open(output_path, 'w') as f:
			f.write(response.text)
	else:
		print(f"ERROR: did not replace inverse properties in {input_path}\n{response}")


def validation(input_path_list, output_path, post_process_url, analyze="True"):
	files = []
	for file in input_path_list:
		filename = file.split("/")[-1]
		files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	payload = {
		'encoding': 'utf-8',
		'rdf_format': 'turtle',
		'analyze': analyze}
	headers = {}
	response = requests.post(post_process_url + '/validate/', headers=headers, files=files, data=payload)

	with open(output_path + "rdf_validation.json", "w") as out_file:
		out_file.write(response.text)


def main():
	post_process_url = "http://0.0.0.0:80"
	#post_process_url = "https://postprocess.lincsproject.ca"

	input_ttl_path = "../data/source_data/moeml_all_locations.ttl"
	output_ttl_path = "../data/postprocessing_output/moeml_all_locations.ttl"
	error_output_path = "error_files/"
	final_data_path = "../../../Datasets/moeml_locations.ttl"

	print("making replacements")
	replacement(input_ttl_path, output_ttl_path, post_process_url, "entity_replacements_noRegex.tsv", "False")

	print("replacing coordinates")
	ttl_contents = replace_coordinates(output_ttl_path)

	print("adding temporary URIs")
	ttl_contents = add_temp_URIs(ttl_contents)

	print("replace inverse properties")
	with open(output_ttl_path, "w") as out_f:
		out_f.write(ttl_contents)
	replace_inverse_props(output_ttl_path, output_ttl_path, post_process_url)
	shutil.copyfile(output_ttl_path, final_data_path)

	print("validation")
	validation([final_data_path], error_output_path, post_process_url)


main()
