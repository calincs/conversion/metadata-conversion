# Converting MoEML location data

Running `python3 postprocess.py` will fully convert the data. 

The input is in `MoEML/locations/data/source_data/` and the output is in `MoEML/locations/data/postprocessing_output/`