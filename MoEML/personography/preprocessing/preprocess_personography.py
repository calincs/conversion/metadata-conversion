import re
import xml.etree.ElementTree as ET
from urllib.parse import unquote


def filter_records(xml_string, output_path, encoding):
	tree = ET.ElementTree(ET.fromstring(xml_string))

	# getting the parent tag of the xml document
	root = tree.getroot()

	# iterating through foaf_person elements
	for element in root.findall('.//foaf_Person'):

		# for each element in the current foaf_person
		for atrb in element:
			# finding rdf resource tag and checking if it equals the /Literary url
			if 'rdf_resource' in atrb.attrib.keys():
				rdf_resource = atrb.attrib['rdf_resource']
				if rdf_resource == 'https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyLiterary':
					# removing the literary foaf_person element from the xml tree
					root.remove(element)
					break

			# check for empty title attrb and remove it
			foaf_titles = element.findall('.//foaf_title')
			for title in foaf_titles:
				if not title.text:
					element.remove(title)

	# modifying the preprocessed xml
	tree.write(output_path, encoding=encoding, xml_declaration=True)


def regex_replace(search_pattern, replace1, replace2, contents):
	matches = re.findall(search_pattern, contents)
	for match in matches:
		contents = contents.replace(
			replace1.replace("match", match),
			replace2.replace("match", unquote(match)))
	return contents


def main(source_path, output_path, encoding):

	with open(source_path, "r", encoding=encoding) as in_file:
		contents = in_file.read()

		# there is an error with leading dashes before some dates. Unsure if there is meaning intended but it's resulting in invalid dates in the ttl.
		contents = contents.replace('dc_date="-', 'dc_date="')
		contents = contents.replace('bio_precedingEvent="-', 'bio_precedingEvent="')

		# there is an error in the rdf_resource for SMYT1
		contents = contents.replace(
		'<dc_source rdf_resource="https://web.archive.org/web/20170413204634/https://en.wikipedia.org/wiki/John_Smythson"/>',
		'<dc_source rdf_resource="https://en.wikipedia.org/wiki/John_Smythson"/>')

		# pull each person ID out into an additional attribute.
		# the word "match" will replaced for the actual regex matches from the search pattern
		# before doing the full replacement.
		contents = regex_replace(
			r'<foaf_Person rdf_about="https\:\/\/mapoflondon\.uvic\.ca\/(.+?)">',
			'<foaf_Person rdf_about="https://mapoflondon.uvic.ca/match">',
			'<foaf_Person rdf_about="https://mapoflondon.uvic.ca/match" id="match">',
			contents)

		# pull wikipedia links out into their own element
		contents = regex_replace(
			r'<dc_source rdf_resource="(.+?wikipedia.+?)"\/>',
			'<dc_source rdf_resource="match"/>',
			'<dc_source_wikipedia rdf_resource="match"/>',
			contents)


		# encode the dc_source links properly
		contents = regex_replace(
			r'<dc_source rdf_resource="(.+?)"\/>',
			'<dc_source rdf_resource="match"/>',
			'<dc_source rdf_resource="match"/>',
			contents)

	# remove xml elements with rdf type /Literary
	# output final XML to output_path
	filter_records(contents, output_path, encoding)


main("../data/personography_source/PERS1.xml", "../data/personography_source/PERS1_preprocessed.xml", "UTF-8")
