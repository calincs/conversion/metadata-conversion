# MoEML Personography Pre-processing

## Preparing data for 3M

1. From `preprocessing/` directory run `python3 preprocess_personography.py` to prepare `data/personography_source/PERS1.xml` for 3M. It will output the file `data/personography_source/PERS1_preprocessed.xml`.

 This script will:
 - Change person elements from the form `<foaf_Person rdf_about="https://mapoflondon.uvic.ca/EMME1">` to `<foaf_Person rdf_about="https://mapoflondon.uvic.ca/EMME1" id="EMME1">`
 - change `<dc_source>` to `<dc_source_wikipedia>` when applicable
 - remove `<foaf_person>` who have elements `<rdf_type rdf_resource="https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyLiterary"/>`
 - remove empty title elements
 - replace the error `https://web.archive.org/web/20170413204634/https://en.wikipedia.org/wiki/John_Smythson` with `https://en.wikipedia.org/wiki/John_Smythson`

 ## Preparing data for person reconciliation
 Run `python3 xml_to_csv_processing.py` to create a file `xml_to_csv_output.csv` containing person details from `data/personography_source/PERS1_preprocessed.xml` which can be used as context when reconciling the people in OpenRefine. This information was used as part of the process in `get_identifiers/`.