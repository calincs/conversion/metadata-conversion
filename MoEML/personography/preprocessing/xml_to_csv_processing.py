import xml.etree.ElementTree as Xet
import pandas as pd


cols = ['foaf_Person']
rows = []

# Parsing the XML file
xmlparse = Xet.parse('../data/personography_source/PERS1_preprocessed.xml')
root = xmlparse.getroot()
element_tags = []
for i in root:
	element_children = i.getchildren()
	for el in element_children:
		if el.tag not in element_tags:
			element_tags.append(el.tag)
			cols.append(el.tag)

for i in root:
	foaf_person = i.attrib['rdf_about']
	row_dict = {'foaf_Person':foaf_person}
	for tag in element_tags:
		row_dict[tag] = ''
		if i.find(tag) != None:
			if i.find(tag).text:
				row_dict[tag] = i.find(tag).text
			elif list((i.find(tag).attrib).keys()):
				keys = list((i.find(tag).attrib).keys())
				row_dict[tag] = i.find(tag).attrib[keys[0]]

	rows.append(row_dict)

df = pd.DataFrame(rows, columns=cols)

# Writing dataframe to csv
df.to_csv('xml_to_csv_output.csv', encoding='utf-8-sig')
