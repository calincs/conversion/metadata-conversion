import SPARQLWrapper
import sys
import xml.etree.ElementTree as ET
from urllib.parse import unquote


def get_meoml_list(source_xml):
	moeml_h_list = []
	moeml_l_list = []

	tree = ET.parse(source_xml)

	# getting the parent tag of the xml document
	root = tree.getroot()

	# iterating through foaf_person elements
	for element in root.findall('.//foaf_Person'):
		moeml = element.attrib['rdf_about']

		# for each element in the current foaf_person
		for atrb in element:
			# finding rdf resource tag and checking if it equals the /Literary url
			if 'rdf_resource' in atrb.attrib.keys():
				rdf_resource = atrb.attrib['rdf_resource']
				if rdf_resource == 'https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyLiterary':
					moeml_l_list.append(moeml)
				elif rdf_resource == 'https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyHistorical':
					moeml_h_list.append(moeml)


	with open("moeml_historical.tsv", "w") as output_f:
		for i in list(set(moeml_h_list)):
			output_f.write(i + "\n")

	with open("moeml_literary.tsv", "w") as output_f:
		for i in list(set(moeml_l_list)):
			output_f.write(i + "\n")

	return moeml_h_list, moeml_l_list


def wikipedia_from_moeml(source_xml):
	moeml_wikipedia_dict = {}

	tree = ET.parse(source_xml)

	# getting the parent tag of the xml document
	root = tree.getroot()

	# iterating through foaf_person elements
	for element in root.findall('.//foaf_Person'):
		moeml = element.attrib['rdf_about']

		# for each element in the current foaf_person
		for atrb in element:
			# finding rdf resource tag and checking if it equals the /Literary url
			if 'rdf_resource' in atrb.attrib.keys():
				rdf_resource = unquote(atrb.attrib['rdf_resource'])
				if "wikipedia" in rdf_resource:
					moeml_wikipedia_dict[moeml] = rdf_resource

	with open("wikipedia_from_moeml.tsv", "w") as output_f:
		for i in moeml_wikipedia_dict:
			output_f.write(i + "\t" + moeml_wikipedia_dict[i] + "\n")

	return moeml_wikipedia_dict


def get_results(query):
	"""
	Performs Sparql query using self.endpoint_url
	:param query: query string
	:param timeout: timeout for query, defaults to ten seconds
	:param bulk_values: a set of values, if true bulk query will be done and will split up values if it times out
	:param max_attempts: max attempts to try again
	:param split_amount: equal portions that bulk values will be split into
	:return: A Json of mappings of ID -> Label
	"""

	endpoint_url = "https://query.wikidata.org/sparql"
	user_agent = "LINCS-https://lincsproject.ca//%s.%s" % (sys.version_info[0], sys.version_info[1])

	try:
		sparql = SPARQLWrapper.SPARQLWrapper(endpoint_url, agent=user_agent)
		sparql.setQuery(query)
		sparql.setReturnFormat(SPARQLWrapper.JSON)
		sparql.setTimeout(5000)
		results = sparql.query().convert()
		if results is not None:
			return results['results']['bindings']
		else:
			return None

	except Exception as e:
		print(e)


def wikidata_from_wikipedia(moeml_wikipedia_dict):
	wikipedia_wikidata_dict = {}
	wikipedia_list = list(moeml_wikipedia_dict.values())

		# this query gets Wikidata identifiers using Wikipedia articles
	query_size = 25

	wikipedia_query = '''
	SELECT ?wikipedia ?wikidata WHERE {
		VALUES ?wikipedia { ?ALL_VALUES }
			?wikipedia schema:about ?wikidata .
	}
			'''

	with open("wikidata_from_wikipedia.tsv", "w") as output_f:

		for idx in range(0, len(wikipedia_list), query_size):
			end = idx + query_size
			bulk_values = wikipedia_list[idx:end]
			bulk = ""
			for UID in bulk_values:
				bulk += "<" + UID + "> "

			wikipedia_query_new = wikipedia_query.replace("?ALL_VALUES", bulk)
			wikiquery = get_results(query=wikipedia_query_new)

			for i in wikiquery:
				output_f.write(i["wikipedia"]["value"] + "\t" + i["wikidata"]["value"] + "\n")
				wikipedia_wikidata_dict[i["wikipedia"]["value"]] = i["wikidata"]["value"]

	return wikipedia_wikidata_dict

def wikipedia_from_wikidata(moeml_wikidata_dict):
	moeml_wikipedia_dict = {}
	wikidata_list = list(moeml_wikidata_dict.values())

	# this query gets Wikipedia articles using Wikidata identifiers
	query_size = 25

	wikipedia_query = '''
SELECT ?id ?article WHERE {
  VALUES ?id { ?ALL_VALUES }
    ?article schema:about ?id .
    ?article schema:isPartOf <https://en.wikipedia.org/> .
}
		'''

	with open("wikipedia_from_wikidata.tsv", "w") as output_f:

		for idx in range(0, len(wikidata_list), query_size):
			end = idx + query_size
			bulk_values = wikidata_list[idx:end]
			bulk = ""
			for UID in bulk_values:
				bulk += "<" + UID + "> "

			wikipedia_query_new = wikipedia_query.replace("?ALL_VALUES", bulk)
			wikiquery = get_results(query=wikipedia_query_new)

			for i in wikiquery:
				wikipedia = unquote(i["article"]["value"])
				wikidata = i["id"]["value"]
				output_f.write(wikidata + "\t" + wikipedia + "\n")
				moeml_wikipedia_dict[wikidata] = wikipedia

	return moeml_wikipedia_dict


def viaf_from_wikidata(wikipedia_wikidata_dict):
	wikidata_viaf_dict = {}
	wikidata_list = list(wikipedia_wikidata_dict.values())

	# this query gets Wikidata identifiers using Wikipedia articles
	query_size = 25

	wikipedia_query = '''
	SELECT ?wikidata ?viaf WHERE {
		VALUES ?wikidata { ?ALL_VALUES }
			?wikidata wdt:P214 ?viaf .
	}
		'''

	with open("viaf_from_wikidata.tsv", "w") as output_f:
		for idx in range(0, len(wikidata_list), query_size):
			end = idx + query_size
			bulk_values = wikidata_list[idx:end]
			bulk = ""
			for UID in bulk_values:
				bulk += "<" + UID + "> "

			wikipedia_query_new = wikipedia_query.replace("?ALL_VALUES", bulk)
			wikiquery = get_results(query=wikipedia_query_new)

			for i in wikiquery:
				output_f.write(i["wikidata"]["value"] + "\t" + "http://viaf.org/viaf/" + i["viaf"]["value"] + "\n")

	return wikidata_viaf_dict


def moeml_from_wikidata():
	moeml_wikidata_dict_fromWikidata = {}
	moeml_query = '''
		SELECT ?moeml ?wikidata WHERE {
				?wikidata wdt:P6060 ?moeml .
		}
		'''

	with open("moeml_from_wikidata.tsv", "w") as output_f:
			wikiquery = get_results(query=moeml_query)

			for i in wikiquery:
				output_f.write("https://mapoflondon.uvic.ca/" + i["moeml"]["value"] + "\t" + i["wikidata"]["value"] + "\n")
				moeml_wikidata_dict_fromWikidata["https://mapoflondon.uvic.ca/" + i["moeml"]["value"]] = i["wikidata"]["value"]

	return moeml_wikidata_dict_fromWikidata


def main():

	# get list of all moeml person IDs
	moeml_h_list, moeml_l_list = get_meoml_list("../../data/personography_source/PERS1.xml")

	# get moeml ID to Wikipedia mapping from the original PERS1.xml file
	moeml_wikipedia_dict = wikipedia_from_moeml("../../data/personography_source/PERS1.xml")

	# Query Wikidata to get Wikidata IDs from the known Wikipedia IDs
	wikipedia_wikidata_dict = wikidata_from_wikipedia(moeml_wikipedia_dict)

	# Query Wikidata to get VIAF IDs from the known Wikidata IDs we got from the original Wikipedia IDs
	wikidata_viaf_dict = viaf_from_wikidata(wikipedia_wikidata_dict)

	# Query Wikidata to get the Wikidata entities that already have MOEML IDs
	moeml_wikidata_dict_fromWikidata = moeml_from_wikidata()

	wikidata_wikipedia_dict_fromWikidata = wikipedia_from_wikidata(moeml_wikidata_dict_fromWikidata)



main()