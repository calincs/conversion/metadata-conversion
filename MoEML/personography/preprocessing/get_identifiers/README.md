- moeml_historical.tsv = list of moeml foaf_person who have rdf_type https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyHistorical extracted from PERS1.xml
- moeml_literary.tsv = list of moeml foaf_person who have rdf_type https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyLiterary extracted from PERS1.xml
- wikipedia_from_moeml.tsv = wikipedia values extracted from PERS1.xml (historical and literary persons)
- wikidata_from_wikipedia.tsv = wikidata values corresponding to the wikipedia values in wikipedia_from_moeml.tsv extracted from wikidata sparql endpoint 
- viaf_from_wikidata.tsv = viaf values corresponding to the wikidata values in wikidata_from_wikipedia.tsv extracted from wikidata sparql endpoint
- moeml_from_wikidata.tsv = wikidata values corresponding to moeml people that are already existing in wikidata, extracted from wikidata sparql endpoint

- Jakob_reconcile.tsv = historical persons from MoEML PERS1_preprocessed that had no wikidata associated with it. This includes through sparql queries using a Wikipedia ID, and through existing matches already in Wikidata.
- Jakob_check.tsv = historical persons that had Wikidata IDs saved in Wikidata already but that conflicted with the information we have in MoEML. This could be because the Wikipedia links in MoEML were wrong or outdated, or the ones in Wikidata were reconcilied incorrectly. So we should check them.
- Jakob_maybe_check.tsv = historical persons that had wikidata identifiers in Wikidata already that we didn't also have already in the MoEML data. We may want to verify that these are correct since we don't know who did that reconciliation work and added it to Wikidata.

There are also copies of those three tsv files ending in `-reconciled` which have been manually reconciled by a student.

Up-to-date version of final reconciliation results are https://docs.google.com/spreadsheets/d/1yBJR7m0skucGaVZSmkmbLBM5-0uOd68BoBw9sTGZKoE/edit#gid=149664487