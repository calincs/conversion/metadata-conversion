# 3M export data/backup for MoEML

## Output Files
- persons_3m_output_full.ttl - Most recent full output for MoEML personography data, using Mapping169 with PERS1_preprocessed.xml as input. Post-processing including reconciliaton is still needed.
- persons_3m_output_sample.ttl - Most recent mapping files for MoEML personography data, using Mapping169 with a manually generated sample of PERS1_preprocessed.xml used as input (Mapping169/PERS1_preprocessed_sample___31-05-2022155747___1956.xml).

## 3M Projects
- Mapping174 (MoEML Personography Data (Best Practices v2)) - Final with edits made to deal with Unknown-Format errors for dates (though that has been solved with a post-processing script) (technically the most up-to-date mapping, but Mapping169 can also be used). Jan 2023 update: this mapping is missing some label languages and has labels like "Death of ..." instead of "Death Event of...". Use mapping 169 until this is fixed.
- Mapping169 (MoEML Personography Data (Best Practices)) - Final without edits made for Unknown-Format error for dates (technically an out-of-date mapping, but can still be used since the error has been dealt with in post-processing)
- Mapping171 (MoEML Personography Data (Best Practices_Full Data)) - Duplicate of Best Practices used to run the full file (back when it was assumed running the full file would mess up the mapping)
- Mapping156 (moeml_persons) - out of date
- Mapping162 (moeml_persons_new1) - out of date
- Mapping166 (moeml_persons_new2)  - out of date