# POST PROCESSING ON 3M OUTPUT

This README contains details on the post processing steps performed on 3M output.

Input file : 'MoEML personography/data/X3ML/persons_3m_output_full.ttl'

The python script 'MoEML personography/postprocessing/postprocess.py' contains all required post processing code divided into multiple functions. Each function performs a unique processing step.
As shown below, the main() function (which is run automatically when you run the python file) calls on these functions.

If there are certain steps that you do not want to perform on the data, you can comment out the function calls that performs those steps. As shown below, the functions replacement(), kg_lookup() and rdf_validation() will not be called.

``` 
def main():
    input_file_path = 'MoEML/data/X3ML/persons_3m_output_full.ttl'
    # input_file_path = 'MoEML/data/postprocessing_output/persons_3m_processed_output_copy.ttl'
    
    # replacement(input_file_path)
    external_labels(input_file_path)
    prefix_validation(input_file_path)
    # kg_lookup(input_file_path)
    # rdf_validation(input_file_path)
    graph_analysis(input_file_path)

```

We will now describe each post processing step. They use the LINCS post-processing service, running locally.

## handle unknown dates
There were three people in the source data who's birth dates did not follow the standard form for the rest of the data. We manually fix the errors caused by those dates here.


## replacement

This function replaces :
* spaces with underscores in temp.lincsproject.ca URIs
* wikidata.org/wiki/ links with wikidata.org/entity/ links and changes https to http
* remove university of victoria ezproxy from doi.org links
* write out mol: as https://mapoflondon.uvic.ca/

It calls on the 'http://0.0.0.0:80/replacement' endpoint from the post processsing service to do this task.

The output for this function is a file called 'persons_3m_processed_output.ttl' which is created in the folder 'MoEML personography/data/postprocessing_output'.

## external labels

The purpose of this function is to enhance a dataset with human-readable labels for entities that have already been reconciled with URIs from external knowledge bases.

For this data set, only wikidata labels were used from external sources.

The function calls on the 'http://0.0.0.0:80/labels/wikidata' endpoint from the post processing service to do this task.

The output for this function is a zip file with two files: one containg the labels and the other a json file with information on any redirects. This is also created in the folder 'MoEML personography/data/postprocessing_output'.

## prefix validation

This function checks an RDF .ttl file to see if it contains any URI prefixes that are not part of the list of LINCS approved prefixes. 

The function calls on the 'http://0.0.0.0:80/validate/prefix/' endpoint from the post processing service to do this task.

The output for this function is a results will be a list of warnings that the user can choose to fix or ignore independently from this API.

## RDF validation

This function validates RDF documents using Apache Jena’s riot package. It currently validates for syntax by starting a shell command which runs in the background.

The function calls on the 'http://0.0.0.0:80/validate' endpoint from the post processing service to do this task.

The output for this function is a list of syntax errors.

## Graph Analysis

This function analyses the triples in the dataset's graph and checks if the rdfs:label and rdf:type properties are present for all records.

The function calls on the 'http://0.0.0.0:80/check_property/label' and 'http://0.0.0.0:80/check_property/type' endpoints from the post processing service to do this task.

The output for this function is a list of the subjects (from the analysed triples) that have either of these properties missing.

## LINCS KG Lookup

TO DO