import requests
import re
import zipfile
import io
from urllib.parse import unquote
import os

from rdflib import Graph


def process_3m(input_path, map_file, policy_file, output_folder, post_process_url, encoding="utf-8", uuid_test_size="2", output_format="text/turtle"):
	"""
	This function sends your data to X3ML.
	"""

	# Checking if input_file is a file or folder
	is_file = os.path.isfile(input_path)
	is_directory = os.path.isdir(input_path)

	if is_directory:
		files = []
		for filename in os.listdir(input_path):
			if filename.endswith(".xml"):
				files.append(os.path.join(input_path, filename))
	elif is_file:
		files = [input_path]
	else:
		print("Error: Please input a valid input file name or path")
		return

	# send each input file to X3ML engine
	for input_file in files:
		print(f"Converting {input_file}")
		output_filename = input_file.split("/")[-1].replace(".xml", ".ttl")

		url = post_process_url + f"/conversion/x3ml/custom?output_file={output_filename}&format_output_file={output_format}&uuid_test_size={uuid_test_size}"
		payload = {}
		files = [
			('input_file', ('input.xml', open(input_file, 'rb'), 'text/xml')),
			('map_file', ('Mapping.x3ml', open(map_file, 'rb'), 'application/octet-stream')),
			('policy_file', ('generator.xml', open(policy_file, 'rb'), 'text/xml'))]
		headers = {}
		response = requests.request("POST", url, headers=headers, data=payload, files=files)
		response.encoding = encoding

		if str(response.status_code).startswith("2"):
			print(f"3m complete conversion for:  {input_file}")
			try:
				z = zipfile.ZipFile(io.BytesIO(response.content))
				z.extractall(output_folder)
			except Exception as err:
				print(f"Failed to save X3ML conversion output\n", response.text)
				print(err)
		else:
			print(f"Error: Your file {input_file} could not be converted.")
			print(response.status_code)
			print(response.text)


def handle_unkown_date(input_file_path):
	# manually replace date errors for 3 people: COUC1, FITZ60, FAIT2

	with open(input_file_path, 'r') as f:
		content = f.read()

	new_content = content
	occurences = [m.start() for m in re.finditer('crm:P82a_begin_of_the_begin  "Unknown-Format"\\^\\^xsd:dateTime ;', content)]

	while True:
		occurences = [m.start() for m in re.finditer('crm:P82a_begin_of_the_begin  "Unknown-Format"\\^\\^xsd:dateTime ;', new_content)]
		if not occurences:
			break
		last_full_stop = new_content.find('.', occurences[0])
		new_content = new_content[:occurences[0] - 10] + new_content[last_full_stop:]

	with open(input_file_path, 'w') as f:
		f.write(new_content)
		f.write('''<http://temp.lincsproject.ca/persons/birth/time/COUC1>
		crm:P82a_begin_of_the_begin  "1340-01-01T00:00:00"^^xsd:dateTime ;
		crm:P82b_end_of_the_end      "1397-12-31T23:59:59"^^xsd:dateTime .

<http://temp.lincsproject.ca/persons/birth/time/FITZ60>
		crm:P82a_begin_of_the_begin  "1397-01-01T00:00:00"^^xsd:dateTime ;
		crm:P82b_end_of_the_end      "1431-12-31T23:59:59"^^xsd:dateTime .

<http://temp.lincsproject.ca/persons/birth/time/FAIT2>
		crm:P82a_begin_of_the_begin  "1620-01-01T00:00:00"^^xsd:dateTime ;
		crm:P82b_end_of_the_end      "1691-12-31T23:59:59"^^xsd:dateTime .
''')


def replacement(input_file_path, post_process_url, replacements_file, output_path, regex=False):
	# calling post processing api to replace wikidata links

	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	# replace spaces in temp lincs URIs with underscores
	original_matches = re.findall('<http:\/\/temp\.lincsproject\.ca\/(.+?>)', content)
	for orig_match in original_matches:
		rep_match = orig_match.strip()
		rep_match = orig_match.replace(" ", "_")
		content = content.replace(orig_match, rep_match)

	to_replace = '''<https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyOther>
        a           crm:E55_Type ;
        rdfs:label  "MoEML Project Historical Person"@en .'''
	new_replace = '''<https://mapoflondon.uvic.ca/mdtEncyclopediaPersonographyOther>
        a           crm:E55_Type ;
        rdfs:label  "MoEML Project Other Person"@en .'''
	content = content.replace(to_replace, new_replace)

	with open(output_path, 'w') as f:
		f.write(content)

	handle_unkown_date('../data/postprocessing_output/persons_3m_processed_output.ttl')


def fix_wikipedia_URIs(input_path, output_path):
	# for every <> element that is a Wikipedia URI, decode and replace in ttl

	with open(input_path, "r") as f_in:
		ttl_file_contents = f_in.read()

	replacements = {}

	matches = re.findall(r"\<https:\/\/[a-z]+\.wikipedia\.org\/wiki\/.*?\>", ttl_file_contents)
	for match in matches:
		decoded = unquote(match)
		if decoded != match:
			replacements[match] = decoded

	for r in replacements:
		ttl_file_contents = ttl_file_contents.replace(r, replacements[r].replace(" ", "_"))

	with open(output_path, "w") as f_out:
		f_out.write(ttl_file_contents)


def external_labels(input_path, output_path, post_process_url, authority):
	url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs&lang_code=fr'
	payload = {}
	headers = {}
	files = [('file', ('test_labels.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	z = zipfile.ZipFile(io.BytesIO(response.content))
	z.extractall(output_path)


def replace_inverse_props(input_path, output_path, post_process_url):
	url = post_process_url + '/inverses/'
	files = {'file': ("inverses.ttl", open(input_path, 'rb'))}
	response = requests.request("POST", url, files=files)
	if "200" in str(response.status_code):
		with open(output_path, 'w') as f:
			f.write(response.text)
	else:
		print(f"ERROR: did not replace inverse properties in {input_path}\n{response}")


def validation(input_path_list, output_path, post_process_url, analyze="True"):
	files = []
	for file in input_path_list:
		filename = file.split("/")[-1]
		files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	payload = {
		'encoding': 'utf-8',
		'rdf_format': 'turtle',
		'analyze': analyze}
	headers = {}
	response = requests.post(post_process_url + '/validate/', headers=headers, files=files, data=payload)

	with open(output_path + "rdf_validation.json", "w") as out_file:
		out_file.write(response.text)


def condense_ttl_graph(rootdir, output_file):

	# get a list of all .ttl files in the rootdir. This includes within nested folders
	filelist = []
	for subdir, dirs, files in os.walk(rootdir):
		for file in files:
			filepath = subdir + os.sep + file

			if filepath.endswith(".ttl"):
				filelist.append(filepath)

	# creates graph and outputs the new graph to replace the original file
	g = Graph()

	for ttl_path in filelist:
		print(ttl_path)
		g.parse(ttl_path)

	g.serialize(destination=output_file)


def main():
	x3ml_step = False
	external_labels_step = False

	post_process_url = "http://0.0.0.0:80"
	#post_process_url = "https://postprocess.lincsproject.ca"

	if x3ml_step:
		process_3m(
			"../data/personography_source/PERS1_preprocessed.xml",
			"../data/X3ML/Mapping169/Mapping169.x3ml",
			"../data/X3ML/Mapping169/gen_policy___31-05-2022195538___3782___15-06-2022173008___10549.xml",
			'../data/X3ML/',
			post_process_url)

	# this path is only for the replacement task
	print("making replacements")
	replacement_input_path = '../data/X3ML/PERS1_preprocessed.ttl'
	replacement(replacement_input_path, post_process_url, 'entity_replacements_noRegex.tsv', "../data/postprocessing_output/persons_3m_processed_output.ttl", regex="False")
	fix_wikipedia_URIs("../data/postprocessing_output/persons_3m_processed_output.ttl", "../data/postprocessing_output/persons_3m_processed_output.ttl")

	# use the file with the replaced output for other functions
	input_path = '../data/postprocessing_output/persons_3m_processed_output.ttl'
	output_path = '../data/postprocessing_output/'
	error_output_path = 'errors/'
	combined_output_path = "../../../Datasets/moeml_personography.ttl"

	if external_labels_step:
		print("fetching external labels")
		external_labels(input_path, output_path, post_process_url, "wikidata")

	# combine final ttl files and add to datasets folder
	print("combining files")
	condense_ttl_graph(output_path, combined_output_path)

	print("replace inverse properties")
	replace_inverse_props(combined_output_path, combined_output_path, post_process_url)

	print("validation")
	validation([combined_output_path], error_output_path, post_process_url)


main()
