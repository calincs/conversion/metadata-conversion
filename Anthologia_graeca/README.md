# Anthologia graeca  Conversion

## Current Conversion Status

This [dataset](https://lincsproject.ca/docs/explore-lod/project-datasets/anthologia-graeca/) has been published, but changes will continue to be made through the transformation workflow rather than through editing in ResearchSpace. Any updates should be made to the source data, pre or post processing scripts and the conversion should be re-run using the preprocessing and then the postprocessing scripts.

We are currently updating the transformation process to pull from Anthologia Graeca directly rather from a database dump.

## Pre-Processing

`preprocessing/` contains the python scripts needed to convert the original Anthologia graeca  relational database in `data/source_RDB/` into XML files in `data/source_xml/` ready for processing in the 3M editor.

### Generating XML:
- Create a local postgres database (e.g., sudo -u username psql -c 'create database anthologiadec2022;')
- Load the sql database dump into the local postgres database (e.g., sudo -u username psql dbname < ap-prod-db_dec2022.sql)
- Run scholium_titles.sql to add titles to the meleager_scholium table (e.g., psql -d "dbname='dbname' user='user' password='password' host='localhost'" -f scholium_titles.sql) (you might need to run chmod 777 scholium_titles.sql first)
- In the same way, run passage_titles.sql to add passage titles to the meleager_passage table
- Connect the preprocessing scripts to your local postgreSQL database by changing the connection details in the first 4 lines of main_script.py
- Run main_script.py to generate XML documents.
	+ Note that we manually added information to work.xml so be careful if you run work_script.py. It is commented out in main_script.py right now to prevent that.
- The XML documents will be generated in the `data/source_XML/` directory

### Generating database id to URI mappings
- Run `map_id_to_urn.py` to update these mappings. The mappings will be saved to `data/source_xml/identifer_mappings/`. It will output mappings for author, city, passage, scholium.
	+ Inserting the proper URIs back into the data happen in the post-processing step. Because we changed the logic of what URI to use as the primary URI in our second round of conversion, the alt_urn fields in the xml files should not be used.
- `edition.txt` in the mapping folder was manually generated.

Encoding used : 'utf-8'

### Manual Cleaning
These changes should be applied by the researchers to the original data. For now, we manually deleted them from the XML after running the preprocessing scripts.
- Deleted alt_urn id's 1223 and 34 as missing URN's
- Delete author_id '332' as it was a test_author
- Remove <None> and <> values from the identifier mapping docs. These will be minted by LINCS in post processing unless the researchers reconcile them before publication.


## X3ML

All input XML files mentioned in this section can be found in [Anthologia_palatina/data/source_xml/full_xml/](https://gitlab.com/calincs/conversion/metadata-conversion/-/tree/master/Anthologia_graeca/data/source_xml/full_xml).

The X3ML mapping files can be found in `X3ML/palatina_v14/`.

The output files can be found in `data/X3ML_output`. Note that the postprocessing script includes an automated step to convert the input XML using the X3ML mapping.

- palatina_v14
	+ This is the most recent mapping for the Anthologia graeca  data as of January 25, 2023. 

## 3M Outputs
`Anthologia_palatina/data/X3ML_output/` contains the following files output from X3ML using `X3ML/palatina_v14` generated using `Anthologia_palatina/postprocessing/postprocess.py`

- author.ttl
	+ Most recent output file for author.xml

- city.ttl
	+ Most recent output file for city.xml

- edition.ttl
	+ Most recent output file for edition.xml

- manuscript.ttl
	+ Most recent output file for manuscript.xml

- passage.ttl
	+ Most recent output file for passage.xml

- reference.ttl
	+ Most recent output file for reference.xml

- scholium.ttl
	+ Most recent output file for scholium.xml

- text.ttl
	+ Most recent output file for text.xml

- work.ttl
	+ Most recent output file for work.xml


## Post-Processing

Run `postprocess.py` from `postprocessing/` to execute the X3ML conversion, final data cleaning, and validation steps.

## Links to Other Documentation
[Data Work Document](https://docs.google.com/document/d/1zy8VOrJ4qMIJZwIOUpVIWnMTAjR5jrbxf7axt_gAIsw/edit#)