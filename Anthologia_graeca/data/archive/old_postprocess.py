from bs4 import BeautifulSoup as bs
import requests
import os


"""
Notes and TODOs

- Bring up the Type labels with the researchers (eg. <http://temp.lincsproject.ca/anthologie/edition/type/3> )
- Check that there are no texts that are missing labels at the end (this would mean there was no passage or scholium associated with a text). Their unique_id's too.
- passage description missing from mapping?
- passage external reference missing from mapping?

- there are known errors that X3ML will give for the mappings on manusript.xml and text.xml about some labels not being possible. Those are fine to ignore.
Such errors include:
[Mapping #: 45, Link #: 0]. LabelGenerator(Name: edition_text_label Args: [(Name: id Type: xpath Value: text_edition_id/text()), (Name: language Type: constant Value: en)])
LabelGenerator Error: The label generator with name "Literal" does not contain any value. [Mapping #: 13, Link #: 0]. LabelGenerator(Name: Literal Args: [(Name: text Type: xpath Value: manuscript_title/text())])
LabelGenerator Error: The label generator with name "Literal" does not contain any value. [Mapping #: 13, Link #: 0]. LabelGenerator(Name: Literal Args: [(Name: text Type: xpath Value: manuscript_title/text())])
LabelGenerator Error: The label generator with name "project_id_named_ent" does not contain any value. [Mapping #: 13, Link #: 1]. LabelGenerator(Name: project_id_named_ent Args: [(Name: id Type: xpath Value: ../manuscript_title/text()), (Name: language Type: constant Value: en)])
LabelGenerator Error: The label generator with name "manuscript_url_label" does not contain any value. [Mapping #: 13, Link #: 3]. LabelGenerator(Name: manuscript_url_label Args: [(Name: title Type: xpath Value: ../manuscript_title/text()), (Name: language Type: constant Value: en)])

edition 18 is missing a label in the original data and the output
- some passages are missing labels


"< http" at the start of some URIs


"""


def skos_keyword(output_path, input_path):
	"""
	This function takes the reference.xml file as input and outputs a .ttl file stating that each keyword is a skos:Concept.

	"""

	with open(output_path, "w", encoding="utf-8") as out_file:
		out_file.write("@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n\n")

		with open(input_path, "r", encoding="utf-8") as in_file:
			# Read each line in the file, readlines() returns a list of lines
			contents = in_file.read()
			soup = bs(contents, 'xml')
			keywords = soup.find_all('keyword')
			for keyword in keywords:
				keyword_id = keyword.find('keyword_id').get_text()
				out_file.write(f'<http://temp.lincsproject.ca/anthologie/keyword/{keyword_id}> a skos:Concept .\n')


def X3ML(input_dir, output_dir, X3ML_dir, X3ML_mapping_file, X3ML_generator_file):

	for input_xml in os.listdir(input_dir):
		if input_xml.endswith(".xml"):
			output_file = input_xml[:-4] + ".ttl"
			output_path = output_dir + output_file

			url = f"https://postprocess.stage.lincsproject.ca/conversion/x3ml/custom?output_file={output_file}&format_output_file=text/turtle&uuid_test_size=2"

			payload = {}
			files = [
				('input_file', (input_xml, open(input_dir + input_xml, 'rb'), 'text/xml')),
				('map_file', (X3ML_mapping_file, open(X3ML_dir + X3ML_mapping_file, 'rb'), 'application/octet-stream')),
				('policy_file', (X3ML_generator_file, open(X3ML_dir + X3ML_generator_file, 'rb'), 'text/xml'))]

			headers = {}
			response = requests.request("POST", url, headers=headers, data=payload, files=files)
			response.encoding = 'utf-8'

			if response.status_code == 200:
				with open(output_path, "w", encoding='utf-8') as f_out:
					f_out.write(response.text)
			else:
				print("X3ML Request Error for file  ", input_xml, "  \n", response.text)


def combine_ttl(rootdir, output_file):
	"""
	merge all ttl files output from X3ML into a single file
	"""

	# get a list of all .ttl files in the rootdir. This includes within nested folders
	filelist = []
	for subdir, dirs, files in os.walk(rootdir):
		for file in files:
			filepath = subdir + os.sep + file

			if filepath.endswith(".ttl"):
				filelist.append(filepath)

	# sort the filelist so that they always get added in the same order
	filelist.sort()

	# get the prefix list
	prefix_list = []
	for file in filelist:
		with open(file, "r", encoding="utf-8") as f_in:
			for line in f_in:
				# track prefixes only once
				if line.startswith("@prefix"):
					prefix_list.append(line.strip("\n"))

	prefix_list = list(set(prefix_list))
	prefix_list.sort()

	with open(output_file, "w", encoding="utf-8") as f_out:
		# output prefix list
		for prefix in prefix_list:
			f_out.write(prefix + "\n")
		f_out.write("\n\n")

		# copy the contents of each small file into the combined.ttl file
		for file in filelist:
			with open(file, "r", encoding="utf-8") as f_in:

				# track previous line to remove duplicate blank lines
				previous_line = ""

				for line in f_in:
					if line.strip() == "":
						if previous_line != "":
							f_out.write(line)
							previous_line = line.strip()

					else:
						# skip comments
						if not line.startswith("#"):
							if not line.startswith("@prefix"):
								f_out.write(line)
								previous_line = line.strip()
			f_out.write("\n")


def fix_text_edition_labels(input_path, output_path):
	# e.g., <http://temp.lincsproject.ca/anthologie/text/9493> rdfs:label "Anthologia Palatina text of edition 3"@en

	# read mapping file
	mapping_file = "../data/source_xml/identifier_mappings/edition.tsv"
	mapping_dict = {}
	with open(mapping_file, "r", encoding="utf-8") as in_mapping:
		for line in in_mapping:
			line_split = line.split("\t")
			mapping_dict[line_split[0].strip("\n")] = line_split[1].strip("\n")

	# read input file
	with open(input_path, "r", encoding="utf-8") as in_ttl:
		ttl_content = in_ttl.read()

		# make title replacement for each edition
		for edition in mapping_dict:
			ttl_content = ttl_content.replace(f"Anthologia Palatina text of edition {str(edition)}", f"Anthologia Palatina text of edition {str(mapping_dict[edition])}")

	# replace file with new version
	with open(output_path, "w", encoding="utf-8") as out_file:
		out_file.write(ttl_content)


def remove_owl_thing(input_path, output_path):
	# Remove lines of the form "wikidata:Q76735200  a  owl:Thing ."
	with open(input_path, "r", encoding="utf-8") as in_ttl:
		with open(output_path, "w", encoding="utf-8") as out_file:
			old_line = ""
			for line in in_ttl:
				if "owl:Thing" not in line:
					if "owl:Thing" not in old_line:
						out_file.write(old_line)
				old_line = line


def fix_language_code(input_path, output_path):
	# https://datatracker.ietf.org/doc/html/rfc5646#page-9

	with open(input_path, "r", encoding="utf-8") as in_file:
		file_content = in_file.read()
		file_content = file_content.replace('"@eng ', '"@en ')

	with open(output_path, "w", encoding="utf-8") as out_file:
		out_file.write(file_content)


def replace_sameas_identifiers(input_path, output_path, map_path, map_files):
	# Replace temporary URIs with the owl:sameAs values when they exist. Relevant for people and places

	with open(input_path, "r", encoding="utf-8") as in_file:
		file_content = in_file.read()
		#file_content = file_content.replace('"@eng ', '"@en ')

	for file in map_files:
		with open(map_path + file, "r") as map_file:
			for line in map_file:
				line_split = line.split("\t")
				file_content = file_content.replace(line_split[0].strip("\n"),line_split[1].strip("\n"))


	with open(output_path, "w", encoding="utf-8") as out_file:
		out_file.write(file_content)


def remove_new_lines(input_path, output_path):
	pass


def get_minted_labels():
	pass


def add_minted_labels():
	pass


def main():
	"""
	These are all of the processing steps applied to the Anthologia Palatina source XML files to prepare them for ingestion into LINCS ResearchSpace
	You can set steps to FALSE when you already have output saved (e.g., skos_keyword, get_external_labels)

	"""

	SKOS_keyword_step = False
	X3ML_conversion_step = False
	combine_ttl_step = False
	clean_ttl_step = False
	wikidata_labels_step = True

	# create the .ttl file that says all keywords are skos:Concept
	input_path = "../data/source_xml/full_xml/reference.xml"
	output_path = "../data/converted_notMinted/Extra/skos_keyword.ttl"
	if SKOS_keyword_step:
		skos_keyword(input_path, output_path)

	# Convert the input XML using the X3ML templates
	input_dir = "../data/source_xml/full_xml/"
	output_dir = "../data/converted_notMinted/3M/"
	X3ML_dir = "../X3ML/Mapping151/"
	X3ML_generator_file = "generator_policy___29-10-2021173646___10258___15-02-2022091743___10157___20-03-2022091050___4927.xml"
	X3ML_mapping_file = "Mapping151.x3ml"
	if X3ML_conversion_step:
		X3ML(input_dir, output_dir, X3ML_dir, X3ML_mapping_file, X3ML_generator_file)

	# Combine all 3M output ttl files into a single ttl file
	xml_dir = "../data/converted_notMinted/3M/"
	combined_xml_path = "../data/converted_notMinted/AP_3M_combined.ttl"
	if combine_ttl_step:
		combine_ttl(xml_dir, combined_xml_path)

	if clean_ttl_step:
		input_path = combined_xml_path
		output_path = combined_xml_path[:-4] + "_clean1.ttl"
		fix_text_edition_labels(input_path, output_path)

		input_path = output_path
		output_path = combined_xml_path[:-4] + "_clean2.ttl"
		remove_owl_thing(input_path, output_path)

		input_path = output_path
		output_path = combined_xml_path[:-4] + "_clean3.ttl"
		fix_language_code(input_path, output_path)

		input_path = output_path
		output_path = combined_xml_path[:-4] + "_clean4.ttl"
		map_path = "../data/source_xml/identifier_mappings/"
		mapping_files = ["scholium_mapping.tsv", "passage_mapping.tsv", "city_mapping.tsv", "author_mapping.tsv"]
		replace_sameas_identifiers(input_path, output_path, map_path, mapping_files)

		# manually created this using postman for now for city.ttl file. add this step later
		# input_path = output_path
		# output_wd_path = "../data/converted_minted/external_labels/wikidata_labels.ttl"
		# if wikidata_labels_step:
		# 	get_wikidata_labels(input_path, output_wd_path)


		# input_path = "../data/converted_notMinted/AP_3M_combined_clean4.ttl"
		# output_path = "../data/converted_minted/AP_3M_combined_clean4.ttl"


		# input_path = "../data/converted_notMinted/Extra/skos_keyword.ttl"
		# output_path = "../data/converted_minted/Extra/skos_keyword.ttl"






main()