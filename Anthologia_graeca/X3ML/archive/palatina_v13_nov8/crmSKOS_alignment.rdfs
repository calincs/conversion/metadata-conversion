<?xml version="1.0" encoding="utf-8"?>

<!--
    CRM E55 > SKOS:Concept encoded in RDFS
    Extension of CRMext4SKOSandLabels file
-->

<rdf:RDF xmlns:dct="http://purl.org/dc/terms/"
  xmlns:owl="http://www.w3.org/2002/07/owl#" 
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" 
  xmlns:skos="http://www.w3.org/2004/02/skos/core#"
  xml:base="http://www.cidoc-crm.org/cidoc-crm/">
  
  <owl:Ontology rdf:about="http://www.w3.org/2004/02/skos/core">
    <dct:title xml:lang="en">SKOS Vocabulary</dct:title>
    <dct:contributor>Dave Beckett</dct:contributor>
    <dct:contributor>Nikki Rogers</dct:contributor>
    <dct:contributor>Participants in W3C's Semantic Web Deployment Working Group.</dct:contributor>
    <dct:description xml:lang="en">An RDF vocabulary for describing the basic structure and content of concept schemes such as thesauri, classification schemes, subject heading lists, taxonomies, 'folksonomies', other types of controlled vocabulary, and also concept schemes embedded in glossaries and terminologies.</dct:description>
    <dct:creator>Alistair Miles</dct:creator>
    <dct:creator>Sean Bechhofer</dct:creator>
    <rdfs:seeAlso rdf:resource="http://www.w3.org/TR/skos-reference/"/>
  </owl:Ontology>

<!-- Classes --> 

<!-- SKOS:Class - CRM:E55 relation -->
  <rdf:Class rdf:about="http://www.w3.org/2004/02/skos/core#Concept">
    <rdfs:label xml:lang="en">Concept</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">An idea or notion; a unit of thought.</skos:definition>
    <rdfs:subClassOf rdf:resource="E55_Type"/>
  </rdf:Class>
  
  <rdf:Class rdf:about="http://www.w3.org/2004/02/skos/core#ConceptScheme">
    <rdfs:label xml:lang="en">Concept Scheme</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A set of concepts, optionally including statements about semantic relationships between those concepts.</skos:definition>
    <skos:scopeNote xml:lang="en">A concept scheme may be defined to include concepts from different sources.</skos:scopeNote>
    <skos:example xml:lang="en">Thesauri, classification schemes, subject heading lists, taxonomies, 'folksonomies', and other types of controlled vocabulary are all examples of concept schemes. Concept schemes are also embedded in glossaries and terminologies.</skos:example>
    <owl:disjointWith rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
  </rdf:Class>
  
  <rdf:Class rdf:about="http://www.w3.org/2004/02/skos/core#Collection">
    <rdfs:label xml:lang="en">Collection</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A meaningful collection of concepts.</skos:definition>
    <skos:scopeNote xml:lang="en">Labelled collections can be used where you would like a set of concepts to be displayed under a 'node label' in the hierarchy.</skos:scopeNote>
    <owl:disjointWith rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
    <owl:disjointWith rdf:resource="http://www.w3.org/2004/02/skos/core#ConceptScheme"/>
  </rdf:Class>
  
  <rdf:Class rdf:about="http://www.w3.org/2004/02/skos/core#OrderedCollection">
    <rdfs:label xml:lang="en">Ordered Collection</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">An ordered collection of concepts, where both the grouping and the ordering are meaningful.</skos:definition>
    <skos:scopeNote xml:lang="en">Ordered collections can be used where you would like a set of concepts to be displayed in a specific order, and optionally under a 'node label'.</skos:scopeNote>
    <rdfs:subClassOf rdf:resource="http://www.w3.org/2004/02/skos/core#Collection"/>
  </rdf:Class>

<!-- Properties --> 

<!-- Object Properties --> 

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#inScheme">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdfs:label xml:lang="en">is in scheme</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Relates a resource (for example a concept) to a concept scheme in which it is included.</skos:definition>
    <skos:scopeNote xml:lang="en">A concept may be a member of more than one concept scheme.</skos:scopeNote>
    <rdfs:range rdf:resource="http://www.w3.org/2004/02/skos/core#ConceptScheme"/>
  </rdf:Property>
  
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#hasTopConcept">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdfs:label xml:lang="en">has top concept</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Relates, by convention, a concept scheme to a concept which is topmost in the broader/narrower concept hierarchies for that scheme, providing an entry point to these hierarchies.</skos:definition>
    <rdfs:domain rdf:resource="http://www.w3.org/2004/02/skos/core#ConceptScheme"/>
    <rdfs:range rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
    <owl:inverseOf rdf:resource="http://www.w3.org/2004/02/skos/core#topConceptOf"/>
  </rdf:Property>
  
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#topConceptOf">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>    
    <rdfs:label xml:lang="en">is top concept in scheme</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Relates a concept to the concept scheme that it is a top level concept of.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#inScheme"/>
    <owl:inverseOf rdf:resource="http://www.w3.org/2004/02/skos/core#hasTopConcept"/>
    <rdfs:domain rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
    <rdfs:range rdf:resource="http://www.w3.org/2004/02/skos/core#ConceptScheme"/> 
  </rdf:Property>
  
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#semanticRelation">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdfs:label xml:lang="en">is in semantic relation with</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Links a concept to a concept related by meaning.</skos:definition>
    <skos:scopeNote xml:lang="en">This property should not be used directly, but as a super-property for all properties denoting a relationship of meaning between concepts.</skos:scopeNote>
    <rdfs:domain rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
    <rdfs:range rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
  </rdf:Property>

<!-- SKOS:broader - CRM:P127 relation -->
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#broader">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdfs:label xml:lang="en">has broader</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Relates a concept to a concept that is more general in meaning.</skos:definition>
    <rdfs:comment xml:lang="en">Broader concepts are typically rendered as parents in a concept hierarchy (tree).</rdfs:comment>
    <skos:scopeNote xml:lang="en">By convention, skos:broader is only used to assert an immediate (i.e. direct) hierarchical link between two conceptual resources.</skos:scopeNote>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#broaderTransitive"/>
    <rdfs:subPropertyOf rdf:resource="P127_has_broader_term"/>
    <owl:inverseOf rdf:resource="http://www.w3.org/2004/02/skos/core#narrower"/>
  </rdf:Property>

<!-- SKOS:narrower - CRM:P127i relation -->
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#narrower">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdfs:label xml:lang="en">has narrower</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Relates a concept to a concept that is more specific in meaning.</skos:definition>
    <skos:scopeNote xml:lang="en">By convention, skos:broader is only used to assert an immediate (i.e. direct) hierarchical link between two conceptual resources.</skos:scopeNote>
    <rdfs:comment xml:lang="en">Narrower concepts are typically rendered as children in a concept hierarchy (tree).</rdfs:comment>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#narrowerTransitive"/>
    <rdfs:subPropertyOf rdf:resource="P127i_has_narrower_term"/>
    <owl:inverseOf rdf:resource="http://www.w3.org/2004/02/skos/core#broader"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#related">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#SymmetricProperty"/>
    <rdfs:label xml:lang="en">has related</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Relates a concept to a concept with which there is an associative semantic relationship.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#semanticRelation"/>
    <rdfs:comment xml:lang="en">skos:related is disjoint with skos:broaderTransitive</rdfs:comment>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#broaderTransitive">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#TransitiveProperty"/>
    <rdfs:label xml:lang="en">has broader transitive</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition>skos:broaderTransitive is a transitive superproperty of skos:broader.</skos:definition>
    <skos:scopeNote xml:lang="en">By convention, skos:broaderTransitive is not used to make assertions. Rather, the properties can be used to draw inferences about the transitive closure of the hierarchical relation, which is useful e.g. when implementing a simple query expansion algorithm in a search application.</skos:scopeNote>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#semanticRelation"/>
    <owl:inverseOf rdf:resource="http://www.w3.org/2004/02/skos/core#narrowerTransitive"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#narrowerTransitive">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#TransitiveProperty"/>
    <rdfs:label xml:lang="en">has narrower transitive</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition>skos:narrowerTransitive is a transitive superproperty of skos:narrower.</skos:definition>
    <skos:scopeNote xml:lang="en">By convention, skos:narrowerTransitive is not used to make assertions. Rather, the properties can be used to draw inferences about the transitive closure of the hierarchical relation, which is useful e.g. when implementing a simple query expansion algorithm in a search application.</skos:scopeNote>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#semanticRelation"/>
    <owl:inverseOf rdf:resource="http://www.w3.org/2004/02/skos/core#broaderTransitive"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#member">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdfs:label xml:lang="en">has member</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Relates a collection to one of its members.</skos:definition>
    <rdfs:domain rdf:resource="http://www.w3.org/2004/02/skos/core#Collection"/>
    <rdfs:range>
      <owl:Class>
        <owl:unionOf rdf:parseType="Collection">
            <owl:Class rdf:about="http://www.w3.org/2004/02/skos/core#Concept"/>
            <owl:Class rdf:about="http://www.w3.org/2004/02/skos/core#Collection"/>
        </owl:unionOf>
      </owl:Class>
    </rdfs:range>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#memberList">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#FunctionalProperty"/>
    <rdfs:label xml:lang="en">has member list</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Relates an ordered collection to the RDF list containing its members.</skos:definition>
    <rdfs:domain rdf:resource="http://www.w3.org/2004/02/skos/core#OrderedCollection"/>
    <rdfs:range rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#List"/>
    <rdfs:comment xml:lang="en">For any resource, every item in the list given as the value of the
      skos:memberList property is also a value of the skos:member property.</rdfs:comment>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#mappingRelation">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdfs:label xml:lang="en">is in mapping relation with</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">Relates two concepts coming, by convention, from different schemes, and that have comparable meanings</skos:definition>
    <rdfs:comment xml:lang="en">These concept mapping relations mirror semantic relations, and the data model defined below is similar (with the exception of skos:exactMatch) to the data model defined for semantic relations. A distinct vocabulary is provided for concept mapping relations, to provide a convenient way to differentiate links within a concept scheme from links between concept schemes. However, this pattern of usage is not a formal requirement of the SKOS data model, and relies on informal definitions of best practice.</rdfs:comment>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#semanticRelation"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#broadMatch">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdfs:label xml:lang="en">has broader match</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">skos:broadMatch is used to state a hierarchical mapping link between two conceptual resources in different concept schemes.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#mappingRelation"/>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#broader"/>
    <owl:inverseOf rdf:resource="http://www.w3.org/2004/02/skos/core#narrowMatch"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#narrowMatch">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdfs:label xml:lang="en">has narrower match</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">skos:narrowMatch is used to state a hierarchical mapping link between two conceptual resources in different concept schemes.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#mappingRelation"/>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#narrower"/>
    <owl:inverseOf rdf:resource="http://www.w3.org/2004/02/skos/core#broadMatch"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#relatedMatch">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#SymmetricProperty"/>
    <rdfs:label xml:lang="en">has related match</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">skos:relatedMatch is used to state an associative mapping link between two conceptual resources in different concept schemes.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#mappingRelation"/>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#related"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#exactMatch">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#SymmetricProperty"/>
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#TransitiveProperty"/>
    <rdfs:label xml:lang="en">has exact match</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">skos:exactMatch is used to link two concepts, indicating a high degree of confidence that the concepts can be used interchangeably across a wide range of information retrieval applications. skos:exactMatch is a transitive property, and is a sub-property of skos:closeMatch.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#closeMatch"/>
    <rdfs:comment xml:lang="en">skos:exactMatch is disjoint with each of the properties skos:broadMatch and skos:relatedMatch.</rdfs:comment>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#closeMatch">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#ObjectProperty"/>
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#SymmetricProperty"/>
    <rdfs:label xml:lang="en">has close match</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">skos:closeMatch is used to link two concepts that are sufficiently similar that they can be used interchangeably in some information retrieval applications. In order to avoid the possibility of "compound errors" when combining mappings across more than two concept schemes, skos:closeMatch is not declared to be a transitive property.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#mappingRelation"/>
  </rdf:Property>

<!-- Annotation Properties --> 

<!-- SKOS:labels to CRM:E1 domain alingment -->
  <rdf:Property rdf:about="http://www.w3.org/2000/01/rdf-schema#label">
    <rdfs:domain rdf:resource="E1_CRM_Entity"/>
    <rdfs:range rdf:resource="http://www.w3.org/2000/01/rdf-schema#Literal"/>
  </rdf:Property>

<!-- SKOS:labels to CRM:E1 domain alingment -->
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#prefLabel">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">preferred label</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">The preferred lexical label for a resource, in a given language.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2000/01/rdf-schema#label"/>
    <rdfs:comment xml:lang="en">A resource has no more than one value of skos:prefLabel per language tag, and no more than one value of skos:prefLabel without language tag.</rdfs:comment>
    <rdfs:comment xml:lang="en">The range of skos:prefLabel is the class of RDF plain literals.</rdfs:comment>
    <rdfs:comment xml:lang="en">skos:prefLabel, skos:altLabel and skos:hiddenLabel are pairwise
      disjoint properties.</rdfs:comment>
    <rdfs:domain rdf:resource="E1_CRM_Entity"/>  
  </rdf:Property>
  
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#altLabel">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">alternative label</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">An alternative lexical label for a resource.</skos:definition>
    <skos:example xml:lang="en">Acronyms, abbreviations, spelling variants, and irregular plural/singular forms may be included among the alternative labels for a concept. Mis-spelled terms are normally included as hidden labels (see skos:hiddenLabel).</skos:example>
    <rdfs:comment xml:lang="en">The range of skos:altLabel is the class of RDF plain literals.</rdfs:comment>
    <rdfs:comment xml:lang="en">skos:prefLabel, skos:altLabel and skos:hiddenLabel are pairwise disjoint properties.</rdfs:comment>
    <rdfs:domain rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2000/01/rdf-schema#label"/> 
  </rdf:Property>
  
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#hiddenLabel">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">hidden label</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A lexical label for a resource that should be hidden when generating visual displays of the resource, but should still be accessible to free text search operations.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2000/01/rdf-schema#label"/>
    <rdfs:comment xml:lang="en">The range of skos:hiddenLabel is the class of RDF plain literals.</rdfs:comment>
    <rdfs:comment xml:lang="en">skos:prefLabel, skos:altLabel and skos:hiddenLabel are pairwise disjoint properties.</rdfs:comment>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#note">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">note</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A general note, for any purpose.</skos:definition>
    <skos:scopeNote xml:lang="en">This property may be used directly, or as a super-property for more specific note types.</skos:scopeNote>
  </rdf:Property>
  
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#changeNote">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">change note</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A note about a modification to a concept.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#note"/>
  </rdf:Property>
  
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#definition">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">definition</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A statement or formal explanation of the meaning of a concept.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#note"/>
  </rdf:Property>
  
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#editorialNote">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">editorial note</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A note for an editor, translator or maintainer of the vocabulary.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#note"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#example">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">example</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">An example of the use of a concept.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#note"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#historyNote">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">history note</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A note about the past state/use/meaning of a concept.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#note"/>
  </rdf:Property>

  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#scopeNote">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#AnnotationProperty"/>
    <rdfs:label xml:lang="en">scope note</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A note that helps to clarify the meaning and/or the use of a concept.</skos:definition>
    <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2004/02/skos/core#note"/>
    <rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
  </rdf:Property>

<!-- Datatype Properties --> 
  
  <rdf:Property rdf:about="http://www.w3.org/2004/02/skos/core#notation">
    <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#DatatypeProperty"/>
    <rdfs:label xml:lang="en">notation</rdfs:label>
    <rdfs:isDefinedBy rdf:resource="http://www.w3.org/2004/02/skos/core"/>
    <skos:definition xml:lang="en">A notation, also known as classification code, is a string of characters such as "T58.5" or "303.4833" used to uniquely identify a concept within the scope of a given concept scheme.</skos:definition>
    <skos:scopeNote xml:lang="en">By convention, skos:notation is used with a typed literal in the object position of the triple.</skos:scopeNote>
  </rdf:Property>

</rdf:RDF>
