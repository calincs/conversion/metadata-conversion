import requests
import re
import zipfile
import io
import os
import json
import shutil
from SPARQLWrapper import SPARQLWrapper
from rdflib import Graph

"""
TODO:

- Changed https://www.wikidata.org/wiki/ to http://www.wikidata.org/entity/ in the URN fields
- Added http:// prefix to URNs that have anthologiagraeca.org/passages/ in them
- Added http:// to URN id 4171
- Deleted URN id's 1223 and 34 as missing URN's
- Delete author_id '332' as it was a test_author
- Removed blank spaces in passage.xml,references.xml and text.xml that were causing encoding errors
- owl1: prefix and https
- temp_lincs_temp to be replaced

"""


def process_3m(input_path, map_file, policy_file, output_folder, post_process_url, encoding, uuid_test_size, output_format):
	"""
	This function sends your data to X3ML.
	"""

	# Checking if input_file is a file or folder
	is_file = os.path.isfile(input_path)
	is_directory = os.path.isdir(input_path)

	if is_directory:
		files = []
		for filename in os.listdir(input_path):
			if filename.endswith(".xml"):
				files.append(os.path.join(input_path, filename))
	elif is_file:
		files = [input_path]
	else:
		print("Error: Please input a valid input file name or path")
		return

	# send each input file to X3ML engine
	for input_file in files:
		print(f"Converting {input_file}")
		output_filename = input_file.split("/")[-1].replace(".xml", ".ttl")

		url = post_process_url + f"/conversion/x3ml/custom?output_file={output_filename}&format_output_file={output_format}&uuid_test_size={uuid_test_size}"
		payload = {}
		files = [
			('input_file', ('input.xml', open(input_file, 'rb'), 'text/xml')),
			('map_file', ('Mapping.x3ml', open(map_file, 'rb'), 'application/octet-stream')),
			('policy_file', ('generator.xml', open(policy_file, 'rb'), 'text/xml'))]
		headers = {}
		response = requests.request("POST", url, headers=headers, data=payload, files=files)
		response.encoding = encoding

		if str(response.status_code).startswith("2"):
			try:
				z = zipfile.ZipFile(io.BytesIO(response.content))
				z.extractall(output_folder)
			except Exception as err:
				print(f"Failed to save X3ML conversion output\n", response.text)
				print(err)
		else:
			print(f"Error: Your file {input_file} could not be converted.")
			print(response.status_code)
			print(response.text)


def create_scholia_map():
	with open("scholia_api.tsv", "w") as scholia_out:
		for i in range(1, 89):
			uri = f"https://anthologiagraeca.org/api/scholia/?page={i}"
			print(uri)
			response = requests.get(uri)
			response_dict = response.json()

			for scholia in response_dict["results"]:
				scholia_id = str(scholia["id"])
				scholia_url = scholia["url"]

				# scholia_out.write(f"<http://temp.lincsproject.ca/anthologie/scholium/{scholia_id}>\t<{scholia_url}>\n")
				scholia_out.write(f"<http://temp.lincsproject.ca/anthologie/scholium/realization/{scholia_id}>\t<{scholia_url}>\n")


def create_passage_map():
	with open("passage_api.tsv", "w") as scholia_out:
		for i in range(1, 84):
			uri = f"https://anthologiagraeca.org/api/passages/?page={i}"
			print(uri)
			response = requests.get(uri)
			response_dict = response.json()

			for passage in response_dict["results"]:
				passage_id = str(passage["id"])
				passage_url = passage["url"]

				scholia_out.write(f"<http://temp.lincsproject.ca/anthologie/passage/realization/{passage_id}>\t<{passage_url}>\n")


def replacement_simple(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	with open(output_rdf_path, 'w') as f:
		f.write(content)


def replacement(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	# replace spaces in temp lincs URIs with underscores
	# the temp URIs had several invalid characters
	original_matches = re.findall('<http:\/\/temp\.lincsproject\.ca\/(.+?>)', content)
	for orig_match in original_matches:
		rep_match = orig_match.strip()
		rep_match = rep_match.replace(" ", "_")
		rep_match = rep_match.replace('"', "_")
		rep_match = rep_match.replace('[', "_")
		rep_match = rep_match.replace(']', "_")
		rep_match = rep_match.replace("'", "_")
		rep_match = rep_match.replace("`", "_")
		rep_match = rep_match.replace(" > .", "> .")
		content = content.replace(orig_match, rep_match)

	# there shouldn't be any uuid prefixes but in case there are, replace so file is valid
	content = content.replace("uuid:", "http://uuid.ca/")

	# fix an invalid URI
	content = content.replace(" > .", "> .")
	content = content.replace("< http", "<http")

	with open(output_rdf_path, 'w') as f:
		f.write(content)


# Removed this from the X3ML mapping so it's no longer needed here. Keeping for future use.
def remove_owl_thing(input_path, output_path):
	# Remove lines of the form "wikidata:Q76735200  a  owl:Thing ."
	with open(input_path, "r", encoding="utf-8") as in_ttl:
		in_ttl = in_ttl.readlines()
	with open(output_path, "w", encoding="utf-8") as out_file:
		old_line = ""
		for line in in_ttl:
			if "owl:Thing" not in line:
				if "owl:Thing" not in old_line:
					out_file.write(old_line)
			old_line = line


def fix_language_code(input_path, output_path):
	# https://datatracker.ietf.org/doc/html/rfc5646#page-9

	with open(input_path, "r", encoding="utf-8") as in_file:
		file_content = in_file.read()
		file_content = file_content.replace('"@eng ', '"@en ')

	with open(output_path, "w", encoding="utf-8") as out_file:
		out_file.write(file_content)


def clean_rdf(post_process_url, input_file_path, output_file_path):
	replacement(post_process_url, input_file_path, "entity_replacements_noRegex.tsv", False, output_file_path)
	fix_language_code(output_file_path, output_file_path)


def alt_urns(post_process_url, mapping_file_path, output_file_path):
	# Reads the identifier mapping files that were generated from the database dump in pre-processing
	# add the alt URIs as same as relationships in a new file

	with open(output_file_path, "w") as f_out:
		f_out.write("@prefix wikidata: <http://www.wikidata.org/entity/> .\n")
		f_out.write("@prefix owl:   <https://www.w3.org/2002/07/owl#> .\n\n")

		mapping_files = ["scholium_mapping.tsv", "passage_mapping.tsv", "city_mapping.tsv", "author_mapping.tsv"]
		for map_file in mapping_files:
			with open(mapping_file_path + map_file, "r") as f_in:
				f_in.readline()
				for line in f_in:
					line_split = line.strip("\n").split("\t")
					f_out.write(f"{line_split[0]} owl:sameAs {line_split[1]} .\n")


def urn_map(input_file_path, output_file_path, pattern, old_prefix, new_prefix):
	# this is set up to create a list of replacement commands for a replacement tsv for our api
	# for entities with the simple anthologiagraeca api URIs
	with open(input_file_path, "r") as in_f:
		content = in_f.read()

	matches = re.findall(pattern, content)
	matches = list(set(matches))

	with open(output_file_path, "a") as out_f:
		for match in matches:
			out_f.write(f"<{old_prefix}{match}>\t<{new_prefix}{match}/>\n")


def create_urn_map(input_file_path):

	# Figured out the patterns here: https://anthologiagraeca.org/api/

	## These entity types all follow basic patterns for URIs so we don't need to call on the API
	# We use LINCS replacement service for this

	# "cities": "https://anthologiagraeca.org/api/cities/"
	# <http://temp.lincsproject.ca/anthologie/place/city/1> becomes <https://anthologiagraeca.org/api/cities/1/>
	urn_map(
		input_file_path,
		"cities_map.tsv",
		r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/place\/city\/([0-9]+?)>",
		"http://temp.lincsproject.ca/anthologie/place/city/",
		"https://anthologiagraeca.org/api/cities/")


	# "books": "https://anthologiagraeca.org/api/books/"
	# http://temp.lincsproject.ca/anthologie/book/14 becomes https://anthologiagraeca.org/api/books/14/
	urn_map(
		input_file_path,
		"books_map.tsv",
		r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/book\/([0-9]+?)>",
		"http://temp.lincsproject.ca/anthologie/book/",
		"https://anthologiagraeca.org/api/books/")


	# "texts": "https://anthologiagraeca.org/api/texts/"
	# http://temp.lincsproject.ca/anthologie/text/1 becomes https://anthologiagraeca.org/api/texts/1/
	urn_map(
		input_file_path,
		"texts_map.tsv",
		r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/text\/([0-9]+?)>",
		"http://temp.lincsproject.ca/anthologie/text/",
		"https://anthologiagraeca.org/api/texts/")


	# "keywords": "https://anthologiagraeca.org/api/keywords/"
	# http://temp.lincsproject.ca/anthologie/keyword/1 becomes https://anthologiagraeca.org/api/keywords/1/
	urn_map(
		input_file_path,
		"keywords_map.tsv",
		r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/keyword\/([0-9]+?)>",
		"http://temp.lincsproject.ca/anthologie/keyword/",
		"https://anthologiagraeca.org/api/keywords/")


	# "keyword_categories": "https://anthologiagraeca.org/api/keyword_categories/"
	# http://temp.lincsproject.ca/anthologie/keyword/category/1 becomes https://anthologiagraeca.org/api/keyword_categories/1/
	urn_map(
		input_file_path,
		"keyword_categories_map.tsv",
		r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/keyword\/category\/([0-9]+?)>",
		"http://temp.lincsproject.ca/anthologie/keyword/category/",
		"https://anthologiagraeca.org/api/keyword_categories/")


	# "languages": "https://anthologiagraeca.org/api/languages/"
	# http://temp.lincsproject.ca/anthologie/language/cmn becomes https://anthologiagraeca.org/api/languages/cmn/
	# urn_map(
	# 	input_file_path,
	# 	"languages_map.tsv",
	# 	r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/language\/([a-z]+?)>",
	# 	"http://temp.lincsproject.ca/anthologie/language/",
	# 	"https://anthologiagraeca.org/api/languages/")


	# "descriptions": "https://anthologiagraeca.org/api/descriptions/"
	# http://temp.lincsproject.ca/anthologie/description/424 becomes https://anthologiagraeca.org/api/descriptions/424/
	urn_map(
		input_file_path,
		"descriptions_map.tsv",
		r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/description\/([0-9]+?)>",
		"http://temp.lincsproject.ca/anthologie/description/",
		"https://anthologiagraeca.org/api/descriptions/")


	# "authors": "https://anthologiagraeca.org/api/authors/"
	#  http://temp.lincsproject.ca/anthologie/author/222 becomes https://anthologiagraeca.org/api/authors/222/
	urn_map(
		input_file_path,
		"authors_map.tsv",
		r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/author\/([0-9]+?)>",
		"http://temp.lincsproject.ca/anthologie/author/",
		"https://anthologiagraeca.org/api/authors/")


	# "comments": "https://anthologiagraeca.org/api/comments/"
	#  http://temp.lincsproject.ca/anthologie/comment/1 becomes https://anthologiagraeca.org/api/comments/1/
	urn_map(
		input_file_path,
		"comments_map.tsv",
		r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/comment\/([0-9]+?)>",
		"http://temp.lincsproject.ca/anthologie/comment/",
		"https://anthologiagraeca.org/api/comments/")


	# "editions": "https://anthologiagraeca.org/api/editions/"
	#  http://temp.lincsproject.ca/anthologie/edition/1 becomes https://anthologiagraeca.org/api/editions/1/
	urn_map(
		input_file_path,
		"editions_map.tsv",
		r"<http:\/\/temp\.lincsproject\.ca\/anthologie\/edition\/([0-9]+?)>",
		"http://temp.lincsproject.ca/anthologie/edition/",
		"https://anthologiagraeca.org/api/editions/")


def primary_urns(post_process_url, input_file_path, output_file_path):
	## These two need to call on the API to get the right URIs
	## We used create scholia and passage map functions first to create the replacement files by calling on their API

	# "passages": "https://anthologiagraeca.org/api/passages/"
	# follows rule here https://framagit.org/anthologie-palatine/anthologyontology/-/blob/preprod/django/web/urls/passages.py#L18-19
	# <http://temp.lincsproject.ca/anthologie/passage/realization/424> becomes https://anthologiagraeca.org/api/passages/urn:cts:greekLit:tlg7000.tlg001.ag:1.1/
	# to use API to get using "id" as passage id
	replacement_simple(post_process_url, output_file_path, "passage_api.tsv", False, output_file_path)

	# "scholia": "https://anthologiagraeca.org/api/scholia/"
	# follows rule here https://framagit.org/anthologie-palatine/anthologyontology/-/blob/preprod/django/web/urls/passages.py#L18-19
	# http://temp.lincsproject.ca/anthologie/scholium/realization/3350 becomes https://anthologiagraeca.org/api/scholia/urn:cts:greekLit:tlg5011.tlg001.sag:1.1.2/
	# to use API to get using "id" as scholium id
	replacement_simple(post_process_url, output_file_path, "scholia_api.tsv", False, output_file_path)


def external_labels(post_process_url, input_path, output_path, authority):
	url = post_process_url + f'/labels/{authority}?skip_predicate=owl:sameAs&lang_code=fr'
	payload = {}
	headers = {}
	files = [('file', ('test_labels.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	try:
		z = zipfile.ZipFile(io.BytesIO(response.content))
		z.extractall(output_path)
	except Exception as err:
		print(f"Output failed for external labels with {authority}\n", response.text)
		print(err)


def handle_wikidata_redirects(post_process_url, input_file_path, redirect_file_path):
	# see if there is a redirects.json file to use as replacements
	try:
		with open(redirect_file_path) as redirects:
			redirects_dict = json.loads(redirects.read())
			redirect_list = redirects_dict.items()
			with open("redirects.tsv", "w") as out_f:
				count = 0
				for r in redirect_list:
					if count < len(redirect_list) - 1:
						out_f.write(r[0] + "\t" + r[1] + "\n")
						if "wikidata" in r[0]:
							out_f.write(r[0].replace("wikidata:", "<http://www.wikidata.org/entity/") + ">\t" + r[1] + "\n")
					else:
						out_f.write(r[0] + "\t" + r[1])
						if "wikidata" in r[0]:
							out_f.write("\n" + r[0].replace("wikidata:", "<http://www.wikidata.org/entity/") + ">\t" + r[1])
					count += 1
			replacement_simple(post_process_url, input_file_path, "redirects.tsv", False, input_file_path)
			os.remove("redirects.tsv")
	except Exception as err:
		print("Did not replace redirects. Error: ", err)


def condense_ttl_graph(input_ttl_list, output_ttl_path):
	# creates graph and outputs the new graph to replace the original file
	g = Graph()

	for ttl_path in input_ttl_list:
		print(ttl_path)
		g.parse(ttl_path)

	g.serialize(destination=output_ttl_path)


def replace_inverse_props(input_path, output_path, post_process_url):
	url = post_process_url + '/inverses/'
	files = {'file': ("inverses.ttl", open(input_path, 'rb'))}
	response = requests.request("POST", url, files=files)
	if "200" in str(response.status_code):
		with open(output_path, 'w') as f:
			f.write(response.text)
	else:
		print(f"ERROR: did not replace inverse properties in {input_path}\n{response}")


def validation(input_path_list, output_path, post_process_url, analyze="True"):
	files = []
	for file in input_path_list:
		filename = file.split("/")[-1]
		files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	payload = {
		'encoding': 'utf-8',
		'rdf_format': 'turtle',
		'analyze': analyze}
	headers = {}
	response = requests.post(post_process_url + '/validate/', headers=headers, files=files, data=payload)

	with open(output_path + "rdf_validation.json", "w") as out_file:
		out_file.write(response.text)


def main():
	x3ml_step = False  # runs xml through x3ml to output ttl files and a combined ttl file

	create_owl_sameas_step = False  # creates an owl same as ttl file for alt URIs from the database
	create_api_urn_map_step = False  # grabs passage and scholia info from API to get a mapping of replacements
	create_alt_urn_step = False  # creates mappings for URIs from the database for other files

	substitute_uris_step = False

	clean_step = False

	labels_step = False
	redirects_step = False
	final_save_step = True

	post_process_url = "http://0.0.0.0:80"
	#post_process_url = "https://postprocess.stage.lincsproject.ca"

	x3ml_path = "../X3ML/anthologia/"
	mapping_file_path = x3ml_path + "3m-x3ml-output.x3ml"
	generator_file_path = x3ml_path + "generator-policy.xml"
	input_xml_folder = "../data/source_xml/full_xml/"
	x3ml_output_folder_path = "../data/X3ML_output/"
	x3ml_output_subs_folder_path = "../data/X3ML_output_with_URIs_swapped/"
	identifier_mapping_path = "../data/source_xml/identifier_mappings/"
	postprocess_output_path = "../data/postprocessing_output/"
	postprocessing_errors_path = "errors/"
	final_rdf_paths = ["../data/postprocessing_output/combined_3m.ttl", "../../Datasets/anthologia_graeca.ttl"]
	lincs_minted_replacement_path = "lincs_minted_replacements.tsv"


	### X3ML ###
	if x3ml_step:
		# Run each XML file through X3ML using Mapping in X3ML/palatina
		# Output to data/X3ML_output as individual ttl files for each xml file
		# Also outputs a combined.ttl file to that folder
		# The combined file is for manual checking and is used to generate the URI tsv maps in postprocessing/
		print("x3ml")
		process_3m(
			input_xml_folder,
			mapping_file_path,
			generator_file_path,
			x3ml_output_folder_path,
			post_process_url,
			"utf-8",
			"2",
			"text/turtle")

	### URI Handling ###
	if create_owl_sameas_step:
		# Create owl:sameAs relationships in a new file for author, city, passage, and scholium
		print("Create owl SameAs file for author, city, passage, scholium")
		alt_urns(post_process_url, identifier_mapping_path, x3ml_output_folder_path + "alt_urns.ttl")

	if create_api_urn_map_step:
		# Need to have run pre-processing scripts first
		# To create data/source_xml/identifier_mappings/ tsv files for author, city, passage, and scholium

		# Connect to Anthologia API to get URIs for scholia and passage
		# These will be primary entity URIs that will be replaced in x3ml_substitute_uris_step
		print("Create URN mapping for passage and scholium using the anthologia API")
		create_scholia_map()
		create_passage_map()

	if create_alt_urn_step:
		# Need to have run x3ml_step before this step
		# These will be primary entity URIs that will be replaced in substitute_uris_step
		print("Create URN mapping for all files other than passage and scholium")

		# First we delete the map.tsv files
		# then this function will keep appending info to them each time it's called

		f_list = [
		"cities_map.tsv",
		"books_map.tsv",
		"texts_map.tsv",
		"keywords_map.tsv",
		"keyword_categories_map.tsv",
		"descriptions_map.tsv",
		"authors_map.tsv",
		"comments_map.tsv",
		"editions_map.tsv",
		]
		# todo ignoring language map for now since manually edited

		for f in f_list:
			try:
				os.remove(f)
			except FileNotFoundError:
				pass

		for filename in os.listdir(x3ml_output_folder_path):
			f = os.path.join(x3ml_output_folder_path, filename)
			if os.path.isfile(f) and filename.endswith(".ttl"):
				print(filename)
				create_urn_map(f)

	if substitute_uris_step:
		## NOTE: you need to run create URN steps again before running this if there is a new version of the data
		# Doing all substitutions on combined files was too slow
		# So we do only relevant substitutions to each individual file then we can combine and do the rest of the processing

		substitution_map = {
			"author.ttl": ["authors_map.tsv", "cities_map.tsv", "passage_api.tsv"],
			"city.ttl": ["cities_map.tsv"],
			"edition.ttl": ["descriptions_map.tsv", "editions_map.tsv", "languages_map.tsv"],
			"manuscript.ttl": [],
			"passage.ttl": ["books_map.tsv", "cities_map.tsv", "comments_map.tsv", "descriptions_map.tsv", "passage_api.tsv", "keywords_map.tsv"],
			"reference.ttl": ["comments_map.tsv", "descriptions_map.tsv", "keyword_categories_map.tsv", "keywords_map.tsv", "languages_map.tsv"],
			"scholium.ttl": ["comments_map.tsv", "passage_api.tsv", "scholia_api.tsv", "texts_map.tsv"],
			"text.ttl": ["editions_map.tsv", "passage_api.tsv", "languages_map.tsv", "texts_map.tsv"],
			"work.ttl": ["descriptions_map.tsv", "languages_map.tsv"],
			"alt_urns.ttl": [
				"authors_map.tsv",
				"books_map.tsv",
				"cities_map.tsv",
				"comments_map.tsv",
				"descriptions_map.tsv",
				"editions_map.tsv",
				"keywords_map.tsv",
				"keyword_categories_map.tsv",
				"languages_map.tsv",
				"texts_map.tsv",
				"passage_api.tsv",
				"scholia_api.tsv"]}

		for filename in substitution_map:
			original_file = x3ml_output_folder_path + filename
			new_file = x3ml_output_subs_folder_path + filename
			# copy the old file to the new file then we do all substitutions on that new file
			shutil.copyfile(original_file, new_file)
			for mapping in substitution_map[filename]:
				replacement_simple(post_process_url, new_file, mapping, False, new_file)

	### Cleaning ###
	if clean_step:
		print("Clean and Replace Step")
		for filename in os.listdir(x3ml_output_subs_folder_path):
			f = os.path.join(x3ml_output_subs_folder_path, filename)
			if os.path.isfile(f) and filename.endswith(".ttl"):
				print(filename)
				clean_rdf(post_process_url, f, postprocess_output_path + filename)
		clean_rdf(post_process_url, postprocess_output_path + "alt_urns.ttl", postprocess_output_path + "alt_urns.ttl")

	### External Labels ###
	if labels_step:
		# These should run on the output from replacements
		# The output in redirects.json were manually added to entity_replacements_noRegex.tsv and then the cleaning step was re-run
		# That step needs to be done again if the entities change
		# LOC and getty added manually added to other_labels.ttl since it was all ontology entities
		# TODO make sure we aren't adding wikidata labels for owl:sameAs objects but that we are getting redirects for them -- this isn't working right now for alt urns
		for authority in ["wikidata", "loc", "getty"]:
			print("Label step: ", authority)
			for filename in os.listdir(postprocess_output_path):
				f = os.path.join(x3ml_output_subs_folder_path, filename)
				if os.path.isfile(f) and filename.endswith(".ttl"):
					print(filename)
					external_labels(post_process_url, f, postprocess_output_path + filename.replace(".ttl", "_"), authority)

	if redirects_step:
		print("handle redirects")
		# add handling of other files if there are redirects. There were none at the time of writing
		handle_wikidata_redirects(post_process_url, postprocess_output_path + "alt_urns.ttl", postprocess_output_path + "alt_urns_/redirects.json")

	# Combine combined_xm.ttl with the external label files
	# Save that new combined file in Datasets/
	# Then rest of validation on that file
	if final_save_step:
		print("Final combine step")
		combine_file_list = []
		for path in [
			postprocess_output_path,
			postprocess_output_path + "alt_urns_/",
			postprocess_output_path + "author_/",
			postprocess_output_path + "city_/",
			postprocess_output_path + "edition_/",
			postprocess_output_path + "manuscript_/",
			postprocess_output_path + "passage_/",
			postprocess_output_path + "reference_/",
			postprocess_output_path + "scholium_/",
			postprocess_output_path + "text_/",
			postprocess_output_path + "work_/"]:
			for filename in os.listdir(path):
				if filename.endswith(".ttl"):
					combine_file_list.append(os.path.join(path, filename))

		print("Combining rdf files and saving new ttl file based on combined graph")
		print(combine_file_list)
		condense_ttl_graph(combine_file_list, final_rdf_paths[1])

		print("removing inverse properties")
		replace_inverse_props(final_rdf_paths[1], final_rdf_paths[1], post_process_url)

		# apply minted LINCS URIs to final file
		# TODO run an optional step to remove lines from the replacement tsv if it's not in the file anymore
		print("Inserting LINCS minted terms")
		replacement_simple(post_process_url, final_rdf_paths[1], lincs_minted_replacement_path, False, final_rdf_paths[1])

		print("validation")
		validation([final_rdf_paths[1]], postprocessing_errors_path, post_process_url, analyze="True")


	# TODO
	# split into 4 files automatically


if __name__ == "__main__":
	main()
