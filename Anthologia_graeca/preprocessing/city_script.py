import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script

conn = psycopg2.connect(dbname= main_script.DB_NAME, user= main_script.DB_USER,password = main_script.DB_PASS,host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/city.xml"

def redirect_output(function):
    original_stdout = sys.stdout #saving a reference to the original standard output
    with open(output_path,function,encoding='utf-8') as f:
        sys.stdout = f # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout # Reset the standard output to its original value    

def change_tag_to(name):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)

def delete_tags():
    with open(output_path, "r",encoding='utf-8') as f:
        lines = f.readlines()
    with open(output_path, "w",encoding='utf-8') as f:
        for line in lines:
            if line.strip("\n") != '</table><table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">':
                f.write(line)

def change_wiki():
    with open(output_path, "r",encoding='utf-8') as f:
        filedata = f.read()
    filedata = filedata.replace("https://www.wikidata.org/wiki","http://www.wikidata.org/entity")
    with open(output_path, "w",encoding='utf-8') as f:
        f.write(filedata) 
  
#Generating City xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    #City tab 1
    # note that alt urns were removed from the returns but we haven't fixed the queries to remove all reference to it.
    cur.execute('''SELECT query_to_xml('SELECT DISTINCT ON(c.id) c.id "city_id",c.unique_id "city_unique_id", n.name "city_name",
                n.language_id "city_name_language_id",c.longitude "city_longitude",c.latitude "city_latitude"
                FROM meleager_city c
                LEFT JOIN (select u.city_id,u.urn,uu.id from (select city_id,MAX(urn) as urn
                from meleager_city_alternative_urns au left join meleager_urn u on u.id = au.urn_id
                group by city_id order by city_id) as u
                left join meleager_urn uu on uu.urn = u.urn) as u on c.id = u.city_id
                LEFT JOIN (select cn.city_id,cn.language_id,n.name from
                (select cn.city_id,min(n.language_id) as language_id from meleager_city_names cn
                Left join meleager_name n on cn.name_id = n.id Group by cn.city_id Order by cn.city_id) as cn
                Left join (select * from meleager_city_names mn left join meleager_name n on n.id = mn.name_id order by mn.city_id asc) as n 
                ON cn.city_id = n.city_id AND cn.language_id = n.language_id) as n ON c.id = n.city_id
                ORDER BY c.id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('w')
    
    # changing tag "row" to "City"
    change_tag_to("city")

    # #City tab 2
    # cur.execute('''SELECT query_to_xml('SELECT c.id "city_id", cu.urn_id "city_urn_id", u.urn "city_urn"
    #             FROM meleager_city c,meleager_urn u, (select * from meleager_city_alternative_urns where urn_id NOT IN (select uu.id from (select city_id,MAX(urn) as urn
    #             from meleager_city_alternative_urns au
    #             left join meleager_urn u on u.id = au.urn_id
    #             group by city_id
    #             order by city_id) as u
    #             left join meleager_urn uu on uu.urn = u.urn)) as cu
    #             where c.id = cu.city_id AND cu.urn_id = u.id
    #             ORDER BY c.id ASC;
    #             ',false,false,'');
    #             ''')

    # # Redirecting output to a file
    # redirect_output('a')

    # #deleting the table tags generated
    # delete_tags()
                
    # # changing tag "row" to "City_alt_urn"
    # change_tag_to("city_alt_urn")

    #City tab 3
    cur.execute('''SELECT query_to_xml('SELECT c.id "city_id",cn.name_id "city_name_id", n.name "city_name", n.language_id "city_name_language_id"
                FROM meleager_city c
                LEFT JOIN meleager_city_names cn ON c.id = cn.city_id
                LEFT JOIN meleager_name n ON cn.name_id = n.id
                ORDER BY c.id ASC;
                ',false,false,''); 
                ''')    

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "City_names"
    change_tag_to("city_names")

    #changing all wiki urn's
    change_wiki()


    #uncomment when table meleager_city_descriptions has values
    #City tab 4
    #cur.execute('''SELECT query_to_xml('SELECT c.id "city_id", 
    #            cd.description_id "description_id", d.description "description",ca.urn_id "city_urn_id", u.urn "city_urn"
    #            FROM meleager_city c
    #            LEFT JOIN meleager_city_descriptions cd ON c.id = cd.city_id
    #            LEFT JOIN meleager_description d ON cd.description_id = d.id
    #            LEFT JOIN meleager_city_alternative_urns ca ON c.id = ca.city_id
    #            LEFT JOIN meleager_urn u ON ca.urn_id = u.id
    #            ORDER BY c.id ASC
    #            ',false,false,''); 
    #            ''')        

    # Redirecting output to a file
    #redirect_output('a')

    #deleting the table tags generated
    #delete_tags() 

    # changing tag "row" to "City_passage"
    #change_tag_to("city_desc")

    
conn.close()
