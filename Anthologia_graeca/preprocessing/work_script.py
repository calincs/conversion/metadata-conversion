import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script

conn = psycopg2.connect(dbname= main_script.DB_NAME, user= main_script.DB_USER,password = main_script.DB_PASS,host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/work.xml"

def redirect_output(function):
    original_stdout = sys.stdout #saving a reference to the original standard output
    with open(output_path,function,encoding='utf-8') as f:
        sys.stdout = f # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout # Reset the standard output to its original value    

def change_tag_to(name):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)

def delete_tags():
    with open(output_path, "r",encoding='utf-8') as f:
        lines = f.readlines()
    with open(output_path, "w",encoding='utf-8') as f:
        for line in lines:
            if line.strip("\n") != '</table><table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">':
                f.write(line)
  
#Generating work xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    #work tab 1
    cur.execute('''SELECT query_to_xml('SELECT w.id "work_id"
                FROM meleager_work w
                ORDER BY w.id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('w')
    
    # changing tag "row" to "work"
    change_tag_to("work")

    #work tab 2
    cur.execute('''SELECT query_to_xml('SELECT wu.work_id "work_id",wu.urn_id "work_urn_id",u.urn "work_urn"
                FROM meleager_work_alternative_urns wu
                LEFT JOIN meleager_urn u ON wu.urn_id = u.id
                ORDER BY wu.work_id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()
                
    # changing tag "row" to "work_alt_urn"
    change_tag_to("work_alt_urn")

    #work tab 3
    cur.execute('''SELECT query_to_xml('SELECT wd.work_id "work_id",wd.description_id "work_description_id",d.description "work_description",d.language_id "description_language_id"
                FROM meleager_work_descriptions wd
                LEFT JOIN meleager_description d ON wd.description_id = d.id
                ORDER BY wd.work_id ASC
                ',false,false,''); 
                ''')    

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "work_description"
    change_tag_to("work_description")


conn.close()
