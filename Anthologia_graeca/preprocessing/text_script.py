import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script

conn = psycopg2.connect(dbname= main_script.DB_NAME, user= main_script.DB_USER,password = main_script.DB_PASS,host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/text.xml"

def redirect_output(function):
    original_stdout = sys.stdout #saving a reference to the original standard output
    with open(output_path,function,encoding='utf-8') as f:
        sys.stdout = f # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout # Reset the standard output to its original value    

def change_tag_to(name):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)

def delete_tags():
    with open(output_path, "r",encoding='utf-8') as f:
        lines = f.readlines()
    with open(output_path, "w",encoding='utf-8') as f:
        for line in lines:
            if line.strip("\n") != '</table><table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">':
                f.write(line)
def cleaning_data():
    with open(output_path, "r",encoding='utf-8') as f:
        filedata = f.read()
    #fixing encoding
    filedata = filedata.replace(" "," ")
    filedata = filedata.replace(" "," ")
    with open(output_path, "w",encoding='utf-8') as f:
        f.write(filedata)  
    
#Generating text xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    #text tab 1
    cur.execute('''SELECT query_to_xml('SELECT t.id "text_id", t.unique_id "text_unique_id", t.text "text", t.edition_id "text_edition_id", t.language_id "text_language_id"
                FROM meleager_text t
                ORDER BY t.id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('w')
    
    # changing tag "row" to "text"
    change_tag_to("text")

    #text tab 2
    cur.execute('''SELECT query_to_xml('SELECT tp.text_id "text_id", tp.passage_id "text_passage_id", p.title "passage_title" 
                FROM meleager_textpassage tp
                LEFT JOIN meleager_passage p ON tp.passage_id = p.id
                ORDER BY tp.text_id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()
                
    # changing tag "row" to "text_passage"
    change_tag_to("text_passage")

    #text tab 3
    cur.execute('''SELECT query_to_xml('SELECT ct.text_id "text_id", ct.comment_id "text_comment_id"
                FROM meleager_commenttextanchor ct
                ORDER BY ct.text_id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()
                
    # changing tag "row" to "comment_text_anchor"
    change_tag_to("comment_text_anchor")

    # cleaning
    cleaning_data()



conn.close()
