import psycopg2
import psycopg2.extras
import main_script
import csv

conn = psycopg2.connect(dbname=main_script.DB_NAME, user=main_script.DB_USER, password=main_script.DB_PASS, host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/identifier_mappings/"

# Generating Author xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    # Author
    cur.execute('''SELECT a.id "author_id",ur.urn "author_urn"
                FROM meleager_author a
                LEFT JOIN (select urn_id, author_id
                from meleager_author_alternative_urns au order by author_id
                ) as u ON a.id = u.author_id
                LEFT JOIN meleager_urn ur ON ur.id = u.urn_id
                ORDER BY a.id ASC;
                ''')

    rows = cur.fetchall()  # get rows
    with open(output_path + 'author_mapping.tsv', 'w', encoding='utf-8', newline='') as f:
        writer = csv.writer(f, delimiter='\t')
        header = ["author_id", "author_alt_urn"]
        writer.writerow(header)
        for row in rows:
            data = []
            data.append("<http://temp.lincsproject.ca/anthologie/author/" + str(row[0]) + ">")
            data.append("<" + str(row[1]) + ">")
            # write the data
            writer.writerow(data)

    # City
    cur.execute('''SELECT c.id "city_id", u.urn "city_urn"
                FROM meleager_city c
                LEFT JOIN (select u.city_id,u.urn,uu.id from (select city_id, urn
                from meleager_city_alternative_urns au left join meleager_urn u on u.id = au.urn_id
                order by city_id) as u
                left join meleager_urn uu on uu.urn = u.urn) as u on c.id = u.city_id
                ORDER BY c.id ASC;
                ''')

    rows = cur.fetchall()  # get rows
    with open(output_path + 'city_mapping.tsv', 'w', encoding='utf-8', newline='') as f:
        writer = csv.writer(f, delimiter='\t')
        header = ["city_id", "city_alt_urn"]
        writer.writerow(header)
        for row in rows:
            data = []
            data.append("<http://temp.lincsproject.ca/anthologie/place/city/" + str(row[0]) + ">")
            data.append("<" + str(row[1]) + ">")
            # write the data
            writer.writerow(data)

    # Passage
    cur.execute('''SELECT p.id "passage_id",u.urn "passage_urn"
                FROM meleager_passage p
                LEFT JOIN (select u.passage_id,u.urn,uu.id from (select passage_id, urn
                from meleager_passage_alternative_urns au left join meleager_urn u on u.id = au.urn_id
                order by passage_id) as u
                left join meleager_urn uu on uu.urn = u.urn) as u on p.id = u.passage_id
                ORDER BY p.id ASC;
                ''')

    rows = cur.fetchall()  # get rows
    with open(output_path + 'passage_mapping.tsv', 'w', encoding='utf-8', newline='') as f:
        writer = csv.writer(f, delimiter='\t')
        header = ["passage_id", "passage_alt_urn"]
        writer.writerow(header)
        for row in rows:
            data = []
            data.append("<http://temp.lincsproject.ca/anthologie/passage/realization/" + str(row[0]) + ">")
            data.append("<" + str(row[1]) + ">")
            # write the data
            writer.writerow(data)

    # Scholium
    cur.execute('''SELECT s.id "scholium_id", u.urn "scholium_urn"
                FROM meleager_scholium s
                LEFT JOIN (select u.scholium_id,u.urn,uu.id from (select scholium_id, urn
                from meleager_scholium_alternative_urns au left join meleager_urn u on u.id = au.urn_id
                order by scholium_id) as u
                left join meleager_urn uu on uu.urn = u.urn) as u on s.id = u.scholium_id
                ORDER BY s.id ASC;
                ''')

    rows = cur.fetchall()  # get rows
    with open(output_path + 'scholium_mapping.tsv', 'w', encoding='utf-8', newline='') as f:
        writer = csv.writer(f, delimiter='\t')
        header = ["scholium_id", "scholium_alt_urn"]
        writer.writerow(header)
        for row in rows:
            data = []
            data.append("<http://temp.lincsproject.ca/anthologie/scholium/realization/" + str(row[0]) + ">")
            data.append("<" + str(row[1]) + ">")
            # write the data
            writer.writerow(data)
