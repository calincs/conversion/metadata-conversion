ALTER TABLE meleager_passage ADD COLUMN title TEXT;

UPDATE meleager_passage
SET title = A.title
from
(select meleager_passage.unique_id,format('%s.%s',b.number,meleager_passage.fragment) title
from meleager_passage
left join meleager_book b on meleager_passage.book_id = b.id
order by meleager_passage.unique_id) as A
where meleager_passage.unique_id = A.unique_id
