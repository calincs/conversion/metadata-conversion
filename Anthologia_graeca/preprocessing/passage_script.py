import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script

conn = psycopg2.connect(dbname= main_script.DB_NAME, user= main_script.DB_USER,password = main_script.DB_PASS,host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/passage.xml"


def redirect_output(function):
    original_stdout = sys.stdout #saving a reference to the original standard output
    with open(output_path,function,encoding='utf-8') as f:
        sys.stdout = f # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout # Reset the standard output to its original value    


def change_tag_to(name):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)


def delete_tags():
    with open(output_path, "r",encoding='utf-8') as f:
        lines = f.readlines()
    with open(output_path, "w",encoding='utf-8') as f:
        for line in lines:
            if line.strip("\n") != '</table><table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">':
                f.write(line)

def cleaning_data():
    with open(output_path, "r", encoding='utf-8') as f:
        filedata = f.read()

    # changing to http
    filedata = filedata.replace("https://anthologiagraeca.org", "http://anthologiagraeca.org")

    # fixing encoding
    filedata = filedata.replace(" ", " ")
    filedata = filedata.replace(" ", " ")
    filedata = filedata.replace("  <passage_sub_fragment />\n", "")

    with open(output_path, "w", encoding='utf-8') as f:
        f.write(filedata)


#Generating Passage xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    # note that alt urns were removed from the returns but we haven't fixed the queries to remove all reference to it.


    # #Passage tab 1
    #-- original query used for 2022 3M mapping
    # cur.execute('''SELECT query_to_xml('SELECT DISTINCT ON(p.id) p.id "passage_id",p.unique_id "passage_unique_id",p.title "passage_title", p.fragment "passage_fragment", p.sub_fragment "passage_sub_fragment", p.book_id "passage_book_id",tp.text_id "text_id", t.unique_id "text_unique_id", t.text "passage_text",t.language_id "passage_text_language_id", u.id "passage_urn_id",
    #             u.urn "passage_urn"
    #             FROM meleager_passage p
    #             LEFT JOIN (select u.passage_id,u.urn,uu.id from (select passage_id,MIN(urn) as urn
    #             from meleager_passage_alternative_urns au left join meleager_urn u on u.id = au.urn_id
    #             group by passage_id order by passage_id) as u
    #             left join meleager_urn uu on uu.urn = u.urn) as u on p.id = u.passage_id
    #             LEFT JOIN meleager_textpassage tp ON p.id = tp.passage_id
    #             LEFT JOIN meleager_text t ON tp.text_id = t.id
    #             ORDER BY p.id ASC
    #             ',false,false,'');
    #             ''')


    #Passage tab 1
    # -- Natalie's Jan 2023 update to remove passage text information from passage because there are multiple texts for the same passage (different languages).
    # If this breaks 3M mapping, then we could add it back but make sure to have one passage per actual text. Natalie thinks there is a better way to fix in 3M though rather than duplicate data.
    cur.execute('''SELECT query_to_xml('SELECT DISTINCT ON(p.id) p.id "passage_id",p.unique_id "passage_unique_id",p.title "passage_title", p.fragment "passage_fragment", p.sub_fragment "passage_sub_fragment", p.book_id "passage_book_id"
                FROM meleager_passage p
                ORDER BY p.id ASC
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('w')
    
    # changing tag "row" to "Passage"
    change_tag_to("passage")

    #Passage tab 2
    cur.execute('''SELECT query_to_xml('SELECT cp.passage_id "passage_id", cp.city_id "city_id",
                c.longitude "city_longitude",c.latitude "city_latitude"
                FROM meleager_citypassage cp
                LEFT JOIN meleager_city c ON cp.city_id = c.id
                ORDER BY cp.passage_id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()
                
    # changing tag "row" to "Passage_alt_urns"
    change_tag_to("passage_cities")

    #Passage tab 3
    cur.execute('''SELECT query_to_xml('SELECT pm.passage_id "passage_id",
                pm.manuscript_id "manuscript_id",mm.title "passage_manuscript_title", mm.url "passage_manuscript_url"
                FROM meleager_passage_manuscripts pm
                LEFT JOIN meleager_manuscript mm ON pm.manuscript_id = mm.id
                ORDER BY pm.passage_id ASC
                ',false,false,''); 
                ''')    

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Passage_names"
    change_tag_to("passage_manuscript")

    # #Passage tab 4
    # cur.execute('''SELECT query_to_xml('SELECT pu.passage_id "passage_id",pu.urn_id "passage_urn_id", u.urn "passage_urn"
    #             FROM (select * from meleager_passage_alternative_urns where urn_id not in (select uu.id from (select passage_id,MIN(urn) as urn
    #             from meleager_passage_alternative_urns au left join meleager_urn u on u.id = au.urn_id
    #             group by passage_id order by passage_id) as u
    #             left join meleager_urn uu on uu.urn = u.urn)) as pu, meleager_urn u
    #             where pu.urn_id = u.id
    #             ORDER BY pu.passage_id ASC
    #             ',false,false,'');
    #             ''')

    # # Redirecting output to a file
    # redirect_output('a')

    # #deleting the table tags generated
    # delete_tags()

    # # changing tag "row" to "Passage_alt_urn"
    # change_tag_to("passage_alt_urn")




    # Passage tab 5
    cur.execute('''SELECT query_to_xml('SELECT pc.passage_id "passage_id",p.title "passage_title", pc.comment_id "passage_comment_id", mc.comment_type "passage_comment_type",d.description "comment_description", d.language_id "comment_description_language_id"
                FROM meleager_passage_comments pc
                LEFT JOIN meleager_passage p on pc.passage_id = p.id
                LEFT JOIN meleager_comment mc ON pc.comment_id = mc.id
                LEFT JOIN meleager_comment_descriptions cd ON pc.comment_id = cd.comment_id
                LEFT JOIN meleager_description d ON cd.description_id = d.id
                ORDER BY pc.passage_id ASC
                ',false,false,''); 
                ''')
    
    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Passage_comments"
    change_tag_to("passage_comments")
    
    # passage tab 7
    cur.execute('''SELECT query_to_xml('SELECT pd.passage_id "passage_id", pd.description_id "passage_description_id",
                d.description "passage_description", d.language_id "description_language_id"
                FROM meleager_passage_descriptions pd
                LEFT JOIN meleager_description d ON pd.description_id = d.id
                ORDER BY pd.passage_id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Passage_desc"
    change_tag_to("passage_description")
    
    # Passage tab 8
    cur.execute('''SELECT query_to_xml('SELECT pi.passage_id "passage_id", 
                pi.medium_id "passage_medium_id", m.type "passage_medium_type",m.url "passage_medium_url"
                FROM meleager_passage_images pi
                LEFT JOIN meleager_medium m ON pi.medium_id = m.id
                ORDER BY pi.passage_id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Passage_img"
    change_tag_to("passage_img")

    # Passage tab 9
    cur.execute('''SELECT query_to_xml('SELECT pk.passage_id "passage_id", pk.keyword_id "passage_keyword_id",
                k.category_id "passage_keyword_category_id",n.name "keyword_name", n.language_id "keyword_name_language_id"
                FROM meleager_keywordpassage pk
                LEFT JOIN meleager_keyword k ON pk.keyword_id = k.id
                LEFT JOIN meleager_keyword_names kn ON pk.keyword_id = kn.keyword_id
                LEFT JOIN meleager_name n ON kn.name_id = n.id
                ORDER BY pk.passage_id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Passage_keywords"
    change_tag_to("passage_keywords")  


    # Passage tab 10
    cur.execute('''SELECT query_to_xml('SELECT pr.passage_id "passage_id", pr.externalreference_id "passage_external_reference_id",
                r.title "passage_external_reference_title",r.url "passage_external_reference_url"
                FROM meleager_passage_external_references pr
                LEFT JOIN meleager_externalreference r ON pr.externalreference_id = r.id
                ORDER BY pr.passage_id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "external_references"
    change_tag_to("passage_external_references")     

    # Passage tab 11
    cur.execute('''SELECT query_to_xml('SELECT pr.from_passage_id "from_passage_id", pr.to_passage_id "to_passage_id"	
                FROM meleager_passage_internal_references pr
                ORDER BY pr.from_passage_id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "passage_internal_references"
    change_tag_to("passage_internal_references")

    # Passage tab 12
    cur.execute('''SELECT query_to_xml('SELECT b.id "book_id",b.number "book_number", b.work_id "book_work_id"
                FROM meleager_book b
                ORDER BY b.id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "book"
    change_tag_to("book")

    # cleaning
    cleaning_data()
conn.close()
