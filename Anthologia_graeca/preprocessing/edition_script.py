import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script

conn = psycopg2.connect(dbname=main_script.DB_NAME, user=main_script.DB_USER, password=main_script.DB_PASS, host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/edition.xml"


def redirect_output(function):
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(output_path, function, encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout  # Reset the standard output to its original value


def change_tag_to(name):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)


def delete_tags():
    with open(output_path, "r", encoding='utf-8') as f:
        lines = f.readlines()
    with open(output_path, "w", encoding='utf-8') as f:
        for line in lines:
            if line.strip("\n") != '</table><table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">':
                f.write(line)


def cleaning_data():
    with open(output_path, "r", encoding='utf-8') as f:
        filedata = f.read()

    filedata = filedata.replace("  <edition_metadata>{}</edition_metadata>\n", "")

    with open(output_path, "w", encoding='utf-8') as f:
        f.write(filedata)


# Generating edition xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    # edition tab 1
    cur.execute('''SELECT query_to_xml('SELECT e.id "edition_id", e.edition_type "edition_type",e.metadata "edition_metadata", e.work_id "work_id", ed.description_id "edition_description_id", d.description "edition_description", d.language_id "edtion_description_language_id"
                FROM meleager_edition e
                LEFT JOIN meleager_edition_descriptions ed ON e.id = ed.edition_id
                LEFT JOIN meleager_description d ON ed.description_id = d.id
                ORDER BY e.id ASC
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('w')

    # changing tag "row" to "edition"
    change_tag_to("edition")

    # edition tab 2
    cur.execute('''SELECT query_to_xml('SELECT ed.edition_id "edition_id", ed.description_id "edition_description_id", d.description "edition_description", d.language_id "edtion_description_language_id"
                FROM meleager_edition_descriptions ed
                LEFT JOIN meleager_description d ON ed.description_id = d.id
                ORDER BY ed.edition_id ASC
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('a')

    # deleting the table tags generated
    delete_tags()

    # changing tag "row" to "edition"
    change_tag_to("edition_description")

    cleaning_data()

conn.close()
