import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script

conn = psycopg2.connect(dbname=main_script.DB_NAME, user=main_script.DB_USER, password=main_script.DB_PASS, host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/manuscript.xml"


def redirect_output(function):
    original_stdout = sys.stdout  #saving a reference to the original standard output
    with open(output_path, function, encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout  # Reset the standard output to its original value


def change_tag_to(name):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)


def delete_tags():
    with open(output_path, "r", encoding='utf-8') as f:
        lines = f.readlines()
    with open(output_path, "w", encoding='utf-8') as f:
        for line in lines:
            if line.strip("\n") != '</table><table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">':
                f.write(line)


def cleaning_data():
    with open(output_path, "r", encoding='utf-8') as f:
        filedata = f.read()

    filedata = filedata.replace("  <manuscript_credit />\n", "")

    with open(output_path, "w", encoding='utf-8') as f:
        f.write(filedata)


# Generating manuscript xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    # manuscript tab 1
    cur.execute('''SELECT query_to_xml('SELECT m.id "manuscript_id", m.title "manuscript_title", m.url "manuscript_url", m.credit "manuscript_credit"
                FROM meleager_manuscript m
                ORDER BY m.id ASC
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('w')

    # changing tag "row" to "manuscript"
    change_tag_to("manuscript")

    # manuscript tab 2
    cur.execute('''SELECT query_to_xml('SELECT md.manuscript_id "manuscript_id", md.description_id "manuscript_description_id",d.description "manuscript_description", d.language_id "manuscript_description_language_id"
                FROM meleager_manuscript_descriptions md
                LEFT JOIN meleager_description d ON md.description_id = d.id
                ORDER BY md.manuscript_id ASC
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('a')

    # deleting the table tags generated
    delete_tags()

    # changing tag "row" to "manuscript"
    change_tag_to("manuscript_description")

    # manuscript tab 3
    cur.execute('''SELECT query_to_xml('SELECT mk.manuscript_id "manuscript_id", mk.keyword_id "manuscript_keyword_id",k.category_id "keyword_category_id", n.name "keyword_name", n.language_id "keyword_name_language_id"
                FROM meleager_manuscript_keywords mk
                LEFT JOIN meleager_keyword k ON mk.keyword_id = k.id
                LEFT JOIN meleager_keyword_names km ON mk.keyword_id = km.keyword_id
                LEFT JOIN meleager_name n ON km.name_id = n.id
                ORDER BY mk.manuscript_id ASC
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('a')

    # deleting the table tags generated
    delete_tags()

    # changing tag "row" to "manuscript"
    change_tag_to("manuscript_keyword")

    cleaning_data()

conn.close()
