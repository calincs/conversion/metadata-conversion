
import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script


conn = psycopg2.connect(dbname=main_script.DB_NAME, user=main_script.DB_USER, password=main_script.DB_PASS, host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/author.xml"


def redirect_output(function, output_path):
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(output_path, function, encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout  # Reset the standard output to its original value


def change_tag_to(name, output_path):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)


def delete_tags(output_path):
    with open(output_path, "r", encoding='utf-8') as f:
        lines = f.readlines()
    with open(output_path, "w", encoding='utf-8') as f:
        for line in lines:
            if line.strip("\n") != '</table><table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">':
                f.write(line)


def change_wiki(output_path):
    with open(output_path, "r", encoding='utf-8') as f:
        filedata = f.read()
    filedata = filedata.replace("https://www.wikidata.org/wiki", "http://www.wikidata.org/entity")
    with open(output_path, "w", encoding='utf-8') as f:
        f.write(filedata)


# Generating Author xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    # <author>
    # Note that author urn was removed from the select but left in the rest of the query. It could be removed but needs to be retested.
    cur.execute('''SELECT query_to_xml(' SELECT DISTINCT ON(a.id) a.id "author_id", a.unique_id "author_unique_id", a.tlg_id "author_tlg_id", a.city_born_id "author_city_born_id", a.city_died_id "author_city_died_id", n.name "author_name",n.language_id "author_name_language_id"
                FROM meleager_author a

                LEFT JOIN (select an.author_id,an.language_id,n.name from

                (select an.author_id,min(n.language_id) as language_id from meleager_author_names an
                Left join meleager_name n on an.name_id = n.id Group by an.author_id Order by an.author_id) as an
                Left join (select * from meleager_author_names mn left join meleager_name n on n.id = mn.name_id order by mn.author_id asc) as n ON an.author_id = n.author_id AND an.language_id = n.language_id) as n ON a.id = n.author_id

                LEFT JOIN (select MAX(urn_id) as urn_id, author_id
                from meleager_author_alternative_urns au group by author_id order by author_id
                ) as u ON a.id = u.author_id 
                LEFT JOIN meleager_urn ur ON ur.id = u.urn_id

                LEFT JOIN (select u.city_id,u.urn,uu.id from (select city_id,MAX(urn) as urn
                from meleager_city_alternative_urns au left join meleager_urn u on u.id = au.urn_id
                group by city_id order by city_id) as u
                left join meleager_urn uu on uu.urn = u.urn) as cb ON a.city_born_id = cb.city_id
                LEFT JOIN (select u.city_id,u.urn,uu.id from (select city_id,MAX(urn) as urn
                from meleager_city_alternative_urns au left join meleager_urn u on u.id = au.urn_id
                group by city_id order by city_id) as u
                left join meleager_urn uu on uu.urn = u.urn) as cd ON a.city_died_id = cd.city_id

                ORDER BY a.id ASC;
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('w', output_path)

    # changing tag "row" to "author"
    change_tag_to("author", output_path)


    # <author_names>
    cur.execute('''SELECT query_to_xml(' SELECT a.id "author_id", an.name_id "author_name_id", n.name "author_name",n.language_id "author_name_language_id"
                FROM meleager_author a
                LEFT JOIN meleager_author_names an ON a.id = an.author_id
                LEFT JOIN meleager_name n ON an.name_id = n.id
                ORDER BY a.id ASC;
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('a', output_path)

    # deleting the table tags generated
    delete_tags(output_path)

    # changing tag "row" to "author_names"
    change_tag_to("author_names", output_path)

    # <author_passage>
    cur.execute('''SELECT query_to_xml(' SELECT a.id "author_id", ap.id "author_passage_id",
                n.name "author_name",n.language_id "author_name_language_id",ap.passage_id "passage_id",p.title "passage_title"
                FROM meleager_author a
                LEFT JOIN meleager_authorpassage ap ON a.id = ap.author_id
                LEFT JOIN meleager_author_names an ON a.id = an.author_id
                LEFT JOIN meleager_name n ON an.name_id = n.id
                LEFT JOIN meleager_passage p ON ap.passage_id = p.id
                ORDER BY a.id ASC;
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('a', output_path)

    # deleting the table tags generated
    delete_tags(output_path)

    # changing tag "row" to "author_passage"
    change_tag_to("author_passage", output_path)

    #changing all wiki urn's
    change_wiki(output_path)


    ## un-comment when there are values in author_image
    #cur.execute('''SELECT query_to_xml(' SELECT a.id "author_id", ai.medium_id "medium_id",
    #            m.type "medium_type", m.url "medium_url",u.urn "author_urn",alt.urn_id "author_urn_id"
    #            FROM meleager_author a
    #            LEFT JOIN meleager_author_images ai ON a.id = ai.author_id
    #            LEFT JOIN meleager_medium m ON ai.medium_id = m.id
    #            LEFT JOIN meleager_author_alternative_urns alt ON a.id = alt.author_id
    #            LEFT JOIN meleager_urn u ON alt.urn_id = u.id  
    #            ORDER BY a.id ASC;
    #            ',false,false,''); 
    #            ''')

    # Redirecting output to a file
    #redirect_output('a')

    #deleting the table tags generated
    #delete_tags()

    # changing tag "row" to "author_image"
    #change_tag_to("author_image")



    ## un-comment when there are values in author_desc

    #cur.execute('''SELECT query_to_xml(' SELECT a.id "author_id", ad.description_id "description_id",
    #            d.description "description",d.language_id "description_language_id",u.urn "author_urn",alt.urn_id "author_urn_id"
    #            FROM meleager_author a
    #            LEFT JOIN meleager_author_descriptions ad ON a.id = ad.author_id
    #            LEFT JOIN meleager_description d ON ad.description_id = d.id
    #            LEFT JOIN meleager_author_alternative_urns alt ON a.id = alt.author_id
    #            LEFT JOIN meleager_urn u ON alt.urn_id = u.id  
    #            ORDER BY a.id ASC;
    #            ',false,false,''); 
    #            ''')   

    # Redirecting output to a file
    #redirect_output('a')

    #deleting the table tags generated
    #delete_tags()

    # changing tag "row" to "author_desc"
    #change_tag_to("auhtor_desc")


conn.close()
