ALTER TABLE meleager_scholium ADD COLUMN title TEXT;

UPDATE meleager_scholium
SET title = A.title
from
(select meleager_scholium.unique_id,format('%s.%s.%s',b.number,p.fragment,meleager_scholium.number) title
from meleager_scholium
left join meleager_passage p on meleager_scholium.passage_id = p.id
left join meleager_book b on p.book_id = b.id
order by meleager_scholium.unique_id) as A
where meleager_scholium.unique_id = A.unique_id
