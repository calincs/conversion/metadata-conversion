DB_NAME = "NAME"
DB_USER = "USER"
DB_HOST = "HOST"
DB_PASS = "PASS"

import author_script
import city_script
import passage_script
import scholium_script
#import work_script
import edition_script
import alignment_script
import manuscript_script
import text_script
import reference_script
