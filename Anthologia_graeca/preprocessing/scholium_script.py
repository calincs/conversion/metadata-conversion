import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script

conn = psycopg2.connect(dbname= main_script.DB_NAME, user= main_script.DB_USER,password = main_script.DB_PASS,host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/scholium.xml"

def redirect_output(function):
    original_stdout = sys.stdout #saving a reference to the original standard output
    with open(output_path,function,encoding='utf-8') as f:
        sys.stdout = f # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout # Reset the standard output to its original value    

def change_tag_to(name):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)

def delete_tags():
    with open(output_path, "r",encoding='utf-8') as f:
        lines = f.readlines()
    with open(output_path, "w",encoding='utf-8') as f:
        for line in lines:
            if line.strip("\n") != '</table><table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">':
                f.write(line)
  
#Generating scholium xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    # note that alt urns were removed from the returns but we haven't fixed the queries to remove all reference to it.

    #scholium tab 1
    cur.execute('''SELECT query_to_xml(
                'SELECT DISTINCT ON(s.id) s.id "scholium_id",s.unique_id "scholium_unique_id",s.title "scholium_title",s.number "scholium_number",s.passage_id "scholium_passage_id"
                FROM meleager_scholium s
                ORDER BY s.id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('w')
    
    # changing tag "row" to "scholium"
    change_tag_to("scholium")

    #scholium tab 2
    cur.execute('''SELECT query_to_xml('SELECT cs.scholium_id "scholium_id", cs.city_id "scholium_city_id"
                FROM meleager_cityscholium cs
                LEFT JOIN meleager_city c ON cs.city_id = c.id
                ORDER BY cs.scholium_id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()
                
    # changing tag "row" to "scholium"
    change_tag_to("scholium_cities")

    # #scholium tab 3
    # cur.execute('''SELECT query_to_xml('SELECT su.scholium_id "scholium_id",su.urn_id "scholium_urn_id", u.urn "scholium_urn"
    #             FROM (select * from meleager_scholium_alternative_urns where urn_id not in (select uu.id from (select scholium_id,max(urn) as urn
    #             from meleager_scholium_alternative_urns au left join meleager_urn u on u.id = au.urn_id
    #             group by scholium_id order by scholium_id) as u
    #             left join meleager_urn uu on uu.urn = u.urn)) as su, meleager_urn u
    #             where su.urn_id = u.id
    #             ORDER BY su.scholium_id ASC
    #             ',false,false,'');
    #             ''')

    # # Redirecting output to a file
    # redirect_output('a')

    # #deleting the table tags generated
    # delete_tags()

    # # changing tag "row" to "ALT_URN"
    # change_tag_to("scholium_alt_urn")

    #scholium tab 4
    cur.execute('''SELECT query_to_xml('SELECT sc.scholium_id "scholium_id",s.title "scholium_title", sc.comment_id "scholium_comment_id", mc.comment_type "scholium_comment_type", d.description "comment_description", d.language_id "comment_description_language_id"
                FROM meleager_scholium_comments sc
                LEFT JOIN meleager_comment mc ON sc.comment_id = mc.id
                LEFT JOIN meleager_comment_descriptions cd ON sc.comment_id = cd.comment_id
                LEFT JOIN meleager_description d ON cd.description_id = d.id
                LEFT JOIN meleager_scholium s ON sc.scholium_id = s.id
                ORDER BY sc.scholium_id ASC
                ',false,false,''); 
                ''')        

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags() 

    # changing tag "row" to "comment"
    change_tag_to("scholium_comments")




    # scholium tab 5
    cur.execute('''SELECT query_to_xml('SELECT sd.scholium_id "scholium_id", sd.description_id "scholium_description_id", d.description "scholium_description", d.language_id "description_language_id"
                FROM meleager_scholium_descriptions sd
                LEFT JOIN meleager_description d ON sd.description_id = d.id
                ORDER BY sd.scholium_id ASC
                ',false,false,''); 
                ''')
    
    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "scholium_comments"
    change_tag_to("scholium_description")
    
    # scholium tab 7
    cur.execute('''SELECT query_to_xml('SELECT si.scholium_id "scholium_id", si.medium_id "scholium_medium_id", m.type "scholium_medium_type",m.url "scholium_medium_url"
                FROM meleager_scholium_images si
                LEFT JOIN meleager_medium m ON si.medium_id = m.id
                ORDER BY si.scholium_id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "scholium_desc"
    change_tag_to("scholium_img")
    
    # scholium tab 8
    cur.execute('''SELECT query_to_xml('SELECT sk.scholium_id "scholium_id", sk.keyword_id "scholium_keyword_id", k.category_id "scholium_keyword_category_id",n.name "keyword_name", n.language_id "keyword_name_language_id"
                FROM meleager_keywordscholium sk
                LEFT JOIN meleager_keyword k ON sk.keyword_id = k.id
                LEFT JOIN meleager_keyword_names kn ON sk.keyword_id = kn.keyword_id
                LEFT JOIN meleager_name n ON kn.name_id = n.id
                ORDER BY sk.scholium_id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "scholium_img"
    change_tag_to("scholium_keyword")

    # scholium tab 9
    cur.execute('''SELECT query_to_xml('SELECT sm.id "scholium_manuscript_unique_id", sm.scholium_id "scholium_id", sm.manuscript_id "scholium_manuscript_id", ms.text_id "scholium_text_id",mm.title "scholium_manuscript_title", mm.url "scholium_manuscript_url"
                FROM meleager_scholium_manuscripts sm
                LEFT JOIN meleager_scholium_texts ms ON sm.scholium_id = ms.scholium_id
                LEFT JOIN meleager_manuscript mm ON sm.manuscript_id = mm.id
                ORDER BY sm.scholium_id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "scholium_keywords"
    change_tag_to("scholium_manuscript")  


    # scholium tab 10
    cur.execute('''SELECT query_to_xml('SELECT st.scholium_id "scholium_id",s.title "scholium_title",st.text_id "scholium_text_id", t.unique_id "scholium_text_unique_id", sm.manuscript_id "scholium_manuscript_id",t.text "scholium_text", t.language_id "scholium_text_language_id" 
                FROM meleager_scholium_texts st
                LEFT JOIN meleager_scholium_manuscripts sm ON st.scholium_id = sm.scholium_id
                LEFT JOIN meleager_scholium s on st.scholium_id = s.id
                LEFT JOIN meleager_text t ON st.text_id = t.id
                ORDER BY st.scholium_id ASC
                ',false,false,''); 
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "external_references"
    change_tag_to("scholium_text")     


conn.close()
