import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script

conn = psycopg2.connect(dbname= main_script.DB_NAME, user= main_script.DB_USER,password = main_script.DB_PASS,host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/alignment.xml"

def redirect_output(function, output_path):
    original_stdout = sys.stdout #saving a reference to the original standard output
    with open(output_path,function,encoding='utf-8') as f:
        sys.stdout = f # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout # Reset the standard output to its original value    

def change_tag_to(name, output_path):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)

#Generating alignment xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    #alignment tab 1
    cur.execute('''SELECT query_to_xml('SELECT a.id "alignment_id", unique_id "alignment_unique_id", a.alignment_data "alignment_data", a.text_1_id "alignment_text_1_id", a.text_2_id "alignment_text_2_id"
                FROM meleager_alignment a
                ORDER BY a.id ASC
                ',false,false,''); 
                ''')

    # Redirecting output to a file
    redirect_output('w', output_path)
    
    # changing tag "row" to "alignment"
    change_tag_to("alignment", output_path)


conn.close()
