import sys
import psycopg2
import psycopg2.extras
import xml.etree.ElementTree as ET
import main_script

conn = psycopg2.connect(dbname= main_script.DB_NAME, user= main_script.DB_USER,password = main_script.DB_PASS,host=main_script.DB_HOST)
conn.set_client_encoding('UTF8')

output_path = "../data/source_xml/full_xml/reference.xml"

def redirect_output(function):
    original_stdout = sys.stdout #saving a reference to the original standard output
    with open(output_path,function,encoding='utf-8') as f:
        sys.stdout = f # Change the standard output to the file
        print(cur.fetchone()[0])
        sys.stdout = original_stdout # Reset the standard output to its original value    

def change_tag_to(name):
    tree = ET.parse(output_path)
    for elem in tree.findall("row"):
        elem.tag = name
    tree.write(output_path, encoding='utf-8', xml_declaration=True)

def delete_tags():
    with open(output_path, "r",encoding='utf-8') as f:
        lines = f.readlines()
    with open(output_path, "w",encoding='utf-8') as f:
        for line in lines:
            if line.strip("\n") != '</table><table xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">':
                f.write(line)
def cleaning_data():
    with open(output_path, "r",encoding='utf-8') as f:
        filedata = f.read()
    #fixing encoding
    filedata = filedata.replace(" "," ")
    filedata = filedata.replace(" "," ")
    filedata = filedata.replace("https://www.wikidata.org/wiki","http://www.wikidata.org/entity")
    filedata = filedata.replace("https://anthologiagraeca.org/passages/","http://anthologiagraeca.org/passages/")
    with open(output_path, "w",encoding='utf-8') as f:
        f.write(filedata)   
#Generating Reference xml file
with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:

    #Reference tab 1
    cur.execute('''SELECT query_to_xml('SELECT k.id "keyword_id", k.unique_id "keyword_unique_id", k.category_id "keyword_category_id"
                FROM meleager_keyword k
                ORDER BY k.id ASC
                ',false,false,'');
                ''')

    # Redirecting output to a file
    redirect_output('w')
    
    # changing tag "row" to "Reference"
    change_tag_to("keyword")

    #Reference tab 2
    cur.execute('''SELECT query_to_xml('SELECT kc.id "keyword_category_id", kc.unique_id "keyword__category_unique_id",n.name "keyword_category_name", n.language_id "keyword_category_name_language_id"
                FROM meleager_keywordcategory kc
                LEFT JOIN meleager_keywordcategory_names kcn ON kc.id = kcn.keywordcategory_id
                LEFT JOIN meleager_name n ON kcn.name_id = n.id
                ORDER BY kc.id ASC
                ',false,false,'');  
                ''')

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()
                
    # changing tag "row" to "Reference_alt_urns
    change_tag_to("keyword_category")

    #Reference tab 3
    cur.execute('''SELECT query_to_xml('SELECT kn.keyword_id "keyword_id", kn.name_id "keyword_name_id",n.name "keyword_name", n.language_id "keyword_name_language_id"
                FROM meleager_keyword_names kn
                LEFT JOIN meleager_name n ON kn.name_id = n.id
                ORDER BY kn.id ASC
                ',false,false,'');
                ''')    

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Reference_names"
    change_tag_to("keyword_names")

    #Reference tab 4
    cur.execute('''SELECT query_to_xml('SELECT kcn.keywordcategory_id "keyword_category_id", kcn.name_id "keyword_name_id"
                FROM meleager_keywordcategory_names kcn
                ORDER BY kcn.id ASC
                ',false,false,'');  
                ''')        

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags() 

    # changing tag "row" to "Reference_alt_urn"
    change_tag_to("keyword_category_names")


    # Reference tab 5
    cur.execute('''SELECT query_to_xml('SELECT c.id "comment_id", c.unique_id "comment_unique_id", c.comment_type "comment_type",d.description "comment_description", d.language_id "comment_description_language_id"
                FROM meleager_comment c
                LEFT JOIN meleager_comment_descriptions cd ON c.id = cd.comment_id 
                LEFT JOIN meleager_description d ON cd.description_id = d.id
                ORDER BY c.id ASC
                ',false,false,'');  
                ''')
    
    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Reference_comments"
    change_tag_to("comment")
    
    # Reference tab 7
    cur.execute('''SELECT query_to_xml('SELECT cd.comment_id "comment_id", cd.description_id "comment_description_id"
                FROM meleager_comment_descriptions cd
                ORDER BY cd.comment_id ASC
                ',false,false,'');  
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Reference_desc"
    change_tag_to("comment_description")
    
    # Reference tab 8
    cur.execute('''SELECT query_to_xml('SELECT ci.comment_id "comment_id", ci.medium_id "comment_medium_id"
                FROM meleager_comment_images ci
                ORDER BY ci.comment_id ASC
                ',false,false,'');  
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Reference_img"
    change_tag_to("comment_img")

    # Reference tab 9
    cur.execute('''SELECT query_to_xml('SELECT m.id "medium_id", m.unique_id "medium_unique_id", m.type "medium_type", m.url "medium_url"
                FROM meleager_medium m
                ORDER BY m.id ASC
                ',false,false,'');  
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Reference_keywords"
    change_tag_to("medium")  


    # Reference tab 10
    cur.execute('''SELECT query_to_xml('SELECT md.medium_id "medium_id", md.description_id "medium_description_id"
                FROM meleager_medium_descriptions md
                ORDER BY md.medium_id ASC
                ',false,false,'');  
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "external_references"
    change_tag_to("medium_description")     

    # Reference tab 11
    cur.execute('''SELECT query_to_xml('SELECT mk.medium_id "medium_id", mk.keyword_id "medium_keyword_id"
                FROM meleager_medium_keywords mk
                ORDER BY mk.medium_id ASC
                ',false,false,'');  
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "Reference_internal_references"
    change_tag_to("medium_key")

    # Reference tab 12
    cur.execute('''SELECT query_to_xml('SELECT u.id "urn_id",u.source "urn_source",u.urn "urn"
                FROM meleager_urn u
                ORDER BY u.id ASC
                ',false,false,'');
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "book"
    change_tag_to("urn")

    # Reference tab 12
    cur.execute('''SELECT query_to_xml('SELECT n.id "name_id",n.name "name",n.language_id "name_language_id"
                FROM meleager_name n
                ORDER BY n.id ASC
                ',false,false,'');  
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "book"
    change_tag_to("name")

    # Reference tab 12
    cur.execute('''SELECT query_to_xml('SELECT l.code "code", l.iso_name "iso_name", l.preferred "preferred"
                FROM meleager_language l
                ORDER BY l.code ASC
                ',false,false,'');
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "book"
    change_tag_to("language")

    # Reference tab 12
    cur.execute('''SELECT query_to_xml('SELECT d.id "description_id", d.description "description", d.language_id "description_language_id"
                FROM meleager_description d
                ORDER BY d.id ASC
                ',false,false,'');  
                ''')   

    # Redirecting output to a file
    redirect_output('a')

    #deleting the table tags generated
    delete_tags()

    # changing tag "row" to "book"
    change_tag_to("description")

    # cleaning
    cleaning_data()

conn.close()
