import requests
import re
import time

# TODO fix encoding (angled brackets showing up as &lt; and &gt; Could just remove that from the labels though)

list_file = "type_uris.txt"
label_file = "type_uris_labels.txt"


URI_list = []
with open (list_file, "r") as in_file:
	for line in in_file:
		URI_list.append(line.strip("\n"))


with open(label_file, "w") as out_f:
	out_f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n\n")

	for URI in URI_list:
		label = ""

		response = requests.get(URI + ".rdf")
		time.sleep(1)

		if response.status_code == 200:
			rdf = response.text

			# english labels
			labels = re.findall(r'<skos:prefLabel xml:lang="en">(.*?)<\/skos:prefLabel>', rdf)
			for l in labels:
				out_f.write(URI + ' rdfs:label "' + l + '"@en .\n')


			# french labels
			labels = re.findall(r'<skos:prefLabel xml:lang="fr">(.*?)<\/skos:prefLabel>', rdf)
			for l in labels:
				out_f.write("<" + URI + '> rdfs:label "' + l + '"@fr .\n')

		else:
			print("ERROR: ", URI, response.status_code)
