import re

with open("no_types_aug30extras.ttl", "r") as in_f:
	with open("no_types_aug30extras_fixed.ttl", "w") as out_f:
		file_string = in_f.read()
		file_string = re.sub(r'@prefix temp_lincs_temp: <temp.lincsproject.ca/>', r'@prefix lincs: <http://id.lincsproject.ca/>', file_string)
		file_string = re.sub(r'http://id.lincsproject.ca/([0-9a-zA-Z]{11})', r'lincs:\1', file_string)

		out_f.write(file_string)