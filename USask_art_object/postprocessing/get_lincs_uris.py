import re
import requests
import json


def get_placeholders(input_dir, file):
	temps_list = []
	with open(input_dir + file) as in_file:
		file_contents = in_file.read()

	temp_lincs = re.findall(r"(temp_lincs_temp:[A-Za-z0-9\#].*?)[;,\.]", file_contents)
	for t in temp_lincs:
		temps_list.append(t)

	temp_lincs = re.findall(r"(<http:\/\/temp\.lincsproject\.ca\/.+?>)", file_contents)
	for t in temp_lincs:
		temps_list.append(t)


	return list(set(temps_list))


def get_uid():
	mint_url = "https://uid.lincsproject.ca/generateuid"
	response = requests.get(mint_url)
	try:
		uuid = response.json()["uid"]
	except KeyError:
		print(response.status_code, ":  Failed to get UID: ", response.content)
		uuid = None
	return uuid





def main():

	def make_uid_list():
		input_dir = "../data/results_templincs/"
		files = ["has_types.ttl", "no_types.ttl"]

		temp_list_master = []
		for file in files:
			temp_list = get_placeholders(input_dir, file)
			temp_list_master = temp_list_master + temp_list

		with open("templincs_list.txt", "w") as lincs_out:
			for i in list(set(temp_list_master)):
				lincs_out.write(i.strip()+"\n")


	#make_uid_list()

	def make_replacement_list():
		temps_list = []
		with open("templincs_list.txt") as f_list:
			for line in f_list:
				temps_list.append(line.strip("\n").strip(" "))

		with open("UUIDs.txt") as uuid_json:
			uuid_dict = json.load(uuid_json)
			uuid_list = uuid_dict["uids"]


		with open("lincs_replacements.txt", "w") as out_f:
			for i in range(len(temps_list)):
				out_f.write(temps_list[i] + "\t" +"<http://id.lincsproject.ca/" + uuid_list[i] +">\n")


	#make_replacement_list()

	def replace():
		input_dir = "../data/results_templincs/"
		files = ["has_types.ttl", "no_types.ttl"]
		output_dir = "../data/results_minted/"

		for file in files:
			with open(input_dir + file, "r") as in_file:
				file_string = in_file.read()

				with open(output_dir + file, "w") as out_file:
					with open("lincs_replacements.txt") as replacements:
						for line in replacements:
							old = line.strip("\n").split("\t")[0]
							new = line.strip("\n").split("\t")[1]
							file_string = file_string.replace(old, new)

					out_file.write(file_string)

	replace()

main()