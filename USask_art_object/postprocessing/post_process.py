import re

# mapping file should be a two column tsv file.
# the first column is the regex pattern we want to replace.
# the second column should be what the above patter is replaced with.
# if there is a variable needed as part of the replacement,
# then you should make the whole pattern a capture group and make the variable a capture group
# then put {VAR} in the replacement value where the variable should be
mapping_file = "replacements_aug30.tsv"


input_dir = "../data/results_templincs/"
output_dir = "../data/results_minted/"
files = ["has_types_aug30extras.ttl"]


def get_replacements(mapping_file):
	replacements_list = []
	with open(mapping_file, "r") as mappings:
		for line in mappings:
			line_split = line.strip("\n").split("\t")
			replacements_list.append(line_split)
	return replacements_list



def main():

	replacements_list = get_replacements(mapping_file)

	for file in files:
		with open(input_dir + file) as in_file:
			file_content = in_file.read()

		for replacement in replacements_list:

			# simple replacements
			if "{VAR}" not in replacement[1]:
				file_content = re.sub(replacement[0], replacement[1], file_content)

			# complex replacements that require variables in the replaced value
			else:
				matches = re.findall(replacement[0], file_content)
				for match in matches:
					original = match[0]
					new = replacement[1].replace("{VAR}", match[1])
					file_content = re.sub(original, new, file_content)


		with open(output_dir + file, "w") as out_file:
			out_file.write(file_content)





main()
