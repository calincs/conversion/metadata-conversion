import re

setting = "getty"
setting = "lincs"

input_file = ["../data/results_templincs/has_types.ttl", "../data/results_templincs/has_types.ttl"]
uris = []


if setting == "getty":
	pattern = "<(http:\/\/vocab\.getty\.edu\/aat\/[0-9]+)>"
	out_file = "type_uris.txt"

# if setting == "lincs":
# 	pattern = "<(http:\/\/vocab\.getty\.edu\/aat\/[0-9]+)>"
# 	out_file = "temp_lincs.txt"


with open(out_file, "w") as out:
	for file in input_file:
		with open(file) as f:
			f_text = f.read()
			uris = uris + re.findall(r"<(http:\/\/vocab\.getty\.edu\/aat\/[0-9]+)>", f_text)


	for i in list(set(uris)):
		out.write(i + "\n")
