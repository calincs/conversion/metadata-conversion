import os
import sys
import getopt
import re
import xml.etree.cElementTree as ET
import xml.dom.minidom as minidom

try:
    # for Python2
    from cStringIO import StringIO as BytesIO
except ImportError:
    # for Python3
    from io import BytesIO

# Dictionary of terms that need to be replaced
fix_terms = {}


def main():
    # Remove 1st argument from the list of command line arguments
    argument_list = sys.argv[1:]

    if len(argument_list) == 0:
        print("Using Default Settings Please run with -h / --help option for available commands")

    # Options
    options = "ioh"

    # Long options
    long_options = ["input", "output", "help"]

    # Parsing argument
    arguments, values = getopt.getopt(argument_list, options, long_options)

    # Default arguments
    in_file = os.path.join('data', 'USask_final15_30Aug.xml')
    out_file = os.path.join('data', 'pre_processed_USask_final15_30Aug.xml')

    # in_file = os.path.join('data', 'USask_18Aug_mult_medium_added.xml')
    # out_file = os.path.join('data', 'pre_processed_USask_18Aug_mult_medium_added.xml')


    try:
        # checking each argument
        for currentArgument, currentValue in arguments:

            if currentArgument in ("-h", "--help"):
                print("\nBy default this script will process data/<in_file> and save the processed "
                      "file as data/pre_processed.xml in utf-8 encoding\n")
                print("\tArguments:")
                print("\t-i --input : input file to be processed")
                print("\t-o --output : output file to be processed")
                print("\nExample: python pre_process.py -i data/y90s-personography-rdfxml.xml -o data/processed.xml")
                exit()

            if currentArgument in ("-i", "--input"):
                in_file = currentValue

            if currentArgument in ("-o", "--output"):
                out_file = currentValue

    except getopt.error as err:
        # output error, and return with an error code
        print(str(err))

    # Declare namespaces used and register to Element Tree
    namespaces = {'owl': 'http://www.w3.org/2002/07/owl#',
                  'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
                  'dcterms': "http://purl.org/dc/terms/"}

    for ns in namespaces:
        ET.register_namespace(ns, namespaces[ns])

    # Load data

    tree = ET.parse(in_file)
    root = tree.getroot()

    # Find all base nodes
    base_node = root.findall('rdf:Description', namespaces)

    #################################################
    # Handle all URMedium[N] and URSupport[N] cases #
    #################################################
    """
    Each UnreconciledMaterial[#] element has a number on the end of it, this number matches the number at the end of
    URMedium[#]_[!] and there can be multiple URMediums for each UnreconciledMaterial this is represented by the "!",
    the ultimate goal is to squash all this information into single UnreconciledMaterial elements.

    Using the keys above:

    # -> base_num
    ! -> num

    For Example

    <UnreconciledMaterial1>latex enamel paint</UnreconciledMaterial1>                    // This is base element 1
    <URMedium1_1>enamel paint</URMedium1_1>
    <URMedium1_URL_1>http://vocab.getty.edu/aat/300014910</URMedium1_URL_1>
    <URMedium1_2>latex</URMedium1_2>
    <URMedium1_URL_2>http://vocab.getty.edu/aat/300013021</URMedium1_URL_2>
    <UnreconciledMaterial2>aluminum paint</UnreconciledMaterial2>                        // This is base element 2
    <URMedium2_1>paint</URMedium2_1>
    <URMedium2_URL_1>http://vocab.getty.edu/aat/300015029</URMedium2_URL_1>
    <URMedium2_2>aluminum</URMedium2_2>
    <URMedium2_URL_2>http://vocab.getty.edu/aat/300011015</URMedium2_URL_2>

    Should transform into

    <UnreconciledMaterial medium="enamel paint" url="http://vocab.getty.edu/aat/300014910">latex enamel paint</UnreconciledMaterial>
    <UnreconciledMaterial medium="latex" url="http://vocab.getty.edu/aat/300013021">latex enamel paint</UnreconciledMaterial>
    <UnreconciledMaterial medium="paint" url="http://vocab.getty.edu/aat/300015029">aluminum paint</UnreconciledMaterial>
    <UnreconciledMaterial medium="aluminum" url="http://vocab.getty.edu/aat/300011015">aluminum paint</UnreconciledMaterial>

    URSupport is handled very much in the same way except there is no base_num, there is always just one base element
    """
    for node in base_node:
        all_nodes = list(node)

        # Get all URMedium elements but not the URL elements
        URMedium_elements = []
        for child in all_nodes:
            if "URMedium" in child.tag and child.text is not None and "URL" not in child.tag:
                URMedium_elements.append(child)
        # URSupport is handled the same as URMedium except there is just a single base num
        URSupport_elements = []
        for child in all_nodes:
            if "URSupport" in child.tag and child.text is not None and "URL" not in child.tag:
                URSupport_elements.append(child)

        # Need to track all base_elements so we can remove them once we have information needed from them
        base_elements = set()
        for URMedium in URMedium_elements:
            # Get num variables
            base_num = re.findall(r'\d+', URMedium.tag.split("_")[0])[0]
            num = URMedium.tag.split("_")[-1]

            # Get UnreconciledMaterial<base_num> Element
            base_element = node.find("UnreconciledMaterial" + base_num, namespaces)
            base_elements.add(base_element)

            #Attempt to find associated url_element
            url_element = node.find("URMedium" + base_num + "_URL_" + num, namespaces)

            # Build new UnreconciledMaterial Element
            if url_element is not None and url_element.text is not None:
                sub = ET.SubElement(node, "UnreconciledMaterial", dict(medium=URMedium.text, url=url_element.text))
            else:
                sub = ET.SubElement(node, "UnreconciledMaterial", dict(medium=URMedium.text))

            sub.text = base_element.text
            sub.tail = '\n'

            # Remove URMedium and URMedium_URL elements
            node.remove(url_element)
            node.remove(URMedium)

        for URSupport in URSupport_elements:
            # Get base element
            base_element = node.find("UnreconciledSupport", namespaces)
            base_elements.add(base_element)

            # Get url element
            url_element = node.find(URSupport.tag + "_URL", namespaces)
            if url_element is not None and url_element.text is not None:
                sub = ET.SubElement(node, "UnreconciledSupport", dict(medium=URSupport.text, url=url_element.text))
            else:
                sub = ET.SubElement(node, "UnreconciledSupport", dict(medium=URSupport.text))

            sub.text = base_element.text
            sub.tail = '\n'

            # Remove URSupport and URSupport_URL
            node.remove(URSupport)
            node.remove(url_element)

        # Final cleanup, remove all base elements used to build information
        for element in base_elements:
            node.remove(element)

    ##########################
    # Handle Everything Else #
    ##########################

    multiple_children = ('ObjectTitle')
    has_url = ('ArtistName', 'ArtistGroupName', 'CurrentOwner', 'PreviousOwner_Group',
               'PreviousOwner_Person', 'Medium', 'Support', 'Category')

    # Iterate through all base nodes
    for node in base_node:
        # Iterate through all elements
        all_nodes = list(node)
        for child in all_nodes:

            # Strip Element of numbers
            element = re.sub(r'\d+', '', child.tag)

            # If element is blank, remove it, move on
            if child.text is None:
                try:
                    node.remove(child)
                except Exception as e:
                    pass

            # If element is in multiple_children or has_url, collapse the element
            elif element in multiple_children.join(has_url):

                # Get URL element tag and value if it exists
                url_element = None
                if element == 'ObjectTitle':
                    url = None
                # Artist_URL did not follow proper naming scheme so url element tag had to be manually specified
                elif element == "ArtistName":
                    url = "Artist_URL" + ''.join([i for i in child.tag if i.isdigit()])
                else:
                    url = child.tag + "_URL"

                if url is not None:
                    url_element = node.find(url, namespaces)
                    node.remove(url_element)

                # Add element stripped of number and url
                if url_element is None or url_element.text is None:
                    sub = ET.SubElement(node, element)
                    sub.text = child.text
                    sub.tail = '\n'
                else:
                    sub = ET.SubElement(node, element, dict(url=url_element.text))
                    sub.text = child.text
                    sub.tail = '\n'

                node.remove(child)

    # Beautify the XML
    buf = BytesIO()
    buf.write(ET.tostring(root))
    buf.seek(0)
    root = minidom.parse(buf)
    with open(os.path.join("data", "pre_processed_unstripped.xml"), 'w', encoding='utf-8') as f:
        f.write(root.toprettyxml(indent='\t', newl='\n'))

    # Beautify the Beautified XML by removing blank lines, and remove timestamps
    with open(os.path.join("data", "pre_processed_unstripped.xml"), 'r', encoding='utf-8') as f_in:
        with open(out_file, 'w', encoding='utf-8') as f_out:
            for line in f_in:
                if line.strip():
                    f_out.write(line.replace("T00:00:00", ''))
    os.remove(os.path.join("data", "pre_processed_unstripped.xml"))


if __name__ == '__main__':
    main()
