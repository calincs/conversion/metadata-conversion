# Pre Processing

## Usage

From the USask_art_object directory call:

```bash
python3 preprocessing/pre_process.py -i data/<filename.xml> -o data/<new_filename.xml>
```

You can also just overwrite the default -i and -o by modifying the __in_file__ and __out_file__ variables.

## What this Script Does

- Squashes UnreconciledMaterial[#], URMedium[#]_[1->N], and URMedium[#]_URL_[1->N] into a single UnreconciledMaterial elment

For example:

```xml
<UnreconciledMaterial1>latex enamel paint</UnreconciledMaterial1>                    // This is base element 1
<URMedium1_1>enamel paint</URMedium1_1>
<URMedium1_URL_1>http://vocab.getty.edu/aat/300014910</URMedium1_URL_1>
<URMedium1_2>latex</URMedium1_2>
<URMedium1_URL_2>http://vocab.getty.edu/aat/300013021</URMedium1_URL_2>
<UnreconciledMaterial2>aluminum paint</UnreconciledMaterial2>                        // This is base element 2
<URMedium2_1>paint</URMedium2_1>
<URMedium2_URL_1>http://vocab.getty.edu/aat/300015029</URMedium2_URL_1>
<URMedium2_2>aluminum</URMedium2_2>
<URMedium2_URL_2>http://vocab.getty.edu/aat/300011015</URMedium2_URL_2>
```
Transforms into:

```xml    
<UnreconciledMaterial medium="enamel paint" url="http://vocab.getty.edu/aat/300014910">latex enamel paint</UnreconciledMaterial>
<UnreconciledMaterial medium="latex" url="http://vocab.getty.edu/aat/300013021">latex enamel paint</UnreconciledMaterial>
<UnreconciledMaterial medium="paint" url="http://vocab.getty.edu/aat/300015029">aluminum paint</UnreconciledMaterial>
<UnreconciledMaterial medium="aluminum" url="http://vocab.getty.edu/aat/300011015">aluminum paint</UnreconciledMaterial>
```

------------------------------

- Squashes UnreconciledSupport[1->N], URSupport[1->N], and URSupport[1->N]_URL into a single UnreconciledSupport

For example:

```xml
<UnreconciledSupport>1/4 waterjet-cut aluminum</UnreconciledSupport>
<URSupport1>aluminum</URSupport1>
<URSupport1_URL>http://vocab.getty.edu/aat/300011015</URSupport1_URL>
<URSupport2>1/4 waterjet-cut </URSupport2>
```

Transforms into:
```xml
<UnreconciledSupport medium="aluminum" url="http://vocab.getty.edu/aat/300011015">1/4 waterjet-cut aluminum</UnreconciledSupport>
<UnreconciledSupport medium="1/4 waterjet-cut ">1/4 waterjet-cut aluminum</UnreconciledSupport>
```
------------------------------

- Squash any declared <element>s in __multiple_children__[1->N] variable (Line 181) into single <element>s

For example

```xml
<ObjectTitle1>The Blacksmith's Shop</ObjectTitle1>
<ObjectTitle2>Little Eccleston, Lancashire, England</ObjectTitle2>
```

Transforms into 

```xml
<ObjectTitle>The Blacksmith's Shop</ObjectTitle>
<ObjectTitle>Little Eccleston, Lancashire, England</ObjectTitle>
```
------------------------------

- Squash any declared <element>s in __has_url__[1->N] variable (Line 182) with their respective <element>[1->N]_URL

For example

```xml
<Category1>landscape</Category1>
<Category1_URL>http://vocab.getty.edu/aat/300015636</Category1_URL>
<Category2>painting</Category2>
<Category2_URL>http://vocab.getty.edu/aat/300033618</Category2_URL>
```

Transforms into

```xml
<Category url="http://vocab.getty.edu/aat/300015636">landscape</Category>
<Category url="http://vocab.getty.edu/aat/300033618">painting</Category>
```
------------------------------
Squashing means creating a single element with one our more attribute tags describing their elements

------------------------------
- Deletes blank nodes such as 
```xml
<Support1></Support1>
```

------------------------------

- Strips the time from any Datetime object
------------------------------
- Beautifies the final XML
------------------------------
- Deletes any unnecessary nodes used in the above squash procedures.