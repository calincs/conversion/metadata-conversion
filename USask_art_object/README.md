# University of Saskatchewan Art Object Data
https://saskcollections.org/kenderdine/Browse/objects

## Technical Documentation:

Note that data model changes in August 2022 were made directly to the 3M output (See https://gitlab.com/calincs/conversion/metadata-conversion/-/issues/141). This means that if the X3ML workflow is re-run, the output will not be the same as the most recent version of the data. Any changes to that data should be made to that file. Updates will need to be made to the 3M mappings to process new data from this project. Also, this 3M mapping was setup before we had finalized our policies for using 3M and it will generate new URIs every time the mapping is executed. This would also need to be updated should more data need to be run through the same 3M mapping.

### USask Keys

* **USask Full Map - All Data - No Types** -> Matching Tab : matching_backup_no_types.xml
* **USask Full Map - All Data - No Types** -> Generator Tab : generator_backup_no_types.xml
* **USask Full Map - All Data - Has Types** -> Matching Tab : matching_backup_has_types.xml
* **USask Full Map - All Data - Has Types** -> Generator Tab : generator_backup_has_types.xml
