from rdflib import Graph
import requests
import re
import zipfile
import io
import os


def process_3m(input_path, map_file, policy_file, output_folder, post_process_url, encoding, uuid_test_size, output_format):
	"""
	This function sends your data to X3ML.
	"""

	# Checking if input_file is a file or folder
	is_file = os.path.isfile(input_path)
	is_directory = os.path.isdir(input_path)

	if is_directory:
		files = []
		for filename in os.listdir(input_path):
			if filename.endswith(".xml"):
				files.append(os.path.join(input_path, filename))
	elif is_file:
		files = [input_path]
	else:
		print("Error: Please input a valid input file name or path")
		return

	# send each input file to X3ML engine
	for input_file in files:
		print(f"Converting {input_file}")
		output_filename = input_file.split("/")[-1].replace(".xml", ".ttl")

		url = post_process_url + f"/conversion/x3ml/custom?output_file={output_filename}&format_output_file={output_format}&uuid_test_size={uuid_test_size}"
		payload = {}
		files = [
			('input_file', ('input.xml', open(input_file, 'rb'), 'text/xml')),
			('map_file', ('Mapping.x3ml', open(map_file, 'rb'), 'application/octet-stream')),
			('policy_file', ('generator.xml', open(policy_file, 'rb'), 'text/xml'))]
		headers = {}
		response = requests.request("POST", url, headers=headers, data=payload, files=files)
		response.encoding = encoding

		if str(response.status_code).startswith("2"):
			try:
				z = zipfile.ZipFile(io.BytesIO(response.content))
				z.extractall(output_folder)
			except Exception as err:
				print(f"Failed to save X3ML conversion output\n", response.text)
				print(err)
		else:
			print(f"Error: Your file {input_file} could not be converted.")
			print(response.status_code)
			print(response.text)


def replacement(input_file_path, post_process_url, replacements_file, regex, output_file_path):
	# calling post processing api to make replacements in the entity_replacements file
	# note that author names were normalized here in post processing rather than in the original spreadsheet

	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	content = content.replace("<http://www.geonames.org/>\n        a       crm:E53_Place .", "")

	# replace spaces in temp lincs URIs with underscores
	# the temp URIs had several invalid characters
	original_matches = re.findall('<http:\/\/temp\.lincsproject\.ca\/((.|\n)+?>)', content)
	for orig_match in original_matches:
		orig_match = orig_match[0]
		rep_match = orig_match.strip()
		rep_match = rep_match.replace(" ", "_")
		rep_match = rep_match.replace('"', "_")
		rep_match = rep_match.replace('[', "_")
		rep_match = rep_match.replace(']', "_")
		rep_match = rep_match.replace("'", "_")
		rep_match = rep_match.replace("`", "_")
		rep_match = rep_match.replace(":", "_")
		rep_match = rep_match.replace("\n", "_")
		rep_match = rep_match.replace("(", "_")
		rep_match = rep_match.replace(")", "_")
		rep_match = rep_match.replace(",", "_")
		rep_match = rep_match.replace("|", "_")
		rep_match = re.sub('_+', '_', rep_match) ## replace multiple underscores with one
		content = content.replace(orig_match, rep_match)

	# fix loc URIs
	original_matches = re.findall('(<http:\/\/id\.loc\.gov\/authorities\/subjects\/(.+?)\.html>)', content)
	for orig_match in original_matches:
		content = content.replace(orig_match[0], orig_match[0].replace(".html", ""))

	original_matches = re.findall('(<http:\/\/id\.loc\.gov\/authorities\/genreForms\/(.+?)\.html>)', content)
	for orig_match in original_matches:
		content = content.replace(orig_match[0], orig_match[0].replace(".html", ""))


	# there shouldn't be any uuid prefixes but in case there are, replace so file is valid
	content = content.replace("uuid:", "http://uuid.ca/")
	content = content.replace("urn:uuid:", "http://uuid.ca/")
	content = content.replace("urn:http://uuid.ca/", "http://uuid.ca/")

	# replace erroneous place declarations
	for homosaurus_id in ["homoit0001128", "homoit0001276", "homoit0000208"]:
		content = content.replace(f"<https://homosaurus.org/v3/{homosaurus_id}>\n        a       crm:E53_Place .", "")

	with open(output_file_path, 'w') as f:
		f.write(content)


def replacement_simple(input_file_path, post_process_url, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	with open(output_rdf_path, 'w') as f:
		f.write(content)


def external_labels(input_path, output_path, post_process_url, authority):
	if authority == "wikidata":
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs&lang_code=fr'
	else:
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs'
	payload = {}
	headers = {}
	files = [('file', ('test_labels.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	try:
		z = zipfile.ZipFile(io.BytesIO(response.content))
		z.extractall(output_path)
	except:
		print(f"Output failed for external labels with {authority}\n", response.text)


def coordinates(input_path, output_path, post_process_url):
	url = post_process_url + f'/coordinates/geonames?labels=True'
	payload = {}
	headers = {}
	files = [('file', ('test_coordinates.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	with open(output_path, "w") as f_out:
		f_out.write(response.text)


def validation(input_path_list, output_path, post_process_url, analyze="True"):
	files = []
	for file in input_path_list:
		filename = file.split("/")[-1]
		files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	payload = {
		'encoding': 'utf-8',
		'rdf_format': 'turtle',
		'analyze': analyze}
	headers = {}
	response = requests.post(post_process_url + '/validate/', headers=headers, files=files, data=payload)

	with open(output_path + "rdf_validation.json", "w") as out_file:
		out_file.write(response.text)


def condense_ttl_graph(input_ttl_dir, output_ttl_path):
	# get a list of all .ttl files in the rootdir. This includes within nested folders
	filelist = []
	for subdir, dirs, files in os.walk(input_ttl_dir):
		for file in files:
			filepath = subdir + os.sep + file

			if filepath.endswith(".ttl"):
				filelist.append(filepath)

	# sort the filelist so that they always get added in the same order
	filelist.sort()

	# creates graph and outputs the new graph to replace the original file
	g = Graph()

	for ttl_path in filelist:
		print(ttl_path)
		g.parse(ttl_path)

	g.serialize(destination=output_ttl_path)


def replace_inverse_props(input_path, output_path, post_process_url):
	url = post_process_url + '/inverses/'
	files = {'file': ("inverses.ttl", open(input_path, 'rb'))}
	response = requests.request("POST", url, files=files)
	if "200" in str(response.status_code):
		with open(output_path, 'w') as f:
			f.write(response.text)
	else:
		print(f"ERROR: did not replace inverse properties in {input_path}\n{response}")


def main(
	post_process_url="https://postprocess.lincsproject.ca",
	input_path="../data/x3ml_output/",
	output_path="../data/postprocessing_output/",
	error_output_path="output/error_files/",
	input_files=[
		"HistSexBooks.ttl",
		"HistSexCollections.ttl",
		"HistSexDigital.ttl"],
	combined_output_path='../../Datasets/histsex.ttl',
	replacement_mapping="entity_replacements_noRegex.tsv",
	minting_mapping="minting.tsv"):

	#post_process_url = "http://0.0.0.0:80"

	run_x3ml = False

	get_external_labels = False
	get_coordinates = False

	for file in input_files:
		print("Processing file: ", file)

		if run_x3ml:
			print("running x3ml")
			xml = file.replace(".ttl", ".xml")
			folder = file.replace(".ttl", "")

			process_3m(
				f"../data/preprocessed_xml/{xml}",
				f"../X3ML/{folder}/3m-x3ml-output.x3ml",
				f"../X3ML/{folder}/generator-policy.xml",
				"../data/x3ml_output/",
				post_process_url,
				"utf-8",
				"2",
				"text/turtle")

		input_file_path = input_path + file
		output_file_path = output_path + file

		# use the file with the replaced output for other functions
		replacements_file = replacement_mapping
		print("making replacements")
		replacement(input_file_path, post_process_url, replacements_file, "False", output_file_path)

		print("file validation")
		validation([output_file_path], error_output_path + file[:-4] + "_", post_process_url, analyze="False")

		# These should run on the output from replacements
		if get_external_labels:
			# note VIAF was done separately using a script that sent batches of the URIs to the label endpoint since there were so many in the books file
			for authority in ["wikidata", "getty", "loc"]:
				print("Getting labels from: ", authority)
				external_labels(output_file_path, output_path + file[:-4], post_process_url, authority)

		if get_coordinates:
			print("Getting Geonames coordinates and labels")
			coordinates(output_file_path, output_path + file[:-4] + "_geonames_coordinates.ttl", post_process_url)

	print("condensing final ttl file")
	condense_ttl_graph(output_path, combined_output_path)

	print("replace inverse properties")
	replace_inverse_props(combined_output_path, combined_output_path, post_process_url)

	print("entity minting")
	replacement_simple(combined_output_path, post_process_url, minting_mapping, "False", combined_output_path)

	print("final rdf validation")
	validation([combined_output_path], error_output_path + "combined_", post_process_url)

	#TODO
	#kg_lookup(input_file_path)
	#mint entities
	#replace reconciled entities


if __name__ == "__main__":
	main()
