# 3M export data/backup for HistSex

## 3M Mappings

All xml files mentioned in this section can be found in [HistSex/data/preprocessed_xml/](https://gitlab.com/calincs/conversion/metadata-conversion/-/tree/master/HistSex/data/preprocessed_xml).

- HistSexBooks
	+ This is the most recent mapping for the `HistSexBooks` data as of November 4, 2022. 

- HistSexCollections
	+ This is the most recent mapping for the `HistSexCollections` data as of November 4, 2022. 

- HistSexDigital
	+ This is the most recent mapping for the `HistSexDigital` data as of November 7, 2022. 

## 3M Outputs

- HistSexBooks_output.ttl
	+ Most recent output file for HistSexBooks data

- HistSexCollections_output.ttl
	+ Most recent output file for HistSexBooks data

- HistSexDigital_output.ttl
	+ Most recent output file for HistSexBooks data
