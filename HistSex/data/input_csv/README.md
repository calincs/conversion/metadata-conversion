The csv files in `original/` (for June and August dumps) were direct exports from the HistSex Omeka storage so they contain combined columns and many empty columns. 

The cleaned up version of these csv files are in [this spreadsheet](https://docs.google.com/spreadsheets/d/1f__kQxlG9VpSMwAzvq0wIABH6jPb3g21sp4gYFI8oD0/edit#gid=0) for August (most up to date). That spreadsheet is the best source to use when running the pre-processing scripts to generate XML for 3M. The csv files in `August2022/cleaned/` come from that cleaned spreadsheet.

Reconciliation for some authors, institutions, and publishers still needs to be done in the google sheet. Then the three main tabs need to be updated to pull from the reconciliation tab. Then the three main tabs can be re-exported and the conversion can be rerun. That should be the final change needed before data can be published.

No future exports from Omeka will be done. Any changes should be made directly to the LINCS copy of the source data or the conversion scripts. Once the data is published in ResearchSpace, changes will be made directly there.