import csv
from dict2xml import dict2xml


class Dictlist(dict):
    # to allow duplicate records
    def __setitem__(self, key, value):
        try:
            self[key]
        except KeyError:
            super(Dictlist, self).__setitem__(key, [])

        self[key].append(value)



def main():

    encoding = "utf-8"

    # each nested list if the name of the spreadsheet followed by the name to be used as the parent elements in the xml
    for spreadsheet in [["Books", "book"], ["Collections", "collection"], ["Digital", "digital_record"]]:
        file = open(f'../data/input_csv/August2022/cleaned/histsex_aug2022_{spreadsheet[0]}.csv', encoding=encoding)

        # reading csv file to pandas dataframe
        csv_reader = csv.reader(file)

        # change this value to edit number of lines to be skipped in csv
        N = 5

        for i in range(N):
            next(csv_reader)

        # get header row and convert to lowercase
        rows = list(csv_reader)

        headers = [header.lower() for header in rows[0]]
        
        #skipping the headers row
        rows = rows[1:]
        #define xml
        xml = ''
        all_books = Dictlist()
        all_collections = Dictlist()
        all_digitals = Dictlist()

        '''The for loop iterates through the rows in the sheet, whereas the while loop iterates through each cell in the row'''
        for row in rows:
            #row = row[:len(headers)]

            # dict to hold all the details of each people artifact
            work = Dictlist()
            record = {}
            book_info = Dictlist()


            # dict to hold each detail present
            items = Dictlist()  # this definition allows duplicate values to stored in the dict

            i = 0
            while i < len(row):
                # check if loop not at the last cell in row
                # check if next column has same name as current col + '_id'


                if headers[i] == 'id' or headers[i] == 'record_datetime'\
                or headers[i] == 'histsex' :
                    if row[i]:
                        record[headers[i]] = row[i].strip()

                elif i != len(row) - 1 and headers[i + 1] == headers[i] + '_id':
                    # check if cell values are not none
                    if row[i] and row[i + 1]:  
                        # changing author_1, author_2 to just author  
                        if 'author' in headers[i]:
                            book_info['author'] = {'name': row[i], 'id': row[i + 1]}
                        # adding nested name and id elements in the xml
                        else:
                            book_info[str(headers[i])] = {'name': row[i], 'id': row[i + 1]}
                        i += 1
                else:
                    if row[i]:
                        # simply add the element in the xml without any nesting
                        book_info[str(headers[i])] = str(row[i])
                i += 1

            # add nested categories
            if record:
                items['record'] = record
            if book_info:
                items['book_info'] = book_info

            # define artifact xml
            if spreadsheet[1] == 'book':
                all_books['book'] = items
            elif spreadsheet[1] =='collection':
                all_collections['collection'] =items
            elif spreadsheet[1] == 'digital_record':
                all_digitals['digital_record'] = items

        # convert to xml
        book_xml = dict2xml({'books':all_books})
        collection_xml = dict2xml({'collections':all_collections})
        digital_xml = dict2xml({'digital_records':all_digitals})

        # write to file
        if spreadsheet[1] == 'book':
            with open(f'../data/preprocessed_XML/HistSex{spreadsheet[0]}.xml', 'w', encoding=encoding) as f:
                f.write("<?xml version='1.0' encoding='UTF-8'?>")
                f.write(book_xml)
        if spreadsheet[1] == 'collection':
            with open(f'../data/preprocessed_XML/HistSex{spreadsheet[0]}.xml', 'w', encoding=encoding) as f:
                f.write("<?xml version='1.0' encoding='UTF-8'?>")
                f.write(collection_xml)
        if spreadsheet[1] == 'digital_record':
            with open(f'../data/preprocessed_XML/HistSex{spreadsheet[0]}.xml', 'w', encoding=encoding) as f:
                f.write("<?xml version='1.0' encoding='UTF-8'?>")
                f.write(digital_xml)
main()
