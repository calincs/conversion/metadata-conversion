# Converting csv to XML

The HistSex data stored in [this spreadsheet](https://docs.google.com/spreadsheets/d/1f__kQxlG9VpSMwAzvq0wIABH6jPb3g21sp4gYFI8oD0) was exported to three different .csv files and processed separately. The input files can be found in `HistSex/data/input_csv/August2022/cleaned/` which was then converted to XML using the python script preprocess.py. The file path for each csv file is hardcoded in the python script. 

The script automatically skips the first 5 lines in the .csv files which contain some information about the data. 

After that it creates three different xml files for the Books, Collections and Digital data.
These xml files are stored in 'HistSex/data/preprocessed_XML'.

The owl:sameAs relationships for books were manually added to ttl files.
