# Completed Datasets

Completed and combined LINCS datasets to support data operations. This project's CI script will push these files to S3 storage and to the user-facing triplestore. New datasets need to be added to the CI script explicitly and their graph names added below.

Graph names and files:

- http://graph.lincsproject.ca/adarchive
  - adarchive_1.ttl
  - adarchive_illustrations.ttl
  - adarchive_2.ttl
  - Status: these three files are published in RS, but will continue to be edited here for now. More files coming soon.
- http://graph.lincsproject.ca/anthologia-graeca
  - anthologia_graeca_split contains a split version of anthologia_graeca.ttl due to uploading size constraints
  - anthologia_graeca.ttl
  - Status: published in RS, but will not be edited there. Changes will happen here.
- http://graph.lincsproject.ca/ethnomusicology
  - ethnomusicology.ttl
  - Status: in RS-Review. Changes happen here.
- http://graph.lincsproject.ca/hist-canada/ind-affairs
  - ind-affairs.ttl
  - Status: in RS-Review. Changes happen here.
- http://graph.lincsproject.ca/hist-canada/hist-cdns
  - hist-cdns/
  - Status: in RS-Review. Changes happen here.
- http://graph.lincsproject.ca/histsex
  - histsex.ttl
  - Status: in RS-Review. Changes happen here.
- http://graph.lincsproject.ca/moeml
  - moeml_personography.ttl
  - moeml_locations.ttl
  - Status: in RS-Review. Changes happen here.
- http://graph.lincsproject.ca/orlando
  - Stored in S3 storage only due to size constraints
  - <https://aux.lincsproject.ca/public/converted_data/datasets/orlando/bibliography.ttl>
  - <https://aux.lincsproject.ca/public/converted_data/datasets/orlando/biography.ttl>
- http://graph.lincsproject.ca/usask-art
  - usaskart.ttl
  - Status: published in RS. Future changes to be applied directly to RS data.
- http://graph.lincsproject.ca/yellow-nineties
  - yellow1890s.ttl
  - Status: published in RS. Future changes to be applied directly to RS data.