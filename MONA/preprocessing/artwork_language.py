import json
import requests
import time


def post_language(text: str):
	url = "https://lincs-api.lincsproject.ca/api/language"
	headers = {
		"accept": "application/json",
		"Content-Type": "application/json"
	}
	data = {"text": text}

	response = requests.post(url, json=data, headers=headers)

	if response.status_code == 200:
		return response.json()
	else:
		return {"error": response.status_code, "message": response.text}


def get_lod_export_from_file():
	file_path = "../data/1_source_data/LOD_export_Jan24_2025.json"
	try:
		# Open and read the JSON file
		with open(file_path, "r", encoding="utf-8") as file:
			data = json.load(file)
		return data

	except FileNotFoundError:
		print(f"The file at {file_path} was not found.")
	except json.JSONDecodeError as e:
		print(f"Error decoding JSON: {e}")
	except Exception as e:
		print(f"An unexpected error occurred: {e}")

	return None


def main():
	data = get_lod_export_from_file()

	with open("artwork_lang_auto.tsv", "w", encoding="utf-8") as f_out:
		for artwork in data["data"]["artworks"]:
			mona_id = artwork["MONA_id"]
			title_en = artwork["title_en"]
			title_fr = artwork["title_fr"]

			if title_en != None and title_en == title_fr:
				time.sleep(1)
				result = post_language(title_en)
				try:
					lang = result["language"]
				except:
					print(title_en)
					print(result)
					print()
					lang = ""
				f_out.write("\t".join([str(mona_id), str(title_en), str(lang)]) + "\n")


main()