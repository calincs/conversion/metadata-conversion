import json
import requests
import xml.etree.ElementTree as ET
import io


"""
- ask mona if we should include category "other". Ignoring for now.
- ask about the owner with no label and no wikidata
- should i add a rule where if the artwork's english name is the same as its french then don't include an english one?
"""


def get_lod_export_from_file():
	file_path = "../data/1_source_data/LOD_export_Jan24_2025.json"
	try:
		# Open and read the JSON file
		with open(file_path, "r", encoding="utf-8") as file:
			data = json.load(file)
		return data

	except FileNotFoundError:
		print(f"The file at {file_path} was not found.")
	except json.JSONDecodeError as e:
		print(f"Error decoding JSON: {e}")
	except Exception as e:
		print(f"An unexpected error occurred: {e}")

	return None


def get_lod_export_from_api():

	url = "https://picasso.iro.umontreal.ca/~mona/api/v3/LODExport"

	try:
		response = requests.get(url)
		response.raise_for_status()
		data = response.json()
		return data

	except requests.exceptions.RequestException as e:
		print(f"An error occurred: {e}")

	return None


def xml_print(root):
	tree = ET.ElementTree(root)
	xml_buffer = io.StringIO()
	tree.write(xml_buffer, encoding="unicode", xml_declaration=True)
	print(xml_buffer.getvalue())


def xml_to_file(root):
	file_path = "../data/2_preprocessing_output/mona.xml"
	tree = ET.ElementTree(root)
	ET.indent(tree, space="\t", level=0)
	tree.write(file_path, encoding="utf-8", xml_declaration=True)


def xml_init():
	return ET.Element("data")


def artist_xml(data, root):
	for artist in data["data"]["artists"]:
		tag = "group" if artist["is_collective"] else "person"

		artist_element = ET.SubElement(root, tag)

		if artist["MONA_id"]:
			ET.SubElement(artist_element, "MONA_id").text = str(artist["MONA_id"])
		if artist["label"]:
			ET.SubElement(artist_element, "label_fr").text = artist["label"]
		if artist["Wikidata_id"]:
			ET.SubElement(artist_element, "sameas").text = f"http://www.wikidata.org/entity/{artist['Wikidata_id']}"
		if artist["ULAN_id"]:
			ET.SubElement(artist_element, "sameas").text = f"http://vocab.getty.edu/ulan/{artist['ULAN_id']}"


def artwork_xml(data, root):
	category_map = {cat["MONA_id"]: cat for cat in data["data"]["categories"]}
	producer_map = {prod["MONA_id"]: prod for prod in data["data"]["producers"]}
	artist_map = {art["MONA_id"]: art for art in data["data"]["artists"]}

	for artwork in data["data"]["artworks"]:
		artwork_element = ET.SubElement(root, "artwork")

		# Add basic artwork details
		ET.SubElement(artwork_element, "MONA_id").text = str(artwork["MONA_id"])

		if artwork["title_fr"]:
			ET.SubElement(artwork_element, "title_fr").text = artwork["title_fr"]

		if artwork["year"]:
			ET.SubElement(artwork_element, "year").text = str(artwork["year"])


		# don't include english label if it's the french one copied by default
		if artwork["title_en"]:
			if artwork["title_fr"]:
				if artwork["title_en"] != artwork["title_fr"]:
					ET.SubElement(artwork_element, "title_en").text = artwork["title_en"]
			else:
				# if it only has an english title then it's actually the french label
				ET.SubElement(artwork_element, "title_fr").text = artwork["title_en"]

		# Add location details
		if artwork["location"]:
			if artwork["location"]["latitude"] and artwork["location"]["longitude"]:
				location_element = ET.SubElement(artwork_element, "location")
				ET.SubElement(location_element, "latitude").text = str(artwork["location"]["latitude"])
				ET.SubElement(location_element, "longitude").text = str(artwork["location"]["longitude"])

		# Add URLs
		for url in artwork["urls"]:
			if url:
				ET.SubElement(artwork_element, "urls").text = url.replace('Https', 'https')

		# Add categories
		for category_id in artwork["categories"]:
			category_from_map = category_map[category_id]

			if category_from_map["Wikidata_id"]:
				ET.SubElement(artwork_element, "category").text = f"http://www.wikidata.org/entity/{category_from_map['Wikidata_id']}"
			if category_from_map["AAT_id"]:
				ET.SubElement(artwork_element, "category").text = f"http://vocab.getty.edu/aat/{category_from_map['AAT_id']}"

		# Add source
		if artwork["source"]:
			ET.SubElement(artwork_element, "source").text = str(artwork["source"])

		# Add producer details
		if artwork["producer"]:
			producer_element = ET.SubElement(artwork_element, "producer")
			ET.SubElement(producer_element, "MONA_id").text = str(artwork["producer"])
			producer = producer_map[artwork["producer"]]
			if producer:
				ET.SubElement(producer_element, "label_fr").text = producer["label"]

		# Add owner details
		if artwork["owner"]:
			ET.SubElement(artwork_element, "owner").text = str(artwork["owner"])

		# Add artist details (individual or group)
		for artist_id in artwork["artists"]:
			artist = artist_map[artist_id]
			if artist:
				if artist["is_collective"]:
					artist_group_element = ET.SubElement(artwork_element, "artist_group")
					ET.SubElement(artist_group_element, "MONA_id").text = str(artist_id)
					ET.SubElement(artist_group_element, "label_fr").text = artist["label"]

				else:
					artist_group_element = ET.SubElement(artwork_element, "artist_person")
					ET.SubElement(artist_group_element, "MONA_id").text = str(artist_id)
					ET.SubElement(artist_group_element, "label_fr").text = artist["label"]


def categories_xml(data, root):
	# if a category has more than one URI then we define it twice, once for each URI
	for category in data["data"]["categories"]:
		if category["label_en"].lower() != "other":
			if category["Wikidata_id"]:
				category_element = ET.SubElement(root, "category")
				ET.SubElement(category_element, "label_fr").text = category["label_fr"]
				ET.SubElement(category_element, "label_en").text = category["label_en"]
				ET.SubElement(category_element, "url").text = f"http://www.wikidata.org/entity/{category['Wikidata_id']}"

			if category["AAT_id"]:
				category_element = ET.SubElement(root, "category")
				ET.SubElement(category_element, "label_fr").text = category["label_fr"]
				ET.SubElement(category_element, "label_en").text = category["label_en"]
				ET.SubElement(category_element, "url").text = f"http://vocab.getty.edu/aat/{category['AAT_id']}"


def institutions_xml(data, root):
	for institution in data["data"]["institutions"]:
		institution_element = ET.SubElement(root, "institution")

		ET.SubElement(institution_element, "MONA_id").text = str(institution["MONA_id"])
		ET.SubElement(institution_element, "label_fr").text = institution["label"]

		if institution["Wikidata_id"]:
			ET.SubElement(institution_element, "sameas").text = f"http://www.wikidata.org/entity/{institution['Wikidata_id']}"


def owners_xml(data, root):
	for owner in data["data"]["owners"]:
		if not owner["label"] and not owner["Wikidata_id"]:
			pass
		else:
			owner_element = ET.SubElement(root, "owner")
			ET.SubElement(owner_element, "MONA_id").text = str(owner["MONA_id"])
			if owner["label"]:
				ET.SubElement(owner_element, "label_fr").text = owner["label"]
			if owner["Wikidata_id"]:
				ET.SubElement(owner_element, "sameas").text = f"http://www.wikidata.org/entity/{owner['Wikidata_id']}"


def producers_xml(data, root):
	for producer in data["data"]["producers"]:
		if not producer["label"] and not producer["Wikidata_id"]:
			pass
		else:
			producer_element = ET.SubElement(root, "producer")
			ET.SubElement(producer_element, "MONA_id").text = str(producer["MONA_id"])
			if producer["label"]:
				ET.SubElement(producer_element, "label_fr").text = producer["label"]
			if producer["Wikidata_id"]:
				ET.SubElement(producer_element, "sameas").text = f"http://www.wikidata.org/entity/{producer['Wikidata_id']}"


def sources_xml(data, root):
	institution_map = {inst["MONA_id"]: inst for inst in data["data"]["institutions"]}

	for source in data["data"]["sources"]:
		if not source["label"] and not source["Wikidata_id"]:
			pass
		else:
			source_element = ET.SubElement(root, "source")
			ET.SubElement(source_element, "MONA_id").text = str(source["MONA_id"])
			ET.SubElement(source_element, "label_fr").text = source["label"]

			if source.get("url"):
				ET.SubElement(source_element, "url").text = source["url"]

			# Add institution details
			institution_id = source["institution"]
			if institution_id in institution_map:
				institution = institution_map[institution_id]
				institution_element = ET.SubElement(source_element, "institution")
				ET.SubElement(institution_element, "MONA_id").text = str(institution["MONA_id"])
				ET.SubElement(institution_element, "label_fr").text = institution["label"]


def main():
	root = xml_init()

	data = get_lod_export_from_api()
	#data = get_lod_export_from_file()

	artist_xml(data, root)
	artwork_xml(data, root)
	categories_xml(data, root)
	institutions_xml(data, root)
	owners_xml(data, root)
	producers_xml(data, root)
	sources_xml(data, root)

	xml_to_file(root)


main()
