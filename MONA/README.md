# MONA

# Dataset Status

[Modelling is complete](https://app.diagrams.net/#G1IlFr1T9cJtGAgjiKqpCQAzEJvkdccmU1#%7B%22pageId%22%3A%22aoFT8eWI8YP2GV4fl8dT%22%7D). X3ML templates are in progress and backed up in `X3ML/`. While developing the X3ML templates, you can test using the sample xml files in `data/2_preprocessing_output/samples/` as input. The templates should follow discussions in [meeting notes](https://docs.google.com/document/d/18bx3ADybik3iRBAh9irI2ueImQ9sjbO3786gmWW4TKg) and [mapping notes](https://docs.google.com/document/d/1OJtJYvoWOwlLJ06TAI_CoB1oxe8bf5yPwgq-3l8lXoU).

# Transformation Steps

From `preprocessing/`, run `python3 preprocess.py`. This will call on the MONA LOD Export API to get updated source data, and will produce an X3ML input XML file stored in `2_preprocessing_output`.

From `postprocessing`, run `python3 postprocess.py`. This will run the XML files through the X3ML templates and save the output ttl file in `3_X3ML_output/` with an error file saved there as well with any errors from the X3ML log. Next it will run some data cleaning steps on the data and save the updated ttl file in `4_postprocessing_output/`. Finally it will run a validation step and save the results to `4_postprocessing_output/errors/`.
