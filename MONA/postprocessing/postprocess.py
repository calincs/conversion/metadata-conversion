import io
import os
import requests
import zipfile
import shutil
import json

from rdflib import Graph, URIRef, Namespace
from rdflib.namespace import RDF

# To check if they want me to pull in any labels from wikidata or aat or ulan


def process_3m(input_path, map_file, policy_file, output_folder, post_process_url, encoding, uuid_test_size, output_format):
	"""
	This function sends your data to X3ML.
	"""

	# Checking if input_file is a file or folder
	is_file = os.path.isfile(input_path)
	is_directory = os.path.isdir(input_path)

	if is_directory:
		files = []
		for filename in os.listdir(input_path):
			if filename.endswith(".xml"):
				files.append(os.path.join(input_path, filename))
	elif is_file:
		files = [input_path]
	else:
		print("Error: Please input a valid input file name or path")
		return

	# send each input file to X3ML engine
	for input_file in files:
		print(f"X3ML Transforming {input_file}")
		output_filename = input_file.split("/")[-1].replace(".xml", ".ttl")

		url = post_process_url + f"/conversion/x3ml/custom?output_file={output_filename}&format_output_file={output_format}&uuid_test_size={uuid_test_size}"
		payload = {}
		files = [
			('input_file', ('input.xml', open(input_file, 'rb'), 'text/xml')),
			('map_file', ('Mapping.x3ml', open(map_file, 'rb'), 'application/octet-stream')),
			('policy_file', ('generator.xml', open(policy_file, 'rb'), 'text/xml'))]
		headers = {}
		response = requests.request("POST", url, headers=headers, data=payload, files=files)
		response.encoding = encoding

		if str(response.status_code).startswith("2"):
			try:
				z = zipfile.ZipFile(io.BytesIO(response.content))
				z.extractall(output_folder)

				# Rename 'x3ml_log.txt' if it exists
				original_file = os.path.join(output_folder, "x3ml_log.txt")
				renamed_file = os.path.join(output_folder, f'x3ml_log_{output_filename.replace(".ttl",".txt")}')
				if os.path.exists(original_file):
					os.rename(original_file, renamed_file)
					print(f"File renamed to: {renamed_file}")
				else:
					print(f"'x3ml_log.txt' not found in the extracted files.")

			except Exception as err:
				print(f"Failed to save X3ML conversion output\n", response.text)
				print(err)
		else:
			print(f"Error: Your file {input_file} could not be converted.")
			print(response.status_code)
			print(response.text)


def get_prefixes():
	prefixes = {
		'rs': Namespace('http://www.researchspace.org/ontology/'),
		'aat': Namespace('http://vocab.getty.edu/aat/'),
		'bf': Namespace('http://id.loc.gov/ontologies/bibframe/'),
		'bibo': Namespace('http://purl.org/ontology/bibo/'),
		'crm': Namespace('http://www.cidoc-crm.org/cidoc-crm/'),
		'crmdig': Namespace('http://www.ics.forth.gr/isl/CRMdig/'),
		'dbr': Namespace('http://dbpedia.org/resource/'),
		'event': Namespace('http://id.lincsproject.ca/event/'),
		'fast': Namespace('http://id.worldcat.org/fast/'),
		'frbroo': Namespace('http://iflastandards.info/ns/fr/frbr/frbroo/'),
		'identity': Namespace('http://id.lincsproject.ca/identity/'),
		'lincs': Namespace('http://id.lincsproject.ca/'),
		'locinsts': Namespace('http://id.loc.gov/resources/instances/'),
		'locprovs': Namespace('http://id.loc.gov/entities/providers/'),
		'locsubjects': Namespace('http://id.loc.gov/authorities/subjects/'),
		'locworks': Namespace('http://id.loc.gov/resources/works/'),
		'occupation': Namespace('http://id.lincsproject.ca/occupation/'),
		'owl': Namespace('http://www.w3.org/2002/07/owl#'),
		'rdfs': Namespace('http://www.w3.org/2000/01/rdf-schema#'),
		'schema': Namespace('https://schema.org/'),
		'skos': Namespace('http://www.w3.org/2004/02/skos/core#'),
		'viaf': Namespace('http://viaf.org/viaf/'),
		'wikidata': Namespace('http://www.wikidata.org/entity/'),
		'worldcat': Namespace('http://worldcat.org/entity/work/id/'),
		'xsd': Namespace('http://www.w3.org/2001/XMLSchema#')
	}

	return prefixes


def init_graph(ttl_path):
	g = Graph()
	g.parse(ttl_path)

	prefixes = get_prefixes()

	for prefix, namespace in prefixes.items():
		g.bind(prefix, namespace)

	return g


def serialize_graph(g, ttl_path):
	g.serialize(destination=ttl_path)


def replace_inverse_props(input_path, output_path, post_process_url):
	print("Replacing inverse CIDOC Properties")

	url = post_process_url + '/inverses/'
	files = {'file': ("inverses.ttl", open(input_path, 'rb'))}
	response = requests.request("POST", url, files=files)
	if "200" in str(response.status_code):
		with open(output_path, 'w', encoding="utf-8") as f:
			f.write(response.text)
	else:
		print(f"ERROR: did not replace inverse properties in {input_path}\n{response}")


def replace_same_as(g):
	"""
	Replace temporary MONA URIs with their sameAs values from Wikidata as first choice and getty as second
	"""
	print("Swapping temp URIs out for owl:sameAs.")

	# Query for all owl:sameAs relationships
	query = """
		SELECT DISTINCT ?s ?o
		WHERE {
			?s <http://www.w3.org/2002/07/owl#sameAs> ?o.
		}
	"""
	qres = g.query(query)

	# Build a mapping of MONA entities to their replacement URIs
	sameas_dict = {}
	for key, value in qres:
		sameas_dict.setdefault(key, []).append(value)

	to_remove = set()
	to_add = set()
	for old_uri, same_as_list in sameas_dict.items():
		# Default replacement logic: Prefer Wikidata, otherwise take the first available URI
		new_uri = next((uri for uri in same_as_list if "wikidata" in str(uri)), same_as_list[0])

		# Collect all triples where old_uri appears
		triples_to_modify = set(g.triples((old_uri, None, None))) | set(g.triples((None, None, old_uri)))

		for subj, pred, obj in triples_to_modify:
			new_subj = new_uri if subj == old_uri else subj
			new_obj = new_uri if obj == old_uri else obj

			if (subj, pred, obj) != (new_subj, pred, new_obj):
				to_remove.add((subj, pred, obj))
				to_add.add((new_subj, pred, new_obj))

	for triple in to_remove:
		g.remove(triple)

	for triple in to_add:
		g.add(triple)

	return g


def remove_duplicate_appellations(g):
	# check if any of the entities now have more than one name appellation with the same content
	query = """
		SELECT ?entity ?name
		WHERE {
			?entity <http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by> ?name .
			?name a <http://www.cidoc-crm.org/cidoc-crm/E33_E41_Linguistic_Appellation> ;
				<http://www.cidoc-crm.org/cidoc-crm/P190_has_symbolic_content> ?string .
		}
		GROUP BY ?entity ?string
		HAVING (COUNT(?name) > 1)
		"""

	print("Querying for duplicate name appellations.")
	duplicates = set()
	for row in g.query(query):
		duplicates.add(row["name"])

	print("Removing duplicate name appellations.")
	for name in duplicates:
		triples_to_remove = set(g.triples((name, None, None))) | set(g.triples((None, None, name)))
		for triple in triples_to_remove:
			g.remove(triple)

	return g


def remove_certain_classes(g):
	# removes triples of the form wikidata:Q3548141  a  owl:Thing .
	# removes person, group, actor double classing

	query = """
		SELECT DISTINCT ?s
		WHERE {
			?s a <http://www.w3.org/2002/07/owl#Thing> .
		}"""

	print("Removing any triples x a owl:Thing")
	qres = g.query(query)
	for row in qres:
		#print(f"Removing {row.s} a owl:Thing")
		g = g.remove((row.s, RDF.type, URIRef("http://www.w3.org/2002/07/owl#Thing")))


	# if actor and person, remove actor
	query = """
		SELECT DISTINCT ?s
		WHERE {
			?s a <http://www.cidoc-crm.org/cidoc-crm/E21_Person> .
			?s a <http://www.cidoc-crm.org/cidoc-crm/E39_Actor> .
		}"""

	print("Removing actor & person double classing")
	qres = g.query(query)
	for row in qres:
		#print(f"Removing {row.s} a crm:E39_Actor")
		g = g.remove((row.s, RDF.type, URIRef("http://www.cidoc-crm.org/cidoc-crm/E39_Actor")))

	# if actor and group, remove actor
	query = """
		SELECT DISTINCT ?s
		WHERE {
			?s a <http://www.cidoc-crm.org/cidoc-crm/E74_Group> .
			?s a <http://www.cidoc-crm.org/cidoc-crm/E39_Actor> .
		}"""

	print("Removing actor & group double classing")
	qres = g.query(query)
	for row in qres:
		#print(f"Removing {row.s} a crm:E39_Actor")
		g = g.remove((row.s, RDF.type, URIRef("http://www.cidoc-crm.org/cidoc-crm/E39_Actor")))

	return g


def find_duplicates(ttl_path):
	"""
	This was run once to produce a tsv file of entities to review

	finds two separate entities that share an rdfs:label (match if lower case is a match)
	"""
	with open("potential_duplicates.tsv", "w", encoding="utf-8") as f_out:
		g = Graph()
		g.parse(ttl_path)
		prefixes = get_prefixes()
		for prefix, namespace in prefixes.items():
			g.bind(prefix, namespace)

		print("Query: two separate entities that share an rdfs:label")
		shared_labels = {}
		labels = {}
		for subj, pred, obj in g:
			if "http://www.w3.org/2000/01/rdf-schema#label" in str(pred):
				if str(obj).lower() in labels:
					labels[str(obj).lower()].append(str(subj))
				else:
					labels[str(obj).lower()] = [str(subj)]

		for label in labels:
			dedupe = list(set(labels[label]))
			if len(dedupe) > 1:
				shared_labels[label] = dedupe

		# Keywords to search for
		keywords = ["/artist/", "/artwork/", "/producer/", "/source/", "/institution/", "/owners/"]

		# Filter dictionary: Keep only keys where at least one list value contains a keyword
		shared_labels = {k: v for k, v in shared_labels.items() if any(any(keyword in item for keyword in keywords) for item in v)}

		for l in shared_labels:
			f_out.write('\t'.join([l] + sorted(list(set(shared_labels[l])))) + "\n")


def condense_ttl_graph(input_ttl_list, output_ttl_path):
	# creates graph and outputs the new graph to replace the original file
	g = Graph()

	for ttl_path in input_ttl_list:
		print(ttl_path)
		g.parse(ttl_path)

	g.serialize(destination=output_ttl_path)


def validation(input_path_list, output_path, post_process_url, analyze="True"):

	files = []
	for file in input_path_list:
		filename = file.split("/")[-1]
		files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	payload = {
		'encoding': 'utf-8',
		'rdf_format': 'turtle',
		'analyze': analyze}
	headers = {}
	response = requests.post(post_process_url + '/validate/', headers=headers, files=files, data=payload)

	with open(output_path + "rdf_validation.json", "w", encoding="utf-8") as out_file:
		json.dump(response.json(), out_file, indent=4, ensure_ascii=False)


def main():
	# If you are running the linked data enhancement API locally then uncomment this next line and comment out the one after that.
	#post_process_url = "http://0.0.0.0:80"
	post_process_url = "https://postprocess.lincsproject.ca"

	# You can change the paths in the next 4 lines if you update the X3ML mapping or want to run X3ML on different files
	mapping_file_path = "../X3ML/3m-x3ml-output.x3ml"
	generator_file_path = "../X3ML/generator-policy.xml"
	postprocessing_error_output_path = 'errors/'

	input_xml_folder = "../data/2_preprocessing_output/"
	x3ml_input_file_path = f"{input_xml_folder}mona.xml"
	x3ml_output_folder_path = "../data/3_x3ml_output/"
	x3ml_output_file_path = f"{x3ml_output_folder_path}mona.ttl"
	postprocessing_output_file_path = '../data/4_postprocessing_output/mona.ttl'
	final_ttl_path = "../../Datasets/mona.ttl"

	X3ML_step = True
	clean_step = True
	validate_step = True
	final_save_step = True

	# Run all .xml files in data/source_data/ through X3ML
	if X3ML_step:
		process_3m(
			x3ml_input_file_path,
			mapping_file_path,
			generator_file_path,
			x3ml_output_folder_path,
			post_process_url,
			"utf-8",
			"2",
			"text/turtle")

	if clean_step:
		replace_inverse_props(x3ml_output_file_path, postprocessing_output_file_path, post_process_url)

		g = init_graph(postprocessing_output_file_path)
		g = replace_same_as(g)  # this need to happen before the class removal step and remove_duplicate_appellations step
		g = remove_duplicate_appellations(g)
		g = remove_certain_classes(g)
		serialize_graph(g, postprocessing_output_file_path)

	if validate_step:
		validation([postprocessing_output_file_path], postprocessing_error_output_path, post_process_url, analyze="True")

	if final_save_step:
		shutil.copy(postprocessing_output_file_path, final_ttl_path)


main()
