from rdflib import *
import sys

input_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/combined.ttl"

def add_prefixes(label_file):
    with open(label_file, "r",encoding='utf-8') as f:
        filedata = f.read()
    filedata = filedata.replace("http://id.lincsproject.ca/", "lincs:")
    with open(label_file, "w", encoding='utf-8') as f:
        f.write("@prefix lincs: <http://id.lincsproject.ca/> .\n")
        f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n")
        f.write(filedata)   

def initialize_out(label_file):
    f = open(label_file, "w",encoding='utf-8')
    f.truncate(0)
    f.close()


# Create a Graph
g = Graph()
# Parse in an RDF file
g.parse(input_file, format='ttl')


#Generating Labels for crm:E33_Linguistic_Object

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/E33_Linguistic_Object_labels.ttl"

# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?label WHERE {

    ?s ?p crm:E33_Linguistic_Object .
    ?s crmpc:P129_is_about ?about .
    ?about rdfs:label ?label .
} order by (?s)

"""


# initializing output file 
initialize_out(label_file)

# iterating through the graph
label =''
for r in g.query(q):
    if r.s == label:
        continue
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.s, 'rdfs:label "Advertising copy about \\"{}\\""'.format(r.label) + "@en .")
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

add_prefixes(label_file)

#Generating Labels for crm:E41_Appellation

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/E41_Appellation_labels.ttl"

# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?label ?content WHERE {

    ?s ?p crm:E41_Appellation .
    ?s crm:P190_has_symbolic_content ?content .
    ?s crm:P2_has_type ?type .
    ?type rdfs:label ?label .

} order by (?s)
"""

# initializing output file 
initialize_out(label_file)

# iterating through the graph
label =''
for r in g.query(q):
    if r.s == label:
        continue
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.s, 'rdfs:label " {} of {} "'.format(r.label,r.content) + "@en .",sep='\t')
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

with open(label_file, "r",encoding='utf-8') as f:
    filedata = f.read()
filedata = filedata.replace("http://id.lincsproject.ca/", "lincs:")
filedata = filedata.replace("address", "Address")
filedata = filedata.replace("telephone", "Telephone")
filedata = filedata.replace("seasonal", "Seasonal")
filedata = filedata.replace("calendar", "Calendar")
with open(label_file, "w", encoding='utf-8') as f:
    f.write("@prefix lincs: <http://id.lincsproject.ca/> .\n")
    f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n")
    f.write(filedata)    

#Generating Labels for crm:E33_E41_Linguistic_Appellation

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/E33_E41_Linguistic_Appellation_labels.ttl"


# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?content ?label WHERE {

    ?s ?p crm:E33_E41_Linguistic_Appellation .
    ?s crm:P190_has_symbolic_content ?content .
    ?s crm:P2_has_type ?type .
    ?type rdfs:label ?label .

} order by (?s)
"""

# initializing output file 
initialize_out(label_file)

# iterating through the graph
label = ''
for r in g.query(q):
    if r.s == label:
        continue
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.s, 'rdfs:label " {} of {} "'.format(r.label,r.content) + "@en .",sep='\t')
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

with open(label_file, "r",encoding='utf-8') as f:
    filedata = f.read()
filedata = filedata.replace("http://id.lincsproject.ca/", "lincs:")
filedata = filedata.replace("address", "Address")
filedata = filedata.replace("telephone", "Telephone")
filedata = filedata.replace("seasonal", "Seasonal")
filedata = filedata.replace("calendar", "Calendar")

with open(label_file, "w", encoding='utf-8') as f:
    f.write("@prefix lincs: <http://id.lincsproject.ca/> .\n")
    f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n")
    f.write(filedata)  


#Generating Labels for crm:E53_Place

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/E53_Place_labels.ttl"

# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?coord WHERE {

    ?s ?p crm:E53_Place .
    ?s crm:P168_place_is_defined_by ?coord .

} order by (?s)

"""


# initializing output file 
initialize_out(label_file)

# iterating through the graph
label =''
for r in g.query(q):
    if r.s == label:
        continue
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.s, 'rdfs:label "Geographic location with coordinates \\"{}\\""'.format(r.coord) + "@en .")
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

add_prefixes(label_file)

#Generating Labels for crm:E35_Title

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/E35_Title_labels.ttl"

# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?content WHERE {

    ?s ?p crm:E35_Title .
    ?s crm:P190_has_symbolic_content ?content .

} order by (?s)

"""


# initializing output file 
initialize_out(label_file)

# iterating through the graph
label =''
for r in g.query(q):
    if r.s == label:
        continue
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.s, 'rdfs:label "Title of {}"'.format(r.content) + "@en .")
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

add_prefixes(label_file)

#Generating Labels for frbroo:F30_Manifestation_Creation

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/F30_Manifestation_Creation_labels.ttl"

# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>

SELECT ?s ?uri ?label WHERE {

    {?s ?p frbroo:F4_Manifestation_Singleton} UNION { ?s ?p frbroo:F2_Expression} .
    ?s frbroo:R24i_was_created_through ?uri .
    ?uri ?p frbroo:F30_Manifestation_Creation .
  	?s rdfs:label ?label .

} order by (?s)

"""


# initializing output file 
initialize_out(label_file)

# iterating through the graph
label =''
for r in g.query(q):
    if r.s == label:
        continue
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.uri, 'rdfs:label "Creation of \\"{}\\""'.format(r.label) + "@en .")
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

with open(label_file, "r",encoding='utf-8') as f:
    filedata = f.read()
filedata = filedata.replace("http://id.lincsproject.ca/", "lincs:")
filedata = filedata.replace("http://worldcat.org/entity/work/id/", "worldcat:")
with open(label_file, "w", encoding='utf-8') as f:
    f.write("@prefix lincs: <http://id.lincsproject.ca/> .\n")
    f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n")
    f.write("@prefix worldcat: <http://worldcat.org/entity/work/id/> .\n")
    f.write(filedata)   


#Generating Labels for frbroo:F28_Expression_Creation

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/F28_Expression_Creation_labels.ttl"

# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>

SELECT ?s ?uri ?label WHERE {

    { ?s ?p frbroo:F18_Serial_Work } UNION { ?s ?p frbroo:F2_Expression} .
    ?s frbroo:R19i_was_realised_through ?uri .
    ?uri ?p frbroo:F28_Expression_Creation .
  	?s rdfs:label ?label .
    
} order by (?s)
"""

# initializing output file 
initialize_out(label_file)

# iterating through the graph
label =''
for r in g.query(q):
    if r.s == label:
        continue
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.uri, 'rdfs:label "Creation of {}"'.format(r.label) + "@en .")
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

with open(label_file, "r",encoding='utf-8') as f:
    filedata = f.read()
filedata = filedata.replace("http://id.lincsproject.ca/", "lincs:")
filedata = filedata.replace("http://worldcat.org/entity/work/id/", "worldcat:")
filedata = filedata.replace("http://www.wikidata.org/entity/", "wikidata:")
filedata = filedata.replace("http://id.loc.gov/resources/works/", "locworks:")
with open(label_file, "w", encoding='utf-8') as f:
    f.write("@prefix lincs: <http://id.lincsproject.ca/> .\n")
    f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n")
    f.write("@prefix worldcat: <http://worldcat.org/entity/work/id/> .\n")
    f.write("@prefix locworks: <http://id.loc.gov/resources/works/> .\n")
    f.write("@prefix wikidata: <http://www.wikidata.org/entity/> .\n")
    f.write(filedata)


#Generating Labels for crm:PC14_carried_out_by

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/PC14_carried_out_by_labels.ttl"

# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?label ?plabel WHERE {

    ?s ?p crm:PC14_carried_out_by .
    ?s crm:P14.1_in_the_role_of ?role .
    ?s crm:P02_has_range ?person .
    ?person rdfs:label ?plabel .
    ?role rdfs:label ?label .

} order by (?s)

"""

# initializing output file 
initialize_out(label_file)

# iterating through the graph
label =''
for r in g.query(q):
    if r.s == label:
        continue


    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.s, 'rdfs:label "{} Role of {}"'.format(r.label,r.plabel) + "@en .")
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

with open(label_file, "r",encoding='utf-8') as f:
    filedata = f.read()
filedata = filedata.replace("http://id.lincsproject.ca/", "lincs:")
filedata = filedata.replace("editor", "Editor")
filedata = filedata.replace("author", "Author")
filedata = filedata.replace("publisher", "Publisher")
with open(label_file, "w", encoding='utf-8') as f:
    f.write("@prefix lincs: <http://id.lincsproject.ca/> .\n")
    f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n")
    f.write(filedata)   

#Generating Labels for crm:E65_Creation

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/E65_Creation_labels.ttl"

# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?label WHERE {

    ?s ?p crm:E65_Creation .
    ?s crm:P14_carried_out_by ?person .
    ?person rdfs:label ?label .

} order by (?s)

"""


# initializing output file 
initialize_out(label_file)

# iterating through the graph
label =''
for r in g.query(q):
    if r.s == label:
        continue
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.s, 'rdfs:label "Creation by {}"'.format(r.label) + "@en .")
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

add_prefixes(label_file)

#Generating Labels for frbroo:F2_Expression

label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/F2_Expression.ttl"

# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX frbroo: <http://iflastandards.info/ns/fr/frbr/frbroo/>

SELECT ?s ?plabel WHERE {

    ?s ?p frbroo:F2_Expression .
    ?s frbroo:R19i_was_realised_through ?role .
    ?role crm:P14_carried_out_by ?person .
    ?person rdfs:label ?plabel .

} order by (?s)

"""

# initializing output file 
initialize_out(label_file)

# iterating through the graph
label =''
for r in g.query(q):
    if r.s == label:
        continue

    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a',encoding='utf-8') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.s, 'rdfs:label "Article in Semiotext(e) Vol. 3 No. 2 by {}"'.format(r.plabel) + "@en .")
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

with open(label_file, "r",encoding='utf-8') as f:
    filedata = f.read()
filedata = filedata.replace("http://id.lincsproject.ca/", "lincs:")
with open(label_file, "w", encoding='utf-8') as f:
    f.write("@prefix lincs: <http://id.lincsproject.ca/> .\n")
    f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n")
    f.write(filedata)   