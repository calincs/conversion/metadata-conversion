import os

replacements = {}
with open("uri_replacements_locnames_viaf.txt") as f:
	for line in f:
		line_split = line.strip("\n").strip().split("\t")
		replacements[line_split[0]] = line_split[1]


in_directory = "../data/with_URIs/sept2021_withoutBnodes/"
out_directory = "../data/with_URIs/oct2021/"

# in_directory = "../data/with_URIs/sept2021_withoutBnodes/external_labels/"
# out_directory = "../data/with_URIs/oct2021/external_labels/"
for filename in os.listdir(in_directory):
	if filename.endswith(".ttl"):
		with open(os.path.join(in_directory, filename)) as f_in:
			with open(os.path.join(out_directory, filename), "w") as f_out:
				#f_out.write("@prefix lincs: <http://id.lincsproject.ca/> .\n")
				data = f_in.read()
				for i in replacements:
					data = data.replace(i, str(replacements[i]))

					# # need to handle strings with and without string tags ^^xsd:string
					# #data = data.replace(i + "^^xsd:string", str(replacements[i]))
					# print(i)
					# data = data.replace(i + " ", str(replacements[i]) + " ")
					# data = data.replace(i + ",", str(replacements[i]) + ",")
					# data = data.replace(i + ".", str(replacements[i]) + ".")
					# data = data.replace("@prefix temp_lincs_temp: <temp.lincsproject.ca/> .", "")

				f_out.write(data)
