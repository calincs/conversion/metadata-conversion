from rdflib import *
import sys
import rdflib
import requests
from bs4 import BeautifulSoup
import re

input_file = "../Datasets/adarchive.ttl"

# Create a Graph
g = Graph()

# Parse in an RDF file 
g.parse(input_file, format='ttl')

# query
q = """
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?place WHERE {

    ?s crm:P74_has_current_or_former_residence ?place .
}
"""

for r in g.query(q):

    q2 = """
    PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
    PREFIX lincs:  <http://id.lincsproject.ca/>

    SELECT ?address WHERE {

        ?s crm:P1_is_identified_by ?address .
    }
    """

    result = g.query(q2, initBindings={'?s': URIRef(r.place)})

    for r2 in result:

        subject = URIRef(r.s)
        predicate = URIRef('http://www.cidoc-crm.org/cidoc-crm/P76_has_contact_point')
        object = URIRef(r2.address)

        g.add((subject,predicate,object))
        
g.serialize(destination='adarchive_new.ttl', format='turtle')