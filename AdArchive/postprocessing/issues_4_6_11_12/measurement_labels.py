from rdflib import *
import sys

input_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/combined.ttl"
label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/measurement_labels.ttl"


# Create a Graph
g = Graph()

# Parse in an RDF file
g.parse(input_file, format='ttl')


# query
q = """
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX crmpc: <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?label ?tlabel ?tlabel2 WHERE {

    ?s ?p crm:E16_Measurement .
    ?s crm:P39_measured ?measured .
    ?measured rdfs:label ?label .
    ?s crmpc:P2_has_type ?type,?type2 .
    ?type rdfs:label ?tlabel .
    ?type2 rdfs:label ?tlabel2 .

    FILTER (?type != ?type2)
} order by (?s)
"""

# adding prefixes
f = open(label_file, "w")
f.write("@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n")
f.close()

# iterating through the graph
label = 'label'
for r in g.query(q):
    if r.s == label:
        continue  # removing duplicates
    original_stdout = sys.stdout  # saving a reference to the original standard output
    with open(label_file, 'a') as f:
        sys.stdout = f  # Change the standard output to the file
        print(r.s, 'rdfs:label "Measurement for \\"{}\\" with types \\"{}\\", \\"{}\\""'.format(r.label, r.tlabel, r.tlabel2) + "@en .")
        sys.stdout = original_stdout  # Reset the standard output to its original value
    label = r.s

with open(label_file, "r") as f:
    filedata = f.read()
filedata = filedata.replace("http://id.lincsproject.ca/", "lincs:")
with open(label_file, "w", encoding='utf-8') as f:
    f.write("@prefix lincs: <http://id.lincsproject.ca/> .\n")
    f.write(filedata)
