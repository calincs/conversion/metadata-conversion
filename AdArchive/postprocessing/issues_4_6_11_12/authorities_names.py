from rdflib import *
import sys
import requests
from bs4 import BeautifulSoup
import re

input_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/combined.ttl"
label_file = "AdArchive/data/with_URIs/sept2021_withoutBnodes/external_labels/authorities_names.tsv"


# Create a Graph
g = Graph()

# Parse in an RDF file 
g.parse(input_file, format='ttl')


# query

q = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
SELECT ?s WHERE {
    ?s ?p ?o

}
"""

label = ''
viaf = ''

#initialize output file
f = open(label_file, "w")
f.truncate(0)
f.close()

for r in g.query(q):
    if  'https://id.loc.gov/authorities/names' not in r.s: #filter query output
        continue
    
    #get label
    req = requests.get(r.s)
    label = req.headers['X-PrefLabel']

    #get viaf link
    html_text = req.text
    soup = BeautifulSoup(html_text, 'html.parser')
    links = soup.findAll('a', attrs={'href': re.compile("^http://viaf")})[0]
    for link in links:
        viaf = link
    
    #output to tsv file
    original_stdout = sys.stdout #saving a reference to the original standard output
    with open(label_file,'a') as f:
        sys.stdout = f # Change the standard output to the file
        print(r.s,label,viaf,sep='\t')
        sys.stdout = original_stdout # Reset the standard output to its original value  
    
    
with open(label_file, "r") as f:
    filedata = f.read()
filedata = filedata.replace("https://id.loc.gov/authorities/names/","locnames:")
filedata = filedata.replace("http://viaf.org/viaf/","viaf:")
with open(label_file, "w",encoding='utf-8') as f:
    f.write("@prefix locnames: <https://id.loc.gov/authorities/names/> .\n")
    f.write("@prefix viaf: <http://viaf.org/viaf/> .\n")
    f.write(filedata)  
    
