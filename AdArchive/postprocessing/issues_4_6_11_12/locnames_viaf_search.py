import requests

# file containing a list of viaf URIs that need to be replaced with canonical URIs
input_list = "viaf_concept_list.txt"
output_list = "viaf_concept_map.txt"
output_sameas = "../data/with_URIs/sept2021_withoutBnodes/external_labels/same_as.ttl"


og_uris = []
new_uris = []
with open(input_list, "r") as f_in:
	for line in f_in:
		og_uris.append(line.strip("\n"))


for url in og_uris:
	r = requests.get(url, allow_redirects=False)
	print(r.status_code)
	print(r.text)
	print(r.url)
	print(r.headers['Location'])
	new_uri = r.headers['Location']
	new_uris.append([line, new_uri])


with open(output_list, "w") as f_out:
	for uri_map in new_uris:
		f_out.write("\t".join(uri_map) + "\n")

