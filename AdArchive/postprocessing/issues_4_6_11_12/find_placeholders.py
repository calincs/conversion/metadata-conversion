import os
import re

placeholders = []

directory = "../"
for filename in os.listdir(directory):
	if filename.endswith(".ttl"):
		with open(os.path.join(directory, filename)) as f:
			for line in f:
				matches = re.findall("\<[A-Z\_0-9]+\>", line)
				for m in matches:
					placeholders.append(m)

unique_placeholders = list(set(placeholders))
for i in unique_placeholders:
	print(i)
