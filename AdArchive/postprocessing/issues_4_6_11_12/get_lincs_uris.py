import re
import requests


def get_uid():
	mint_url = "https://uid.lincsproject.ca/generateuid"
	response = requests.get(mint_url)
	try:
		uuid = response.json()["uid"]
	except KeyError:
		print(response.status_code, ":  Failed to get UID: ", response.content)
		uuid = None
	return uuid


count = 5
uids = []
i = 0
while i <= count:
	uids.append(get_uid())
	i += 1


with open("uids.txt", "w") as f:
	for i in uids:
		f.write("lincs:" + i)
		f.write("\n")
