import io
import os
import re
import requests
import zipfile

from rdflib import Graph, URIRef, Namespace
from rdflib.namespace import RDF


def process_3m(input_path, map_file, policy_file, output_folder, post_process_url, encoding, uuid_test_size, output_format):
	"""
	This function sends your data to X3ML.
	"""

	# Checking if input_file is a file or folder
	is_file = os.path.isfile(input_path)
	is_directory = os.path.isdir(input_path)

	if is_directory:
		files = []
		for filename in os.listdir(input_path):
			if filename.endswith(".xml"):
				files.append(os.path.join(input_path, filename))
	elif is_file:
		files = [input_path]
	else:
		print("Error: Please input a valid input file name or path")
		return

	# send each input file to X3ML engine
	for input_file in files:
		print(f"Converting {input_file}")
		output_filename = input_file.split("/")[-1].replace(".xml", ".ttl")

		url = post_process_url + f"/conversion/x3ml/custom?output_file={output_filename}&format_output_file={output_format}&uuid_test_size={uuid_test_size}"
		payload = {}
		files = [
			('input_file', ('input.xml', open(input_file, 'rb'), 'text/xml')),
			('map_file', ('Mapping.x3ml', open(map_file, 'rb'), 'application/octet-stream')),
			('policy_file', ('generator.xml', open(policy_file, 'rb'), 'text/xml'))]
		headers = {}
		response = requests.request("POST", url, headers=headers, data=payload, files=files)
		response.encoding = encoding

		if str(response.status_code).startswith("2"):
			try:
				z = zipfile.ZipFile(io.BytesIO(response.content))
				z.extractall(output_folder)
			except Exception as err:
				print(f"Failed to save X3ML conversion output\n", response.text)
				print(err)
		else:
			print(f"Error: Your file {input_file} could not be converted.")
			print(response.status_code)
			print(response.text)


def fix_entity_double_classing(ttl_path):
	# finds entities that are double classed as E1_CRM_EntityEntity and another type
	# removes the E1 class
	# outputs the new graph to replace the original file
	g = Graph()
	g.parse(ttl_path)

	prefixes = {
		'rs': Namespace('http://www.researchspace.org/ontology/'),
		'aat': Namespace('http://vocab.getty.edu/aat/'),
		'bf': Namespace('http://id.loc.gov/ontologies/bibframe/'),
		'bibo': Namespace('http://purl.org/ontology/bibo/'),
		'crm': Namespace('http://www.cidoc-crm.org/cidoc-crm/'),
		'crmdig': Namespace('http://www.ics.forth.gr/isl/CRMdig/'),
		'dbr': Namespace('http://dbpedia.org/resource/'),
		'event': Namespace('http://id.lincsproject.ca/event/'),
		'fast': Namespace('http://id.worldcat.org/fast/'),
		'frbroo': Namespace('http://iflastandards.info/ns/fr/frbr/frbroo/'),
		'identity': Namespace('http://id.lincsproject.ca/identity/'),
		'lincs': Namespace('http://id.lincsproject.ca/'),
		'locinsts': Namespace('http://id.loc.gov/resources/instances/'),
		'locprovs': Namespace('http://id.loc.gov/entities/providers/'),
		'locsubjects': Namespace('http://id.loc.gov/authorities/subjects/'),
		'locworks': Namespace('http://id.loc.gov/resources/works/'),
		'occupation': Namespace('http://id.lincsproject.ca/occupation/'),
		'owl': Namespace('http://www.w3.org/2002/07/owl#'),
		'rdfs': Namespace('http://www.w3.org/2000/01/rdf-schema#'),
		'schema': Namespace('https://schema.org/'),
		'skos': Namespace('http://www.w3.org/2004/02/skos/core#'),
		'viaf': Namespace('http://viaf.org/viaf/'),
		'wikidata': Namespace('http://www.wikidata.org/entity/'),
		'worldcat': Namespace('http://worldcat.org/entity/work/id/'),
		'xsd': Namespace('http://www.w3.org/2001/XMLSchema#')
	}

	for prefix, namespace in prefixes.items():
		g.bind(prefix, namespace)

	query = """
		SELECT DISTINCT ?s
		WHERE {
			?s a <http://www.cidoc-crm.org/cidoc-crm/E1_CRM_Entity> .
			?s a ?type2 .
			FILTER(?type2 != <http://www.cidoc-crm.org/cidoc-crm/E1_CRM_Entity>) .
		}"""

	qres = g.query(query)
	for row in qres:
		print(f"Removing {row.s} a crm:E1_CRM_Entity")
		g = g.remove((row.s, RDF.type, URIRef("http://www.cidoc-crm.org/cidoc-crm/E1_CRM_Entity")))

	# if actor and person, remove actor
	query = """
		SELECT DISTINCT ?s
		WHERE {
			?s a <http://www.cidoc-crm.org/cidoc-crm/E21_Person> .
			?s a <http://www.cidoc-crm.org/cidoc-crm/E39_Actor> .
		}"""

	qres = g.query(query)
	for row in qres:
		print(f"Removing {row.s} a crm:E39_Actor")
		g = g.remove((row.s, RDF.type, URIRef("http://www.cidoc-crm.org/cidoc-crm/E39_Actor")))

	# if actor and group, remove actor
	query = """
		SELECT DISTINCT ?s
		WHERE {
			?s a <http://www.cidoc-crm.org/cidoc-crm/E74_Group> .
			?s a <http://www.cidoc-crm.org/cidoc-crm/E39_Actor> .
		}"""

	qres = g.query(query)
	for row in qres:
		print(f"Removing {row.s} a crm:E39_Actor")
		g = g.remove((row.s, RDF.type, URIRef("http://www.cidoc-crm.org/cidoc-crm/E39_Actor")))

	g.serialize(destination=ttl_path)


def replace_inverse_props(input_path, output_path, post_process_url):
	url = post_process_url + '/inverses/'
	files = {'file': ("inverses.ttl", open(input_path, 'rb'))}
	response = requests.request("POST", url, files=files)
	if "200" in str(response.status_code):
		with open(output_path, 'w') as f:
			f.write(response.text)
	else:
		print(f"ERROR: did not replace inverse properties in {input_path}\n{response}")


def condense_ttl_graph(input_ttl_list, output_ttl_path):
	# creates graph and outputs the new graph to replace the original file
	g = Graph()

	for ttl_path in input_ttl_list:
		print(ttl_path)
		g.parse(ttl_path)

	g.serialize(destination=output_ttl_path)


# should no longer be necessary, as URI replacements are now done in preprocessing
def replacement_simple(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	content = content.replace("@prefix temp_lincs_temp:   <http://temp.lincsproject.ca/> .", "")

	with open(output_rdf_path, 'w') as f:
		f.write(content)


# should no longer be necessary, as URI replacements are now done in preprocessing
def replacement(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):
	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text
	content = "@prefix temp_lincs_temp:   <http://temp.lincsproject.ca/> .\n@prefix lincs:   <http://id.lincsproject.ca/> .\n" + content

	for pattern in [r"<(temp_lincs_temp:.*?)>", r"<(wikidata:Q.*?)>", r"<(viaf:.*?)>", r"<(loc[a-z]+?:.*?)>", r"<(aat:.*?)>", r"<(cwrc:.*?)>", r"<(fast:.*?)>", r"<(bf:.*?)>", r"<(lincs:.*?)>", r"<(schema:.*?)>"]:
		matches = re.findall(pattern, content)
		for match in matches:
			content = content.replace(f"<{match}>", match)

	with open(output_rdf_path, 'w') as f:
		f.write(content)


def external_labels(post_process_url, input_path, output_path, authority):
	if authority == "wikidata":
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs&lang_code=fr'
	else:
		url = post_process_url + f'/labels/{authority}?skip_predicate=http://www.w3.org/2002/07/owl%23sameAs'
	payload = {}
	headers = {}
	files = [('file', ('test_labels.ttl', open(input_path, 'rb'), 'application/octet-stream'))]
	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	try:
		z = zipfile.ZipFile(io.BytesIO(response.content))
		z.extractall(output_path)
	except Exception as err:
		print(f"Output failed for external labels with {authority}\n", response.text)
		print(err)


def validation(input_path_list, output_path, post_process_url, outfilename="", analyze="True"):

	files = []
	for file in input_path_list:
		filename = file.split("/")[-1]
		files.append(('files', (filename, open(file, 'rb'), 'application/octet-stream')))

	payload = {
		'encoding': 'utf-8',
		'rdf_format': 'turtle',
		'analyze': analyze}
	headers = {}
	response = requests.post(post_process_url + '/validate/', headers=headers, files=files, data=payload)

	with open(output_path + f"{outfilename}_rdf_validation.json", "w") as out_file:
		out_file.write(response.text)


def convert_coordinates_in_file(input_file, output_file):
	"""
	convert strings of the form
	'<http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by> "X,Y"@en .'
	into
	'<http://www.cidoc-crm.org/cidoc-crm/P168_place_is_defined_by> "POINT(Y X)"@en .'
	where X and Y are numbers that can be positive or negative and can have decimal points
	"""
	pattern = r'<http:\/\/www\.cidoc-crm\.org\/cidoc-crm\/P168_place_is_defined_by> "(([0-9\.\-]+), ?([0-9\.\-]+))"@en .'
	new_lines = []

	with open(input_file, 'r') as f_in:
		old_lines = f_in.readlines()

	with open(output_file, 'w') as f_out:
		for line in old_lines:
			# Find all matches of coordinates in the line
			matches = re.findall(pattern, line)

			# Iterate over matches and replace with formatted coordinates
			for match in matches:
				full, x, y = match[0], match[1], match[2]
				line = line.replace(full, f'POINT({y} {x})')
			new_lines.append(line)
		for new_line in new_lines:
			f_out.write(new_line + "\n")


def main():
	"""
	To run the linked data enhancement API on your own computer, you first need to install Docker. If you are using Windows or Mac, Docker Desktop will be the easiest for you to use.

	In terminal, clone the repo:

	git clone git@gitlab.com:calincs/conversion/post-processing-service.git

	Now, go into that folder with terminal and run these two commands one after another:

	docker build --no-cache -t postprocess .

	docker run -p 80:80 -t -i postprocess

	The API is now accessible locally at http://0.0.0.0:80

	"""

	# If you are running the linked data enhancement API locally then uncomment this next line and comment out the one after that.
	#post_process_url = "http://0.0.0.0:80"
	post_process_url = "https://postprocess.lincsproject.ca"


	# You can change the paths in the next 4 lines if you update the X3ML mapping or want to run X3ML on different files

	illustrations_only = False  # note you need to run the code with this as True and then again with this as False, if any updates affect adarchive_1_illustrations and adarchive_2

	input_xml_folder = "../data/source_data/"
	mapping_file_path = "../X3ML/3m-x3ml-output.x3ml"
	generator_file_path = "../X3ML/generator-policy.xml"
	x3ml_output_folder_path = "../data/x3ml_output/"
	final_ttl_path = "../../Datasets/adarchive/adarchive_2.ttl"
	postprocessing_output_path = '../data/postprocessing_output/'
	postprocessing_error_output_path = 'errors/'

	if illustrations_only:
		input_xml_folder = "../data/source_data_illustrations/"
		x3ml_output_folder_path = "../data/x3ml_output_illustrations/"
		final_ttl_path = "../../Datasets/adarchive/adarchive_1_illustrations.ttl"
		postprocessing_output_path = '../data/postprocessing_output_illustrations/'

	# You can change some of these to True or False to turn on and off some conversion steps.
	# Typically, once you've run the data through 3M once, you don't need to again unless you change the 3M mapping
	# And once you've gotten external labels, you don't need to again unless you change what URIs are in your data
	X3ML_step = False
	clean_step = True
	replace_lincs_uris_step = True  # clean_step must be TRUE too
	get_external_labels_step = False
	final_combine_step = True
	validation_step = True


	# Run all .xml files in data/source_data/ through X3ML
	if X3ML_step:
		process_3m(
			input_xml_folder,
			mapping_file_path,
			generator_file_path,
			x3ml_output_folder_path,
			post_process_url,
			"utf-8",
			"2",
			"application/n-triples")

	# Now we will run a data cleaning step on each ttl file in /data/x3ml_output/
	# This will do a bulk search and replace using postprocessing/replacements.txt
	# replacements.txt is a TSV file, where the first column is the string we want to replace and the second column is the new string
	# the replacement function is also fixing URIs of the form <prefix:xxx> and changing it to prefix:xxx to make it valid turtle
	for filename in os.listdir(x3ml_output_folder_path):
		filepath = os.path.join(x3ml_output_folder_path, filename)
		if os.path.isfile(filepath) and filename.endswith(".ttl"):
			replacement_input_path = filepath
			replacement_output_path = f"{postprocessing_output_path}{filename}"
			replacement_file_path = "replacements.txt"

			if clean_step:
				print("cleaning")
				# replacements on ntriples
				replacement(post_process_url, replacement_input_path, replacement_file_path, "False", replacement_output_path)
				convert_coordinates_in_file(replacement_output_path, replacement_output_path)

				# this is for _illustrations file (on ntriples)
				if illustrations_only:
					replacement(post_process_url, replacement_output_path, "4_6_11_12_uri_align.tsv", "False", replacement_output_path)

				# on ntriples
				if replace_lincs_uris_step:
					print("minting")
					replacement(post_process_url, replacement_output_path, "lincs_uris.tsv", "False", replacement_output_path)

				# remove and do to all files combined later
				fix_entity_double_classing(replacement_output_path)
				replace_inverse_props(replacement_output_path, replacement_output_path, post_process_url)

				# do the replacements that need to happen on ttl not ntriples
				replacement(post_process_url, replacement_output_path, "ttl_swaps.tsv", "False", replacement_output_path)

	# Now we can use that cleaned up combined.ttl file to grab labels from external sources like wikidata
	# And we can run RDF validation on it
	validation_input_path = replacement_output_path

	# Get Wikidata labels
	# Note that once you've done this once, you can comment out this line
	# You only need to re-run this if you change your data and have different/new wikidata URIs
	# For wikidata, it will grab english and french labels
	# Note that it will not get labels for entities that are only the object of an owl:sameAs relationship
	if get_external_labels_step:
		external_labels(post_process_url, validation_input_path, postprocessing_output_path, "wikidata")
		external_labels(post_process_url, validation_input_path, postprocessing_output_path, "getty")
		external_labels(post_process_url, validation_input_path, postprocessing_output_path, "viaf")
		external_labels(post_process_url, validation_input_path, postprocessing_output_path, "loc")

	# get the list of all ttl files that should be included in the final data
	combine_file_list = []
	for filename in os.listdir(postprocessing_output_path):
		if filename.endswith(".ttl"):
			combine_file_list.append(os.path.join(postprocessing_output_path, filename))

	# run validation on final files
	# it will run syntactic validation on each one and then if that succeeds it will do a bunch of other checks on a combined version of the files
	# see wiki for details on how to interpret the results
	# https://gitlab.com/calincs/conversion/post-processing-service/-/wikis/rdf-validation
	if validation_step:
		print(f"Checking if individual files are syntactically valid")
		validation(combine_file_list, postprocessing_error_output_path, post_process_url, outfilename="individual_files", analyze="False")

	# combine final ttl file with label files and add to datasets folder
	if final_combine_step:
		print("Final Combine Step")
		# this will combine all files into a graph and then output a condensed version with no duplicate statements
		# but it won't work if any of your input files are not valid
		print("Try condensing files into one graph. This will fail if one of the files are not valid according to the previous step. Check the error files.")
		condense_ttl_graph(combine_file_list, final_ttl_path)

		print("Removing double classed E1_CRM_Entity")
		fix_entity_double_classing(final_ttl_path)

		print("Replace inverse CIDOC properties with the forward ones. LINCS no longer uses inverse properties.")
		replace_inverse_props(final_ttl_path, final_ttl_path, post_process_url)

		if validation_step:
			print("Final validation and full error checking for all adarchive files combined")
			validation(["../../Datasets/adarchive/adarchive_1.ttl", "../../Datasets/adarchive/adarchive_1_illustrations.ttl", "../../Datasets/adarchive/adarchive_2.ttl"], postprocessing_error_output_path, post_process_url, outfilename="combined", analyze="True")


main()
