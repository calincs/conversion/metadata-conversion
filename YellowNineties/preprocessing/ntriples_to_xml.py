import re
import requests


# prints one sided relationships in source ntriples file
# outputs new version of that ntriples file with those relationships removed
def remove_one_sided(source_file, new_file):
	with open(source_file, "r") as f_in:
		with open(new_file, "w") as f_out:
			for line in f_in:
				remove_line = False
				if "<https:/>" in line:
					remove_line = True
					#print(line)
				else:
					matches = re.findall(r"<(https:\/\/personography\.1890s\.ca\/persons\/.+?\/{0,1})> <.+?> <(https:\/\/personography\.1890s\.ca\/persons\/.+?\/{0,1})> \.", line)
					for m in matches:
						sub = m[0].strip("/")
						obj = m[1].strip("/")

						if sub == obj:
							remove_line = True

				if not remove_line:
					f_out.write(line)


def replace_invalid_uris(source_file, new_file):
	# these were manually grabbed from https://personography.1890s.ca/ whenever "a:24:" was present in the source data due to error in export
	with open(source_file, "r") as f_in:
		ntriples = f_in.read()

		mapping = {
			"<https://personography.1890s.ca/persons/binyon-laurence/> <http://personography.1890s.ca#friend_of> <a:24:> .": "<https://personography.1890s.ca/persons/binyon-laurence/> <http://personography.1890s.ca#friend_of> <https://personography.1890s.ca/persons/destree-olivier-georges/> .",
			"<https://personography.1890s.ca/persons/chambers-robert/> <http://personography.1890s.ca#relative_of> <a:24:> .": "<https://personography.1890s.ca/persons/chambers-robert/> <http://personography.1890s.ca#relative_of> <https://personography.1890s.ca/persons/dowie-menie-muriel/> .",
			"<https://personography.1890s.ca/persons/de-lisle-charles-marie-rene-leconte/> <http://personography.1890s.ca#friend_of> <a:24:> .": "<https://personography.1890s.ca/persons/de-lisle-charles-marie-rene-leconte/> <http://personography.1890s.ca#friend_of> <https://personography.1890s.ca/persons/heredia-jose-maria-de/> .",
			"<https://personography.1890s.ca/persons/france-anatole/> <http://personography.1890s.ca#colleague_of> <a:24:> .": "<https://personography.1890s.ca/persons/france-anatole/> <http://personography.1890s.ca#colleague_of> <https://personography.1890s.ca/persons/mendes-catulle/> .\n<https://personography.1890s.ca/persons/france-anatole/> <http://personography.1890s.ca#colleague_of> <https://personography.1890s.ca/persons/zola-emile/> .",
			"<https://personography.1890s.ca/persons/gaugin-paul/> <http://personography.1890s.ca#friend_of> <a:24:> .": "<https://personography.1890s.ca/persons/gaugin-paul/> <http://personography.1890s.ca#friend_of> <https://personography.1890s.ca/persons/serusier-paul/> .",
			"<https://personography.1890s.ca/persons/geddes-patrick/> <http://personography.1890s.ca#colleague_of> <a:24:> .": "<https://personography.1890s.ca/persons/geddes-patrick/> <http://personography.1890s.ca#colleague_of> <https://personography.1890s.ca/persons/reclus-elisee/> .",
			"<https://personography.1890s.ca/persons/gray-john/> <http://personography.1890s.ca#intimate_of> <a:24:> .": "<https://personography.1890s.ca/persons/gray-john/> <http://personography.1890s.ca#intimate_of> <https://personography.1890s.ca/persons/raffalovich-marc-andre/> .",
			"<https://personography.1890s.ca/persons/heredia-jose-maria-de/> <http://personography.1890s.ca#friend_of> <a:24:> .": "<https://personography.1890s.ca/persons/heredia-jose-maria-de/> <http://personography.1890s.ca#friend_of> <https://personography.1890s.ca/persons/mendes-catulle/> .\n<https://personography.1890s.ca/persons/heredia-jose-maria-de/> <http://personography.1890s.ca#friend_of> <https://personography.1890s.ca/persons/de-lisle-charles-marie-rene-leconte/> .",
			"<https://personography.1890s.ca/persons/horne-herbert-p/> <http://personography.1890s.ca#friend_of> <a:24:> .": "<https://personography.1890s.ca/persons/horne-herbert-p/> <http://personography.1890s.ca#friend_of> <https://personography.1890s.ca/persons/destree-olivier-georges/> .",
			"<https://personography.1890s.ca/persons/mendes-catulle/> <http://personography.1890s.ca#friend_of> <a:24:> .": "<https://personography.1890s.ca/persons/mendes-catulle/> <http://personography.1890s.ca#friend_of> <https://personography.1890s.ca/persons/heredia-jose-maria-de/> .",
			"<https://personography.1890s.ca/persons/millais-john-everett/> <http://personography.1890s.ca#friend_of> <a:24:> .": "<https://personography.1890s.ca/persons/millais-john-everett/> <http://personography.1890s.ca#friend_of> <https://personography.1890s.ca/persons/paton-noel/> .",
			"<https://personography.1890s.ca/persons/moore-george-augustus/> <http://personography.1890s.ca#friend_of> <a:24:> .": "<https://personography.1890s.ca/persons/moore-george-augustus/> <http://personography.1890s.ca#friend_of> <https://personography.1890s.ca/persons/zola-emile/> .",
			"<https://personography.1890s.ca/persons/noguchi-yone/> <http://personography.1890s.ca#legal_spouse_of> <a:24:> .": "<https://personography.1890s.ca/persons/noguchi-yone/> <http://personography.1890s.ca#legal_spouse_of> <https://personography.1890s.ca/persons/gilmour-leonie/> .",
			"<https://personography.1890s.ca/persons/norman-henry/> <http://personography.1890s.ca#legal_spouse_of> <a:24:> .": "<https://personography.1890s.ca/persons/norman-henry/> <http://personography.1890s.ca#legal_spouse_of> <https://personography.1890s.ca/persons/dowie-menie-muriel/> .",
			"<https://personography.1890s.ca/persons/reclus-elie/> <http://personography.1890s.ca#sibling_of> <a:24:> .": "<https://personography.1890s.ca/persons/reclus-elie/> <http://personography.1890s.ca#sibling_of> <https://personography.1890s.ca/persons/reclus-elisee/> .",
			"<https://personography.1890s.ca/persons/waller-max/> <http://personography.1890s.ca#colleague_of> <a:24:> .": "<https://personography.1890s.ca/persons/waller-max/> <http://personography.1890s.ca#colleague_of> <https://personography.1890s.ca/persons/destree-olivier-georges/> .",
			"<https://personography.1890s.ca/persons/kropotkin-pyotor/> <http://personography.1890s.ca#colleague_of> <a:24:> .": "<https://personography.1890s.ca/persons/kropotkin-pyotor/> <http://personography.1890s.ca#colleague_of> <https://personography.1890s.ca/persons/reclus-elisee/> .",
			"<https://personography.1890s.ca/persons/ritter-carl/> <http://personography.1890s.ca#mentor_of> <a:24:> .": "<https://personography.1890s.ca/persons/ritter-carl/> <http://personography.1890s.ca#mentor_of> <https://personography.1890s.ca/persons/reclus-elisee/> ."
			}

		for key in mapping:
			ntriples = ntriples.replace(key, mapping[key])

	with open(new_file, "w") as f_out:
		f_out.write(ntriples)


# converts that ntriples file into rdf/xml and saves it.
# then that rdf/xml file can be processed as a separate step in pre_process.py
def ntriple_to_xml(source_file, new_file, post_process_url):

	url = post_process_url + '/serialize/?input_serialization=ntriples&output_serialization=rdfxml&encoding=utf-8'
	files = {'file': ("serialize.ttl", open(source_file, 'rb'))}
	response = requests.request("POST", url, files=files)
	with open(new_file, 'w') as f:
		#fix xml header
		xml = response.text.replace("""<rdf:RDF
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:j.0="http://personography.1890s.ca#" > """, """<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:y90s="https://personography.1890s.ca/ontology/" xmlns:owl="http://www.w3.org/2002/07/owl#">""")

		xml = xml.replace(""";
</""", ';</')

		xml = xml.replace(""";
</""", ";</")

		matches = re.findall(r""";\n[A-Za-z]""", xml)
		for match in matches:
			#print(match.replace("\n", ""))
			xml = xml.replace(match, match.replace("\n", ""))

		f.write(xml)


def main():
	source_ntriple = "../data/source_data/June2023_database_export/P11y-RDF-NTriples-3/Persons-Export-2022-December-06-1420.nt"
	updated_ntriple = "../data/source_data/June2023_database_export/P11y-RDF-NTriples-3/EDITED_Persons-Export-2022-December-06-1420.nt"
	updated_xml = "../data/source_data/June2023_database_export/P11y-RDF-XML/persons.xml"

	post_process_url = "http://0.0.0.0:80"

	remove_one_sided(source_ntriple, updated_ntriple)
	replace_invalid_uris(updated_ntriple, updated_ntriple)
	ntriple_to_xml(updated_ntriple, updated_xml, post_process_url)


if __name__ == '__main__':
	main()
