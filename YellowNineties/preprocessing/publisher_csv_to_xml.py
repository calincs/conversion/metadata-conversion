
def create_publisher_xml():
	# this script makes an xml file about publishers and an xml file about magazines and their issues
	# manually changed csv file to tsv, deleted messed up columns, fixed commas and semi colons and missing / on urls
	# manually changed to https://1890s.ca/tprv1_all/ https://1890s.ca/tpr-all/
	# pageantv1_all to pag1-all and pageantv2_all to pag2_all
	# venturev1_all to vv1-all and venturev2_all to vv2-all
	input_tsv = "../data/source_data/Feb2023_database_export/P11y-CSV/Publishers-and-Printers-Export-2022-August-08-1944.tsv"
	output_xml = "/Users/nataliehervieux/Documents/Conversion/metadata-conversion/YellowNineties/data/preprocessing_output/publisher.xml"


	with open(input_tsv) as csvfile:
		with open(output_xml, "w") as xmlfile:
			xmlfile.write('<?xml version="1.0" ?>\n')
			xmlfile.write("<publishers>\n")

			rows = csvfile.readlines()
			#rows = csv.reader(csvfile, delimiter='\t', quotechar='|')
			for row in rows[1:]:
				row = row.strip("\n").split("\t")

				publisher_id = row[0]
				publsher_title = row[1]
				publisher_permalink = row[4]
				publisher_slug = row[5]
				publisher_xml_id = row[6]
				publisher_sameas = row[7]
				published_y90s_magazine = row[8].strip("\n").split(", ")


				xmlfile.write(f"\t<publisher>\n")
				xmlfile.write(f"\t\t<permalink>{publisher_permalink}</permalink>\n")
				if publisher_sameas:
					xmlfile.write(f"\t\t<sameas>{publisher_sameas}</sameas>\n")
				for magazine in published_y90s_magazine:
					if magazine:
						magazine = magazine.replace('"', '').replace(";", "").strip(" ")
						if not magazine.endswith("/"):
							magazine = magazine + "/"
						magazine_slug = magazine.split("/")[-2]
						print(magazine)
						xmlfile.write(f'\t\t<published_magazine slug="{magazine_slug}">{magazine}</published_magazine>\n')
				xmlfile.write(f"\t</publisher>\n")

			xmlfile.write("</publishers>\n")



def create_magazine_xml():
	# ended up just manually making an xml file.

	"""
	evergreen - 4 volumes egv1 to egv4
	yellow books - 13 volumes yb-v1 to yb-v13
	savoy - 8 volumes savoyv1 to savoyv8
	dial - 5 volumes dialv1 to dialv5
	pagan review - 1 volume tpr
	green sheaf - 13 volumes gsv1 to gsv13
	pageant - 2 volumes pageantv1 to pageantv2
	venture - 2 volumes vv1 to vv2

	"""

	#output_xml = "/Users/nataliehervieux/Documents/Conversion/metadata-conversion/YellowNineties/data/preprocessing_output/magazine.xml"

	magazine = [
		"https://1890s.ca/evergreen-volumes/",
		"https://1890s.ca/yellow-book-volumes/",
		"https://1890s.ca/savoy-volumes/",
		"https://1890s.ca/dial-volumes/",
		"https://1890s.ca/pagan_review/",
		"https://1890s.ca/green-sheaf-volumes/",
		"https://1890s.ca/pageant_volumes/",
		"https://1890s.ca/venture-volumes/"
		]

	issues = [
		"https://1890s.ca/ybv1_all/",
		"https://1890s.ca/ybv2_all/",
		"https://1890s.ca/ybv3_all/",
		"https://1890s.ca/ybv4_all/",
		"https://1890s.ca/ybv5_all/",
		"https://1890s.ca/ybv6_all/",
		"https://1890s.ca/ybv7_all/",
		"https://1890s.ca/ybv8_all/",
		"https://1890s.ca/ybv9_all/",
		"https://1890s.ca/ybv10_all/",
		"https://1890s.ca/ybv11_all/",
		"https://1890s.ca/ybv12_all/",
		"https://1890s.ca/ybv13_all/",

		"https://1890s.ca/tprv1_all/",

		"https://1890s.ca/venturev1_all/",
		"https://1890s.ca/venturev2_all/",

		"https://1890s.ca/gsv1_all/",
		"https://1890s.ca/gsv2_all/",
		"https://1890s.ca/gsv3_all/",
		"https://1890s.ca/gsv4_all/",
		"https://1890s.ca/gsv5_all/",
		"https://1890s.ca/gsv6_all/",
		"https://1890s.ca/gsv7_all/",
		"https://1890s.ca/gsv8_all/",
		"https://1890s.ca/gsv9_all/",
		"https://1890s.ca/gsv10_all/",
		"https://1890s.ca/gsv11_all/",
		"https://1890s.ca/gsv12_all/",
		"https://1890s.ca/gsv13_all/",

		"https://1890s.ca/pageantv1_all/",
		"https://1890s.ca/pageantv2_all/",

		"https://1890s.ca/savoyv1_all/",
		"https://1890s.ca/savoyv2_all/",
		"https://1890s.ca/savoyv3_all/",
		"https://1890s.ca/savoyv4_all/",
		"https://1890s.ca/savoyv5_all/",
		"https://1890s.ca/savoyv6_all/",
		"https://1890s.ca/savoyv7_all/",
		"https://1890s.ca/savoyv8_all/",

		"https://1890s.ca/egv1-all/",
		"https://1890s.ca/egv2_all/",
		"https://1890s.ca/egv3_all/",
		"https://1890s.ca/egv4_all/",

		"https://1890s.ca/dialv4-all/",
		"https://1890s.ca/dialv5-all/",
		"https://1890s.ca/dialv1-all/",
		"https://1890s.ca/dial2-all/",
		"https://1890s.ca/dialv3-all/"
		]


create_publisher_xml()
