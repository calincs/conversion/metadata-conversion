import xml.etree.ElementTree as ET
import io
import os
import sys
import getopt
import requests
import re


## Run ntriples_to_xml.py before running this pre_process.py script.
## But some changes have been made manually to clean person.xml
## This script (pre_process.py) prepares the rdf/xml data for X3ML.

def magazine_mapping(magazine_uri):
	mapping = {
		"https://1890s.ca/eg": "https://1890s.ca/evergreen-volumes/",
		"https://1890s.ca/yb": "https://1890s.ca/yellow-book-volumes/",
		"https://1890s.ca/savoy": "https://1890s.ca/savoy-volumes/",
		"https://1890s.ca/dial": "https://1890s.ca/dial-volumes/",
		"https://1890s.ca/tpr": "https://1890s.ca/pagan_review/",
		"https://1890s.ca/gs": "https://1890s.ca/green-sheaf-volumes/",
		"https://1890s.ca/pag": "https://1890s.ca/pageant_volumes/",
		"https://1890s.ca/vv": "https://1890s.ca/venture-volumes/",
		"https://1890s.ca/venture": "https://1890s.ca/venture-volumes/"}

	for key in mapping:
		if key in magazine_uri:
			return mapping[key]
	else:
		return magazine_uri


def add_magazines(file_text):
	#<y90s_contributed_to rdf:resource="https://1890s.ca/pageantv2_all"
	contributed_to_matches = re.findall(r'(y90s:contributed_to rdf:resource="https:\/\/1890s\.ca\/(.*?)" *?\/\>)', file_text)
	for match in contributed_to_matches:
		magazine_uri = magazine_mapping(match[0])
		new_match = match[0].replace("/>", f' slug="{match[1]}" magazine="{magazine_uri}"/>')
		file_text = file_text.replace(match[0], new_match)

	return file_text


# Dictionary where key is the value to be replaced and value is what it should be replaced with.
def fix_y90s_urls(rdf_file_content):
	# finds https://personography.1890s.ca/ URIs and if they don't end in a /, add a /

	replacements = []

	matches = re.findall(r'"(https:\/\/personography\.1890s\.ca\/.*?[a-z])"', rdf_file_content)

	for match in matches:
		replacements.append(match)
	replacements = list(set(replacements))

	for r in replacements:
		rdf_file_content = rdf_file_content.replace(f'"{r}"', f'"{r}/"')

	rdf_file_content = rdf_file_content.replace('><y90s:name>', '>\n    <y90s:name>')

	return rdf_file_content


def clean_lists(rdf_file_content, tag):
	"""
	takes input of the form:
		<y90s:educated_at>&lt;p&gt;Instituto Portuense; University of Coimbra&lt;/p&gt;</y90s:educated_at>

	and turns it into
		<y90s:educated_at>Instituto Portuense</y90s:educated_at>
		<y90s:educated_at>University of Coimbra</y90s:educated_at>
	"""

	matches = re.findall(fr"\<{re.escape(tag)}>(.*?)\<\/{re.escape(tag)}>", rdf_file_content)
	for match in matches:

		original_match = match

		new_element_string = ""

		match = match.replace("&lt;p&gt;", "")
		match = match.replace("&lt;/p&gt;", "")
		match = match.replace("&lt;br /&gt", "")
		match = match.replace("&amp;#8217;", "'")
		match = match.replace("&amp;#8217", "'")
		match = match.replace("&amp;#8216;", '"')
		match = match.replace("&amp;#8216", '"')
		match = match.replace("&amp;#8220;", '"')
		match = match.replace("&amp;#8220", '"')
		match = match.replace("&amp;#8221;", '"')
		match = match.replace("&amp;#8221", '"')

		match_list = match.split(";")

		for education in match_list:
			if education.strip(" ") != "":
				new_element_string = new_element_string + f"    <{tag}>{education.strip(' ')}</{tag}>\n"

		rdf_file_content = rdf_file_content.replace(f"<{tag}>{original_match}</{tag}>", new_element_string[:-1])
		rdf_file_content = rdf_file_content.replace("        ", "    ")

		rdf_file_content = rdf_file_content.replace('<y90s:contributed_to rdf:resource="https://1890s"/>\n', '')
		rdf_file_content = rdf_file_content.replace('<y90s:contributed_to rdf:resource="https://1890"/>\n', '')
		rdf_file_content = rdf_file_content.replace('<y90s:contributed_to rdf:resource="https://189"/>\n', '')
		rdf_file_content = rdf_file_content.replace('<y90s:contributed_to rdf:resource="https://18"/>\n', '')
		rdf_file_content = rdf_file_content.replace('<y90s:contributed_to rdf:resource="https://1"/>\n', '')
		rdf_file_content = rdf_file_content.replace('<y90s:death_date rdf:datatype="http://www.w3.org/2001/XMLSchema#gYear">0</y90s:death_date>\n', '')
		rdf_file_content = rdf_file_content.replace('<y90s:birth_date rdf:datatype="http://www.w3.org/2001/XMLSchema#gYear">0</y90s:birth_date>\n', '')
		rdf_file_content = rdf_file_content.replace('\n\n', '\n')

	return rdf_file_content


def replacement(post_process_url, input_file_path, replacements_file, regex, output_rdf_path):

	# calling post processing api to replace wikidata links
	url = post_process_url + f'/replacement?regex={regex}'
	payload = {}
	headers = {}
	files = [
		('data_file', ('test_replacement.ttl', open(input_file_path, 'rb'), 'application/octet-stream')),
		('map_file', ('entity_replacements.tsv', open(replacements_file, 'rb'), 'application/octet-stream'))
	]

	response = requests.request("POST", url, headers=headers, data=payload, files=files)
	content = response.text

	for tag in ["y90s:educated_at", "y90s:death_place", "y90s:is_member_of", "y90s:birth_place", "y90s:aka", "y90s:other_pen_name"]:
		content = clean_lists(content, tag)

	content = fix_y90s_urls(content)
	content = add_magazines(content)

	with open(output_rdf_path, 'w') as f:
		f.write(content)


def sorted_id(id1, id2, node):
	'''
	Takes two id's and a node name and returns a string starting with the node name continued by the ids
	in numeric order.
	'''

	id1 = id1.split('/')[-2]
	id2 = id2.split('/')[-2]
	node = node.split('}')[-1]

	if id1 < id2:
		return(f"{node}#{id1}_{id2}")
	else:
		return(f"{node}#{id2}_{id1}")


def sorted_name(name1, name2, node):
	'''
	Takes two names and a node and builds a string label in alphabetical order
	'''
	relation_map = {'colleague_of': 'Colleague',
					'friend_of': 'Friend',
					'intimate_of': 'Intimate',
					'legal_spouse_of': 'Legal Spouse',
					'extralegal_spouse_of': 'Extralegal Spouse',
					'sibling_of': 'Sibling',
					'relative_of': 'Relative'}
	names = [name1, name2]
	names = sorted(names)
	node = node.split('}')[-1]

	if node == 'other_pen_name':
		return(f"Name used by {names[0]} and {names[1]}")
	else:
		return(f"{relation_map[node]} relationship between {names[0]} and {names[1]}")


def main():

	#post_process_url = "http://0.0.0.0:80"
	#post_process_url = "http://127.0.0.1:8000"
	post_process_url = "https://postprocess.stage.lincsproject.ca"

	# Default arguments
	in_file = "../data/source_data/June2023_database_export/P11y-RDF-XML/persons.xml"
	out_file = "../data/preprocessing_output/persons.xml"
	out_uris_file = "../data/preprocessing_output/persons_uris.tsv"
	out_no_uris_file = "../data/preprocessing_output/persons_no_uris.tsv"
	out_sameas_file = "../data/postprocessing_output/persons_sameas.ttl"

	# Use this format to save middle steps in processing
	out_temp_file = "../data/preprocessing_output/step_NUM_persons.xml"
	NUM = 0

	replacement(post_process_url, in_file, "replacements_noregex.tsv", False, out_temp_file.replace("NUM", str(NUM)))
	in_file = out_temp_file.replace("NUM", str(NUM))
	NUM += 1

	# Declare namespaces used and register to Element Tree
	namespaces = {'owl': 'http://www.w3.org/2002/07/owl#',
				  'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
				  'y90s': 'https://personography.1890s.ca/ontology/',
				  'dcterms': "http://purl.org/dc/terms/"}

	for ns in namespaces:
		ET.register_namespace(ns, namespaces[ns])

	# Load data
	tree = ET.parse(in_file)
	root = tree.getroot()

	# Define elements that need to be modified

	inverse_relations = {'{https://personography.1890s.ca/ontology/}colleague_of',
						'{https://personography.1890s.ca/ontology/}friend_of',
						'{https://personography.1890s.ca/ontology/}intimate_of',
						'{https://personography.1890s.ca/ontology/}legal_spouse_of',
						'{https://personography.1890s.ca/ontology/}extralegal_spouse_of',
						'{https://personography.1890s.ca/ontology/}sibling_of',
						'{https://personography.1890s.ca/ontology/}relative_of'}

	# Find all baseNodes

	base_node = root.findall('rdf:Description', namespaces)
	has_occupation = '{https://personography.1890s.ca/ontology/}has_occupation'
	mentor_of = '{https://personography.1890s.ca/ontology/}mentor_of'
	mentored_by = '{https://personography.1890s.ca/ontology/}mentored_by'


	# output a tsv file of all owl same as relations in the xml
	# we will add those to the final converted data during post processing
	# also output a persons_sameas.ttl file in post processing output
	# that file maps the VIAF URIs that we'll use to the original Y90s IDs
	# commented out right now because we made manual changes that are not reflected in source y90s ntriples
	with open(out_uris_file, "w") as f_uris:
		#with open(out_sameas_file, "w") as f_sameas:
		with open(out_no_uris_file, "w") as f_no_uri:
			#f_sameas.write("@prefix owl: <http://www.w3.org/2002/07/owl#> .\n\n")

			for person_node in base_node:
				y90s_uri = person_node.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about']

				sameas_node = person_node.find('{http://www.w3.org/2002/07/owl#}sameAs')

				if sameas_node is not None:
					sameas_uri = sameas_node.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource']
					sameas_uri = sameas_uri.replace("http://viaf.org/viaf/http://viaf.org/viaf/", "http://viaf.org/viaf/")
					sameas_uri = sameas_uri.replace("https://viaf.org", "http://viaf.org")
					if sameas_uri[-1] == "/":
						sameas_uri = sameas_uri[:-1]
					#f_sameas.write(f"<{sameas_uri}> owl:sameAs <{y90s_uri}> .\n")
					f_uris.write(f"{y90s_uri}\t{sameas_uri}\n")
				else:
					f_no_uri.write(f"{y90s_uri}\n")


	# Iterate through all basenodes (each rdf:Description element)
	for node in base_node:

		# Get first node information (information about the main person for this record)
		p1 = node.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about']
		p1_label = node.find('y90s:name', namespaces)
		if p1_label is not None:
			p1_label = p1_label.text


		# Iterate through all elements of interest (the person to person relationship elements)
		for relation in inverse_relations:
			# Check if elements exists
			all_elements = node.findall(relation)

			for element in all_elements:
				p2 = element.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource']

				# Find person 2, get label
				p2_root = root.find(f'rdf:Description[@rdf:about="{p2}"]', namespaces)
				p2_label = p2_root.find('y90s:name', namespaces)

				if p2_label is not None:
					p2_label = p2_label.text

				# Write new element tags for new id and label
				element.set('relationship_id', sorted_id(p1, p2, relation))
				element.set('relationship_label', sorted_name(p1_label, p2_label, relation))

			# Get Occupation Label

			for element in node.findall(has_occupation):
				occupation = element.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource']
				occupation = occupation.split("/")[-2].replace('-', ' ')
				element.set('person_label', f"{occupation} occupation of {p1_label}")
				element.set('label', occupation)

			# Mentor of labels
			for element in node.findall(mentor_of):
				p2 = element.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource']
				# Find person 2, get label
				p2_root = root.find(f'rdf:Description[@rdf:about="{p2}"]', namespaces)
				p2_label = p2_root.find('y90s:name', namespaces)
				if p2_label is not None:
					p2_label = p2_label.text

				# Write new mentor label
				mentor_label = f"Mentorship relationship between {p1_label} (mentor) and {p2_label} (mentee)"
				element.set('mentor_label', mentor_label)
				element.set('mentor_id', "#mentor-{}".format(p1.split("/")[-2]))
				element.set('mentee_id', "#mentee-{}".format(p2.split("/")[-2]))
				element.set('relationship_id', sorted_id(p1, p2, "mentor_of"))

			# Mentored by labels
			for element in node.findall(mentored_by):
				p2 = element.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource']
				# Find person 2, get label
				p2_root = root.find(f'rdf:Description[@rdf:about="{p2}"]', namespaces)
				p2_label = p2_root.find('y90s:name', namespaces)
				if p2_label is not None:
					p2_label = p2_label.text

				# Write new mentor label
				mentor_label = f"Mentorship relationship between {p2_label} (mentor) and {p1_label} (mentee)"
				element.set('mentor_label', mentor_label)
				element.set('mentee_id', "#mentee-{}".format(p1.split("/")[-2]))
				element.set('relationship_id', sorted_id(p1, p2, "mentor_of"))
				element.set('mentor_id', "#mentor-{}".format(p2.split("/")[-2]))

	tree.write(out_temp_file.replace("NUM", str(NUM)), encoding='utf-8')
	in_file = out_temp_file.replace("NUM", str(NUM))
	NUM += 1

	# Convert y90s:<node> to y90s_<node>

	fix_terms = {'y90s:': 'y90s_', 'dcterms:': 'dcterms_'}

	with io.open(in_file, 'r', encoding='utf-8') as pp:
		with io.open(out_file.replace("NUM", str(NUM)), 'w', encoding='utf-8') as fp:
			for line in pp.readlines():
				for term in fix_terms:
					line = line.replace(term, fix_terms[term])
				fp.write(line)
				line = pp.readline()


if __name__ == '__main__':
	main()
