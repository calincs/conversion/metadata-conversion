import json

with open("y90s_original.json") as f:
	data = json.load(f)

people = data["Description"]

child_of = []
colleague_of = []
friend_of = []
mentored_by = []
mentor_of = []
parent_of = []
legal_spouse_of = []
intimate_of = []
sibling_of = []
relative_of = []
extralegal_spouse_of = []


def make_list(label, llist, person):
	if label in i:
		if type(i[label]) is dict:
			llist.append([person, i[label]["@resource"]])
		else:
			for sub in i[label]:
				llist.append([person, sub["@resource"]])
	return llist


def check_inverse(label, llist):
	print()
	print("*" * 10, label, "*" * 10)
	for rel in llist:
		if rel[0] == rel[1]:
			print("relationship to oneself:", rel[0])
		elif [rel[1], rel[0]] not in llist:
			print("one sided relationship:", rel)

def check_inverse_twolists(label1, label2, list1, list2):
	print()
	print("*" * 10, label2, "*" * 10)
	for rel in list1:
		if rel[0] == rel[1]:
			print("relationship to oneself:", rel[0])
		elif [rel[0], rel[1]] in list2:
			print("logical inconsistency:", rel)
		elif [rel[1], rel[0]] not in list2:
			print("one sided relationship:", rel)

	print()
	print("*" * 10, label1, "*" * 10)
	for rel in list2:
		if rel[0] == rel[1]:
			print("relationship to oneself:", rel[0])
		elif [rel[0], rel[1]] in list1:
			print("logical inconsistency:", rel)
		elif [rel[1], rel[0]] not in list1:
			print("one sided relationship:", rel)


for i in people:
	person = i["@about"]

	child_of = make_list("child_of", child_of, person)
	parent_of = make_list("parent_of", parent_of, person)

	sibling_of = make_list("sibling_of", sibling_of, person)

	mentored_by = make_list("mentored_by", mentored_by, person)
	mentor_of = make_list("mentor_of", mentor_of, person)

	colleague_of = make_list("colleague_of", colleague_of, person)

	friend_of = make_list("friend_of", friend_of, person)

	legal_spouse_of = make_list("legal_spouse_of", legal_spouse_of, person)

	intimate_of = make_list("intimate_of", intimate_of, person)

	relative_of = make_list("relative_of", relative_of, person)

	extralegal_spouse_of = make_list("extralegal_spouse_of", extralegal_spouse_of, person)


#child_of
#parent_of
check_inverse_twolists("child_of", "parent_of", child_of, parent_of)

# sibling_of
check_inverse("sibling_of", sibling_of)

# mentored_by
# mentor_of
check_inverse_twolists("mentor_of", "mentored_by", mentor_of, mentored_by)

# colleague_of
check_inverse("colleague_of", colleague_of)

# friend_of
check_inverse("friend_of", friend_of)

# legal_spouse_of
check_inverse("legal_spouse_of", legal_spouse_of)

# intimate_of
check_inverse("intimate_of", intimate_of)

# relative_of
check_inverse("relative_of", relative_of)

# extralegal_spouse_of
check_inverse("extralegal_spouse_of", extralegal_spouse_of)