import xml.etree.ElementTree as ET


in_file = "step_0_persons.xml"

# Declare namespaces used and register to Element Tree
namespaces = {'owl': 'http://www.w3.org/2002/07/owl#',
			  'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
			  'y90s': 'https://personography.1890s.ca/ontology/',
			  'dcterms': "http://purl.org/dc/terms/"}

for ns in namespaces:
	ET.register_namespace(ns, namespaces[ns])

# Load data
tree = ET.parse(in_file)
root = tree.getroot()

base_node = root.findall('rdf:Description', namespaces)

for person_node in base_node:
	y90s_uri = person_node.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about']

	contrib_node = person_node.find('{https://personography.1890s.ca/ontology/}contributed_to')
	contrib_type = person_node.find('{https://personography.1890s.ca/ontology/}contribution_type')


	if contrib_type is not None:
		if contrib_node is None:
			print("\t".join([y90s_uri, contrib_type.text]))
