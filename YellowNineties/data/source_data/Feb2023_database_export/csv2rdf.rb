require 'csv'
require 'php-serialize'
require 'json'
require 'rdf'
require 'pry'
require 'pry-rescue'

#######################################################

# This converter processes PHP-serialized strings  
# see: https://en.wikipedia.org/wiki/PHP_serialization_format
php_converter = proc do |field_value, field|
  
  if field_value.is_a?(String) & field_value.include?(';')

    # WP All Export uses a pipe character to concatenate multiple values
    field_value.split('|').map do |value|

      # unserialize the value
      begin
        unserialized_value = PHP.unserialize(value)

        case unserialized_value
        when Hash
          # Generate a URI using the 'post_type' and 'post_name' values
          # This is probably a better approach than using guids, see: https://en.wikipedia.org/wiki/Cool_URIs_don%27t_change
          uri = URI.parse(unserialized_value['guid'])
          cool_uri = RDF::URI.new(scheme: uri.scheme, host: uri.host) / unserialized_value['post_type'] / unserialized_value['post_name']
          
          cool_uri.to_s
        when Array
          unserialized_value
        else
          value
        end

      rescue
        value
      end

    end

  else
    field_value
  end
end

# This converter processes multi-value strings concatenated with ; or ,
=begin
p11y_regexp = /[,;] /

p11y_converter = proc do |field_value, field|
  if field_value.is_a?(String) & p11y_regexp.match?(field_value)
    field_value.split(p11y_regexp)
  else
    field_value
  end
end
=end
p11y_converter = proc do |field_value, field|
  
  values = URI.extract(field_value)
  
  if values.count > 1
    values.map {|v| v.chomp(',') }
  else
    field_value
  end
  
end

# This converter strips HTML tags
html_converter = proc do |field_value, field|
  test = field_value.split(/\<.*?\>/)
  
  if test.count > 1
    test.map(&:strip).reject(&:empty?)
  else
    field_value
  end

end

# Convert a String into an RDF::URI object
def string_to_uri(str)
  return str unless str.is_a? String
  ext = URI.extract(str)

  return RDF::URI(ext.first) unless ext.empty?

  str
end

#######################################################

# open CSV file
abort('No CSV filename provided') if (ARGV[0].nil? or not File.exists? ARGV[0])

# read CSV->RDF predicate mapping file into hash
abort('No CSV->RDF mapping filename provided') if (ARGV[1].nil? or not File.exists? ARGV[1])

# read predicate map file into hash
predicate_hash = JSON.parse(File.read(ARGV[1]))

# create new RDF graph to insert statements into
graph = RDF::Graph.new
RDF::VOCABS[:y90s] = {:uri=>"http://personography.1890s.ca#", :class_name=>"Y90S"}

# read and iterate CSV rows
CSV.foreach(ARGV[0], headers: true, converters: [:all, php_converter, html_converter, p11y_converter]) do |row|
  #   read row and iterate columns
  row.each do|header, value|
    next if value.nil?
    value.flatten! if value.is_a? Array

    # search predicate hash for column title
    if predicate_hash.has_key? header
      prefix, suffix = predicate_hash[header].split(':', 2).map {|v| v.to_sym}

      if RDF::VOCABS.keys.include?(prefix)
        prefix_uri = RDF::VOCABS[prefix][:uri]

        # WP All Export uses different headers for Posts and Terms
        if row.header? "Permalink"
          # This is a WP-Post
          subject = RDF::URI.new(row['Permalink'])
          graph << RDF::Statement(subject, RDF.type, RDF::URI.intern(prefix_uri + 'Person'))

        else
          # This is a WP-Term
          subject = RDF::URI.new(row['Term Permalink'])
          value = subject.parent if header == "Parent Slug"

          case subject.parent.path.count('/')
          when 2
            graph << RDF::Statement(subject, RDF.type, RDF::URI.intern(prefix_uri + 'LabourClass'))
            suffix = :labour_class_name if header == "Term Name"

          when 3
            graph << RDF::Statement(subject, RDF.type, RDF::URI.intern(prefix_uri + 'Order'))
            suffix = :order_name if header == "Term Name"
            suffix = :within_labour_class if header == "Parent Slug"

          when 4
            graph << RDF::Statement(subject, RDF.type, RDF::URI.intern(prefix_uri + 'SubOrder'))
            suffix = :sub_order_name if header == "Term Name"
            suffix = :within_order if header == "Parent Slug"

          else #5
            graph << RDF::Statement(subject, RDF.type, RDF::URI.intern(prefix_uri + 'Occupation'))
          end
        end

        # TODO: check that this suffix exists in the vocab's RDF properties?
        predicate = RDF::URI.intern(prefix_uri + suffix.to_s)

        # insert RDF statement into graph
        if value.is_a? Array
          value.each do |v|
            graph << RDF::Statement(subject, predicate, string_to_uri(v))
          end
        else
          graph << RDF::Statement(subject, predicate, string_to_uri(value))
        end
        
      end
      
    end

  end

end

print graph.dump(:ntriples)

exit
#######################################################
