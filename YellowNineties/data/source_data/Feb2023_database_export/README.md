## Changes applied to NTriples file

Ran remove_rel_oneself.py to remove one sided relationships from the ntriples file

Used Jena Riot to transform Persons NTriples file into rdf/xml. Then used that as input for the preprocessing step. 

I manually removed a line that contained <https:/> in the Ntriple file before converting with Riot.

`./riot --output=rdfxml --syntax=ntriples EDITED_Persons-Export-2022-December-06-1420.nt`

Then I manually replaced the XML header with:
```
<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:y90s="https://personography.1890s.ca/ontology/" xmlns:owl="http://www.w3.org/2002/07/owl#">
```

I manually removed 5-10 new line characters that were splitting the text of a single xml element. By searching this regex pattern:
```
;
[A-Za-z]
```


And I did a search and replace for 
```
;
</
```
And removed the new line.


## Changes that are added to preprocessing script due to this new data:
- <y90s:educated_at>&lt;p&gt;South Kensington School of Art; Slade School of Art&lt;/p&gt;</y90s:educated_at>
	+ Deal with lists like these and the encoding. Many tags have these so I dealt with them all.
- <y90s:has_occupation_copy rdf:datatype="http://www.w3.org/2001/XMLSchema#integer">748</y90s:has_occupation_copy>
	+ added the mapping from the occupation spreadsheet into the replacements_noregex.tsv. Used excel to create those mappings.
- <y90s:name>Bowerley, A.</y90s:name>
	+ this is missing from the data now. I added to the replacements_noregex.tsv file to add them back. I used OpenRefine with the Persons csv file 
	
- There is an inconsistent use of / at the end of 1890s URIs. Added them to the ends when missing

The following replacements:
- search and replace to change the blank node (j.0: in this case) to y90s:
- y90s:xml_id tags to dcterms:identifier
- https://www.wikidata.org/wiki/ to http://www.wikidata.org/entity/
- http://wikidata.org/wiki/ to http://www.wikidata.org/entity/
- https://viaf.org/viaf/ to http://viaf.org/viaf/
- y90s:same_as to owl:sameAs
- y90s:also_known_as to y90s:aka
- y90s:birthplace_uri to y90s:birth_place_uri
- y90s:deathplace_uri to y90s:death_place_uri
- y90s:has_occupation_copy to y90s:has_occupation 
- y90s:other_y90s_pen_name to y90s:other_pen_name 
- datatype http://www.w3.org/2001/XMLSchema#gYear is now integer in new data http://www.w3.org/2001/XMLSchema#integer. I changed that back for birth and death dates
	+ <y90s:birth_date rdf:datatype="http://www.w3.org/2001/XMLSchema#integer"> to <y90s:birth_date rdf:datatype="http://www.w3.org/2001/XMLSchema#gYear">
	+ <y90s:death_date rdf:datatype="http://www.w3.org/2001/XMLSchema#integer"> to <y90s:death_date rdf:datatype="http://www.w3.org/2001/XMLSchema#gYear">
	
- "https://personography.1890s.ca/publisher-or-printer/patrick-geddes-company/" and "https://personography.1890s.ca/publisher-or-printer/patrick-geddes-colleagues" are both listed as publishers in the person data. Used "https://personography.1890s.ca/publisher-or-printer/patrick-geddes-colleagues" for both.
- publisher-or-printer and publisher_or_printer are both used in the data. Switched to publisher-or-printer
- Some person lists are not separated by semi colons and it was ambiguous as to how to split them. like <y90s_other_pen_name>Charles Verlayne, Geo Gascoigne, James Marazion, Lionel Wingrave, S., Willard Dreeme, W.S. Fanshawe, W.H. Brooks, Wm. Windover</y90s_other_pen_name>
- person same as generated using the y90s tab in this sheet https://docs.google.com/spreadsheets/d/1YmBU_mAjBKBR2GsZCEsczYktJLPYGNkwehg-a4SJ0TQ/edit#gid=2133505968

## Changes that I don't think matter and haven't addressed:
- <rdf:type rdf:resource="http://personography.1890s.ca#Person"/> is now in the data


## Still to deal with:

- <y90s:published_with rdf:resource="http://dbpedia.org/resource/The_Bodley_Head"/> became <y90s:published_with rdf:resource="https://personography.1890s.ca/publisher_or_printer/bodley-head"/>
	+ Added publisher same as file to postprocessing output but maybe want to replace URIs instead. To replace other URIs in post processing anyways.
- Not currently using the rest of the information in the publishers document other than owl:same as (moved to postprocessing_output folder)
- Not currently using other information in the occupationg document. Need to look into this once we have figured out the vocabulary work.
- <y90s:contributed_to rdf:resource="https://1890s.ca/ybv13_all"/>
	+ This is in the new data and not captured in the mapping
- Other pen name is now a string so the handling in 3M will need to change
- <j.0:sibling_of rdf:resource="https://personography.1890s.ca/persons/cory-annie-sophie"/> this person is referenced but doesn't have a record (removed for now)
- missing occupation 775, 843, 760, 763, 774, 765, 764 (removed from data for now)
