import re

# prints one sided relationships in source ntriples file
# outputs new version of that ntriples file with those relationships removed

with open("P11y-RDF-NTriples-2/Persons-Export-2022-December-06-1420.nt", "r") as f_in:
	with open("P11y-RDF-NTriples-2/EDITED_Persons-Export-2022-December-06-1420.nt", "w") as f_out:
		for line in f_in:
			remove_line = False
			matches = re.findall(r"<(https:\/\/personography\.1890s\.ca\/persons\/.+?\/{0,1})> <.+?> <(https:\/\/personography\.1890s\.ca\/persons\/.+?\/{0,1})> \.", line)
			for m in matches:
				sub = m[0].strip("/")
				obj = m[1].strip("/")

				if sub == obj:
					remove_line = True
					print(line)

			if not remove_line:
				f_out.write(line)
