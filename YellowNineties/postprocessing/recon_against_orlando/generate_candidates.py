

def get_y90s_dict():

	y90s_dict = {}

	with open("y90s_reconSheet.tsv") as y90s_f:
		y90s_f.readline()
		for line in y90s_f:
			line_split = line.strip().split("\t")
			y90s_id = line_split[0]
			viaf = line_split[1]
			wikidata = line_split[2]
			label = line_split[3]
			labelAltOrder = line_split[4]
			altName = line_split[5]
			gender = line_split[6]
			birthDate = line_split[7]
			birthPlace = line_split[8]
			deathDate = line_split[9]
			deathPlace = line_split[10]


			all_name_tokens = []
			labelAltOrdersplit = labelAltOrder.split(" ")
			for token in labelAltOrdersplit:
				if token != "":
					token_split_initials = token.split(".")
					for t in token_split_initials:
						if t not in ["", '"', "the", "of"]:
							all_name_tokens.append(t)

			# split altName into individual names
			altNameSplit = altName.split(";")
			for name in altNameSplit:
				nameCommaSplit = name.split(",")
				for n in nameCommaSplit:
					nameSpaceSplit = n.split(" ")
					for i in nameSpaceSplit:
						token_split_initials = i.split(".")
						for t in token_split_initials:
							if t not in ["", '"', "the", "of"]:
								all_name_tokens.append(t.replace('"', ''))

			all_name_tokens = list(set(all_name_tokens))

			y90s_dict[y90s_id] = {"viaf": viaf, "wikidata": wikidata, "label": label.replace('"', ''), "labelAltOrder": labelAltOrder.replace('"', ''), "altName": altName.replace('"', ''), "nameTokens": all_name_tokens, "gender": gender, "birthDate": birthDate, "birthPlace": birthPlace, "deathDate": deathDate, "deathPlace": deathPlace}

	return y90s_dict


def get_orlando_dict():
	orlando_dict = {}

	with open("orlandosparqlresults.tsv") as orlandosparql_f:
		orlandosparql_f.readline()
		for line in orlandosparql_f:
			line_split = line.strip().split("\t")
			orlando_id = line_split[0]
			label = line_split[1]
			try:
				altLabel = line_split[2]
			except:
				altLabel = ""
			try:
				subjectOf = line_split[3]
			except:
				subjectOf = ""
			try:
				sameAs = line_split[4]
			except:
				sameAs = ""

			all_name_tokens = []
			labelsplit = label.split(" ")
			for token in labelsplit:
				if token != "":
					token_split_initials = token.split(".")
					for t in token_split_initials:
						t = t.replace('"', '').replace(",", "")
						if t not in ["", '"', "the", "of"]:
							all_name_tokens.append(t)

			# split altName into individual names
			altNameSplit = altLabel.split(";")
			for name in altNameSplit:
				nameCommaSplit = name.split(",")
				for n in nameCommaSplit:
					nameSpaceSplit = n.split(" ")
					for i in nameSpaceSplit:
						token_split_initials = i.split(".")
						for t in token_split_initials:
							t = t.replace('"', '').replace(",", "")
							if t not in ["", '"', "the", "of"]:
								all_name_tokens.append(t)

			all_name_tokens = list(set(all_name_tokens))

			orlando_dict[orlando_id] = {"label": label.replace('"', ''), "altLabel": altLabel.replace('"', ''), "subjectOf": subjectOf, "sameAs": sameAs, "nameTokens": all_name_tokens, "viaf": "", "title": "", "wikidata": "", "getty": ""}


	with open("orlandoallpeople.tsv") as orlandoall_f:
		orlandoall_f.readline()
		for line in orlandoall_f:
			line_split = line.strip().split("\t")
			orlando_id = line_split[1]
			try:
				title = line_split[2]
			except:
				title = ""
			try:
				viaf = line_split[3]
			except:
				viaf = ""
			try:
				getty = line_split[5]
			except:
				getty = ""
			try:
				wikidata = line_split[7]
			except:
				wikidata = ""


			all_name_tokens = []
			labelsplit = title.split(" ")
			for token in labelsplit:
				if token != "":
					token_split_initials = token.split(".")
					for t in token_split_initials:
						t = t.replace('"', '').replace(",", "")
						if t != "":
							all_name_tokens.append(t)


			if orlando_id in orlando_dict:
				orlando_dict[orlando_id]["title"] = title
				orlando_dict[orlando_id]["viaf"] = viaf
				orlando_dict[orlando_id]["wikidata"] = wikidata
				orlando_dict[orlando_id]["getty"] = getty
				orlando_dict[orlando_id]["nameTokens"] = list(set(orlando_dict[orlando_id]["nameTokens"] + all_name_tokens))
			else:
				orlando_dict[orlando_id] = {"title": title, "viaf": viaf, "wikidata": wikidata, "getty": getty, "nameTokens": all_name_tokens, "label": "", "altLabel": "", "subjectOf": "", "sameAs": ""}

	return orlando_dict


def main():
	y90s_dict = get_y90s_dict()
	orlando_dict = get_orlando_dict()

	candidate_dict = {}

	priority_1 = {}
	priority_2 = {}
	priority_3 = {}

	for y90s_person in y90s_dict:
		candidates = []

		for orlando_person in orlando_dict:

			# intersection of two name token lists
			intersection = list(set(y90s_dict[y90s_person]["nameTokens"]) & set(orlando_dict[orlando_person]["nameTokens"]))

			if len(intersection) > 1:
				candidates.append({orlando_person: orlando_dict[orlando_person]})

		if candidates != []:
			candidate_dict[y90s_person] = candidates
			if len(candidates) == 1:
				priority_1[y90s_person] = candidates
			elif len(candidates) == 2:
				priority_2[y90s_person] = candidates
			else:
				priority_3[y90s_person] = candidates

	with open("candidates_priority1.tsv", "w") as p1_out:
		headings = [
			"y90_person",
			"label",
			"altLabels",
			"gender",
			"birthDate",
			"birthPlace",
			"deathDate",
			"deathPlace",
			"viaf",
			"wikidata",
			"match?",
			"Jakob_found_id (only if no viaf or wikidata)",
			"candidate_id",
			"candidate_label",
			"candidate_title",
			"candidate_altLabels",
			"candidate_subjectOf",
			"candidate_viaf",
			"candidate_wikidata",
			"candidate_searchLink"]

		p1_out.write("\t".join(headings) + "\n")

		for y90s_id in priority_1:
			y90s_context = y90s_dict[y90s_id]

			candidate_list = priority_1[y90s_id]
			for orlando_id in candidate_list[0]:
				orlando_context = candidate_list[0][orlando_id]

				if orlando_context["viaf"] != "":
					orlando_context["viaf"] = "http://viaf.org/viaf/" + orlando_context["viaf"]
				if orlando_context["wikidata"] != "":
					orlando_context["wikidata"] = "http://www.wikidata.org/entity/" + orlando_context["wikidata"]

				row = [
					y90s_id,
					y90s_context["label"],
					y90s_context["altName"],
					y90s_context["gender"],
					y90s_context["birthDate"],
					y90s_context["birthPlace"],
					y90s_context["deathDate"],
					y90s_context["deathPlace"],
					y90s_context["viaf"],
					y90s_context["wikidata"],
					"",
					"",
					orlando_id,
					orlando_context["label"],
					orlando_context["title"],
					orlando_context["altLabel"],
					orlando_context["subjectOf"],
					orlando_context["viaf"],
					orlando_context["wikidata"],
					orlando_id.replace("https://commons.cwrc.ca/orlando:", "https://orlando-cambridge-org.subzero.lib.uoguelph.ca/people/")]

				p1_out.write("\t".join(row) + "\n")

	with open("candidates_priority2.tsv", "w") as p2_out:
		headings = [
			"y90_person",
			"label",
			"altLabels",
			"gender",
			"birthDate",
			"birthPlace",
			"deathDate",
			"deathPlace",
			"viaf",
			"wikidata",
			"match_candidate_id",
			"Jakob_found_id (only if no viaf or wikidata)",
			"candidate1_id",
			"candidate1_label",
			"candidate1_title",
			"candidate1_altLabels",
			"candidate1_subjectOf",
			"candidate1_viaf",
			"candidate1_wikidata",
			"candidate1_searchLink",
			"candidate2_id",
			"candidate2_label",
			"candidate2_title",
			"candidate2_altLabels",
			"candidate2_subjectOf",
			"candidate2_viaf",
			"candidate2_wikidata",
			"candidate2_searchLink"]

		p2_out.write("\t".join(headings) + "\n")

		for y90s_id in priority_2:
			y90s_context = y90s_dict[y90s_id]

			candidate_list = priority_2[y90s_id]

			for orlando_id in candidate_list[0]:
				orlando_context = candidate_list[0][orlando_id]

				if orlando_context["viaf"] != "":
					orlando_context["viaf"] = "http://viaf.org/viaf/" + orlando_context["viaf"]
				if orlando_context["wikidata"] != "":
					orlando_context["wikidata"] = "http://www.wikidata.org/entity/" + orlando_context["wikidata"]

				row = [
					y90s_id,
					y90s_context["label"],
					y90s_context["altName"],
					y90s_context["gender"],
					y90s_context["birthDate"],
					y90s_context["birthPlace"],
					y90s_context["deathDate"],
					y90s_context["deathPlace"],
					y90s_context["viaf"],
					y90s_context["wikidata"],
					"",
					"",
					orlando_id,
					orlando_context["label"],
					orlando_context["title"],
					orlando_context["altLabel"],
					orlando_context["subjectOf"],
					orlando_context["viaf"],
					orlando_context["wikidata"],
					orlando_id.replace("https://commons.cwrc.ca/orlando:", "https://orlando-cambridge-org.subzero.lib.uoguelph.ca/people/")]

			for orlando_id in candidate_list[1]:
				orlando_context = candidate_list[1][orlando_id]

				if orlando_context["viaf"] != "":
					orlando_context["viaf"] = "http://viaf.org/viaf/" + orlando_context["viaf"]
				if orlando_context["wikidata"] != "":
					orlando_context["wikidata"] = "http://www.wikidata.org/entity/" + orlando_context["wikidata"]

				row = row + [
					orlando_id,
					orlando_context["label"],
					orlando_context["title"],
					orlando_context["altLabel"],
					orlando_context["subjectOf"],
					orlando_context["viaf"],
					orlando_context["wikidata"],
					orlando_id.replace("https://commons.cwrc.ca/orlando:", "https://orlando-cambridge-org.subzero.lib.uoguelph.ca/people/")]

				p2_out.write("\t".join(row) + "\n")

main()
