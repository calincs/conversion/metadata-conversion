import SPARQLWrapper
import sys


def get_results(query):
	"""
	Performs Sparql query using self.endpoint_url
	:param query: query string
	:param timeout: timeout for query, defaults to ten seconds
	:param bulk_values: a set of values, if true bulk query will be done and will split up values if it times out
	:param max_attempts: max attempts to try again
	:param split_amount: equal portions that bulk values will be split into
	:return: A Json of mappings of ID -> Label
	"""

	endpoint_url = "https://query.wikidata.org/sparql"
	user_agent = "LINCS-https://lincsproject.ca//%s.%s" % (sys.version_info[0], sys.version_info[1])

	try:
		sparql = SPARQLWrapper.SPARQLWrapper(endpoint_url, agent=user_agent)
		sparql.setQuery(query)
		sparql.setReturnFormat(SPARQLWrapper.JSON)
		sparql.setTimeout(5000)
		results = sparql.query().convert()
		if results is not None:
			return results['results']['bindings']
		else:
			return None

	except Exception as e:
		print(e)


def wikidata_query():
	# this query gets Wikipedia articles using Wikidata identifiers
	query_size = 25

	wikidata_file = "get_wikipedia_from_wikidata_input.txt"
	wikidata_list = []
	with open(wikidata_file) as wikidata:
		for line in wikidata:
			wikidata_list.append(line.strip())

	wikipedia_query = '''
SELECT ?id ?article WHERE {
  VALUES ?id { ?ALL_VALUES }
    ?article schema:about ?id .
    ?article schema:isPartOf <https://en.wikipedia.org/> .
}
		'''

	with open("wikipedia_from_wikidata_output.tsv", "w") as output_f:

		for idx in range(0, len(wikidata_list), query_size):
			end = idx + query_size
			bulk_values = wikidata_list[idx:end]
			bulk = ""
			for UID in bulk_values:
				bulk += "<" + UID + "> "

			wikipedia_query_new = wikipedia_query.replace("?ALL_VALUES", bulk)
			wikiquery = get_results(query=wikipedia_query_new)

			for i in wikiquery:
				output_f.write(i["id"]["value"] + "\t" + i["article"]["value"] + "\n")


def viaf_query():
	# this query gets wikidata identifers from viaf records using viaf identifiers
	query_size = 40

	# VIAF
	viaf_file = "get_wikidata_from_viaf_input.txt"
	viaf_list = []
	with open(viaf_file) as viaf:
		for line in viaf:
			viaf_list.append(line.strip())

	viaf_query = '''
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
select ?id ?a where {
  VALUES ?id { ?ALL_VALUES }
  ?a wdt:P214 ?id .
}
		'''

	with open("wikidata_from_viaf_output.tsv", "w") as output_f:

		for idx in range(0, len(viaf_list), query_size):
			end = idx + query_size
			bulk_values = viaf_list[idx:end]
			bulk = ""
			for UID in bulk_values:
				bulk += '"' + UID + '" '

			viaf_query_new = viaf_query.replace("?ALL_VALUES", bulk)
			viafquery = get_results(query=viaf_query_new)

			for i in viafquery:
				output_f.write(i["id"]["value"] + "\t" + i["a"]["value"] + "\n")


#wikidata_query()
viaf_query()