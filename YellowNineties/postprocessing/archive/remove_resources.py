import re

# this script removes the rdf:type rdfs:Resource definitions for URIs that are included only as the object of OWL:SameAs relationships

input_f = "../data/oct18_3M_update/no_relationships.ttl"
output_f = "../data/oct18_3M_update/no_relationships_resourceRemoved.ttl"

with open(input_f, "r") as in_f:
	with open(output_f, "w") as out_f:
		f_string = in_f.read()
		out_string = re.sub(r'<.*>\n\s*a\s*rdfs\:Resource .\n', "", f_string)
		out_f.write(out_string)
