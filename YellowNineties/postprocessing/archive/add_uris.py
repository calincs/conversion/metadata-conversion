import re

# assumes the file is a tab separated file of lincs:id \t placeholder
def get_mappings(file):
	map_dict = {}
	with open(file) as map_file:
		for line in map_file:
			line_split = line.strip("\n").split("\t")
			map_dict[line_split[1]] = line_split[0]
	return map_dict


def main():

	input_dir = "../data/oct18_3M_update/"
	output_dir = "../data/oct18_3M_update/minted/"
	files = ["no_relationships_resourceRemoved", "relationships", "other"]

	for file in files:
		input_file = input_dir + file + ".ttl"
		mappings = input_dir + "URIs.txt"
		output_file = output_dir + file + ".ttl"

		map_dict = get_mappings(mappings)

		with open(input_file) as in_file:
			file_content = in_file.read()

		for key in map_dict:
			file_content = re.sub(key, map_dict[key], file_content)

		file_content = re.sub("@prefix temp_lincs_temp: <http://temp.lincsproject.ca/> .", "@prefix lincs: <http://id.lincsproject.ca/> .", file_content)
		file_content = re.sub("@prefix temp_lincs_temp: <temp.lincsproject.ca/> .", "@prefix lincs: <http://id.lincsproject.ca/> .", file_content)

		# prefix_additions = [
		# 	"@prefix y90s_person: <https://personography.1890s.ca/persons/> .",
		# 	#"@prefix y90s_occupation: <https://personography.1890s.ca/occupation/>",
		# 	"@prefix wd: <http://www.wikidata.org/entity/> .",
		# 	"@prefix dbr: <http://dbpedia.org/resource/> .",
		# 	"@prefix dcterms: <http://purl.org/dc/terms/> .",
		# 	"@prefix viaf: <http://viaf.org/viaf/> ."
		# 	]


		# file_content = re.sub(r"<https://personography\.1890s\.ca/persons/([0-9]+)>", r"y90s_person:\1", file_content)
		# #file_content = re.sub(r"<https://personography\.1890s\.ca/occupation/(.+?)>", r"y90s_occupation:\1", file_content)
		# file_content = re.sub(r"<http://purl\.org/dc/terms/identifier>", "dcterms:identifier", file_content)
		# file_content = re.sub(r"<http://www\.wikidata\.org/entity/(Q[0-9]+)>", r"wd:\1", file_content)
		# file_content = re.sub(r"<http://viaf\.org/viaf/(.+?)>", r"viaf:\1", file_content)
		# file_content = re.sub(r"http://dbpedia\.org/page/", "http://dbpedia.org/resource/", file_content)
		# file_content = re.sub(r"<http://dbpedia\.org/resource/(.+?)>", r"dbr:\1", file_content)


		with open(output_file, "w") as out_file:
			#out_file.write("\n".join(prefix_additions) + "\n")
			out_file.write(file_content)

main()
