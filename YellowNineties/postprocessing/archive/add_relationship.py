from rdflib import *
import sys
import rdflib
import requests
from bs4 import BeautifulSoup
import re

input_file = "../Datasets/yellow1890s.ttl"

# Create a Graph
g = Graph()

# Parse in an RDF file 
g.parse(input_file, format='ttl')

# query - crm:P14.1_in_the_role_of  "direct"@en .
q = """
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

SELECT ?s ?contribution ?creation ?label ?predicate WHERE {

    ?s crm:P02i_is_range_of ?contribution .
    ?contribution crm:P01_has_domain ?creation .
    ?contribution crm:P14.1_in_the_role_of "direct"@en .
    ?contribution rdfs:label ?label.
    ?contribution ?predicate crm:PC14_carried_out_by.
    ?creation  rdfs:label  "Yellow Nineties Creation"@en .
}
"""

for r in g.query(q):

    subject = URIRef(r.contribution)
    predicate = URIRef('http://www.cidoc-crm.org/cidoc-crm/P01_has_domain')
    object = URIRef(r.creation)

    g.remove((subject,predicate,object))

    subject = URIRef(r.contribution)
    predicate = URIRef('http://www.cidoc-crm.org/cidoc-crm/P14.1_in_the_role_of')
    object = Literal('direct', lang='en')

    g.remove((subject,predicate,object))

    subject = URIRef(r.contribution)
    predicate = URIRef('http://www.w3.org/2000/01/rdf-schema#label')
    object = Literal(r.label)

    g.remove((subject,predicate,object))

    subject = URIRef(r.contribution)
    predicate = r.predicate
    object = URIRef('http://www.cidoc-crm.org/cidoc-crm/PC14_carried_out_by')

    g.remove((subject,predicate,object))

    subject = URIRef(r.s)
    predicate = URIRef('http://www.cidoc-crm.org/cidoc-crm/P02i_is_range_of')
    object = URIRef(r.contribution)

    g.remove((subject,predicate,object))

    subject = URIRef(r.s)
    predicate = URIRef('http://www.cidoc-crm.org/cidoc-crm/P14i_performed')
    object = URIRef(r.creation)

    g.add((subject,predicate,object))

# query - crm:P14.1_in_the_role_of  "indirect"@en .

q = """
PREFIX crm:  <http://www.cidoc-crm.org/cidoc-crm/>

SELECT ?s ?contribution ?creation ?label ?predicate WHERE {

    ?s crm:P02i_is_range_of ?contribution .
    ?contribution crm:P01_has_domain ?creation .
    ?contribution  crm:P14.1_in_the_role_of  "indirect"@en .
    ?contribution rdfs:label ?label.
    ?contribution ?predicate crm:PC14_carried_out_by.
    ?creation rdfs:label "Yellow Nineties Creation"@en .
}
"""

for r in g.query(q):

    subject = URIRef(r.contribution)
    predicate = URIRef('http://www.cidoc-crm.org/cidoc-crm/P01_has_domain')
    object = URIRef(r.creation)

    g.remove((subject,predicate,object))

    subject = URIRef(r.contribution)
    predicate = URIRef('http://www.cidoc-crm.org/cidoc-crm/P14.1_in_the_role_of')
    object = Literal('indirect', lang='en')

    g.remove((subject,predicate,object))

    subject = URIRef(r.contribution)
    predicate = URIRef('http://www.w3.org/2000/01/rdf-schema#label')
    object = Literal(r.label)

    g.remove((subject,predicate,object))

    subject = URIRef(r.contribution)
    predicate = r.predicate
    object = URIRef('http://www.cidoc-crm.org/cidoc-crm/PC14_carried_out_by')

    g.remove((subject,predicate,object))

    subject = URIRef(r.s)
    predicate = URIRef('http://www.cidoc-crm.org/cidoc-crm/P02i_is_range_of')
    object = URIRef(r.contribution)

    g.remove((subject,predicate,object))

    subject = URIRef(r.s)
    predicate = URIRef('http://www.cidoc-crm.org/cidoc-crm/P11i_participated_in')
    object = URIRef(r.creation)

    g.add((subject,predicate,object))

g.serialize(destination="yellow1890s_new.ttl", format='turtle')