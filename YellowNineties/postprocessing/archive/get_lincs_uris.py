import re
import requests


def get_placeholders(input_dir, file):
	temps_list = []
	with open(input_dir + file) as in_file:
		file_contents = in_file.read()

	temp_lincs = re.findall(r"(temp_lincs_temp:[A-Za-z0-9\#-]+) ", file_contents)
	for t in temp_lincs:
		temps_list.append(t)


	temp_lincs = re.findall(r"<http:\/\/temp\.lincsproject\.ca\/.+?>", file_contents)
	for t in temp_lincs:
		temps_list.append(t)

	return temps_list


def get_uid(uid_count):
	mint_url = "https://uid.lincsproject.ca/generateMultipleUID"
	params = {"num": uid_count}
	response = requests.get(mint_url, params=params)

	try:
		uuid = response.json()["uids"]
	except KeyError:
		print(response.status_code, ":  Failed to get UID: ", response.content)
		uuid = None
	return uuid


def main():
	input_dir = "../data/oct18_3M_update/"
	output_dir = "../data/oct18_3M_update/minted/"
	files = ["no_relationships_resourceRemoved.ttl", "relationships.ttl", "other.ttl"]

	temps_list = []
	for file in files:
		temps_list_part = get_placeholders(input_dir, file)
		temps_list = temps_list + temps_list_part

	temps_list = sorted(list(set(temps_list)))


	uid_count = len(temps_list)
	uid_list = get_uid(uid_count)
	uid_list = ["lincs:" + s for s in uid_list]


	with open(output_dir + "URIs.txt", "w") as out_file:
		for i in range(uid_count):
			out_file.write(str(uid_list[i]) + "\t" + str(temps_list[i]) + "\n")

main()