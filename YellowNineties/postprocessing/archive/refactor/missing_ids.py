from rdflib import Graph
import pandas as pd


ref_spreadsheet = pd.read_csv('YellowNineties/data/Persons-Export-2022-August-08-1907 - Persons-Export-2022-August-08-1907.csv',
                                usecols=[3,5])

g = Graph()
g.parse("Datasets/yellow1890s.ttl")
#g.parse("Datasets/sample.ttl")

rows = []
for triple in g:
    if triple[1].n3() == "<http://www.cidoc-crm.org/cidoc-crm/P190_has_symbolic_content>" and triple[2].n3().startswith("\"#"):
        identified_id = triple[0].n3()
        xml_id = triple[2].n3()
        # to remove the quotes
        xml_id = xml_id[1:len(xml_id)-1]
        if xml_id not in list(ref_spreadsheet['xml_id']):
            #print(xml_id)
            for triple2 in g:
                if triple2[1].n3() == "<http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by>" and triple2[2].n3() == identified_id:
                    rows.append([triple2[0].n3(),xml_id])

rows.insert(0,["URI","xml_id"])
df = pd.DataFrame(rows)          
df.to_csv("missing_uris.csv", index=False)