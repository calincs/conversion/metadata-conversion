from rdflib import Graph
import csv
g = Graph()
g.parse("YellowNineties/postprocessing/refactor/new_script/Yellow1890s_changed_names.ttl")

missing_names = []
for triple in g:
    #print(triple)
    #print()
    if triple[1].n3() == "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>" and triple[2].n3() == "<http://www.cidoc-crm.org/cidoc-crm/E21_Person>":
        for triple2 in g:
            if triple[0].n3() == triple2[0].n3() and triple2[1].n3() == "<http://www.w3.org/2000/01/rdf-schema#label>":
                if ',' in triple2[2].n3():
                    missing_names.append(triple2[2].n3())

rows = zip(missing_names)
with open("no_commas.tsv", "w") as f:
    writer = csv.writer(f,delimiter='\t')
    for row in rows:
        writer.writerow(row)
print("Done")