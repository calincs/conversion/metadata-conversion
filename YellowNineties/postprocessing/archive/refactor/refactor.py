from rdflib import Graph
import pandas as pd


ref_spreadsheet = pd.read_csv('YellowNineties/data/Persons-Export-2022-August-08-1907 - Persons-Export-2022-August-08-1907.csv',
                                usecols=[3,5])

#print(ref_spreadsheet)
g = Graph()
g.parse("Datasets/yellow1890s.ttl")
#g.parse("Datasets/sample.ttl")



mappings = {}
for row in ref_spreadsheet.itertuples():
    permalink = row.Permalink
    id = row.xml_id

    for triple in g:
        if triple[1].n3() == "<http://www.cidoc-crm.org/cidoc-crm/P190_has_symbolic_content>" and triple[2].n3() == "\"%s\""%(id):
            iden_uri = triple[0].n3()
            if iden_uri:
                for triple2 in g:
                    if triple2[1].n3() == "<http://www.cidoc-crm.org/cidoc-crm/P1_is_identified_by>" and triple2[2].n3() == "%s"%(iden_uri):
                        if triple2[0].n3():
                            mappings[triple2[0].n3()] = "<%s>"%(permalink)

df = pd.DataFrame([key,value] for key, value in mappings.items())          
df.to_csv("mappings.tsv", index=False)


