# Refactoring Y90s data

This folder contains code and additional files used to refactor the Y90s data.

Two major steps were performed:
* Replacing all person URIs with the official URI.
    Example: <https://personography.1890s.ca/persons/2727> becomes <https://personography.1890s.ca/persons/a-new-writer/>
* Replacing all person names of the form LastName, FirstName with FirstName LastName.
    Example: Shreiner, Olive becomes Olive Shreiner


The following steps were performed:

## To replace URIS
1. Run the python script ```refactor.py``` to create the mapping file ```mappings.csv``` for the URIs replacement. 
* The script accesses this [spreadsheet](https://gitlab.com/calincs/conversion/metadata-conversion/-/blob/master/YellowNineties/data/Persons-Export-2022-August-08-1907%20-%20Persons-Export-2022-August-08-1907.csv) and the Y90s data that needs to be refactored.

2. Use the /replacement endpoint from the [Linked Data Enhancement API](https://gitlab.com/calincs/conversion/post-processing-service). Supply the Y90s .ttl file and the mappings file you created in step 1 as input. The resulting .ttl file will now have the replaced official URIs.

3. There are some xml ids (e.g. #AFO) that are only in the .ttl file and not in the spreadsheet. Consequently, no official uris were found for those and they were replaced with temporary LINCS uris. The python script ```missing_ids.py``` was used to find the ids mentioned above, which were then stored in the ```missing_uris.csv``` file. Yet again, the /replacement endpoint was used to replace the uris corresponding to these missing ids.


## To replace Names
1. Extract Column B "Title" from this [spreadsheet](https://gitlab.com/calincs/conversion/metadata-conversion/-/blob/master/YellowNineties/data/Persons-Export-2022-August-08-1907%20-%20Persons-Export-2022-August-08-1907.csv). These are the person names whose format we want to change.

2. Using Excel, split the extracted names into columns with comma as the delimeter. Then, join the columns so the resultant name is in the format FirstName LastName. Save the file as ```mappings_names.txt``` which is the required mappings file.

3. Use the /replacement endpoint from the [Linked Data Enhancement API](https://gitlab.com/calincs/conversion/post-processing-service). Supply the Y90s .ttl file and the mappings file you created in step 2 as input. The resulting .ttl file will now have the names in the required format.

4. Some names are present in the .ttl file and not in the spreadsheet. Run the file ```missing_names.py``` to get a list of these missing names. Using the same procedure as above, change the format of the names to create the mapping file ```mappings_missing_names.tsv.``` Use the \replacement endpoint to replace the person names' formats.
