# YellowNineties Conversion

## Current Conversion Status
Data transformation is complete for this project. **Future changes should be made directly to the data in ResearchSpace.**

## Pre Processing

Default input: __data/y90s-personography-rdfxml.xml__
Default output: __data/pre_processed.xml__ 

To run the processing script using the default values, call `python3 pre_process.py`. This will use the default input 
and output files. To manually define the input and output file, pass the `-i <path_to_input_file>` argument and `-o 
<path_to_output_file>` arguments respectively.

`pre_process.py` will:
1. take in a Y90's xml file
2. replace all y90s: with y90s_ (see fix_terms dictionary in code)
3. add various element tags to the data in order to facilitate relationship mapping in X3ML

For example:

```xml
<rdf:Description rdf:about="https://personography.1890s.ca/persons/2628">
    <y90s:mentor_of rdf:resource="https://personography.1890s.ca/persons/3021"/>
```

becomes

```xml
<rdf:Description rdf:about="https://personography.1890s.ca/persons/2628">
    <y90s_mentor_of rdf:resource="https://personography.1890s.ca/persons/3021" 
                    mentor_label="Mentorship relationship between France, Anatole (mentor) and Proust, Marcel (mentee)" 
                    mentor_id="#mentor2628" 
                    mentee_id="#mentee3021" 
                    relationship_id="mentor_of#26283021" />
```

## X3ML

### 3M Mappings

Used [dec14_pre_processed.xml](https://gitlab.com/calincs/conversion/metadata-conversion/-/tree/master/YellowNineties/data/dec14_pre_processed.xml) as input

- Y90s_combined_v3
	+ This is the most recent mapping for the Yellow Nineties Personography data as of January 18, 2023. 
- Y90s - Big Map Final - No Relationships - OWL
	+ Import from old 3M, uses OWL schema (owl:sameAs)
- Y90s - Big Map Final - Relationships - PreProcessed
	+ Import from old 3M, does not use OWL schema

### 3M Outputs

- Y90s_combined_v3.ttl
	+ Most recent output file for Yellow Nineties Personography data

## Post-Processing

## Links to other documentation
- [Google Drive Folder](https://drive.google.com/drive/u/1/folders/1u1YE1V7jd8O4j5nKI_yXvGcQTA2r1SQh)
- [Conversion Steps](https://docs.google.com/document/d/147JjQDHbEDNv6dNk5cd_yZYjknxnroYoI627YPr1c9s/edit#heading=h.6nyjd74x47g1) 
- [Preprocessing Documentation](https://docs.google.com/document/d/147JjQDHbEDNv6dNk5cd_yZYjknxnroYoI627YPr1c9s/edit#heading=h.815aqgtlenu)

## Previous Conversion Workflow

### Moved on January 17, 2023:

The most recent version of the data is in `Datasets/yellow1890s.ttl`. We have made changes directly to that file so the data files within the `YellowNineties/` directory are no longer up to date. Similarly, we setup the 3M files before we finalized our process of adding temporary identifiers. So any changes to the current data should be made directly to `Datasets/yellow1890s.ttl` and if there are new versions of the data that need to go through a 3M conversion, the 3M mappings must be updated.

Repository Contents (most of these files can be found in the Archive folder):
- __data/y90s-personography-rdfxml.xml__ : Y90s dataset
- __data/pre_processed.xml__ : Processed y90s dataset to be loaded using 3m
- __generator.xml__ : Generator policy
- __data/app_matching.xml__ : Backup for **Y90s - Big Map Final - No Relationships - OWL** : Matching Tab 
- __data/app_matching_rel.xml__ : Backup for **Y90s - Big Map Final - Relationships - PreProcessed** : Matching Tab
- __data/app_generator.xml__ : Backup for **Y90s - Big Map Final - No Relationships - OWL** : Generator Tab 
- __data/app_generator_rel.xml__ : Backup for **Y90s - Big Map Final - Relationships - PreProcessed** : Generator Tab 
- __data/Persons-Export-2022-August-08-1907 - Persons-Export-2022-August-08-1907.csv__ : data export to help us get the correct person URIs