from os import sys
import re

MAX = 1049
CHUNK = 165

# Returns list of placeholders (ex: data:pluma2_GenderEvent_1_1)
def get_placeholders(file_contents):
    placeholders = re.findall(r"(data:[_A-Za-z0-9\#].*?)[;,\.]", file_contents)
    placeholders = map(str.strip, placeholders)
    placeholders = [x.split(" ")[0] for x in placeholders]
    placeholders = list(set(placeholders))
    return sorted(placeholders, key=len,reverse=True)


# Returns list of placeholder strings that were unmatched by CWRC vocabs (ex: "comments on the inadequacies of women's education"^^xsd:string ;)
def get_strings(file_contents):

    regex =  r"\"(.*?)\"(\^\^xsd:string ;)$"
    strings = re.findall(regex, file_contents, re.MULTILINE)
    
    strings = [F'"{x[0]}"{x[1]}'.rstrip(" ;") for x in strings ]
    strings = list(set(strings))
    return sorted(strings, key=len,reverse=True)


def print_usage():
    script = sys.argv[0]
    print("Usage:")
    print(F"\t{script} file.ttl \n")
    print("\t\tfile.ttl : file within /data/results_placeholders/ to find strings and placeholders")
    print("\nExample:")
    print(F"{script} cf.ttl")
    sys.exit(-1)


def listToFile(filepath, entities):
    with open(filepath, "w") as out_file:
        for x in entities:
            out_file.write(str(x) + "\n")
def main():
    input_dir = "data/results_placeholders/"
    if len(sys.argv) != 2:
        print_usage()
    
    file = sys.argv[1]
    filepath = input_dir+file

    with open(filepath) as in_file:
        file_contents = in_file.read()

    placeholders = get_placeholders(file_contents)
    strings = get_strings(file_contents)
    
    listToFile(F"{input_dir}/placeholders/{file[:-4]}_all_placeholders.txt", placeholders+strings)
    listToFile(F"{input_dir}/placeholders/{file[:-4]}_placeholders.txt", placeholders)
    listToFile(F"{input_dir}/strings/{file[:-4]}_strings.txt", strings)
    
    print(len(strings) + len(placeholders))

if __name__ == "__main__":
    main()
