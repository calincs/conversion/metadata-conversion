import re
from typing import Match
import requests
import sys

CHUNK = 100

def get_placeholders(input_dir, file):
    with open(input_dir + file) as in_file:
        file_contents = in_file.read()

    placeholders = re.findall(r"(data:[A-Za-z0-9\#].*?)[;,\.]", file_contents)
    placeholders = map(str.strip, placeholders)
    placeholders = [x.split(" ")[0] for x in placeholders]
    placeholders = list(set(placeholders))

    return sorted(placeholders, key=len,reverse=True)
  

def get_uid(total=CHUNK):
    mint_url = F"https://uid.lincsproject.ca/generateMultipleUID?num={total}"
    response = requests.get(mint_url)
    try:
        uuids = response.json()["uids"]
    except KeyError:
        print(response.status_code, ":  Failed to get UIDs: ", response.content)
        uuids = None
    return uuids


def print_usage():
    script = sys.argv[0]
    print("Usage:")
    print(F"\t{script} file.ttl \n")
    print("\t\tfile.ttl : file within /data/results_placeholders/ to find strings and placeholders")
    print("\nExample:")
    print(F"{script} cf.ttl")
    sys.exit(-1)

def update_uid_file(file,uids):
    for uid in uids:
        file.write("http://id.lincsproject.ca/" + str(uid) + "\n")

def main():
    if len(sys.argv) != 2:
        print_usage()
    
    input_dir = "data/results_placeholders/"
    total_uris = int(input("Total of URI needed: "))
    subset = total_uris // CHUNK
    extra = total_uris % CHUNK

    
    file = sys.argv[1]
    filepath = F"{input_dir}{file[:-4]}_URIs.txt"

    with open(filepath, "w") as out_file:
        for x in range(subset):
            uids = get_uid()
            if uids:
                update_uid_file(out_file,uids)
            else:
                print("Missed placeholder: ")
        if extra:
            update_uid_file(out_file, get_uid(extra))


if __name__ == "__main__":
    main()
