#!/bin/sh
# Steps:
# 1. Get placeholders + strings --> Should export to two files
# 2. Get lincs identifiers 
# 3. replace identifiers
# 4. Validate
    # - all lincs URIs have labels
    # lincs prefixes added
    # any data:XXX left?
    # any xsd:string left

# file="cf"
file="biography"
file_name=$file".ttl"
echo $file_name


# python3 get_placeholders.py $file_name #| python3 get_lincs_uris.py $file_name
# paste -d "\t"  data/results_placeholders/placeholders/${file}_all_placeholders.txt data/results_placeholders/uris/${file}_URIs.txt > data/results_placeholders/uris/${file}_replacements.tsv

python3 replace_placeholders.py $file_name data/results_placeholders/uris/${file}_replacements.tsv

#if New data placeholders haven't been extracted
# python3 get_placeholders.py $file_name | python3 get_lincs_uris.py $file_name
# paste -d "\t"  data/results_placeholders/placeholders/${file}_placeholders.txt data/results_placeholders/uris/${file}_URIs.txt > data/results_placeholders/uris/${file}_replacements.tsv
# python3 replace_placeholders.py $file_name data/results_placeholders/uris/${file}_replacements.tsv

# If old data with some minor additions, need to see what new placeholders exist