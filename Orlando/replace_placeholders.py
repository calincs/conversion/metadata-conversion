import os
import csv
import sys


def print_usage():
    script = sys.argv[0]
    print("Usage:")
    print(F"\t{script} file.ttl \n")
    print("\t\tfile.ttl : turtle file within /data/results_placeholders/ to find strings and placeholders")
    print("\t\tpath\\to\\file_replacements.tsv : tsv of replacements no header (placeholder, URI) ")
    print("\nExample:")
    print(F"{script} cf.ttl")
    sys.exit(-1)

def get_replacements(file):
    replacements = []
    with open(file) as csvfile:
        reader = csv.reader(csvfile, delimiter="\t", quoting=csv.QUOTE_NONE)
        for row in reader:
            if row[0] and row[1]:
                uri = "lincs:" + row[-1].split(".ca/")[1]
                replacements.append([row[0], uri])
    return replacements

def replace_line(line, replacements):
    for x in replacements:
        line = line.replace(x[0],x[1])
    return line
def main():
    if len(sys.argv) != 3:
        print_usage()
    
    src_file = F"data/results_placeholders/{sys.argv[1]}"
    replacement_path = sys.argv[2]
    result_path = F"data/results_minted/{sys.argv[1]}"

    replacements = get_replacements(replacement_path)

    f_out = open(result_path, "w")

    with open(src_file) as f_in:
        for line in f_in:
            f_out.write(replace_line(line,replacements))

    f_out.close()



if __name__ == "__main__":
    main()
