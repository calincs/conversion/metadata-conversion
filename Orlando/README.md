Orlando -- 

Technical Documentation:

Current Status:
Source Data last extracted from CWRC: October 13 2021.

data/ contains smaller subsets of the data that are used in the larger file in S3. Ex. Organizations, Places.

https://aux.lincsproject.ca/minio/public/converted_data/orlando/biography_triples.ttl also contains places and orgs, currently contains location, occupation, birth and death.


Post processing:
`Orlando/postprocessing/get_lincs_uris.py`: modification of `YellowNineties/postprocessing/get_lincs_uris.py` uses https://uid.lincsproject.ca/generateMultipleUID to get list of UUIDS and print to file.

`Orlando/postprocessing/replace_placeholders.py`: Uses file generated in above script to replace placeholder `data:XXXX` uris used by the Orlando data. 

- Need to replace strings for occupations with lincs uuids, and save this list to be used for occupation vocab
- Replace data:XXX_XXX with lincs:UUIDs
- Check that all lincs:UUIDS have label 

Links to other documentation:

Conversion Steps:

Mapping Document:
